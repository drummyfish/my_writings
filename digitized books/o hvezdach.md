WORK IN PROGRESS

https://archive.org/details/ohvezdach

# O HVĚZDÁCH

ČTENÍ PRO KAŽDÉHO.

SEPSAL

PROF. DR. FR. NÁBĚLEK,
C. K. ŠKOLNÍ RADA.

Cena K 1.50

KROMĚŘÍŽ 1906.

Nákladem vlastním. — Tiskem Jindř. Slováka.

Poznámka.

K dílku tomuto dlužno míti aspoň mapu hvězd u nás viditelných **„Hvězdné nebe severní"**. Na plátně, v kapesním formátě, barvotisk, cena K 1.40 a **„Obzor“**, cena 20 h. Dále doporučuje se dílko **„Nebeské hodiny"**, praktické poučení, kterak lze poznati po hvězdách, kolik jest hodin a datum. Cena 60 h. — **Velká nástěnná mapa** 195×185 cm., na plátně, barvotisk. Cena 27 K. Objednati možno přímo u autora. **Dra. Pr. Nábělka v Kroměříži**.

## PROSLOV.

»Klečím a hledím v nebe líc,
myšlenka letí světům vstříc
vysoko — převysoko —
a slza vhrkla v oko.«

Jan Neruda.

Jestliže vědy vůbec bystří rozum a šlechtí srdce, tož vědám přírodním po této stránce zajisté patří místo první: vždyť zabývají se dílem ne lidským. nýbrž dílem neobsáhlé moci, moudrosti, dobroty, krásy, dílem neobsáhlého rozumu . . .

Mezi tělesnými tvory jedině člověk dovede povznésti mysl od zjevů, kteréž postihuje smysly tělesnými, ku příčinám postižitelným toliko činností rozumovou: umí z díla souditi o mistru. Čím dílo bude důmyslnější, umělejší, krásnější. užitečnější . . ., tím výše budeme
ceniti původce jeho: bude-li však dílo takové, že kteroukoli část jeho zkoumáme, všude shledáme zákonnitost, účelnost a krásu, a že ku prozkoumání a úplnému pochopení ani tisíce nejnadanějších hlav lidských nestačí, ať by zkoumali celá tisíciletí: jaký pak bude mistr toho díla?

Země naše jest jen malinkou částí malého ostrůvku — naší soustavy sluneční — v nezměrném moři

1*

4

všehomíra a jest jevištěm dosud neprozkoumaného množství zjevů přírody neživé, zjevů fysikálních v nejširším slova smyslu, zjevů života organického — nižšího v rostlinstvu a vyššího ve zvířectvu; není člověka, jehož rozum by stačil obsáhnouti vše, což ve vědách přírodních dosud bylo vyzkoumáno, a přece všechno to jest jen částí toho, co příroda v sobě tají pro pokolení budoucí, a nesmíme ani přibližně říci, jakou částí celku; vždyť téměř co den objevují se nové pravdy. Co tají v sobě pak soustava sluneční, co soustava soustav takových, co svět viditelný pouhým okem, a svět viditelný okem ozbrojeným — a hranic nedozíráme nikde, ani směrem v malost světa mikroskopického, ani směrem ve velikost světa teleskopického — a co tají v sobě vesmír, jehož částí — a neznámo nám ani jakou části — jest všechno, cokoli postřehují naše smysly, ať již pouhé, nebo ozbrojené přístroji důmyslně sestrojenými: a ve všem, co spatřujeme, vidíme zákonnitosti! Kdyby nic jiného, tož vědy přírodní byly by s to, aby člověku rozumného přivedly ku poznání, že dílo takové nemůže býti hříčkou náhody, nýbrž že jeho původcem jest bytost rozumová.

Astronomie jest částí věd přírodních zabývajíc se tělesy nebeskými v prostoru světovém, vyvádí nás jaksi z mezí úzkých, vyměřených v bezmezí, z konečnosti v nekonečnost v příčině pojímatelnosti lidským rozumem. Rozšiřuje náš obzor duševní asi tak, jako cestování rozšiřuje náš obzor tělesný, a v pravdě nás vzdělává. Víc než kde jinde přesvědčujeme se, že to, co nám podávají smyslové tělesní. jest sice východiskem a základem pro činnost rozumovou, ale že to není jediným zdrojem poznatků našich, jak mnozí by rádi

5

nemluvili, namluvili totiž, že pravdou jest pouze to, co postřehujeme tělesnými smysly, — nýbrž že člověk na rozdíl od ostatních tělesných tvorů dovede poznati, že smyslové nás někdy i klamou, a že člověk r o z u m e m i ten klam odhalí a dobádává se pravdy skryté
smyslům. Toho svědkem mimo jiné jest i slavný Koperník.

Však astronomie povznáší i srdce naše nad všednost. Toho člověka nebylo a nebude, aby hvězdnaté nebe na mysl jeho nemělo účinku: ať nízký, ať postavený vysoko, ať prostý, ať učený, ať blouznivý snílek, ať hloubavý myslitel, ať šťastný, ať neštěstím stíhaný — na hvězdnatém nebi každý něco vyčte, ono každému něco poví i tehdy, kdy všechen ostatní svět jest němým a hluchým; toho svědkem jsou naše pohádky a písně národní. Slavný myslitel německý, E m a n. K a n t, chladný, ba až mrazivý ve svých úvahách, zvolal, patře na hvězdnatá nebe: »Dvě věci jest, kteréž naplňují mysl moji obdivem vždy novým a mocnějším, čím hlouběji a vytrvaleji duch jimi se zabývá: hvězdnaté nebe nade mnou a mravní zákon ve mně!«

Hvězdnaté nebe bylo lidstvu prvními hodinami, prvním kalendářem a spolehlivým vůdcem po krajinách neznámých a po moři; za starých dob lidé více všímali si hvězdnatého nebe: nám hodiny a tištěné kalendáře přivedly z mody ty krásné zlaté hodiny nebeské. — Jak nerozumné počínáme si v příčině hvězdosloví: učíme se ve Školách třebas o věcech, jichž nikdy nikdo neviděl, jež kdysi byly jen v obrazotvornosti básníků: učíme se o věcech, jichž třebas nikdy věděti nebudeme: učíme se o cizích dílech světa, o jejich zvířeně a květeně, o rostlinách i zvířatech předpotopních, o rostlinách i zvířatech na dnech vzdálených

6

moří . . . Učíme se u nábytku, kroji a zbroji různých národů, jež žily před tisíci lety . . ., ale o tom, co vidíme téměř denně nad sebou, nevíme mnohdy ničeho. Abychom viděli cizí země, musíme tam cestovati — a jak málo lidem se toho dostane —; rostliny musíme hledati, lisovati; brouky, motýly chytati, opatrovati; různá zvířata vycpávati, abychom je měli déle ku svému a jiných poučení; hvězdy však máme vždy nad sebou, a jest nám jen hlavy pozvednouti a patřiti — a my toho nečiníme!

K tomu, co z astronomie ku k a ž d ý vzdělaný člověk by měl věděti, není potřebí snad zvláštního rozumu, zvláštního nadání; není potřebí znalosti vyšší mathematiky ani trigonometrie; není potřebí ani velkých dalekohledů a přístrojů druhých — toho ovšem jest
potřebí odborníkům v astronomii —; postačí, abychom dovedli rozuměti jen tomu, co hvězdné nebe skytá oku prostému. Spisek tento má na zřeteli jen toto prosté minimum, jež chce podati tak, aby každý, koho ajímá nebe hvězdnaté — a kohož by nezajímalo? — snadno porozuměl; k dalšímu vzdělání ve hvězdářství poslouží obšírná díla skutečných hvězdářů z povolání, a ta doporučuji ku čtení co nejvřeleji. V jazyce českém máme obšírné dílo hvězdářské, jež sepsal professor astronomie při c. k. české universitě v Praze, Dr. Gustav Gruß. —

## Pohled na hvězdnaté nebe.

Přijdeme—li do neznámé krajiny nebo do neznámého města, všimneme si předmětů zvláště vynikajících: hor, řek, lesů . . ., věží, velkých budov; ty slouží nám za pomůcky, abychom brzy znali se v té krajině, v tom městě: tak i na nebi vyhledáme si některé vynikající skupiny hvězd, a ty nám poslouží, abychom snáze poznali i skupiny méně vyznačené a pak i jednotlivé hvězdy. Pro snadnější přehled a dorozumění nebe rozděleno jest na krajiny, označené souhvězdími, z nichž každé má své jméno, jako povrch zemský rozdělen jest na země, moře: země pak rozděleny jsou na krajiny a pojmenovány. Poznačujíce místo nějaké události, jmenujeme zkrátka krajinu, město, horu a řekneme na př. na Moravě řádily bouře, v Praze byla povodeň, na Spionskopu Burové dobyli vítězství nad Angličany; tak označíme i místo nějakého zjevu na nebi, třebas kde objevila se vlasatice, že jmenujeme souhvězdí a určitěji, že jmenujeme i hvězdu, odkud nějaký zjev vzal počátek, na př. meteor zazářil (zdánlivě) u hvězdy Kapelly a zanikl u hvězdy Regula. Přehled

8

povrchu zemského podávají nám mapy, přehled města podává nám jeho plán; mapy a plány více nám poví než sebe lepší popisy. Vůdce byl by nám ovšem milejší — ale ten bývá obyčejně drahý — a někdy ho vůbec ani není: tak i v astronomii, aspoň s počátku, vůdce jest velmi žádoucí; hleďme si ho získati a neostýchejme se poprositi, známe-li někoho, jenž jest s to, aby nám posloužil: nepřijdeme v nevhod; hvězdičky každý znalec nám ukáže r á d.

Místo popisu hvězdného nebe lépe poslouží mapa a to napnutá na plátně v kapesním formátě. Vydána též mapa otáčecí »Obzor hvězdný«, již možno zřídili tak, aby ukazovala jen ty hvězdy, kteréž v určitou dobu právě jsou nad obzorem. Mapy obsahují hvězdy v krajinách našich pouhým okem vůbec viditelné. Oběžnice na mapách nejsou označeny, poněvadž mění místa svá na nebi; taktéž i Měsíc a Slunce. Kde máme hledali Slunce mezi hvězdami na mapě, určíme takto: od Polárky vedeme si přímku k okraji, kdež jest označeno datum dne  (třebas 21. srpna) v průsečíku této přímky s ekliptikou na mapě zvláště vyznačenou znameními ♈, ȣ, II . . . jest toho dne Slunce (dne 22. srpnu u hvězdy ': Velkého lva). Měsíc i oběžnice daleko od ekliptiky nejsou; místa jejich dlužno hledali dle astronomického kalendáře.

Počátek nejvhodněji učiníme za jasné noci pod širým nebem; pohled na skutečné nebe hvězdné ničím nahraditi se nedá. Vyhledáme si Velký Vůz (Velkého Medvěda) vůbec známý a hvězdu Polárku, k níž přijdeme, spojíce si v duchu zadní dvě kola Velkého Vozu přímkou a prodloužíce ji směrem nahoru, až přijdeme na první hvězdu tak světlou, jako jsou hvězdy znamenající zadní dvě kola Velkého Vozu. Tato hvězda

9

— Polárka — jest na konci voje souhvězdí Malého Vozu (Malého Medvěda). zcela podobného Vozu Velkému, jen že menšího; jedno ze zadních kol jest hvězda světlá, druhá jest méně světlá: hvězdy tvořící přední kola a přední dvě hvězdy na voji, za mésíčna jen málo jsou viditelný.

Polárka jest nám hvězdou nejdůležitější, jí se řídíme. Hledíce k ní, máme před sebou sever, za sebou jih, v pravo východ, v levo západ; podle ní poznáváme strany světa, orientujeme se. Ona není úplně na pólu, ale pravému pólu není blíže žádná jiná jasnější hvězda. Kdybychom Polárku spojili si přímkou s prostřední hvězdou voje Velkého Vozu — ζ — vzdálenost první hvězdy ve voji — ε — ode druhé — γ — rozdělili na tři stejné díly a jeden takový odměřili od Polárky, dospěli bychom tak asi k pravému pólu. Osa zemská prodloužená až k nebi, ukazovala by k tomuto pólu, tedy zhruba k Polárce. Za den a noc nebe se všemi hvězdami zdánlivě otočí se kolem této hvězdy. Tytéž hvězdy za různých ročních časů — na jaře, v létě, na podzim, v zimě — ve stejnou dobu noční — třebas o 11. hodině — na různých místech nebe se objevují, jen Polárka vždy podrží své místo, až na nepatrnou odchylku. Zřizujeme-li sluneční hodiny na jakékoli ploše, ukazováku jejich, pevné tyči, jejíž stín ukazuje hodiny, dáváme směr osy zemské; má tedy ukazovati k polárce.

Když jsme poznali několik jasných hvězd na nebi, najdeme jiné tím způsobem, že myslíme si od Polárky ku hvězdě již poznané čáru, kteráž dostatečně jsouc prodloužena, prochází hvězdou (nebo aspoň nedaleko ní) na mapě vyznačenou; tam najdeme i její značku, a je-li znamenita, i její jméno a jméno souhvězdí,

10

jemuž náleží, pak jiné hvězdy téhož souhvězdí. — Dbáti jest toho, že hvězdy v souhvězdí tím blíže při sobě býti se zdají, čím výše stojí na nebi; souhvězdí
zdá se býti pak tím menším a nejmenším tehdy, když stojí nám nad hlavou. Čím souhvězdí na nebi stojí níže, tím hvězdy jeho od sebe zdají se býti vzdálenějšími, a souhvězdí pak samo rozsáhlejším. Tak na př. souhvězdí Velkého Vozu zdá se býti mnohem větším, spatřuje-li se na severní straně od Polárky, než když vidíme je nad sebou; podobně zdá se nám býti velmi rozsáhlým souhvězdí právě vycházející; zdá se pak býti tím menším, čím výše vystoupí na nebe, jako známé »Kosy« čili souhvězdí Oriona. Podobně i Slunce a Měsíc vycházející a zapadající mnohem většími zdají se býti, než stojí-li vysoko na nebi.

Many zobrazujíce v rovině, co v prostoru světovém jest prostorného, hvězdy, zdánlivě umístěné na křivé ploše nebes báně, nemohou dopodrobna souhvězdí zobraziti tak, jak ono spatřuje se na nebi; hlavní podoba ovšem může býti i na mapě vyznačena a to tím lépe, čím projekce čili průmět volí se vhodnější — demografický jest výhodnější globulárního.

## Jména souhvězdí a hvězd.

Za starých dob hvězdnaté nebe bylo lidstvu kalendářem, hodinami a průvodčím. Staří národové východní vedli život kočovný, přebývali ve stanech u svých stád a nemajíce sídel stálých, stěhovali se i se stády, svým majetkem, s místa na místa; tak to bylo u Arabův. Nocujíce pod širým nebem, nemohli

11

nepozorovati hvězd a pojmenovali vynikající skupiny, jakož i hvězdy, nad jiné jasnější. Jména ta ponejvíce vzata od jmen zvířat a vztahují se obyčejně k životu pastýřskému. Později obrazotvornost lidská přičinila svůj díl, a znamenití heroové — bohatýrové, za svá hrdinství odměněni pomníky na nebi. Ještě mladšího původu jsou jména souhvězdí dle osob o hvězdářství zasloužilých, dle nástrojů, důležitých vynálezů, pak dle jmen panovníků nebo osob mocných, jimž někteří hvězdáři chtěli se zalichotiti. Souhvězdí tato skládají se z hvězd jen málo světlých a utvořena jsou z částí souhvězdí starších, velkých. — Hvězdy každého souhvězdí označeny jsou obyčejně písmeny řeckými a to tím způsobem, že nejsvětlejší mají jména začátečních písmen řecké abecedy (α, β, γ, δ . . .); proto hvězdy první velikosti slují α — alfa —, ovšem i hvězda velikostí druhé, třetí i čtvrté může jmenovati se α, nemá-li souhvězdí hvězd jasnějších. Někdy nepostačí abeceda řecká, pak přibírají se i písmena latinská na označenou hvězd. Hvězdy občasné označují se posledními písmeny velké abecedy latinské. Ovšem i to hvězdářům nepostačí, a proto velmi vhodně označují hvězdy číslicemi.

Pro snadnější přehled souhvězdí na mapě jsou číslována, na okraji podána jména a některé poznámky, aby mapa sama nebyla přeplněna.

Pro začátek postačí seznati jen souhvězdí hlavní, zapamatovati si jejich jména a jména hvězd nejjasnějších — první velikosti — jest jich asi třináct; ty ostatní necháme si na dobu pozdější. Část bájeslovná, mythologická, jest jen zábavným čtením.

12

## Mléčná dráha.

Za jasné, bezměsíčné noci poutá oko naše bělavý pruh na nebi, jest to Mléčná dráha, u lidu našeho jest to Cesta do Říma. Od nás Řím leží jižně. Od měsíce června, kdy máme nejdelší den, polosvit dlouho trvá, a šero brzy počíná, takže za noci bezměsíčné Mléčná dráha čili »Cesta clo Říma« značně jest viditelna teprv při půlnoci; tehdy na nebi má směr celkem od severu k jihu. V červenci pravá noc začíná již dříve, a Mléčná dráha, když počíná býti zřetelnou po desáté hodině, má směr od severu k jihu; v srpnu Mléčná dráha večer má tento směr po deváté, v září po sedmé, v říjnu hned z večera. V říjnu, v listopadě ona část Mléčné dráhy, v níž jest souhvězdí Vozky, má směr k jihu z rána, v březnu a i v dubnu i květnu, kdy den počíná vždy dříve, ona část Mléčné dráhy, v níž jest souhvězdí Kozorožce, před svítáním má směr k jihu. Tedy právě v ty doby, kdy lid má příležitost pozorovati nebe, Mléčná dráha směřuje k jihu, kde od nás leží Řím, a pojmenování to nebylo by nepřípadným. Ale i z jiné příčiny mohl lid pojmenovati ten světlý pruh cestou do, Říma. V Římě sídlí viditelná hlava církve, náměstek Krista Pána, a věřícímu zbožnému lidu město Řím proto jest městem svatým, jest středem světa, a tudíž obrazotvornost cestu k němu poznačila i na nebi.

Název Mléčná dráha jest překlad latinského názvu Via lactea (via = cesta, lac = mléko, lacteus = mléčný), tento pak vzat jest z řeckého Gallaxia hodos (gala = mléko, galaxios mléčný, hodos = cesta), latinsky sluje též Orbis nebo Circus lacteus = okruh mléčný. Pruh ten jest světlý, bílý, a proto barva jeho

13

případně po mléku jest pojmenována. Zjevu tak zvláštního, jako jest Mléčná dráha, obrazotvornost záhy se zmocnila a spřádala báje o něm. Dle řeckého mythu Mléčná dráha na nebi povstala takto: Hera, manželka Zevova, byla pohroužena ve spánek. Zevs přiložil k jejím prsoum svého malého tehdy synáčka, Herkula, aby z ní ssál nesmrtelnost. Hera probudivši se, prudkým pohybem odhodila synáčka: z mléka jejích prsou, jež po nebi se rozlilo, povstala Mléčná dráha. — Jiná báj tomu chce, že Mléčná dráha jest zbytek památky po vzplanutí nebe, když Faëton, syn Heliův, boha slunce, neuměje říditi slunečního vozu, z dráhy sluneční se vyšinul a byl tím příčinou velkého požáru nebeského. — Římský básník Ovid (r. 48 před Kr. a r. 17 po Kr.) ve svých Metamorfosách čili Přeměnách líčí Mléčnou dráhu jako cestu, po níž bohové se ubírají k paláci nejvyššího boha hromovládce. Po obou stranách té cesty jsou sídla bohů vyšších, obydlí bohů řádu nižšího roztroušena jsou tu a tam po nebi opodál této světlé dráhy. — Arabové jmenovali Mléčnou dráhu prostě širokou »cestou«, nebo užívali i jména Om el-sema, matka nebe, jako by svým mlékem živila nebe.

## Souhvězdí nebe u nás vůbec viditelného, t.j. až po 40° pod rovníkem nebeským.

Čís. 1. **Hlídač polní**, francouzsky Messier, lat Cuslos segetum, jest souhvězdí skládající se jen z několika malých hvězdiček. Francouzský hvězdář Lalande (1752—1807) ku cti slavného hvězdáře jménem Messier, jenž více než po 30 let pilně po-

14

zoroval nebe jako bedlivý hlídač a objevil i prozkoumal mnoho vlasatic; to souhvězdí tak pojmenoval a tak vyznamenal Messiera jako pomníkem na nebi.

Čís. 2. **Kassiopea**. Hlavní hvězdy toho souhvězdí seřaděny jsou v podobě písmene dvojitého W. Řekové nazývali to souhvězdí Kassiopeia, Arabové Dsát el-khursi — na vysokém trůně sedící — Kassiopea, krásná manželka krále aitiopského Kefea (čís. 72.) zpyšněla a opovážila se rovnali se krásou Nereidám, dcerám mořského boha Nerea. Nereidy vymohly na svém dědu Neptunovi, aby Kassiopeu za tu pýchu potrestal. Proto velká obluda vynořila se z moře a pustošila říši Kefeovu. Pustošení mělo přestati, až by dcera Kefeova, **Andromeda** (č.3.) byla obětována té obludě; byla tedy přikována ku skále. Již blíží se obluda ku své oběti, tu **Perseus** (čís. 12), syn Joviše a Danaë přichvátal, zabil obludu a vysvobodil Andromedu, jež stala se pak jeho chotí.

Však to nebyl jediný hrdinský skutek Perseův. Zevs Persea i s matkou Danaou zavřel do skříně a vydal vlnám mořským na pospas; ale byli oba z vln u ostrova jménem Serifos rybářskou síti vyloveni od Diktya, bratra Polydektova, pána toho ostrova. Polydektes chtěl Danau pojati za manželku, ale Perseus zatím dospělý jemu v tom bránil. Polydektes, aby překážky té se zbyl, kázal Perseovi jíti na Gorgony, dcery mořského boha Forkya a přinésti mu hlavu Medusinu. Gorgony bydlely na tmavé západní straně země blízko zahrad hesperidských, byly okřídleny, měly hady na hlavě a okolo pasu; kdo se na ně podíval, zkameněl. Vlastnost proměniti v kámen hrdinu podržela i useknutá hlava Medusina, nejmladší dcery Forkyovy. Však udatným pomáhají bohové, zbabělce

15

nechají zahynout. Bůh Merkur půjčil Perseovi přilbici, jež hrdinu činila neviditelným a půjčil mu i svého obuví, jež hrdinu jako křídla ptáka neslo vzduchem; Bůh Vulkán dal mu meč, jimž by uťal Meduse osudnou a strašnou hlavu; bohyně Pallas půjčila mu ocelový štít, v němž viděl hlavu Medusinu jako v zrcadle, aby přímým pohledem na ni nezkameněl. Perseus takto jsa podporován samými bohy, vykonal nesnadný úkol a vraceje se domů, vysvobodil pak ještě Andromedu. — Byltě statným a proto měl i štěstí: fortes fortuna iuvat, statným Štěstěna přeje.

Ona obluda, již Andromeda měla padnouti za oběť, byla asi mořská ryba, **Velryba** (čís. 5), lat. u Ovida bellua, Cetus, z řeckého Kaitos, u Cicerona též Pistrix nebo Pistris, kterouž viděti málo jen nad obzorem, jakoby (národům pomořským) vynořovala se z vln popohltit svou oběť, co zatím blíží se vysvoboditel Perseus.

**Kassiopea** vyobrazovala se na mapách sedící na trůně s tváři od nebe odvrácenou, truchlící o svou dceru. — Bohové vždy přísně trestali smrtelníky, kdykoli zpyšněli štěstím a odepírali jim povinnou úctu, chtějíce rovnati se jim, povznášeti se nad meze člověku
vyměřené. Odtud ty výstrahy, aby nikdo neodvažoval se vzbuzovati zášť bohů přílišným štěstím a nezapomínal vzdáti jim povinnou úctu. — Pýcha předchází pád.

Jména hvězd v Kassiopei jsou: α Šedir (arab. sadr = prsa), β Khaf, což prý znamená též ruku i hrb velblouda, γ Rukhba = koleno. — V Andromedě hvězdy jmenují se: α Sirrah, β Mirarh, γ Alamak.

**Kefeus** vyobrazuje se jako muž rozkročený s turbanem (a s korunou) na hlavě, obrácený obličejem ku Kassiopei, ruce má rozpřaženy, v levé třímá žezlo, pravou drží ozdobu s hlavy jemu dolů splývající. —

16

Hlavní hvězdy jsou: α Alderamin = pravé rámě, β Alfirk = stádo, γ Errai = pastýř. — Perseus vyobrazuje se jako bohatýr v bojovném postavení, jenž právě sťal hlavu Medusinu a drží ji za vlasy (hady) levou rukou, pravou drží napřažený meč. Hlavní hvězdy jsou: α Marfik, h a z Mi'sam, β Algol, na hlavě Medusině, γ Algenib, δ Menkhib. — Ve Velrybě jsou: α Menkar = nos, nozdry, β Deneb Kaitos = ocas velryby, δ Baten Kaitos = břicho velryby. Hvězda o sluje Mira, podivná — pro znamenitou změnu světlosti a barvy.

Čís. 4. **Ryby**, lat. Pisces, řecky Ichthyes, arab. El-semakhatein, též El-hhût = ryba. Báj o tomto souhvězdí povstala v Syrii a vztahovala se k jednomu z tamnějších božstev, jež obyčejně bylo zobrazováno ve způsobě ryby s ženskou hlavou. Ctili ryby a bájili o nich, že jsou původu božského, a proto jich nepožívali. Římané přenesli pověst syrskou na svou Venuši, kteráž v boji proti obrům utekla se k Eufratu a tam i se svým synem, jménem Kupido, strachem vrhla se do řeky, aby unikla Tyfonovi, jenž ji pronásledoval. Matka i syn proměnili se v ryby a nejvyšší bůh umístil je mezi hvězdami. Ovid, Fasti, II. kniha.

Rozeznáváme Rybu severní a Rybu jižní; v této nejsvětlejší hvězda — první velikosti  jmenuje se α Fomalhaut = fom el-hhûl, huba ryby.

Čís. 6. **Sochárna**, latinsky Apparatus sculptorum, Francouz La Caille pozoroval jižní nebe a uznal za dobré poctiti sochařství souhvězdím; pojmenoval ještě jiných třináct souhvězdí.

Čís. 7. **Elektrika**, něm. hvězdář Bode (1741—1826) ve svém díle o hvězdnatém nebi tím jménem zvěčnil znamenitý pokrok, jejž učinila věda v oboru elektřiny.

17

Čís. 8. **Lučebna**, lat. Fonax chymiae. Jméno zavedl La Caille, jakožto pomník vědě nadmíru důležité; chemie i hvězdářům koná důležité služby, na př. spektralní analysa; jí dovídají se, z čeho hvězdy jsou, ve kterém stavu skupenství, o jejich pohybu . . . — Sochárna, Elektrika i Lučebna jsou pouhým okem sotva viditelny, skládajíce se z hvězdiček páté a šesté velikosti; vystupují málo nad obzor, i když vrcholí nahoře. Méně světlých hvězd na nebi nízko stojících obyčejně nevídáme, protože hledíme skrze tlustou vrstvu par, jež pohlcují světlo.

Čís. 9. **Trojúhelníky**, lat. Trianguli. **Velký Trojúhelník**, řecky Deltodon, arabsky El-muthalleth. — Ostrov Sicilie, ve starých dobách Trinakria jmenovaný, slynul úrodností a byl oblíbeným sídlem bohyně polní úrody, Cerery (řecky Demeter). Bohyně ta vyžádala si od Zeva, aby sídlo její bylo přijato mezi hvězdy. Dle jiné báje Trojúhelník má znamenati egyptské Delta na výtoku Nila do Středozemního moře. — Hvězda α jmenuje se arabsky Ras el-muthalleth — hlava, vrchol trojúhelníka.

**Malý trojúhelník** pojmenován od hvězdáře Hevela.

Nejdůležitějším obrazcem v měřictví, geometrii, bez níž hvězdář se neobejde, jest trojúhelník a proto vhodně má místo mezi hvězdami.

Čís. 10. **Moucha**. Jméno to zavedeno od dánského hvězdáře Tycho de Brahe (1547—1601). Jsou to tři hvězdičky: jedna čtvrté a dvě páté velikosti.

Čís. 11. **Skopec**, lat. Aries, Corniger, Laniger, u Ovida: Ovis phrixea, Pecus Athamantidos Helles, řecky Kriós, Chrysomallos = zlatorouný. — Frixos a Helle, děti thébského krále Athamanta a jeho choti Nefele, pronásledováni jsouce od své macechy

2

18

lnony, prchali na zlatorouném skopci, jehož jim dala jejich pravá matka Nefele, do cizí země jménem Aia. Helle spadla do moře, jež bylo pak zváno Helles-pontos (pontos = moře). Frixos šťastně dostal se do říše krále Aieta, obětoval z vděčnosti skopce Zevu, ochraniteli
prchajících — Zeus fyxios — a pověsil jeho zlaté rouno ve chrámě Martově. Skopec tento nyní stkví se mezi hvězdami.

Jiná báj vypravuje, že Zevs proměnil se ve Skapce, když před netvorem Tyfonem prchal i se všemi jinými bohy do Egypta. Na jeho rozkaz pak skopec onen povznešen mezi hvězdy.

Pravděpodobno jest, že chaldejští kočovníci první pojmenovali dvanáctero souhvězdí zvířetníku, zdánlivé to dráhy Slunce na nebi. Jejich bohatstvím byla stáda dobytka, jež vzrůstala zvláště na jaře, a proto snad poznačili souhvězdí, jímž Slunce zdánlivé prochází dobou jarní, podle ovce, býka a dvojici koz (jež. později od básníků byly přetvořeny v Blížence). Před 2000 lety bod jarní rovnodennosti byl v souhvězdí Skopce. — Jména hvězd: α Hhamel, β Šeratan. γ Mesarthim, δ Botein.

Čís. 12. **Perseus**, zmíněn při čís. 3. — V Perseovi objevila se nová hvězda, jež dne 22. února 1901 nabyla velikosti větší než prvé třídy, pak světlosti jí ubývalo; dne 10. dubna téhož roku byla hvězdou již jen asi šesté třídy.

Čís 13. **Býk**, latinsky Taurus, Bos, řecky Tauros, arabsky El-thauer. — Na mapách kreslívá se jen první polovice býka, zadek schází. Latinský básník Ovid praví o tom Býku, že předek jest zřejmý, zadek se tají.

Egypťané poznačovali toto souhvězdí hieroglyfem jedenáctého měsíce — Epep, květen — svého roku

19

a mívali v tu dobu žně, při nichž hlavním pomocníkem byl vůl. Na hlavě býkově jest skupina sedmi hvězd v podobě značky >, ležaté římské pětky. Řekové tu skupinu jmenovali Hyades — Hyady = souhvězdí dešťů. Hyady _byly nymfy a odkojily Zeva — dle jiných bájí odkojily boha Dionysa čili Baccha; jejich otcem byl buď Atlas, nebo Okeanos, nebo Melisseus, nebo Hyas (Hesiod jich jmenuje pět). Z vděčnosti Zevs umístil je mezi hvězdami. Hvězda první velikosti α mezi Hyadami jest červenavá a jmenuje se Aldebaran — Eldebaran nebo Ain el-thauer = oko býka; β Nath.

Jiná skupina hvězd toho souhvězdí, v obrazci nad předními lopatkami zvířete, jsou Pleady; lid rád je jmenuje Kuřátka, lat. Pleadae nebo i Vergiliae — hvězdy jara.

Bystré oko vidí jich sedm a proto řecky též se jmenovaly Pleias heptásteros = sedmihvězdí; oko méně bystré vidí jich jen šest. — Hlavní hvězda η jmenuje se **Alkione** a zdá se býti středem velké soustavy hvězd, z nichž vidíme šest nebo sedm; dříve byla považována od mnohých za slunce sluncí.

Dle bájí byli Okeanos a Thethys děd a bába, Atlas a Pleione rodiči Plead, a proto mohly býti sestrami Hyad. Obr Orion pronásledoval je po pět měsíců, a ony prosily Zeva za ochranu; Zevs proměnil je napřed v holubice, řecky Peleiades, a pak ve hvězdy na nebi. Nejstarší z nich byla Maja a byla matkou boha Merkura; proto bývá též zvána velesvatou —
sanctissima. Obyčejně viděti jest těch hvězdiček šest a ne sedm, proto prý, že šest sester Plead spojily se v manželství s bohy nesmrtelnými, jen jedna — Merope — dítě lidské, provdala se za Sisyfa a proto prý se skrývá. — Nedivme se. Kdyby některá kněžna

2*

20

vzala si chalupníka, též ve společnosti šlechticů se svým nešlechtickým manželem nesměla by se ukázati — když se tak »zahodila a zapomněla«.

Tak jsme si rovni! Snad až v té »hospodě« Čelakovského, za černou brankou, kdež všichni na nocleh »pán nepán se sejdem«. — Za života, kdy bychom něco z toho měli, to sotva kdy bude!

Souhvězdí Býka ve starých dobách nad jiné bylo důležito. Řekům a Římanům Hyady zapadaly večer v měsíci dubnu, zapadaly ráno v měsíci listopadu. Za obou těchto ročních časů bývalo mokré počasí a bouře; proto Horac (r. 65 před Kr. až r. 8 po Kr.) nazývá Hyady smutnými — tristes. Římané slavívali dne 21. dubna den založení Říma, slavnost pastýřskou — Parilia nebo Palilia ku cti bohyně stád — Pales. Prostý rolník představoval si nejstkvělejší hvězdu Hyad — α Aledebamn — jako matku ostatních hvězd k té skupině patřících, jež jmenoval prasátka — suculae. Slovo sucula jest zdrobnělé od slova sus, svině, řecky ὗς (hys). Latinské Suculae a řecké Hyades znamená totéž. Obě odvozeniny slova hyades od. hys = svině, prase i od hys = pršeti, mají svůj smysl.

Pleady večer zapadaly v únoru a březnu, ráno zapadaly v září a říjnu: to jsou počátky ročního času teplého a studeného — čas plavby a prací v polích. Latinský básník Virgil (r. 70—19 před Kr.), varuje své krajany, aby neseli dřív než budou zapadati Pleady.

Čís. 14. **Harfa Jiřího**, lat. Harpa Georgii. Opat Hell navrhnul již v r. 1789 a r. 1791 a zavedl to jméno ku cti anglického krále Jiřího III., jenž má harfu za odznak ve štítě.

21

Čís. 15. **Eridan**. Původně souhvězdí to slulo pouze řekou, latinsky Amnis nebo Flumen, řecky Potamos, arabsky El-nahr. Faëthon, syn boha slunce Helia a jeho choti Klymene, choval se pyšně ku svým soudruhům, zakládaje si mnoho na svém vysokém původu. Uražení druhové však pýchy té mu nechtěli snášeti a potupovali jej řkouce, že není ničím více než oni a pohrdali jím. On si to stěžoval matce, a ona jej ujistila, že jest božského původu. Faëthon odhodlal se žádati na otci důkazů svého původu a odebral se k němu, ač matka mu zrazovala. Helios synovi slíbil, že vyplní každé jeho přání na důkaz, že jest jeho otcem. Faëthon žádal, aby otec mu dovolil samému říditi vůz sluneční. Otec znaje nedostatečnost synovu k takovému úkolu, vylíčil mu nebezpečnost té jízdy a varoval jej, aby toho po něm nežádal, ale nadarmo; syn stál na svém, a otec zavázav se přísahou, že dostojí svému slibu a splní synovi, začkoli by žádal, odevzdal Faëthontovi sluneční vůz, aby jej sám řídil a udělil mu rady, čeho má se varovati a jak si má počínati. Když na své dráze přijel ke strašnému Štírovi, Faëthon vzpínajících se ořů slunečních nemohl udržeti, a sám poděšen jsa zvířetem tak hrozným, vypustil z rukou otěže; sjel s pravé dráhy a dostal se tak blízko Země, že hořelo všecko. Zevs vida tu pohromu, bleskem Faëthonta srazil s vozu, takže spadl do řeky Eridanu, kdež zahynul. Helios pak vyžádal si, aby na památku nešťastného syna řeka ta přijata byla mezi hvězdy.

Otec i syn tak ztrestáni, onen, že slibem se ukvapil a povolil, tento že nenaklonil sluchu dobré radě, neposlechl a stál urputně na svém.

Sice i neschopní páni, synové pánů ještě větších zastávají často vysoké úřady, ale vždy mají někoho,

22

kdo za ně myslí; však stává se, že svou neschopností bývají příčinou velkého neštěstí.

Jména hvězd: β Kursa, o Themim.

Čís. 16. **Vozka**, něm. Fuhrmann, lat. Auriga, řecky Heniochos, arab. Mumsik el-ainna. Athenský král Erichthonios, syn Vulkanův, svrhnul svého předchůdce Amfiktyona s trůnu. Byl ducha bystrého a vynalezl mimo jiné umělá díla i čtverkolý krytý vůz, aby v něm skryl své ohyzdné nohy. Tím velice zalíbil se Zevovi, jenž ho odměnil za to tím, že jej vřadil mezi hvězdy. — Jiná báj vypravuje: Myrtilus, syn Merkurův, byl vozkou krále Oinomana, jenž při dostihách nikým nemohl býti překonán. Pelops, syn Tantalův, zeť Oinomanův, však přece nad ním zvítězil — lstí. Velkými dary dosáhl u Myrtila, že jeho řízením vůz Oinomanův ve dráze na závodišti se zlámal. Myrtilus za to od svého pána byl vržen do moře, ale otec jeho Merkur povznesl jej na nebe — Tak odjakživa vítězily dary: kdo chce překonati pána, podplať jeho sluhu; dareba pak dostane ještě i vyznamenání — je-li synem mocného pána.

Jména hvězd: α Kapella, arab. El-aijuk, obyčejně Alhajoth, β Menkalinam (rameno vozky), hvězdy ζ, η jmenují se Kozelci, něm. Zicklein, lat. Hoedi, řecky Erifoi; ζ Kozelec I., Kozelec II.

I hvězda Kapella má svou báj. Bohu Saturnovi bylo souzeno, že bude s trůnu svržen jedním ze svých synů; proto pojídal své vlastní děti. Když narodil se mu Jupiter čili Zevs, mymfa Amalthea, ukryla synáčka řed Saturnem v jeskyni a vychovala jej mlékem své nejmilejší kozy — lat. capra, zdrobnělé capella. — Ona koza zlomila si roh a strom. Amalthea naplnila roh ten chutným ovocem, ovinula vonnými bylinami

23

a přinesla jel malému Zevovi. Zevs když svrhnul Saturna s trůnu a ujal se vlády nad světem sám, proměnil tu kozu v jasnou hvězdu a umístil na nebi; její roh pak učinil rohem hojností — něm. Füllhorn, lal. cornu copiae. — Ovid, Fasti V. — Dle Apollodora jmenovala se ona koza Amalthea; dle Hygina obě nymfy Aega a Helice vychovaly Zeva.

Tak tedy osudu ani bohové neunikali; naši Slováci zpívají: »Zpievalo ptáča na kosodrevine —
čo kemu súděné, toho ver nemine.« — Z báje té viděti, jak dobře se vyplácí posloužiti budoucímu pánu.

Když souhvězdí Vozky večer na nebi opět výše vystupuje — z dolní kulminace — pak souhvězdí Bootes — s hlavní hvězdou α Arktur — již sestoupilo na nebi s vrchní kulminace, schází čím dále tím níže a mizí v paprscích slunečních. — Vozka vychází, Arktur schází. Za dob starých Římanů dělo se to v měsíci září a říjnu, a byla to doba bouří. Tím snadno vysvětlují se slova římského básníka Horáce, jenž praví, že skromného nevyruší z klidné mysli ani mořské bouře, jež řádí za doby, kdy Artur mizí, zapadá, a Kozelci vzcházejí. Zvláště u Ovida a Vergila jest mnoho míst, kdež doba ta označena vzestupem Vozky a sestupem Arktura. I polním
hospodářům hvězdné nebe bylo kalendářem, v němž uměli čísti; nyní málo kdo mu rozumí, ač jest psán zlatými hvězdami a otevřen nám nad hlavami téměř každý den!

Čís. 17. **Orion**, u lidu též Kosy, Mojžíš jej jmenuje Kesil, u Homera, Hesioda a Pindara sluje Orion, arab. El—dšebbâr, obr, obyčejně El-dsauzâ, Elgeuze, Teuza. Vyobrazuje se jako muž připravený k boji,

24

levou rukou drží štít, pravicí napřažený kyj. Hvězdy: λ El-heka, též Ras el-dšauzâ — hlava bohatýrova. Viditelny jsou zde tři hvězdičky a sluji též oříšky, jsouce tak postaveny, jak u Řeků a Římanů v oblíbené jim dětské hře. α Beteigeuze neho Menkib — el-dšauzâ, též Jed el-dšauzâ — rámě bohatýrovo (pravé); γ El-râdšid nebo El-mirzam. lat. obyčejně Bellatrix — na levém rameni. — β El-ridšl el-dšebbâr — levá noha, β jest v obraze na levém koleně — obyčejně jmenuje se Rigel. Κ El-ridšl el-jumna — pravá noha, na koleně. Tři hvězdy tvoří jeho pás a jmenují se lat. lugulae, i celé souhvězdí tak bývá jmenováno. δ Mintaka, ε Anilam, ζ Anitak, h Šaif el-dšebbâr — meč obrův, též Lekat, zlatá zrnka, hvězdy π (1,2,...) tvoří štít Orionův.

Toto souhvězdí. nejvýznačnější ze všech, bylo podnětem k mnoha bájím. Orion dle Homera byl nejkrásnějším mužem. Růžová Zora — Aurora, Eos — vzala si jej za muže, čehož bohové mu tak dlouho záviděli, až jej Artemis — Diana — přepadla a usmrtila šípy. — Kdož rozhodne, bylo-li to ze závisti, že ho neměla sama, anebo proto — jak jiná baj vypravuje, — že pronásledoval lovící Dianu na ostrově Chios. Dbajíc své počestnosti Diana vzbudila ohromného štíra, spícího v podzemní sluji, že silným dupnutím o zem sluji tu prolomila a otevřela štírovi, jenž Oriona usmrtil. —

Neměl býti dotěrným — takový »neodolatelný« všem rozumným dámám konečně se zmrzí.

Jiná báj vypravuje o původě Orionově: Bohové Zevs, Poseidon, Neptun a Hermes čili Merkur přišli do Tainagry v Boiotil ku králi jménem Hyrieus. Ten hosty přijal a počastoval velmi vlídně, začež bohové jemu slíbili, že mu splní každou žádost. Poněvadž Hirieus neměl dětí, vyprosil sobě Syna — Oriona.

25

Orion byl statným lovcem, jenž ostrov Chios očistil ode všech dravých zvířat: on si zamiloval honbu tak, že i po smrti v podsvětí dále lovil a ustal, jen když pěli Alkaios a Sato.

Když Orion s nebe mizí večer, přicházeje do paprsků slunečních, což dělo se záhy zjara, nyní v květnu — kosmický západ —, pak když ráno zapadal, to jest na podzim, bývaly velké bouře. Čas ten básníci Horac, Ovid, Vergil označovali též mizením Oriona s nebe.

Čís 18. **Braniborské žezlo**, něm. Das brandenburgische Zepter, lat. Sceptrum Brandenburgicum, Gottfried Kirch, první hvězdář král pruské společnosti pro vědy, roku 1688, jméno to zavedl na_označenou královské moci a důstojnosti.

Čís. 19. **Zajíc**, něm. der Hase, lat. Lepus, řecky Lagos, arab. El—arneb = zajíc, též Khursi el-dšauzâ = trůn Orionův. Hvězdy: α El-arneb, β Nihal. Egypťanům zajíc byl odznakem opatrnosti, bázlivosti a rychlosti. — Řecký učenec Eratosthenes (276 až 194 před Kr.), jenž proslul jako pilný šiřitel vědy hvězdářské mezi Řeky, bájí, že zajíc pro svou rychlost vřaděn byl mezi hvězdy od Merkura. — Dějepisec Hygin vypravuje, že kdysi zajíci na ostrově jménem Leron rozmnožili se tak, že sežrali všechnu úrodu a byli příčinou hladu; stěží jen byli vyhubení, a z vůle bohův jeden objevil se na nebi ku poučení ostrovanů, že každá věc má svou dobrou i zlou stránku. — Čeho moc, škodí.

Čís. 20. **Rydlo**, něm. der Grabstichel, lat. Caelum scalptorium. Jménem tím La Caille zvěčnil umění rytecké v mědi. — Souhvězdí to i při horní kulminaci

26

— když vrcholí nahoře — u nás málo nad obzor vystoupí a skládajíc se z hvězd páté a šesté velikosti, sotva může býti pozorováno pouhým okem.

Čís. 21. **Holubice**, něm. die Taube, lat. Columba Noae. Astronom Royer sestavil r. 1679 toto souhvězdí z několika hvězd, jež náležely souhvězdí Velikého Psa. Vyobrazuje se s větvičkou v zobáku letící směrem k souhvězdí Lodi. Poněvadž někteří křesťanští Arabové
Loď — Argo — sobě představovali jakožto loď Noemovu, Holubice ta sluje Holubicí Noemovou.

Čís. 22. **Žirafa**, něm. das Kameelopard, latinsky Camelopardalis. Hvězdář Hevel zavedl to jméno souhvězdí sice rozsáhlého, jež však skládá se jen z hvězd čtvrté, páté a šesté velikosti. Ta část nebe pouhému oku jeví se hvězd dosti prázdnou.

Čís. 23. **Ostrovid**, něm. der Luchs, lat. Lynx, pochází taktéž od Hevela. Jest vskutku potřebí oči ostrovida, aby bylo viděti ty hvězdičky, z nichž souhvězdí to se skládá.

Čís. 24. **Herschelův dalekohled**, něm. Herschel's Teleskop, lat. Telescopium Herschelii. Slavný hvězdář Herschel, jehož praděd pocházel z Moravy, odkudž asi z příčin náboženských počátkem 17. století se vystěhoval, pořídil si roku 1789 zrcadlový dalekohled s délkou ohniska 12 m; zrcadlo mělo průměru 126 cm. Dalekohled zvětšoval 7000krát a sváděl do oka
36.000krát tolik světla, než od předmětu do oka neozbrojeného může dopadati. Shotovil mimo to na 200 teleskopů s délkou ohniska 2.4 m, 150 s délkou ohniska 3 m, 80 s délkou ohniska 6 m a několik s délkou ohniska 7.5 m. Nejzajímavější hvězdářské práce však vykonal svým šestimetrovým teleskopem. Herschel svými teleskopy rozšířil obzor hvězdářský

27

nadmíru a objevil pravé divy v nekonečném prostoru: vhodně tedy zásluhy jeho o hvězdářství jsou zvěčněny mezi hvězdami. Souhvězdí to skládá se z nepatrných několika hvězdiček nad čárou od β Vozky k α Blíženců. — Již to jinak nejde, než že zásluhy pravé, jsou-li vůbec oceňovány, musí spokojiti se nějakým jen koutečkem v přijímací síni, kdežto prázdné hlavy
zaujímají třebas místa nejpřednější. První služné Herschelovo, jež mu bylo vyplaceno na čtvrt roku, bylo 50 lib., současně malíři Jenisovi z téže pokladny vyplaceno bylo za oltářní obraz 80.000 lib.: za tolik peněz byl by Herschel musel sloužiti 150 let! — Herschel však získal si značného jmění ale prodejem svých teleskopů.

Učenci, badatelé proti umělcům velmi nízko se cení, nejchudším jest filosof, a přece ani filosof ideí se nenají. Však to nepatří sem a pojďme raději dále.

Čís. 25. **Blíženci**, něm. die Zwillinge. lat. Gemini, řecky Dioskuroi a dle toho i něm. často die Dioskuren, arab. El-tawâmain. α sluje Kastor, β Pollux, původně to bylo dvě koz: η nejzápadnější hvězdička Blíženců čtvrté velikosti, měnlivá, sluje Propus = přednoží, γ Al Hena, ε Meboula, δ Wasat, μ Tejat.

Kastor a Pollux byli bratři krásné Heleny, manželky spartanského krále Manelaa, kterouž unesl trojanský princ Paris a byl takto původcem války trojanské. Narodili se v Amyklách z matky Lady. Kastor maje za otce Tyndara, byl smrtelným a vyznamenával se jako krotitel koní a vítěz v dostihách, Pollux měl za otce samého Zeva, byl proto nesmrtelným a vyznamenával se zvláště v soubojích na pěstě. Oba súčastnili se výpravy Argonautů a zápasu s divokým kancem. jenž na rozkaz Diany pustošil

28

Aetolii. Dle Homera již před válkou trojanskou zmizeli se Země a požívajíce cti bohův, žili vždy ob den; byli tedy i smrtelnými i nesmrtelným. Později bájeno, že Kastor padl v boji a Pollux z bratrské lásky prosil Zeva, aby mu též dopřál smrti. Však to by bylo proti nezměnitelnému osudu, a proto Zevs dal mu na vůli, buď aby žil sám (bez bratra) na Olympu s bohy, nebo s bratrem jeden den v Navi — v Hadu, podsvětí — druhý pak den na Olympu. Pollux rozhodnul se ve prospěch milovaného bratra. Tato láska bratrská oslavena jasnými hvězdami na nebi. — Jestli obě hvězdy, Kastor i Pollux, jasně zářily, bylo znamením šťastné plavby.

Čís. 25. **Malý Pes**, něm. der Kleine Hund, řecky Prokyon, poněvadž jsa blíž Polárce než Velký Pes, před ním na nebi vycházel, arab. El-kheb el-mutakaddem = pes prchající. — Jména hvězd: α Prokyon, El-šira el-šamija, β Mirzam nebo El-mirzam.

Eratosthenes líčí Malého Psa jakožto průvodce Oriona; u Hygina sluje Maira. Ikarion athenský i s dcerou Erigonou pohostil jednou velmi štědře putujícího boha révy, Dionysa — Baccha — a byl za to od něho obdarován sudem toho božského nápoje a množstvím rév, jež měl pěstovati ve svém okolí. Když jednou i s dcerou a psem šel na pole dohlížet na vinicích, dělníci, neznajíce účinku vínu, opili se jím a myslíce, že požili jedu, zabili Ikariona. Věrný pes střežil nepohřbeného pána. Erigona uzřevši otce mrtvého, hořem se oběsila. Všichni tři od bohů byli přijati mezi hvězdy.

Čís. 27. **Jednorožec**, něm. das Einhorn, řecky Monokeras. Anglické lodě mají v odznaku obraz koně s rohem (ryby narvala, monodon) na čele, jenž drží

29

štít. Hvězdář Hevel umístil Jednorožce na nebi mezi hvězdami Malého a Velkého Psa; skládá se z hvězdiček páté a šesté velikosti, jen jedna jest velikosti čtvrté; vystupuje málo nad obzor, i když vrcholí nahoře a proto nikterak nevyniká.

Čís. 28. **Velký Pes**, něm. der Große Hund, lat. Canis, Canicula, řecky Astrokyon nebo pouze Kyon, arab. El-khelb el akhber = velký pes. Hvězdy slují: α Sirius, Seitios = záříčí, arab. El šira el-abur, též Alhabor, egyptsky Sothin, Siris; β Mirzam, δ Aludra, ε Adara, ξ El-furud.

Řečtí básníci bájí, že Pes náleží Zoře a v rychlosti žádným jiným psem nemůže býti přestižen. — Dle jiné řecké báje Velký Pes byl věrným průvodčím Orionovým a byl proto Orionu na nebi přidělen. Dle egyptské pověsti souhvězdí to představuje boha jménem Anubis, jehož sestra byla proměněna v krásnou hvězdu Sothin — Sirius —. Obou dětí otcem byl král Osiris.

Sotva které jiné souhvězdí od řeckých a římských básníků bývá tak často jmenováno jako souhvězdí Velký Pes a jeho hlavní hvězdu Sirius; jméno Sirius pak značí i celé souhvězdí. Když Slunce jest v souhvězdí Lva, Sirius vychází ze ranního šera; tehdy dni byly co nejvíc parné, a za příčinu toho parna byl pokládán Sirius. — Dies caniculares, něm. Hundstage. Proto u Homera sluje opornios t.j. parno zvěstující, u Arata (od r. 271 před Kr.) sluje pálící, — u Horace flagrans (hořící) Canicula, sidus fervidum, hvězda žhoucí. Ovoce tehdá dozrávalo a požívalo se: slavný lékař Hippokrates (460—377 před Kr.) myslí, že častá onemocnění za parných dnů pochází z požitého ovoce. Proto u Virgilia Sirius přináší nemoci — morbos ferens; Doba největšího parna byla označována kosmickým

30

východem Siria — kdvž totiž Sirius, byv po nějakou dobu skryt v paprscích slunečních, opět jeví se na nebi před východem Slunce. Doby roční velmi případně bývaly označovány dle hvězd.

Čís. 29. **Loď**, něm. das Schiff, lat. Navis, Argoa puppis, Ratis heroum, řecky Argo, arab. El-sesima nebo El—markheb. — Eratosthenes a Hygin vypravují, že bohyně Minerva způsobila, aby loď, na níž odvážní Argonautové, vedeni jsouce Jasonem, vykonali cestu do Kolchidy dobyt zlatého rouna, byla povznešena mezi hvězdy. Pozdější mythus jest, že jest to loď Noemova.

Čís. 30. **Tiskárna**, něm. die Buchdruckerpresse, lat. Officina typographica. Jméno bylo zavedeno opatem jméno La Caille na oslavu vynálezu, jímž teprv osvěta mohla rychle se šířiti. Vynález ten zajisté si toho zasloužil, aby měl důstojný pomník. Bohužel místa
na nebi již byla zadána, a kdo se tam dostane, nikdo rád nepostoupí místa jinému; proto nezbylo než spokojiti se s několika nepatrnými hvězdičkami nad lodí, jíž byly odňaty. Vysoko nad obzor nevystoupí ani při vrchní kulminaci, a proto souhvězdí to sotva jest
viditelné, poněvadž skládá se jen z nepatrných hvězd.

Čís. 31. **Kompas**, něm. der Schiffskompas. lat. Pyxis nautica, zavedl též La Caille. Vůdce námořníků zasluhuje býti na nebi mezi hvězdami, k nimž obracejí zraky na svých nebezpečných cestách. Bode ovinul kolem kompasu měřidlo hloubky. — i toto souhvězdí
u nás sotva může býti viděno; skládá se z hvězdiček jen páté a šesté velikosti; i když vrcholí nahoře, malinko vystoupí nad obzor.

Tak opět vidíme, jak již na světě bývá, že nejlepší ceny a místa rozeberou si velcí páni, a skutečné

31

zásluhy — dostane-li se jim vůbec odměny — musí spokojiti se s koutečkem tmavým a s drobty, jež padají se stolů velkých pánů!

Čís. 32. **Rak**, něm. der Krebs, lat. Cancer, Nepa, řecky Karkinos, Astakos, arab. El-sertan. Jména hvězd: α Ezzaban nebo Azubene = klepeto; shluk hvězd u ε jmenuje se Jesle = die Krippe, lat. Praesepe nebo Praesepium, řecky Fatne; hvězdy: γ Osílek severní, δ Osílek jižní = der nördliche und der südliche Esel, lat. Asellus borealis, Asellus australis.

Z různých bájí vypravuje jedna, že polobohu Herakleovi bylo postoupiti též boj s hadem borejským, zplozeným od Tyfoan a Echidny, živeným bohyní Herou, kteráž Herakleovi nepřála. Rak v boji pomáhal hadovi a štípal Heraklea, jenž však raka zašlápl. Hera povznesla raka mezi hvězdy, kdež stkví se dosud.

Osli neb Osílci v Raku jsou oni ušáci, kteří buď svým vydatným hlasem, nebo že nesli břemena, Jovišovi dopomohli k vítězství nad obry a došli tak vyznamenání. — i osel bývá vyznamenán, měl-li štěstí, že sloužil velkému pánu.

Čís. 33. **Velký Medvěd** nebo **Velký Vůz**, něm. der Große Bär, der Große Wagen, lat. Ursa = medvědice, též Triones nebo Septem Triones = sedm mláticích volův, řecky Arktos, arab. El-dub el akhber = velký medvěd. Hvězdy: α Dubhe, β Merak, γ Fekda, vlastně F&chds, δ Megrez, ε Alioth, ζ Mizar, u ní malá hvězda g Alkor = das Reiterlein, arab. El-suchâ = zapomenutý, η Benetnaš, Alhaid, π Muscida.

**Velký Medvěd**, případněji Medvědice, nebo Velký Vůz jest v krajinách našich nejznámějším souhvězdím; však i ve starověku připomíná se co nejčastěji. Mojžíš zmiňuje se o tom souhvězdí — Vůz — vedle jiných

32

— Orion, Sirius, Pleady. U Homera jest i vyobrazeno na štítě Achilleově, jakož i Pleady, Hyady; též vozem — hamaxa — je nazývá; souhvězdí to nikdy do moře se neponořuje — jest totiž obtočnové.

Hvězdy α, β, γ δ tvoří čtyři kola vozu Erichthoniova. (Viz čís. 16.). — Dle jedné báje nymfa Kallisté — nejkrásnější nebo velmi krásná, překrásná — kteréž epitheton příslušelo vlastně bohyni honby, Artemidě čili Dianě, provázela Artemidu na honbách po Arkadii a nyní stkví se na nebi. Dle jiné báje dcera Lykaonova, ukrutného krále arkadského, výtečnými vlastnostmi získala si přízeň Zevovu, ale Juno dalo jí netvornou podobu medvědice.

Kdo vidí hvězdičku g Alkor u hvězdy ζ Mizar, má dobré oko; proto Arabové říkali o člověku, jenž malicherností víc si všímal než věcí důležitých: Viděl jsi Alkor, ale Měsíce ne.

Souhvězdí Velkého Medvěda jest blízko pólu a tedy nad severnějšími, studenějšími krajinami; proto básníkům jest »ledovým, mrazivým, studeným« a slouží jim též na označenou zemí severních.

Čís. 34. **Malý Lev**, něm. der Kleine Löwe, lat. Leo minor. Hvězdář Havel utvořil z hvězd Velkého Vozu a Velkého Lva, jež nesnadno daly se vřaditi v obrazce těch souhvězdí, a proto od Řeků »neforemnými« — amorfotoi — sluly, souhvězdí Malého Lva.

Čís. 35. **Lev**, něm. der Löwe. lat. Leo, řecky Leon, arabsky El-ased, α Romulus, Rex, Stella regia, řecky Basiliskos, arab. Kalb el-ased = srdce lvovo, Kalbeleced, Alkabor; β Danebola = ocas, γ Algieba, δ Zosma, též Dhar neb Hur el-ased = hřbet lvův, ϑ El zubra = srst, nebo Koksa = kloub, χ Minšir = nos.

33

Lev ten pocházel od netvorů Tyfona a Echidny; sílou žádný jiný se mu nerovnal, kůže jeho byla nezranitelna; řádil a dávil v jednom údolí Peloponnesu. Když Herkules dostal se ve služby Eurystheovy, krále v Mykenách, obdržel rozkaz, aby králi přinesl kůži toho lva. Bohatýr nepořídiv ničeho šípy, vehnal lva do jeho doupěte, zastavil vchod, jiným otvorem dostal se na lva a uškrtil jej rukama. Juno Herkulovi nepřející způsobila, že přemožený lev stkví se na nebi.

Když Slunce bylo v souhvězdí Lva, byla největší parna; proto básníkům starého věku i to souhvězdí sloužilo na označenou parného času ročního.

Čís. 36. **Sextant**, něm. der Sextant, lat. Sextans Uranjae. Souhvězdí toto pode Lvem skládá se jen z nepatrných hvězdiček a bylo pojmenováno od Hevela, jenž sextantem pozoroval stálice v létech 1658—1679. Hevel zřídil si hvězdármu, kteráž však shořela a jeho sextant též.

Čís. 37. **Hydra**, něm. die Große Wasserschlange, Hydra, arabsky El-šuďša. Hvězdy: α Alfard nebo Alferat = hvězda osamocená, ϑ Ninšir = nos. K tomu souhvězdí druží se

Čís. 39. **Pohár**, něm. der Becher, latinsky Crater, řecky Krater, arabsky El-bâtija a

Čís. 40. **Havran**, ném. der Rabe, latinsky Corous, řecky Korax, arabsky El-gorâb, δ Algorab, ε Minkâr el-gorâb = zobák havranův.

Hydra dle jedněch jest ona lernejská obluda, kterou zahubil Herkules. Jiní bájí takto: Apollo chystaje Zevovi tabuli, poslal Havrana na rychlo, aby ve zlatém poháru přinesl vody. On však uzřel fík plný ovoce; okusiv poznal, že ovoce není ještě zralé

3

34

a čekal pod fíkem, až by uzrálo zúplna. Když se nazobal, chytil hada a letěl i s vodou ku svému pánu a vymlouval se, že had ten mu bránil, aby nabral vody. Za. tu lež Havran musil žízniti, dokud fíky mléčné visely na stromě. Děj ten nyní jest zobrazen na nebi hydrou, pohárem s hadem a havranem.

Čís. 38. **Vývěva** (die Luftpumpe, Antlia pneumatica). Hluboko na obzoru, malé hvězdičky a sotva mohou býti pozorovány. Tím souhvězdím La Caille zvěčnil důležitý vynález, jejž učinil Otto Guerike, starosta Děvínský r. 1650. Ovšem vývěva naše jest zdokonalena a má jiný tvar.

Čís. 41. **Malý Medvěd, Malý Vůz**, něm. der kleine Bär, der kleine Wagen, lat. Ursa minor, řecky Arktos mikra, arabsky El-dub el-asger. Římanům obě souhvězdí Velký i Malý Medvěd někdy slula Arktos, Ursa, Triones a Plaustra. Thales prý souhvězdí Malého Medvěda doporučil Foiničanům za vůdce po moři, a proto jmenuje se Malý Medvěd též Foinike. α Polárka (Polarstern, Kynosura = psí ocas, arab. Rukkaba, β Kokab, γ Ferkad, obě dvě dohromady El-Ferkadain = obě telata, δ Vildun. Hvězdy β a γ sluly táž »Hlídači«, protože za Velkým Medvědem (sedmi voly = septem triones) ustavičně se otáčejí, jako by je hlídali.

Arkas, od něhož krajina Arkadie, kdež panoval, má prý své jméno, byl synem nymfy Kalliste. Na honbě pronásledoval medvědici a již již chtěl ji zabiti; nevěděl, že pronásleduje matku proměněnou v medvědici (viz čís. 33.). Aby zabránil zločin zavražděni matky, Jupiter proměnil lovce v medvěda a umístil jej i medvědici vedle sebe mezi hvězdami. Hera, manželka Zevova, záviděla jim toho vyznamenání a nemohouc ho zvrátiti, aspoň toho si vyžádala na

35

božstvech moře, aby ti Medvědi nikdy nesměli se koupati v moři. — Souhvězdí to jsou totiž obtočnová a nezapadají proto nikdy pod obzor.

Čís. 42. **Drak**, něm. der Drache, latinsky Serpens, řecky Drakon, arabsky El-tinnin. α Lucida candae, Fuban, γ Ettanin, β Grumium, ξ Alwuid, ν Errakis, λ Giauzar, η Aldib, ζ Nodus I, δ Nodus II.

Dle řeckého bájesloví byl to drak Ladon, jenž střežil vchodu do zahrad Hesperid, dcer noci. Zahrady ty byly na nejzápadnějším kraji země, a rostla v nich zlatá jablka. Juno dostala je darem ku sňatku s Jupiterem od své matky Země (Gaja). Herkules měl přinésti těch jablek svému pánu Euryslheovi. Herkules nevěděl ani, kde hledati ty zahrady a bloudil po rozličných zemích, až se dostal k otci Hesperidek, Atlantovi, jenž na ramenou držel nebeskou oblohu. Než mohl vejíti do zahrad, bylo mu u vchodu podstoupiti boj s Drakem, jehož konečně zabil. Za věrnou službu drak od Jurony byl povznešen na nebe a umístěn
mezi hvězdami.

Čís. 44. **Ohaři**, Lovčí Psi, něm. die Jagdhunde, lat. Canes venatici. Hevel uvádí je ponejprv ve svém díle »Prodromus Astronomiae«, vedeni jsou od Boota nu šňůře. Když souhvězdí mizí v paprscích slunečních, v druhé polovici září a v první polovici října, odbývají se hony, při nichž lovčí psi konali důležité služby. — Anglický hvězdář Halley (1656—1742) prý nazval hvězdu α na nákrčníku jižního psa Cor Caroli II, = srdce Karla Druhého. Pes, hvězda severnější, sluje Asterion, jižnější Chara.

Čís. 45. **Kštice Bereniky**. něm. das Haupthaar der Berenice, lat. Coma Berenices. Jest původu nového, ač báj, na níž se zakládá, sahá do starověku. Tycho

3*

36

de Brahe jmenuje to souhvězdí ve svém díle, nazvaném »Progymnasmata Astronomiae instauratae« (v Praze 1603). Berenike, manželka egyptského krále Ptolomaea Euergeta, něžně milovala svého chotě. Když válečně táhl do Syrie vysvobodit své sestry z rukou jejích nepřátel, Berenike učinila slib, že bude obětovali své krásné vlasy bohům, vratí-li se manžel její vítězem. Euergetes zvítězil, Berenike dostála slibu a zavěsila prstence svych vlasů ve chrámě Venušině, odkudž však zmizely.
Krále nad tím zarmouceného upokojil jeho hvězdář Konon, řka, že oběť ta bohům tak se líbila, že vzali ji na nebe a vřadili mezi hvězdy; i ukázal je králi Euergetovi.

Čís. 46. **Bootes** = volař. též Arktofylax = medvědář, též Trygetes = vinař, poněvadž když přede dnem vycházel, byl čas vinobraní: arabsky El-auvâ = volající. Hvězdy: α Arktur, arab. Hâris el—somà = hlídač nebe, β El-nekkâr. ε Mikar nebo Izar, Mirak. η Mufrid = odloučený.

**Bootes** byl syn Cerery, bohyně polní úrody, a jmenovali jej Filomelos. Svým bratrem, jménem Plutus, zbaven jsa všech statků, z nouze o potravu vynalezl pluh, zapřáhl do něho voly a vzdělával půdu. Demetře (Ceres) vynález tak se zalíbil, že vynálezce s pluhem i s voly vřadila mezi hvězdy.

I Bootem spravovalo se polní hospodářství; byl to podzim, kdy mizel v paprscích slunečních. Když Arktur zapadal večer, i když vycházel přede dnem, panovaly mořské bouře: proto nazývá se v Aeneidě neblahou Minervy hvězdou = triste Minervae sidus, neboť lodě vracejících se Reků z trojanské války byly bouří ztrozkotány za trest, že Rekové ukradli Paladium.

Čís. 47. **Panna**, něm. die Jungfrau, latinsky Virgo, řecky Parthenos, Astrea, též Dike = Spravedlnost, arabsky El-adsrà = panna nebo El-sumbela = klas. Bývá vyobrazena s palmou v pravici a s klasem — spika — v levici nebo též jako bohyně spravedlnosti. Hvězdy: α Spika = klas, arabsky Azimech, β Zâwijava. γ Zawiah, ε Vindemiatrix, řecky Trygeter, též Protrygeter — vinař, poněvadž hvězda ta krátce před vinobraním na nebi přede dnem vycházela. ε vychází něco dříve.

Bohyně spravedlnosti Astrea prodlévala na Zemi za zlaté doby, za doby slříbrné jen někdy a za doby měděné zmizela zcela se Země a bydlí na nebi, kdež dává se viděti jen v noci. Jí za odznak spravedlnosti u nohou leží váhy. Dle jiných bájí jest to bohyně polních plodin, Ceres. a proto drží v ruce klas — spika. — Též znamenalo to souhvězdí egyptské božstvo Isis, kteráž se svým manželem, jenž slul Osiris, blahodárně působila. Byli později uctíváni v Řecku. — Možno jest, že v pradávném čase, kdy Slunce bylo v tomto souhvězdí, odbývaly se žně, a že volen pro něj obraz žnečky. Hvězda ε Vindemiatrix, též Vindemiator má svou báj, Dárce vína Bacchus daroval svému miláčkovi Ampelonu révu, uvázanou k jilmu. Když hoch Ampelon trhal zralé hrozny, spadl a zabil se. Bacchus proměnil jej ve hvězdu a vsadil na nebe. Nechtěli tím naznačíti účinek vína? Člověk vínem opojený jest v nebi Bacchově, jakž zní v písni jedné: »Člověk opojený s knížaty nemění a jest šťastnější než mnohý král.«

Čís. 49. **Kentaur**. U nás jen hořejší část toho souhvězdí může býti viděna nad obzorem, když nahoře vrcholí. Vyobrazuje se v téže podobě jako Střelec; přední část člověk, zadní kůň; onen drží pravicí napřaženou kopí, levicí štít, tento právě vystřeluje šíp.

88

Čís. 49. Váhy, něm. die Wage. lat. Libni. Jugum,
řecky Chelai = klepetn štírovn. později Zygos, arab.
El-mlzůn. V nejstarších dobách hvězdy a a [) patřily
ke Stírovi jako konce jeho klepet: teprv později váhy
myšleny jako souhvězdí o Sobě. Hvězdy: a lšl—zubén
el-genubi == jižní klepeto štírovo, 7 Zuben el-nkrab.
Přede dvěma tisíci léty Slunce bylo v čas rovnoden-
nosti podzimní v souhvězdí Váh, den vyviižil se
5 nocí, a proto pojmenování vhodně voleno.

Čís. 50. Vlk. ném. dur Wolf, lat. Lupus, teprv
později, jinak Fera, řecky Theríon = zvíře, nrnbsky
lšl-šemarich = ratolest pnlmová. Až téměř na obzoru
i když nahoře vrcholí. sotva viditelný: hvězdičky
čtvrté a páté velikosti.

Jest prý to ukrutný Lykaon, král nrkndský, jenž
nosíš přívětivě přijímal, ale potom je obětoval Zevovi.
Kdy. tento v podobě cizince o ukrutností Lyknonovč
sám se pře5vědčil, bleskem zapálil králův hrud :: krále
ukrutuíka proměnil v zuřivého vlka.

C'is. 51. Koruna severní. německy die nórdliche
Krone, lot. Corona borcnlis. řecky Stefanos borcios,
arabsky lil-fekka = otvor. :: Gemma = drahokam.
arabsky El-nair min cl-fckkn :- jnsnost otvoru. [iNu—
sakun, snad Kasa el-musůkhim = žebrácktí mísu.

Athéňané museli odvádčti každý devátý rok po
sedmi jinoších a dčvách Kretenským, kteří je dávali
sežrnti hroznému Minotuurovi, obludě to půl člověka
a půl býka. Theseus chtějc vlast svou osvoboditi od
té hanebné daně. odebml se na Kretu a přispěním
Ariadny. dcery kretského Imilc Minoa, zabil netvora
sídlícího v bludišti — labyrintu. Na to Ariadne prchla
s hrdinou 'na lodi. Když. odpočívali na osnmělém
ostrově Naxu, Ariadne usnula na skále, ale Theseus jí

1:9
uprchl. Bůh Bacchus však smilovnl se nad uholiuu
npuštčnbtí a plačící, učinil ji svou manželkou a dal
jí dle sebe — Liber — jméno Libera n= starosti prosta.
Aby bylo přesvědčena, že manžel jeji jest bohem, snnl
jí čelenku s hlavy a vyhodil k nebi. kdež mízní-ilo
n září tnm dosud. — Theseus dokud Ariadny potře-
boval, držel se ji, dosáhnuv svého cíle, ztratil se »—
.;patné se zachoval!

Čís. 62. Herkules, v nejstarších dobách Ičn go-
nasin, Nixus in genibus nebo prostě Níxus, arabsky
lšl-dšéthi. a Ras Algethi ==- hlnvn klečícího, faltutitikus,
z Marlik, w Kajam.

Řekům bohatýr Herkules byl vzorem pravého
hrdiny. Vyznamenávnlť se statečností u silou jak tělu,
tak ducha. Byl synem thebnnského krále Amlltryonu
a již jako dítě v kolébce aútlýmn ručkamnc udávil
dva silné hady, kteréž na něho poslala nepřátelská
:nu bohyně Juno. Junoniným míšlím stálo se, že
Herkuloví bylo vykonati dvanácti: přcobtížných skutků
hrdinských. On maje volítí bud život nečinný a plný
nejslnstnčjšich rozkoší tělesných nebo život plný práce,
bojů a zúpnsů, rozhodl se pro život hrdinských činů.
Za to bohové mu přáli proti Junonč a pomohli mu
překonali nebezpečí, do nichž upadal. I strážce pod-
světí, třihlavého psa Kerbem přemohl. Konečně sešilel
n sám se spálil na hranici. Jupiter za hrdinské skutky
Herkula odměnil na nebi: ijeho dvnnáctero skutků
tam lze nnjiti. — Možno totiž v souhvězdí tom vyčistí
číslo 12.

Nepřátelství Junonino bylo příčinou slávy Herku-
lovy. Tak tedy i nepřítel nám může prospěti,— ale
nesmíme býti zbabělci, nýbrž musíme počínati sobě
jako Herkules, pak i nám Bůh přispěje, jako Herkulovi

40
pomáhali bohové. Přísloví praví: »Pán Bůh dává po-
žehnání, ale do chléva sám nenahání.

Čís. 53. Hud, něm. dic Schlangc, lat. Anguis
nebo Serpens, řecky Ofis, arabsky El-hhnwijn. Hvězdy-
a lřnuk nebo Unk el-hhnwlja = krk hudúv, 3 Jed“ I.
.! Jed ll, 8 Alyn — na konci ocasu.

Čís. 54. Hndonoš, něm. der Schlnngcmrňger, lm.
Anxuilenens, Anuuifer. řecky oliuchos, arab. lÉI-hlmuwá
== zaklínač hadů. Hvězdy: a Ras (hlava) Alhugue,
iš Kelbatrai, vlastně thlhcl-růi =- pes pastýř-úv. pn-
nčvudž souhvězdí to předslanvali si jako stádo 15 par
slýřcm a psem; a MarSik.

Hndonoš s hadem představuje búječnúho
Aeskulnpa, jehnž otcem byl Apollo. matkou Koronis.
Svým lékařským uměním dovedl i mrtvé křísíti “ tn
bylinou. kterouž jemu přinášel had. Hippolylus. syn
Thcscův a Hippolyty amnzonky, byl od své hmcechy
Faculry .u otec nsočen. jakoby jí byl sváděl k ncpu.
čcstnosti. Byla to jen zlomyslné msln, poněvadž
Hlppulyt nechtěl býti Facglřc po vůli v její nešlechet-
nosti. Nicméně otec proklel syna 0. Jul jej na čtyři
kusy roztrhali splašcnými koňmi. Převozník duší nn
onen svět v pndsvčtí. Chura-n, již doprovázel synu
Theseovn do temné Navi. lu Aeskulnp. vynálezce
léčebného umění. smilovnl se nad nqvinným. sebral
údy jeho, spojil v tělo. jak bývalo :! vzkřísil mrtvého.
Vše—mocný leva rozhněval se na Acskulapa. že npo-
vážil se zasahovali v jeho prava a bleskem jej svrhnul
do říše stínů; však na prosbu Apollinn, syna svého.
připustil, aby Aeskulap, vnuk Zevův, snlčl v noci
dlíli mezi hvězdami. ! had. jeho věrný pomocník.
odznak opatrnosti a bdělosti. byl Aeskulapovi na. nebi
přidružen.

ll.

Čis._ 55. Pravítka a Úhel, něm. das Lineal und
Winkclmnll. lat. Regula ot Norma. Jméno zavedeno
:_nd La Cnille. Jsou to jen nepatrné hvězdičky pod
h'h'rem a mohou býti jen stěží vlděny, i když nahoře
vrcholí. a vzduch jest zvláště čistý.

Cia. 56. Štír, něm. der Skorpion, latinsky Nepa.
Seurpius nebo řecky Scorpios. též Mega Therion-=
vulkévzvíí'e, arab. El-akrab. Hvězdy: a Ix'nlh el-Akrnb
n- srdce štírovo. obyčejně A ntnrcs, Vespertilio. [: též
nlnla El-Akrab, 3 Dšuhba, vlastně Dchebhn cI-akrnb
m čelo štírovo. — _Zn starých dob hvězdy a a 3 Váh
náležely ještě ku'Stíru. byly na konqich jeho klepet.
„.... Jest lu onen Stír, jehož Artemis čili Diana poslala
mt Oriona, [viz čís. W.) a-protn Orion prchá před ním
„_ jOSt totfž na nebi co možná velmi yzdúlen od něho.
Nešťastný Fněthon uzřct' strašného Stím. pominul se
nmysly mrazivým strachem a upustil otěže (viz čis. 1.5. ).

Čís. 57. Dalekohled. něm. dns Fernrohr, latinsky
Tubus zavedeno od Lu Cnillc. Nástroj hvězdáři nej-
důležitější a nejpotřebnější. jenž otvírá nám bránu
v Ipemnczí prostoru, znsluhuje býti ctčn od hvězdůh't
ll prot-) obdržel místo na nebi. V našich krajinách
nemůže býti viděn, poněvadž jest velmi nízko :: skládá
ne jen z nepatrných hvězd.

Čís. 58. Jižní Koruna, něm. clic s[ldliche,Kr0ne,
latinsky (:oronn australis, řecky Stefanos nothios, arab.
I'll-ikilíl, cl-dšenůbi ujižní koruna, Bújesloví o ní se
nezmiňuje.

_ _Čis. 59. Střelec. něm. der .Schmze, lat. Sagittarius.
Areas Haemonius; Arcitencns, řecký Toxotes, amb.
l-Zl-rňmi-u střelec, El-kaus = luk. Horní část těla jest
střelec. spodní část jest kůň.

42

Pohoří Pelíon bylo prý sídlem thesalského národa
Kenmurů, kteří první znali se v umění jezdili koňmo.
Homer jmenuje len národ divokým. Díváme-li se na
jezdce rychlo uhánějíciho, vypadá podivně, zvláště
stočí-li kůň hlavu !( zemi; téz sedí-li jezdec na koni
pnsoucim se. Kdo by jezdce takového viděl ponejprv,
mohl by jej míti i s koněm za bytost jednu — člověk
od polovice kůň. — Mezi Kentaury žil prý Chiron,
jenž znal se v léčivý-ch bylinách. byl moudrým kníže-
tem a proto starý věk jemu vykázal místo mezi
hvězdami.

Dle jiné pověsti souhvězdí Střelce znamemi syna
Eufemy, ctitelky Mus čili Umčn: jmenoval se Krotos
n vynalezl střelbu šípem. .— Za doby Hipparchovy,
když Střelec večer zapadal, čilí když zmizel v září
sluneční, pilně tehdy Se honilo :! lovilo, a proto Apollo
vykazuje svému synu liaěthonlovijemuT. nemohl Miep—řiti
prosby, aby směl řídili sluneční vůz, též cestu, kde
na nebi září Střelec: »Arcus Haemoniosa

Čís. so. Štíť Sobieského. něm. Sobieski's Schild,
lat. Scutum Sobiescianurn. též Firmnmentum Sobies-
cinnum.

Na oslavu Chrabrého kriile polského, Jnnu Sobi-
eského, jenž s válečným lidem Vídeň od svcí-cpých
Turků obleženou a již již podléhající osvobodil (liší-531:
hvězdář Hevcl v díle svém vydaném r. mam volil
jméno a místo na nebi v Mléčné dráze hned nade
Střelcem. Ono místo v Mléčné dráze má skutečně
podobu štítu: jsou tam hvězdy čtvrté a páté velikosti.
Sobiesky vykonal svým hrdinslvim dílo bohntýrské,
jež zachránilo nejen Vídeň, nýbrž celou západní Evropu
od pohromy ji hrozící. Zasluhuje proto býti zvěčněnu
na nebi mezi hvězdami.

48

Čís. 61. Býk Poníamvského, něm. der Poninmw-
nki'sche Slier, lat. Tnurus Poniatowii. — Opat Vilenský
Pnczobut vydal své pozorování na nebi r. 1777. Po-
nčvadž skupine hvězdiček nad pravým ramenem hado-
nuše poněkud má podobu skupiny na hlavě Býku.
(Hyady), volil jméno býka a na počest svého příznivce,
polského krále Stanislava Poniatowského,
x-nlil jméno Poniatovski. Bylo( to zvykem astronomů
koncem osmnáctého století odměňovali mocné příznivce
krajinami — na nebi.

Čís. 62. Orel, něm. der Adler, l_at. Aquila, u bás-
níků též Jovis ales, Armiger Joris -= zbrojnoš Jovišův,
řecky Aětos, arab. El-nesr el ulit = orel letící, Amir.
I'lvězdy: :: Atair, ,') Alšaín, 7 Tnrazed, šDeneb Okůb.

Orel byl sluhou Jovišův: přinášel malému ještě
.Im-išovi nápoj do sluje, kdož dle vůle svého otce
Sním—na měl zahynouti: íe'ž nesl .lovišovi zbraň v boji
proti obrům. Unesl G a a ymedn, jemuž při hostinách
lvnhův byl svěřen ouřad číšníka. Za tyto služby byl
vznesen mezi hvězdy.

Čís. 63. Antlnous. Jméno i souhvězdí zavedeno
dánským hvězdářem jménem Tycho de Brahe, jenž
7. několika hvězd náležejících k Orlu sestavil souhvězdí
a nazval jménem Anlinous. Znázorňuje mládence, jenž
drží pravicí luk. levici šíp. Jeho místo jest pod Orlem.
A ntinous byl pážetem císaře Hadriana.

Čís. 64.„Lýra, něm. die Leier = lat. Lyra, u Rí-
manů též: Fides. Fidipula, řecky Chelys == želva »-
nrnb. El-selibůk nebo El-šelitlk. — Hvězdy: a Wege.
I.llcida Lyme - Hvězda u s hvězdami člvrtě velikosti
u a tvoří malý trojúhelník a sluly arabským kočovníkům
Athén; hyly to lři kameny; na něž v poli stavěl se
kotel. 48 Seliak, y Sulafat.

44

Athenský spisovatel Apollodor vypnu-uje. Božské
dítě Merkur vykradl se :. kolébky, Odehnal voly
Apollinovi a dva 7. nich zabil. Zlazy jejich napnul na
štít želví, jejž našel na arbadské hoře Kyllene a sho-
tovil takto hudební nástroj slrunový. jenž vydával tak
líbezný zvuk, že Apollo krádež Merkurovi odpustil a
vyžádal si od něho ten nástroj, lýru, daroval ji pak
slavnému pěvci Orfeovi, po jehož smrti Merkur, dle
iinýcl) bájí sám Jupiter lýru. umístil mezi hvěadami.

Čís. 65. Labuť. něm. dur Schwan, lat. Cygnus,
Olor, řecky Ornis. Kýknos, amb. El-uiir nebo obyčejně
El-dqdšadše = kvočnn. a Deneb = ocas. ,':Albiuro.
7 Sadr == prsa. d' (Linah.

Cygnus. lx'ýknos byl syn Slhunelův; Apollo
obdařil jej líbezným hlasem. Nad úmním svého přítele
Iřněthonta tak se rmoutll, že bohové proměnili jej v Iabut
— (cygnus v lm. jest mužského mám, a Apollo jej
vřadild mezi hvězdy.

(.“ís. 66. Liška s Husou, něm. dcr Fuchs mil der
Gans. lat. Vulpecula. Zavedeno od hvézdáře Hcvcla
na místě pod Labutí, jež staří astronomové blíže nc-
označovali. Hex-uloví možná že byli vzorem šakalové
= canes lupi uurci mr- lišky zlaté barvy. kteréž Am-
bové_znali na nebi.

Cls. 67. Šíp, něm. der l'leil. m. Sagilm, řecky
Toxun. arab. EI-snhm. — Jest to buď onen šíp, jímž
Herkules zranil .lunonu a I'lulona aneb len. jímž zabil
orla. kterýž kloval ja'tm Prmnctheovi za to. že uzmul
bohům oheň a snesl jej lidem na zem. začež byl na
rozkaz Zevův od Vulkmm na hoře Kaukasus ku skále
přikovan. Co jater dravcc- za den mu vykloval, za noc
opčl narostla, a muka opět počala se dnem. — Tak
proti vůli mocných věru nevyplácí se býli dobrodincem
lidstva.

45

Čís. 68. Dolůn, něm. der Delphin. lm. Dulphinus.
mnh. El-dulrln. — Hvézdv: “ Svalnzin. ?: Romana.-v. —
.lrin n. potomek Tritom'n', mořského boha. hrál na
hře a pěl tak mistrně. že při zúmdcch nu hmm-ě
Mcilii dobyl si nejen slávy nýbrž i mnoho zluta. Když
plavil se domů, lodníci chtěli jej : pokladů oloupili a
uvrci do moře. l vyžádal si, než umře, aby směl
mpěti. Dellln krásným zpěvem přiltiknný vzal pěvce
Arinnn. když se byl vrhl do moře, na záda a přenesl
jej do Korintu. Za to byl od Zeva odměněn. že stkví
w mezi hvězdami. »- Dle jiných jest to dellln. v něhož
bůh Apollo se proměnil. když bohové před Giganty
— obry — utíkali do Egypta. —! Jiní opět bájí, Že
lest to Triton. syn bnha moři, Neptuna.

Čís. 69. Hříbě, Koníček. něm. das Fllllen. lm.
quuuleus. Equus minor, amb. l'x'it'n elán-res = hi'ibč.
Just pozdějšího původu. Aral. jenž popsal nebe ve
způsUbu básně. a Cicero toho souhvězdí neznají. Vy-
ohrazuje se jen jako hlava a krk hříběte mezi Dellincm
n hlavou Pegasu.

Čís. 70. Kozorožec, něm. der Steinbock, lat.
t'npricornus nebo (fapcr = kozel — řecky Aigokeros,
umb. El-dšedi, — u Emtoslhena má jméno Pan: on
praví, že to souhvězdí má podobu Aigipuna. pastýře
koz. jehož hlava má rohy. část ostatni jest zvířecí.
Hvězdy: a Dšabc ]. :: l)šahe II. 7 Deneb Algedi ==
ocas kozlův — ? sluje též Dšůbih — obě pak v. i „'$
Sa'd el dšúbih == šťastné hvězdy zabijejícího _ obě—
tujíciho, snad že kočovným Arabům, kteří slavili slu-
nečni východ toho souhvězdí — totiž dobu. kdy to
souhvězdí před východem Slunce na nebi vycházelo,'
a přinášeli oběti. Tehdy slunovrat byl v souhvězdí
Kozorožce', nyní jest ve Střelci.

m

Když. bohové utíkali před T ytoncm, proměňovali
se v různé podoby, aby snáze unikli. Bůh Pan vzal
na sebe shora podobu kozla. zes—pod podobu ryby.
I'řctvořehi to Zevovi tak se líbilo. že obraz jeho vradil
mezi hvězdy. — I.)le jiné báje Koprikornus, syn Aigi-
prmův. byl na hoře jménem Ida na ostrově Kretě se
Zevem zároveň vychován a pomáhal Zevovi statné
v boji s Titany, jež přinutil k útěku silným hlasem
svého rohu, na nějž t'roubil. Proto nyní za odměnu
stkví se na nebi mezi hvězdami.

Kozorožec rád po skalách lozí nahoru dolů. Za
starých dob Slunce v lx'ozorožci na nebi ve své dráze
nejníž sestoupívši, počalo opět stoupntí. Nemohl tedy
kozorožec toho býti symbolem, odznakem?

Čís. 71. Drobnohled, něm. das Mikroskop. —
Zavedena od La Chille, jímž zvěčnil nástroj předůlcžitý,
jenž nám otvírá obzor směrem v malost, jak to činí
dalekohled směrem ve velikost a vzdálenost. Ba ted'
i ve hvězdářství drobnohledu se užíva. a hledají se jim
tělesa nebeská! totiž stopy asteroid na fotogmtlcké
desce, jež zachytí asteroidy tak málo světlé, že jich
ani dalekohled news-tíhne. I.n Caille neznal ještě toho
způsobu hlednti hvězdy v prostoru světovém.

Čís. 72. Kefeus. amb. Kifaus nebo Fiknus. —
Viz čís. 2.

Čis. 73. Ještěrka, něm. dic Eiduchse. Laccrta.
Nepatrné souhvězdí jako mezi zvířaty jeho originál,
pod Keíeem, skládající se ze tří hvězd čtvrté velikosti,
n ostatních menších. Udrželo se. ač někteří slavní
kvězdářové byli pro to, aby se ho pominulo.

Čís. 74. Čest Bedřichova, něm. Friedrichschre,
lat. Honores Friderici. — l-lvězdúř Bode r. 1787 utvořit
to souhvězdí % hvězdiček náležejících souhvězdím

47
Andromedy. Kaasiopee. Kefea a Labutě: ku cti pru-
kkého krále Bedřicha pojmenoval je dle svého příznivce.
Ubruz skládal se ?. koruny. pod ní mcč. pak péro 3 mm-
Icsl olivová. Tím licholil Bedřichovi jnkn hrdinskému
králi. učenci a knížeti míru. — Nám Rakušanům jeho
»mírumilovnostc byla poněkud drahou.

Čís. 75. Pegasus. U řeckých astronomů Hippos
— kůň, í Int. básníci jej jmenují Equus, též Sonipes.
Cnrnipes, nrub. El feres el-a'dam = větší kůň. —
Jméno Pegasus povstalo teprv později přetvořením
hájemi. Hvězdv (a, jež náleží Andromedě Sirrah)
a Mai-kab, (? Šeut w lopatka, 7 Algenih ,. křídlo.
a l'lníf. :; Maiar, Ž.“ Saul cl-homnin. — Hvězdy: a Andro-
medy. a. ,9; y Pegasu tvoří líchoběžník na nebi, a sku-
pcní to jmenuje se Stůl. něm. Tísch, řecky Trapezn.

Pegasus zobrazuje se jako okřídlený kůň. Povslal
z krve Medusy, již zahubil Perseus. Bellerol'on, syn
Ciluukův, jej zkrotil a užil ho v boji proti strašné obludč
(.“himaiře. Vílčzslvím jsa opojcn povznesl se na svém
ni'i do výše, chtěje přiblížili se k sídlu bohův. Bohové
nikdy nedopouštěli. aby člověk meze sobě určené
překročil. proto poslali velkého šršně. jehož bod-
nutím kůň se splnšil a. shodil se sebe Bcllgmfonm.
Pak prý Pegasus mipcn jsa žízní, na hoře Helikonu
kopytem ze země vydupal pramen — Hippokréne.
I—lippokrénc značil pramen nadšení básnického. Pega-
sus konečně byl vzal na nebe.

Čís. 76. Vodnář. něm. der Wnsserumnn. latinsky
Aquarius, řecky Hydrochóos, amb. Sakhib el-má :.
vodu lející, nebo El-delv = vědérce. — Hvězdy:
a též o Sndnlmelik =- štastná hvězda. 3:15 Sadalsud

— Sa'd el su'ůd == štastná hvězda všeho světa. —
*.' Sadachbin = štastná hvězda stanů. — 8 Skad nebo
Sůk — píšlěl na noze. — .? Ancha, :: Situla s džbán.

48

Dle některých bájepiscův souhvězdí to značí
Deukalinna, potomka Prometheoya. Se svou manželkou
Pyrrhou žil ctnostné i za všeobecné zvrhlosti lidského
pokolení. Proto když sesluna byla potopa na hříšný
svět. Deukalion a Pyrrha sami byli zachráněni. Aby
povstalo pokolení nové. házeli na rozkaz bohyně
Themídy, dcery Země, zahaliyše si hlavy, knmením
za sebe, z něhož vzešlo nové pokolení lidské. Dle
Ovida však Vodná-': jest číšník bohův Ganimedes.

Starým národům východu Vodnář mohl byti
symbolem doby mokré. deštivé. jež rmstdvnln, když.
Slunce probíhalo timto souhvězdím. antávaln lež doba
lovení ryb. když Slunce postoupilo dále v souhvězdí Ryby.

Čis. 77. Ryba jižní, něm. der sůdliche Fisch —
Piscis nustmlis — a Pomalhnul == hubu ryby (Vide č.4.).

lsis, slavné božslvo egyptské. jehož poctu. přešla
později i do Řecka a Rima. byla prý zachráněna
rybou, jsouc —v nebezpečí zahynouti ve vodě. si proto
nyni slkví se nn nebi, jnkož i její sourozenec —
Ryba severni.

Čís. 78. Jeřáb, jest původu mladšího. u nás již
léměi: neviditelný pod Jižní Rybou.

(jis. 79. Balon. der Luílballon. latinsky Globus
něrosmticus. La Lande zvěčnil tím souhvězdím vy-
nález bratří Mondolňerů. kteří roku 1783 naplnili
balon vyhřatým vzduchem: Charles naplnil balon
vodíkem, l-lkrút lehčím než vzduch, a vystoupil dne
[. prosince roku I783 s Tuilerii v Paříži pře—I očima
celého dvoru a několika siy tisíc diváků v lodici
balonu do vzduchu. Angiičan Green plnil balony
svítiplynum. — Skutek prvnich včtroplm'ců znsloužil
si býti oslaven; proto i balon má své místo na nebi,
ovšem ski-cené.

Znajíce jednotlivá souhvězdí — aspoň hlavní — a nejsvětlejší hvězdy, máme již dobrý začátek a vydatnou oporu pro další postup. Nejdůležitějším jest pak poznali ekliptiku čili zdánlivou dráhu Slunce na nebi. vlastně dráhu Země kolem Slunce v prostoru světovém. Měsíc by nám mohl býti vůdcem po ekliptice, pokud jest označena zhruba celými souhvězdími, jimiž prochází. Nám však běží o to, abychom poznali zevrubněji onu dráhu jako linii. K tomu cíli vyhledáme si na nebi hvězdy, jimiž ta linie přesněji jest označena; postačí, jsou-li právě aspoň dvě takové
hvězdy nad obzorem: linie těmi hvězdami vedená označí nám ekliptiku.

## Ekliptika.

**Ekliptika** dosti blízko označena jest hvězdami: μ a δ Blíženců, β Raka, α Lva, β Panny, asi čtyři zdánlivé průměry Slunce nad Spikou — α Panny, ne celý zdánlivý průměr Slunce pod α Vah, mezi β a ω Štíra, asi tři průměry sluneční nad ζ Hadonoše, středem mezi λ a μ Střelce, ne celé tři průměry pod π Střelce, právě

4

50
3 Kozorožce, pak něco výše podél hvězd t, y, :! Kozo-
rožce — nad hvězdami )., cp Vodnáře — asi prostředkem
mezi 1 Pegasu a : Velryby, konečně něco nad pro-
středkem mezi 1) v Kuřútách a a v Hyaditch — totiž mezi
Alkionou a Aldebarnnem. — Z těchto hvězd každého
večera nejméně dvě až tři jsou nad obzorem, a možno
tedy lehce ekliptiku vyznačiti. Tím pro pozorování
pouhým okem získali jsme mnoho. Ekliptika jest
vlastně dráhu Země; Měsíc otáčí se kolem Země, po-
stupuje se Zemí kolem Slunce a. tedy opisuje kolem
této linie — ekliptiky — jakoby šroubovitou linii: my
obraz Měsíce promítáme na nebo: proto budeme jej
sputřovati brzy nnd ckliptikou — nejvýše 5“ 8'],'. pak
bude blížiti se ekliptice, překročí ji, půjde vždy níže,
až sejde nejníže 5“ 8'„' a bude opět stoupati.

l'lný Měsíc můžeme míti, jen“ když jest od Slunce
půl kruhu vzdálen: proto nemůže býti v létě, když
je na úplňku, vysoko na nebi. poněvadž tehdy Slunce
nám jest v té části ekliptiky. jež vystupuje vysoko na
nebi, a Měsíc tedy na protější straně. jež nad obzor
vystupuje nejméně. * Opak toho nastává v zimě.
Dvanáctero souhvězdí. jimiž Měsíc prochází, tvoří zví-
řetnik, tak že Měsíc potřebuje málem půl třetího duc,
aby prošel jedním souhvězím. Slunce zdánlivě zn. rok
těmi souhvězdími projde a prodlévá celkem měsíc
v jednom souhvězdí.

Ekliptika rozdělena jest na 360% Od souhvězdí
k souhvězdí Zvířetníku počítá se 30". Každé souhvězdí
má svou značku. Rozdělení to bylo stanoveno od
místa, kde v ekliptice jest Slunce, když na jaře deni
noc jsou stejny — rovnodennost jarní. Více než
přede dvěma tisíci léty bylo tehdy Slunce na počátku
souhvězdí Skapce: tehdy znameni souhvězdí a sou-

bl
hvězdí samo se shodovalo. [ nyní počítá se od budu
v ekliptice, v němž jest Slunce za jarní rovnodennosti,
ale bod ten již jest posunut téměř o tři čtvrtky sou—
hvězdí. Značky souhvězdí byly podrženy a posunovziny
zzír0veň s počátečným bodem počtu — bodem rovn o-
d e a n osti — tak že nyní znamení, na př. Býka, jest
v první čtvrtině souhvězdí Skopce, znamení Skapce
v první čtvrtině souhvězdí Ryb atd. Nyní již znamení
souhvězdí a souhvězdí samo se neshodují. Ta znamení
fsou: “(“ Skopcc; O"—30", ó Býk, 30'—60", ]] Blí-
ženci, 60'—90', © Rak, 90'—120", 23 Lev, l20“—l.ě0',
np Panna,'160“—180 , á—Váhy, 180"—210", m Slír,
210'—240“, ,? Střelec, 240'—270"_. 2, Kozorožec,
270'—300", m Vodnář, 300'—330", )(Ryby, MIV—3600.
Znamení to jest celkem na tom místě, kde zdán-
livě jest Slunce asi 21. den v měsíci. Cteme-li v ka-
lendáři, že „Slunce nebo Měsíc v ten a ten den vchází
ve znamení Skopcc Y, tož nesmíme mysliti, že jest
pak i v souhvězdí Skapce; nyní jest to znamení
v souhvězdí Ryb: podobně u jiných znamení. V ka-
lendáři na rok 1906 čteme: Slunce vstupuje ve zna-
mení Raka (8) dne 22. června o 3 hod. 57 min.;
to znamená, že Slunce v ekliptice ode dne rovno-
dennosti jarní, t. j. ode dne. 21. března dospělo až
ke znamení Raka, t. j. po 90“ (v Blížencích).
Dráha Měsíce a oběinlc.
Rovinn, v níž leží dráha Měsíce, tvoří s rovinou,
v níž leží dráha Země — zdánlivě dráha Slunce —
úhel 5“ S'lg'; proto dráha Měsíce, již promítáme na
nebe, brzy vystupuje nad ekliptiku, brzy pod ni se-
48

52

stupuje. Ony body, ve kterých ty dráhy se protínají,
jmenuji se uzly a sice uzel vzestupný, na tom
místě, kde dráhu Měsíce vychází nad ekliptiku, a u zel
sestupný tam, kde dráhu Měsíce schází pod eklip—
tiku, 181.1o od uzlu vzestupného. Zatmění Měsíce můžeme
míti jen když střed Slunce. střed Země a střed Měsíce
jsou v jedné přímce aneb téměř v jedné přímce, a sice
když Země jest mezi Sluncem a Měsícem, tak že
stín Země padá na Měsíc: střed Měsíce jest zúplna
neb téměř.zúplnu v rovině zemské dráhy. To jest
možno jen za úplňku. Při zatmění Slunce střetlh'lunce,
Měsíce i Země opět musí býti v jedné přímce nebo
téměř v jedné přímce, Měsíc jest mezi Zemi a Sluncem,
tak že stín Měsíce padá na Zemi, stí-etl Měsíce jest
v ekliptice nebo téměř v ekliptice, t. j. Měsíc prO'
chází ekliptikuu.

Pozorujeme-li bedlivě Měsíc, můžeme aspoň pří-
blížnč určití od oka. kdy bude procházeti ekliptikou:
bude-li to za úplňku, může nastati zatmění Měsíce,
bude-li to v nový Měsíc, může nastuti zatmění
Slunce. Cvíkem nebudeme i míry. oč Měsíc denně
postoupí v před “ oč vystoupí nebo sestoupí, a tim
budeme moci i bez nástrojů poznali, kdy bude zatmění.
Jakž jinak byli by to poznali hvězdáří starověku ? Thules
již uměl předpovítlnti zatmění.

Ještě možno i vypozorovatí, že Měsíc při každém
následujícím oběhu prochází ekliptikou v bodech, jež
se posouvají nn zad, totiž v opačném směru než Měsíc
postupuje na své dráze od západu k východu; do
roka to činí 19'340. — Doba. již Měsíc potřebuje, aby
vykonal dráhu kolem Země tak, aby v_t'jdn od některé
stálice, opět k ní přišel, jmenuje se měsíc side-
ric k ý a trvá 27-32167 dne čili 27 d 7 h 43 m 11'5 vt.
Uzel vzestupný i sestupný posouvá se opačným

58
unčrem — jako by přicházeli naproti Měsíci: dobu. jež
uplyne, aby Měsíc, vyjdu :; úhlu vzestupného, opět
k němu přišel, sluje měsíc d ručí (Dmehenmonat) “a
trvá 27'21222 dne čili 27d 5 h bm 35'8 vt. Doba od
uplnku k úplňku, nebo od nového Měsíce )( novému
Měsíci. sluje m ěsic sy a od ic ký a trvá 29'53059 dne
Uli 29 d 12 h 44 m 2'9 vt.: jest delši měsíce side-
t'lckého čili pravého oběhu Měsíce kolem Země. po-
něvadž Slunce (vlastně Země) za tu dobu, než Měsíc
x ykuná cestu kolem Země, na své dráze postoupí dále
h-mčř o celé souhvězdí zvířetnikn. Měsíc musi Slunce
ještě jakoky dohánětí a potřebuje více času: proto
měsíc synodický jest delší. V praktickém životě po-
(- mime obyčejně tyto měsíce. — Aby Měsíc vykonnl dráhu
tukovou. aby vyjde od bodu jarní rovnodennosti. opět
k němu přišel, potřebuje 27'32158 dne čili ?? d 7 h
ll m 4'7 vt.. adobe ta sluje měsíc tropický. Jest
(a'll'lčl' o 7 vteřin kratší pravého oběhu, měsíce side-
llt'líéhO, poněvadž bod rovnodennosti jarní (i podzimní)
pusuuvá se zpět — přichází naproti — ovšem ročně
jen o 50-25 vteřin.

Co zde řečeno bylo o dobách oběhu Měsíce, to
plnti o dobách oběhů planet: rozeznává se oběh
niderický, od stálice (ve dráze oběžnice, promítnuté
nn nebe) k téže stálici. tropický, od bodu jarního opět
k tomu bodu a synodický, doba, již oběžnice potře-
l-uje, aby přišla ke Slunci opět do stejného postavení,
ll-kněme třeba, aby se Sluncem zároveň vycházela.

Dráhu Měsíce kolem Země není kruh, nýbrž
elipsa, v jejíž jednom ohnisku jest Země. Proto při
wsd-m oběhu Měsíc přichází jednou Zemi nejblíž, jest
t přízemí, pierigaeum, tl. jednou nejdál, jest v od-
zumí, apogaeum. Doba, již Měsíc potřebuje, aby

64
z perigaea přišel opět (lo perigaea, sluje měsíc ano-
mnlistický a trvá 27'5647 dne čili 27 (1 131118 m
371 vt. — Ovšem tak nepatrných rozdílů pouhým
okem vypozorovmi nelze.

Dráhy občžnk.

Dráhy oběžnic už na Merkura mají sklon k eklip-
tice ještě menší. Znajice ekliptiku vime, kde mohou
býti viděny: Merkur nikdy daleko od Slunce. u to
buď po západě nebo před východem Slunce. ohyčejnč
se mysli, že Merkuru jen stěží jest viděti a pruvi se
též o něm, že jest vzácným (řídkým) hostem na nebi.
Avšak dlužno poznamenati, že není příliš nesnadno
viděti ho i pouhým okem. tož ještě snáze operním
kukálkem večer po západu Slunce, když totiž jest
v největší (zdánlivé) vzdálenosti od něho na východ,
a ráno před východem Slunce. rozumí se, když jest
od Slunce co nejvíce zdánlivě vzdálen. Jest k tomu
potřebí, aby obloha byla jasná a vzduch čistý.

Venuše může býti hvězdou bud' večerní nebo
ranní čili dennici. Ty ostatní málo jen mohou vystou-
piti nad ekliptiku nebo sejíti pod ni, takže jimi. pokud
jsou nad obzorem, ukliplika přibližně jest vyznnčenn.
Známe-Ii kde jest oběžnice v určitý čas, a dobu jejího
oběhu, můžeme i určili. kde bude v jiný určitý čas,
kdy bude viditelnn večer a kdy ráno.

Pozorujeme pohyb oběžnic na dráze v před i
zdánlivý v zad a ty různé kliky, jež zdánlivě opisují.
Pozorujeme konečně zdánlivý pohyb Slunce v ekliptice
čili skutečný pohyb Země. Vždy s jiného bodu v eklip-
tice promítáme Sluncem nebe: souhvězdíekliptiky. která

b';
v nějaký čas viděli jsme znpadnti po západu Slunce.
mtm čím dál tím víc v paprscích slunečních. až zmizi
uplne na nějakou dobu; pak vidime je vycházeli před
východem Slunce, až se tak za rok vystřídají všechnu
untilwi—zdi, jež vůbec vycházejí a zapalují. Zmizí-li
thin! v paprscích slunečních souhvězdí na př. Blíženců.
hledíme se Země směrem od souhvězdí Střelce ke
Hluuei a k Blíženet'tm. Tak určili jižw'e starověku délku
tuku dnsti přesně na 365% dne. — Již z toho jest po-
muti. jak důležim jest poZnuti ekliptiku na nebi.
Soustava ekllptlkálni.
ltoztlčlimeoli vzdálenost hvězdy ': od 3 v Draku
ltll pět stejných dílů u odpočteme tukové díly dva od
hvězdy ';, máme pól ekliptiky. Myslíme-li si na nebi
ml tnllnlo pólu vedené kruhy k bodům dělícím eklip-
tiktt na 360 dílů a kruhy rovnaběžnék ekliptice těmi
llmly. kteréž dělí část kruhu od pólu k ekliptice na 900
ntejnýeh dílů, obdržíme souřadnice ekliptikální soustavy
“ mužeme dle ní určovati posiCe hvězd. Vedeme totiž
lwčzduu. jejiž místo čili posici určujeme, kruh od pólu
ekliptiky až k ekliptice snmé: vzdálenost průsečíku
ml hodu jarního jest délkou hvězdy, a počet stupňů
mt ekliptiky už k hvězdě jest šířkou hvězdy. Délky
pus-imji se už do 3609. šířky od ekliptiky k 'pólu do
lllv“ .'l jsou Severni, pod ekliptikou jsou jižní. Tak
l'ln yř. hvězda y Draka má 260 54' délky a 74“ 57'
huturtlí šířky. (Viz mapu). Však těchto souřadnic
v praxi méně se užívá: v kalendář-ich astronomických
jich obyčejně nemáme.

56

## Rovník.

Druhý důležitý kruh na nebi jest rovník; ten dosti dobře vyznačen jest hvězdami: δ Oriona, nejvyšší ze tří hvězd v pásu, pak asi 10 zdánlivých průměrů Slunce pod Prokyonem, pod polovicí vzdálenosti hvězdy α Velkého Lva od hvězdy α Hydry, hvězdy η, γ a ξ Panny jsou téměř v rovníku; nad μ Hada a δ Hadonoše. asi šest průměrů Slunce nad η Hadonoše, mezi η a ζ Orla, podle α a ζ Vodnáře, čtyři průměry Slunce pod α Ryb, šest pod α Velryby, a šest nad ν Eridanu.

I z těchto hvězd každou jasnou noc jest tolik viditelno, že rovník lze vyznačiti. Sice snáze se to řekne než ve skutečnosti provede, ale obtíže nejsou takové, aby se nedaly překonati. Viz mapu.

Znajíce tyto dva kruhy na nebi, vidíme zrovna sklon rovníku a ekliptiky, jich vzdálenost od sebe na každém místě. Sklon ten můžeme viděti i tak, že pozorujeme vzdálenost Polárky od pólu ekliptiky (asi v druhé pětině vzdálenosti hvězdy ζ a δ v Draku počítaje od ζ), ty póly jsou v kolmicích vztyčených ve středech rovníku a ekliptiky; ty přímky svírají spolu týž úhel, jako roviny, na nichž stojí kolmo. — Odchylka okem měřená buď od znamení Střelce ʒ k rovníku nebo při souhvězdí Oriona. — od jeho pasu k hvězdě ζ Býka pod souhvězdím Vozky — zdá se býti větší než měřená na hoře od pólu rovníku k pólu ekliptiky — ale ta věc má se tak, jako dříve připomenutý zjev, že hvězdy v souhvězdí tím od sebe zdají se býti vzdálenější, čim níže stojí nad obzorem.

Kde ekliptika s rovníkem se protíná, jsou uzly, a v nich jest bod jarní a podzimní rovnodennosti; onen jest v souhvězdí Ryb a sice, myslíme-li si vzdálenost

57

hvezdy tu v Rybách od hvězdy : Velryby rozdělenu
nu pět dílů, tedy v druhé pětině počítaje od hvězdy
m. nebo: hruba, naměříme-li vzdálenost hvězdy a Andro—
medy a y Pegasu ve spojnici těchto hvězd ještě pod
7 Pegasa, _— lemo leží přibližně v polovici vzdálenosti
hvězdy a od v; v Panně.

Vedeme—li oblouk od Polárky podle hvězdy ? Kn-
sinpee, a Andromedy. tož prochází tež bodem jsu-ni
rnvnmlennosli, a na druhé straně od Poldrky usi sli-edem
mezi předními koly Velkého Vozu, pak prochází bodem
podzimnírovnodennosti,jes! to kolur rovnodennosti.
V bodě jarním a podzimním prolínají se tedy ekliptikn,
rovník a kolur rovnodennosti. — Kruh vedený od
l'olnrky kolmo na kolur rovnodennosti, podle hvězd
;) Vozky. 7 Draka a y Střelce, jest kolur sluno-
vratů. V průsečíku jeho 5 ekliptikou v Blížencích
:: znamení Raka = '$ — Slunce dostupuje v poledne
nejvýše nn nebi dne 21. června, a v prusečíku s eklip—
liknu v souhvězdí Střelce u znamení Kozorožce =
7, - sestoupí nejníže. Kruhy rovnoběžná s.rovníkem.
a sice kruh vedený bodem se znamením Raka -= 63: —
sluje o b rn ! a ik Ra ka, kruh vedený bodem. jenž jest
označen znamením Kozorožce .= 7, „_ sluje o bral a ik
Kozorožce. Kruh rovnoběžný s rovníkem, vedený
pólem ekliptiky, gluje kruh polární. Kruh vedený
hvězdou :; Velkého Vozu prochází souhvězdími, kteniž
vreholíce nahoře jsou v nnšich krajinách nám nad
hlavou. ,—

Soustava rovníková.

Rovník dělíme na 360“ nebo 24 hodin, tak že
hodina obnáší 150 stupňů, čtyři minuty činí jeden stupeň:
počítá se od bodu jarního směrem k východu do 24

68

hodin nebo 360“ — t. ]. 'rektaseen'se,'aseensio
recta (Alta), přímý výstup. Kruh vedený od Polárkyt
k některému bodu rovníku — třeba k bodu jarnímu —
dělíme na 900 a počítáme od rovníku k pólu: to jest
_d e-k li a nc e — (D. 8) odchylka — počítá-li se k severu,
jest'positivní, se verní, počítá-li se k jihu, jest-nega-
tivni. jižní.

Obyčejně dle této soustavy udava se místo,.čili
posice hvězd tímto způsobem: myslíme si hvězdou,.
jejiž posici určujeme.a Polúrkou vedený kruh na nebi
až k rovníku: Počet hodin nebo stupňův od bodu
jarního až k 'prusečíku toho kruhu s rovníkem jest
rektascense hvězdy, počet stupňů nit-tom kruhu
od rovníku až k hvězdě, jest její de klinace. — Viz
mapu. — Na př. hvězda a Býka. Aldebaran, má 4 h 30 m,-
čili 670 30“ reklascense, a 16“ 18' severní nebo posi-
tivni deklinace (+ 16018'), hvězda Fomnlhaut mii
22 h 52 m, čili 343“ rektascense a 300 24' jižní čili
negativni deklinace, (— 300 24'). Tím posice hvězdy
jest určena. Dlužno připomenouti,. že musí býti udúno,
pro který čas to určeni platí. Neboť jarní bod, od něhož
se počítá. posunuje se směrem k západu, ač jen 50'25“
ročně, stálice však (už na vlastní pohyb proti tomuto
nepatrný) zůstávají na svých místech, tim pak i posice
hvězd se mění : rektascense roste. i deklinace se,
pozměňuje. Jest to posunovziní měřítka po tom. co má
býti měřeno. Za čtyřicet let rektescense přibude o 335
minuty obloukové čili o 2 minuty a 14 vteřin časových:
jestliže zapamatovali jsme si rektascense některých
hvězd, za čtyřicet let ve skutečnosti Jsou o 33'6' větši;
nedbáme-li toho a udávúmc jaké byly před 40 lety,
málo udúvrime. Však ta chyba je'st nepatrná, kde ne-
běží 0 přesnost 'rhnthematickou, a může jí býti pominuto.

59

Užijme ihned prakticky souřadnic rovníkových na
určení času střed ního dle hvězd a lu bez všelikých
miStrojů.

## Jak určuje se čas vůbec.

Abychom určili čas sluneční dle hvězd, musili bychom věděti na každý den rektascensi Slunce, kteráž jest zaznamenána v astronomickém kalendáři; též musili bychom znáti rektascensi hvězdy, která právě vrcholí. Ve 24 hodinách (vlastně ve 23 h 56 m 313 vt.) nebe s hvězdami zdánlivé otočí se kolem osy, jdoucí od Polárky k našemu stanovisku. Kdy vrcholí Slunce, jest pravé poledne sluneční. Kolik hodin některá hvězda nahoře vrcholí později než Slunce, tolik hodin jest po poledni; kolik hodin vrcholí dole později než Slunce, tolik hodin jest po půlnoci: jest nám jen odečísti od rektascense hvězdy rektascensi Slunce toho dne. O kolik hodin hvězda nahoře vrcholí dříve než Slunce, tolik hodin jest před polednem, a o kolik hodin hvězdu dole vrcholí dříve než Slunce, tolik hodin jest před půlnocí: odečítáme od rektascense Slunce toho dne rektascensi hvězdy. Tak vyzvíme, kolik jest hodin času slunečního. Zevrubněji rozvedeno v dílku »Nebeské hodiny«.

Však v občanském životě řídíme se tak zvaným časem středním, jejž nám ukazují naše hodiny: chceme tedy po hvězdách raději poznati, kolik jest hodin času středního a to bez měřidel, pouze od oka; proto určení to nebude přesné, nýbrž jen přibližné. K tomu postačí mysliti si místo Slunce v ekliptice, Slunce jiné v rovníku, jež vykoná do roka dráhu kolem pohybujíc se stejnoměrně v rovníku. Kdy toto Slunce vrcholí, jest poledne času středního.

60

Smyšlené toto Slunce čtyřikrát do roka zároveň vrcholí se Sluncem skutečným a sice: dne 16. dubna, 15. června, 1. září. 25. prosince. Od 16. dubna hodiny ukazují v pravé poledne vždy méně než dobře sestrojené hodiny sluneční, už dne 15. května jdou o 3 m 15 vt. později: potom odchylky ubývá až do 15. června: odtud hodiny předbíhají Slunce, až dne 27. července jdou o 6 m 17 vt. napřed; odtud odchylky opět ubývá do. 1. září; pak hodiny se opozdívají až do 3. listopadu, kdež v pravé poledne ukazují 11 h 43 m 38.4 vt., t.j. o 16 m 21.6 vt. méně. Pak odchylky ubývá do 25. prosince. Potom hodiny předbíhají Slunce, až dne 11. února jdou o 14 m 271. vt. napřed, načež odchylky ubývá až do 16. dubna.

**Rovnice časová** nám udává, oč dobré hodiny v poledne ukazují jinak než správné hodiny sluneční. Dobré hodiny jdou stejnoměrně a nemohou jíti zároveň se slunečními hodinami, poněvadž Slunce ve své zdánlivé dráze nejdu stejnoměrně. Vlastně Země kolem Slunce neobíhá s rychlostí stejnou.

Na drahách počítá se dle času středoevropského: dvanáct hodin v poledne, když »střední Slunce« v rovníku vrcholí čili prochází poledníkem 15° v. od Greenwiche. Oč místo které leží východněji (západněji) než 15°, o tolik hodiny nádražní jdou později (napřed) než hodiny ukazující střední čas místní.

Doba, ve které táž stálice dvakrát po sobě vrcholí, doba to pravého otočení se Země kolem osy, 23 h 56 m 3.5 vt., jmenuje se den hvězdný; doba, v níž Slunce dvakrát po sobě vrcholí, sluje den sluneční (24 h). Doba oběhu Země kolem Slunce má o jeden hvězdný den více než dnů slunečních. Hvězdáři čítají dní od vrcholení bodu jarního; od vrcholení tohoto bodu k vrcholeni též sluje dnem hvězdným, jest však o nepatrnou částku vteřiny kratší než pravý den hvězdný.

61

protože bod rovnodennosti postupuje směrem od východu
k západu, jakoby Slunci šel vstříc. Počet hodin. minut
„ vteřin od vrcholeníbodu jarního sluje čas hvězdný.
Když vrcholí právě některá hvězda, jeji rektasCense
znnmená již čas hvězdný. Když ku př. dne 21. února
1 Lvu — Regulus — vrcholí nahoře, jest 10 h 3 m
:: vt. času hvězdného, ale půlnoc času středního,
poněvadž AR Regula jest 10 h 3 m 3 vt.

Země vykoná dráhu kolem Slunce v úhlu 3600
téměř za 365'256 dne: my ten pohyb přomišíme na
Slunce. a proto Slunce popojde denně 0 59' B", skorem
o to , čili o 3 m 56 vt., nebo takořka o 4 m. AR Slunce
denně přibýt-u tedy 0 59'8“ nebo o 3 m 56 vt., čili
téměř o 10 nebo 4 m. l)le toho snadno vypočteme si
přibližnou AR Slunce. známe-li ji pro některý den.

Rektascense Slunce pro (Š'-ž den roku není rok
co rok stejná, poněvadž Zem dráhu kolem Slunce
nevykonri za 365 dní, nýbrž přibližně za 3657, dne:
proto první den jarní. kdy AR Slunce jest O“ , připadá
někdy na den 20., jindy na den 21. března; první
den podzimní, kdy AR Slunce obnáší 12 h čili 1800,
připadá někdy nn den 22., někdy na 23. září. Též
určitá rovnice časová platí jen pro určitý rok. Avšak
AR Slunce a rovnice časová až na nepatrné odchylky
opakují se v období čtyř let. Střední změna AR Slunce
při bodech rovnodenností obmtší ročně —53 vteřin
(ubývá ji). při slunovratu letním o l m, při slunovratu
zimním o 1 m 6 vt., obnáší tedy průměrně +'5 vt.
ipřibývá ji); v přestupných létech AR Slunce a rovnice
časová bývá téměř stejná.

Následujicí tabulka.obsahuje posice Slunce od
půl do půl měsíce.

|I Rok 1901 měsíc:| II Den | III AR Rektascence (přímý výstup) pro skuteč. čas sluneční v poledne střed. času | IV střední přírůstek denní | V D Deklinace (odklon) + severní - jižní | VI Střední denní změna: + přibývá - ubývá | VII Rovnice časová: hodiny ukazují v pravé poledne sluneční | VIII Střední denní změna: + přibývá - ubývá | IX AR pro střední čas denně přibývá o 4 m | X |
|----------|---|--------|-----|-------|------------------------|----------|-----|------|------|
|          |   |h  m  s |m  s | D + ' |Denní noční rovnodennost|h  m  s   | s   |      |      |
|Březen III|21 |0  0  32|3 38 |0  3.5 |+23.7'                  |12 7  28.8|-18.2|      |21    |
|          |23 |        |     |       |                        |          |     |      |      |




TODO


84

Roku 1902 AR Slunce pro každý den bude
menší téměř o l m, v roku 1903 o 2 m; rovnice
časová bude pro každý den roku větší 0 5 vteřin,
v roce “103 o 2x5 vt. V roce 1904 a v každém roku
přestupném AR Slunce a rovnice časová budou malo
se uchylovali od AR Slunce a rovnice časové r. 1900.
kdy AR Slunce byla o l m větší, rovnice časová
o 5 vt. menší než v r. 1901.

l'osmčilo by pro hrubé odhadování času dle hvězd
položili na den 22. března AR SluxtCe 00 nebo 2-4 b.
na 22. každého následujícího měsíce 0 2 h více: tudy
dnu 22. dubna AR Slunce 2 h, dne 22. května 4- atd.:
na den ?. každého měsíce položiti číslo liché: tedy
dnc ?. dubna AR Slunce [ h. dne 7. května 3 h atd.
Na každý následující den počítcjme AR o 4 m větší.
Spravnčji jest položítí AR Slunce pro přibližné odhad-
nutí času středního dle hvězd podle předcházejících
tabulek.

Hodiny nebeské.

Kdybychom položili se na znak, patřili 24 hodiny
směrem k Polárcc, a Slunce po celý ten čas ani no-
vy'slo, zdalo by se nam. jako by nebe za tu dobu
se otočilo kolem Polárky. Kdyby v kruhu dříve zmí-
něném. jenž prochází hvězdou v; Velkého Vozu byly
24 hvězdy stejně daleko od sebe rozestavené. vždy
za hodinu měli bychom jinou hvězdu nad sebou čili
v nadhlavníku: počínajíce půlnocí, měli bychom o jedné
hodině hvězdu třebas č. l. o druhé hodině hvězdu
čis. 2 . . .. o dvanácté čís. 12, o jedná s poledne čís.
13 . . .. o půlnoci čís. 24. Zároveň by na nebi ly
hvězdy byly nejvýše, čili vrcholily by nahoře. čilí

65
procházely by kruhem vedeným od Polárky k našemu
nadhlavníku: v téže čáře, však vedené od našeho
nadhlnvniku přes Polárka dolů, byly by zároveň ony
hvězdy, které vrcholí dole. Tak pohodlné to ve sku—
tečností ovšem není, ale potíž není velká. Cns odpo-
lední počítáme od doby, kdy Slunce vrcholí nahoře,
čili kdy jest na nebi nejvýše, totiž v poledne; čas ranní
od doby. kdy Slunce bylo na nebi nejníže, čili kdy
vrcholila dole.

Jak poznáváme po hvězdách, kolik jest hodln.
Abychom po hvězdách od oka poznali kolik jest
hodin času středního, postavíme se tváří k Polňrce
a pozorujeme, která hvězda, jejíž AR známe, právě
vrcholí čili přechází čáru na nebi vedenou od Poliirky
)( nadhlavníku a prodlouženou na obě strany, odečteme
AR Slunce toho dne od AR hvězdy vrcholící a obdr-
žíme počet hodin po poledni, když ona hvězdu vrcholí
nahoře; mnmc počet hodin po půlnoci, když hvězda
ona vrcholí dole. Msi-li hvězda AR menší než Slunce,
odečteme od AR Slunce AR hvězdy a obdržíme počet
hodin před polednem, když hvězdavvrcholí nahoře;
máme počet hodin před půlnocí, jestliže hvězda vrcholí
dole. Na př. dne 14. srpna, když nahoře vrcholí a Lyry
čili Vega., jejížroktuscense jest 18 h 33 m, jest 9 h 5 m
odpoledne: AR Vegy 18 h 33 m — AR Slunce
9 h 28 m — 9 h 5 m. Když dne 29. červnu dole
vrcholí hvězdu a Vozky čili Kapolla, jest ] h 19 m
před půlnocí, čilí téměř tři čtvrti na jedenáctou v'nocí:
AR Slunce 6 h 28 m — AR Kapelly 5 h 9 m
1 h 19 m. — Když dne 30. dubna dole vrcholí hvězdu
6

66
Kapclla, jsou 2 hod. 37 m po půlnoci: AR Kupelly
5h 9m — ARSlunco 2h 32m =2h37m.
— Přišlo-Ii by odčítali od AR menší AR větši, dle
potřeby k číslu menšímu napřed připočteme 12 h neb
24 h a teprv odčíláme. — Na př. dne 29. listopadu
AR Slunce 16 h 32 m: když u Draka, AR: 14 h 2 m,
vrcholí dole, jest: 16 h 32 m — 14 h 2 m - 2 h 30 m
před půlnocí, čili 9 h 30 m večer; nebo přičítá se
k AR :: Draka 12 h a odečte su AR Slunce: 14 h+
2m+12h—16h 32m—9h BOm. Dne 10. ledna
jest AR Slunce 19 h 16 m; když vrcholí nahoře a
Vozky, Knpella, AR: 6 h 9 m, jest tedy 5 h 9 m + 24 h
— 19 h 16 m =- 12 h 53 m po poledni. čili 58 m po
půlnoci.
Pro cvik posloužiž mio tabulka:
3 AR ' v l
Dne dmg?! Hvězd. ] AR Tlouill'.Iíiy_z;df.l._h.“ld
cu [ nahořcl dole
h m h m'h m Jmhmhmhm
odpol. % povila.
23. srpna 10 4 GX'oM'ozu 10 57 63 58 * 58
, . ldlabulč 20 83 10 34 10 34 (10 84)
16. října 13 36 akcí“ 2! IB _? 40 7 40 7 40
. . 'pAnamm. 1 57 12 21 lz 21 (12 21)
24.pfosinco 18 8 - - 749 749; (749)
. . anýk. 4301029 1022 30092)
nam. 2 a avan „ u 59 u mě 1152
. . 'oV-Vnzky 698181381
' dopou- * papa
11. června 516 „ - - 1168 1158 1153
. odpol. popu-.
26. července. s w :aom 119 .la: „ ao 1130 i (1130)

67
. _ . “malovali
nl W'febf-p a klinnCl
_ ., hvčZd “e. . ,čden ?
K určen. Ču“ dlale dobře 1%? šel-“ovál“ se 11.8.
. ce hvězd, (, snaz51 0 - hvězdami-
,_, deklln“ ich hvězd Pl' 'kazeCh mez!
Il—„rnň ně!;tegeme zprávy 0 “
“LEN-. kdy . 61
l- “» o půlno
- 65 autumn" D,:go ,' vnho
3 g MliťilT-am“
: _ l ' ga. *
'9 „i "I 0' '68 ma' ' „ub-
ed? ' - 4 I 1 mm
“ Andfom 9.3 ' U 4 w l ' 85 či ' , 91 )
now . „: nf + „ „, 29- m. .)
„x..dromod) “. l na. 2 “28 '__'22. - '(“ _)
7 “mm“ ? lg. al. 1.34 sůl mg.m. * ““"““
„skopw :. 512 "i „„ 7_ — 'n' .
„„júholnlk 'g 57 3 . mi 0. ' _ _ ,
F “ s [m' ] 10. » 19
, in.—rs“ Alsol g... ' 3 " 3 15 49 vw 16 . 19- »
„ l'cl'Seli ? 13 1758 40 23 '$“ ' . (ga. :)
; rasu . Knřv) “ I 8 41.4 47 90" 29. , :(31- , )
,.. „nulu-nk 4 la, 1'4 80 „, 18 28. m. [“.-vn.
! ""u“ „.n l l4 '80'5, " 41 6 GP.. 9. '
(. Hykl-“'“ Iti-64 4 “hl; lol 45 “'“ 8' . -(u. »)
,..x'nzki K lll'l llčl 9., 80 _1,10_1Í' , (M- :)
(, $%)ka- IPO Ig lb 31'; 80 21 a. l' . ' 1m .
. .nzsool 3'3'6 8116 60%“ 56 19. . .(w' ,)
“yk“ 9 *5 59 6 „O' 16 W'm'mnferW-
% Volit? 9.3“ 6 82: 40 .16 34 1- , ( 6- ' )
„ „Zimů glul '. 8 'n' 6 Im Ia 5“ . Ida »)
uwl'Pn'sr 4 uů'muz 30.32 6 12. . (1-1. ')
(ražená Kato, „“Filííl; so :; gg: :; _ '(m .)
IS'i Gn : . .
Znam-"mm“ :.au'ÍlBg'? 404ml " ' a.
.., „nm-n g h *

68
_w—
1 x mm.—smí T _ - _“ — -
- (__—,“ ' nm. '
Hvozd. ! Šum“ “mu—... nn.. ', Vrcholí 0 „mm,
;; kufr,—„AL_—
l.h„m.h1m ní:.nm_ -
11,06“ €:; iai " I' " ma' do“
_ ' a': a . ne
o Velkého vn,.. (33:1 8.22' císař—„?“ liIQIJodm-(ašcrw
* - » :3-4' a'a-r" ! “ [ 31526“ ' 97- -
. - , 9 147'331 -
h » . . —r ' , 4- “Wl 441- u
„9 B“" “1% 'lw [m;m "
' ' "3 „'0'265930452 3,11. , ll. '
am, Regulus 1 10. z:"xo „ 10 IH - » 12. .
ll Vůl. vozu B "104 [“ltlo 16. 4' m '21 ' 22. i
“V“. Vanu g „OJBTWII " 2 . ',“- ' 25. i
). Draka a-4f'll395'11 ao" :: 'n': ““*““ “' ““
[il.vn. Dcncbola V2 „Ill-“Ulm“ 53'_18. ' la. .
' Vel V * Í ' '15 3'18. , '1
$ - ozu ÍB'B'IH'WJH'WI'M mířil) (gg. .
' ' 34:19 1x _ __, - ' , . .
Střed m'iylůl I' ilollšllo'l ól 'sll ŽŠ. » “_ .
11 Druh; 8.4l12 . :,  _ ?! . » 22. ,
i Vol. Vom g "lg'fs'imlnq 'o WWO- ; !. Njn.
. [ . „94 12 50 56 30
“Pau 3 “12,514“le I 4-dubna Il. .
: (Cor Caron) ! lí , ". 3 I) 38 m;. 4.. . 7. ,
Vel-Vozu.Mimr“2 .la : ' I' '
. % l 20. . .
')  el. Vozu 12 "mI-!$“ lghčlrřwinďž. » .ló. . *
“ „film '8'4:']4l 2'114i lk 49 48:117. ! PH. »
a Bootcu,Ax-ktur =l 1'14' 1 r“ „1.322 ' 9“— ' .
x . u . .“ 14: “Ho 42 2- !
““"““ “u“ “9 '14 51'14=w'.'-4 Isa“ “' ' I'm '
? B““ '“ .14gr.8"15n ": ..[, Ž'h'čm', ""“"“
( Dr.-nk. ;] I ' . . 1 0,4" " ' 10. s
_ ' llfi'Žd 15:43:59 19:13
“Komny,(,emm. ') .*lň W'lólwho ' ] - ' lli. .
auta. !4 ., l l. i i „ sua. , „. _
16 .16 , 58'60'1
BŠHra .; hnis *““ : 198 522. ' 26. .
QDuln 3.9“16. .-' '23'1'“ li,“ . '(25. .
0 13,16 91 44 I
5. zvrhla 3'45'16158' „' II 81 ' *IFÍ'! ' lw- '
€ II.Vozu .. : .? : _ ' '. wem-uv. ms.
llafflallnyprr _.82 5.9) “_ _ ' 8._P .

89
Š 53221! 29.39" '_. Vrcholí o půlnoci
-„ pňvmwuwd____.a„_.____
.)?th hm“ l'*_ nahořo I dole
n, , ,' I' '! dno
„mu. 9 u_n, 9.517 s.; 85 [nm-; 8.čcn'n9i10.pros.
nilcrcuh a 2317glo1n171105.14,ao._ 9. . ](u. . )
[Sin-aka a-aunomuťnmonba .mn. . .15. .
uHadcmoše '91"17)ao;111!so_ffmlaa'.u. . ha. .
7 mm. [9'3Š117.“.':“ un; 51 laofiao. - '91. .
,' šzrelce '8'4917159318 :r—BO %“92. » i(22 ' )
„ Hndonošo (19) 94.119 9 :. 9 189992. » (99. . )
aLýry,chn 1 519.99419 m.;ssl4n'y90. . (ao. . )
; on. a ,:19, 1,519 ::la 4m; mam. 4.199...
Mam „98519542419 40444 5:11:19. . 14. » )
»; om. .mn ll (im.-goals; 45'; 8 391,13. . ua. .
„mm 4 319159339 1,92 use. . ua. . )
y . Ia-mmouom 20,539 99391. . km. ..
»; Labutě, Deneb a ŠWIÚŠ'W 40,441551 Lsrpnalss. .
. . [9me 42599 45.433 85% a'. . 99. . )
£ » 4 .,“snf IM „43,905 0. . Limon
L' - 9 1m; 9321 mam,-194 s. . 4. .
aw.. a-apmlmqal 16392 10 10. . a'. .
_' . a in mm 30.170 7 m. . a, y
: Pegas. 981,21 39491 m:; 9 95410. . u. »
! Keren 4 m 59,2, _usa 50 99. . 11. »
uvodníh 3 ::99 l_l ! l.,—0448222. - 17. .
L' Pegasu 34:32 381.92 893: 10 18:181. . (20. » )
„ . s 3.92 88229 40929 mým. . 97. .
(: .hžni Ryby ," *, ," 'I
Fomlhnut ragga 99292 back-ao: 9' 4. u'H (4. břez.)
(svem. :: „samba 3213995 5. . 5. . )
(: Pagan 2 (122459. 61434055“ . 5. ».
yuga.. avaým.l951_mtaa477. ms. . 16. .
“Andromedy 2 424; 3324! :'QSIBQÉQB. . lm. .
h : u, a L

70

Z tabulky předcházející volma si hvězdy k zaparim-
tování jejich rektascensí; hvězdy jasně __ první, druhé
i třetí velikosti — mají! tu výhodu, že je vidime dobře
i za měsična a když vzduch není právě čistý. Kde
ve sloupci :; nadpisem »vrcholí dole: datum jest v zá-
vorkách, hvězda, když vrcholí dole, jest pod obzorem,
a nevidíme ji.

Čas takto sice nebývá přesně určen, ale o více
než o čtvrt hodiny se nezmýlíme. Předně Polárka není
právě na pólu, ten přibližne leží v přímce vedené od
Polárky ku hvězdě š (Mizar) Velkého Vozu, asi jednu
třetinu zdánlivé vzdálenosti od sebe hvězd ; a s, na—
nesené od Polárky směrem k Mizaru; pak od oka
nepoznáme, zdnž hvězda právě kulminujc nebo „ie-li
pouze kulminaci blízka; po třetí AR Slunce (i myšle-
ného v rovníku) Způsobem nahoře vytčeným určovnná
může býti o několik — ovšem jen málo — minut
rozdílna od AR pravé. Obyčejně o kolik minut AR
myšleného Slunce v rovníku větší (menší) jest než
AR Slunce skutečného v ekliptice, o tolik minut hodiny
ukazující střední čas jdou v poledne později (dříve)
než hodiny slunečné, — a chyba tím se vyrovnává.

Čím více rektascensi známe, tim přesněji určíme
čas dle hvezd bez všelikého přístroje. Není příliš no-
snadno pamatovatí si rektnsconse značného počtu hvězd,
učíme-li se z pohledu samého na hvězdy, jež právě
kulmínace nejsou daleky; jinak ovšem ukládáme paměti
těžký úkol. Z pohledu samého “tvoříme si jakési
měřítko: znajíce pak rektascensi některých jen hvězd.
snadno zapumatujerne si rektascensi hvězd značného
počtu. Dobrou měrou jedné hodiny čili 150 rektascense
jest vzdálenost (v úhlu) a a :) Andromedy, mezi : a 7;
Draka; o a h, též mezi 7 a : Velkého Vozu; půl ho—

11
diny-dobře jest označeno vzdáleností (ir-úhlu) mezi
a u "; Velkého Vozu. — Chceme-li měřiti přesněji,
ml.-jme na paměti, že Polárka není na pravém pólu;
lemu dosti dobře jest určen takto: rozdělme přímku
mezi a a ; Velkého Vozu na tři stejné části a. odměřme
jednu takovou třetinu od Pohárky nn přímce vedené
ml I'olárky ]: hvězdě C Velkého Vozu.

Někdy vrcholí právě hvězda, jejíž AR neznáme,
ole třebas věc se má tak, že myslíme—li si oblouk od
létu hvězdy k pólu a zde kolmo na něj jiný, tento
Ernclnizl hvězdou, jejíž AR jest nám známa; jo-Ii to

vůzda v oblouku od Polárky — když na ni patříme
— nn levo, vrcholí právě hvězda. jejíž AR jest o 6 hodin
větší než AR té hvězdy; je-Ii však v oblouku jdoucím
ml l'olárky na pravo, jest 'AR hvězdy právě vrcholící
.. Ii h menší. Příklad: Dne 6. října večer vrcholila by
pum-ě hvězda, jejíž AR neznáme; myslíme si oblouk
ml ni k pólu :: kolmo na něj jiný; v tom právě by
bylo hvězda C(Miznr) Velkého Vozu, jejiž AR jest
tmm známo: 13 h 20 m; vrcholí tedy hvězda, jejiž
AR jest 19 h 20 m, a jest tedy 6 h 20 m času
nl'rc—lního, poněvadž na AR Slunce připadá toho dne
l:! h. V týž večer, ale v jinou dobu, vrcholila by
vrstvě hvězda, jejiž AR neznáme: v kolmém oblouku
v pravo od pólu vedeném byla by hvězda a Býka,
jejíž AR jest 4 h 30 m: vrcholí tedy právě hvězda
na AR 22 h 30 m, jest tedy 9 h 30 m. Též můžeme
užíti dolní kulminnce k určení času, když tam vrcholí
hvězda se známou AR. Z toho patrno, že by nám
pnsltlužil již poměrně jen malý počet hvězd, zvláště
kdybychom nežádali velké 'm-čitosti. Však nejlepší jest
k určení času vrchní kulminace; odmčřiti dobře od
nku pravý úhel nedovedeme snadno.

72

To vše týká se přibližného určeni času střed níh o.
AR Slunce pro toto měření pojímaná, ponejvíce ne-
shoduje se se skutečnou AR pro čas slunečný, na
př. na den 6. října 1905 položili- jsme AR Slunce
13 h, ve skutečnosti jest 12 h 46 m v poledne, však
zvětší se do 6 h již téměř o minutu — položme však
z hrubo. jen 12“h 46 m, odečetli jsme pak, určujíce
čus z hruba, o 14 m více, a zbylo nám tedy o 14 m
méně; ale hodiny, ukazujíce čas sti-ední dne 6. října,
ukazují z ln'uba téměř o 12 111 méně než čas slunečný;
byl tedy čas střední určen dosti dobře. Dne 11. února
(1903) pro čas střední vzato 21 h 24 m v poledne.
Slunce však má skutečně 21 h 36 m: odečetli jsme
tedy od AR hvězdy kulminující o 12 m méně a zbylo
mi tedy o 12 111 více. Hodiny čas střední ukazující
jdou toho dne 0 MW, m napřed; byl tedy čas střední
přibližně též určen .dobře. Ostatně reklascense v týž
den roku — na př. dne 11. února — není stejná.
rozdíl může obnášeti až 4 minuty. Podobné jest to
v jiných případech.

## Rozdíl dnů.

Myslíme-li si kolmo na obzoru směrem od severu k jihu postavenou rovinu a rozšířenou, až by se protínala se zdánlivým klenutím nebeským, tož by na nebi určila oblouk jdoucí od pólu nadhlavníkem k jihu. Za 24 hodin by každá hvězda prošla touto čarou, čili vrcholila by. Myslíme si v bodě na nebi, kde protínají se ekliptika a rovník, stálici a Slunce, tož doba, jež
by uplynula od vrcholení stálice k opětnému jejímu vrcholení, jest pravá doba otočení se Země kolem osy;

78

jest to den siderický čili hvězdný a obnáší 23 hodin a téměř 56 minut času středního. Bod rovnodennosti za rok posune se zpět o 50.25" obloukových, za den asi o 365.246tý díl; bude tedy procházeti tou čarou, řekl bych poledníkem, o 50.25 : 365:246.15 vt. časových dříve, neboť 15" obloukových dá jednu vteřinu časovou; to jest den astronomický, řekl bych tropický. Slunce však pojde zdánlivě na nebi denně o 59.13' čili o 3.942 minuty časové čili 3 m 56.5 vt., téměř o 4 m na své dráze dále a vrcholí tedy co den téměř o čtyři minuly později. — to jest den slunečný, od pravého poledne slunečného až zase k poledni slunečnému. Tyto dni v občanském životě počítáme. Hvězdáři počítají poledne v dobu, kdy vrcholí bod jarní, a mají průměrně dne 21. břežna poledne jako den občanský; jejich den jest téměř
o 4 minuty kratší dne občanského. Do roku vrcholí hvězda stálice o jedenkrále více než Slunce.

Po hvězdách v noci snadno poznáme, kolik jest hodin; věc není tak snadna ve dne; kdybychom chtěli též od oka podle Slunce poznati, kolik jest hodin aspoň na půl hodiny správně, musili bychom míti velký cvik v pozorování. Slunečné hodiny ukazují
nám čas slunečný, a ten obecně není stejný s tak zvaným časem středním.

## Slunečné hodiny.

Slunce zdánlivě oběhne kolem Země za 94 hodiny. Kdyby osa Země jako nějaká tyč vyčnívala z pólů, stín její by za den kolem té osy jako ručičku na hodinách jednou se otočil tak, že by za hodinu

74

opsal úhel 15°. Totéž učiní stín tyče rovnoběžné s osou zemskou na rovině postavené kolmo na této tyči, čili rovnoběžné s rovníkem neboli ekvátorem zemským. Opíšeme-li na rovině kruh, rozdělíme na 24 stejné díly a označíme ciframi prvního půl kruhu od 1 do 12 a odtud opět druhého půl kruhu od 1 do 12, středem kruhu toho kolmo na jeho rovinu protkneme tyč a upevníme tak, aby byla rovnoběžna s osou zemskou, a stín aby padal na 12, když slunce vrcholí nahoře (v poledne), máme slunečné hodiny ekvatorialné. Nejdůležitější jest tyči dáti směr osy zemské; jest to směr se stanoviska našeho k polární hvězdě nebo přesněji k bodu, jenž nahoře byl označen jakožto pól. Polárka jest tak daleko, že jest úplně lhostejno, hledíme-li k ní ze středu Země (směrem zemské osy) nebo s některého bodu povrchu zemského. Ve dne dlužno určiti si poledník čili čáru od severu k jihu
toho místa, kde chceme zříditi slunečné hodiny. To nám ukáže magnetická střelka deklinační, v těžišti volně se otáčející kolem kolmé osy, známe-li pro to místo odchylku magnetického meridiánu od meridiánu (poledníku) zeměpisného: je-li odchylka ta na př. 9° západně, poledník toho místa s osou magnetické střelky bude svírati úhel 9° směrem k východu. Tyč držená v kolmé rovině poledníku svírati bude s rovinou vodorovnou úhel φ, znamená-li úhel φ zeměpisnou šířku toho místa, na př. v Brně 49° 11.7', v Ostravě 49° 51.2', v Praze 50° 5.3'.

Však poledník lze určiti i bez magnetické střelky takto. Na rovině vodorovné opišme si několik soustředných kruhů, postavme ve společném jejich středu kolmo tyčinku a dávejme pozor, kdy konec stínu tyčinky dosahuje už k některé kružnici — dopoledne —, a pak kdy konec stínu dotýká se opět téže kružnice odpo—

76

ledne. Stín tyčinky v tolik času před polednem jako po poledni jest stejně dlouhý, na př. v deset hodin před polednem a ve dvě hodiny po poledni. Ty body spojíme se společným středem kruhů; přímku půlící ten úhel středný označuje poledník toho místa. V pravé poledne sluneční stín kolmé tyče nebo i tyče rovnoběžná s osou zemskou kryje se s touto čarou, totiž s poledníkem. Toto určení bylo by přesnější o slunovratu letním, kolem 22. června a o slunovratu zimním, kolem 22. prosince, kdy deklinace slunce v několika dnech téměř nic se nemění.

Hodiny slunečné zřizují se obyčejně na rovině vodorovné nebo na kolmých stěnách. Ať zřizujeme je na rovině jakéhokoli sklonu, vždy jest dbáti toho, aby tyč, jejíž stín má ukazovati čas, byla rovnoběžnou s osou zemskou, aby směřovala k pólu severnímu hvězdného nebe, celkem tedy k Polárce.

Když jsme byli dali tyči slunečných hodin tento směr, poznamenáváme si od hodiny k hodině, nebo i po půl, po čtvrt hodině, jakž ukazuje stín na slunečných hodinách rovníkových, směr stínu a pořizujeme takto ciferník. — Ovšem ještě praktičtěji děje se tak, že určíme, pokud možno přesně poledník místa (směr stínu kolmé tyče v pravé poledne sluneční), tyči, jejíž stín má ukazovati čas, dáme směr rovnoběžný s osou zemskou, pak dle hodin, jež jdou správně, znamenáme směr stínu a tak pořídíme ciferník. Však to bychom již neměřili čas Sluncem bezprostředně; díti by se to muselo v dobu, kdy střední čas shoduje se s časem slunečným, tedy asi dne 15. dubna, 14. června, 1. září, 24. prosince.

Takové hodiny slunečné bývají na zdech kolmých, na kamenech vodorovných neb i šikmých. — Jsou

78

však i hodiny sluneční přenosné, kapesní, jež aspoň přibližně ukazují, kolik jest hodin. Základní myšlenkou,
dle níž takové hodiny jsou sestrojeny, jest, že Slunce
téhož dne v různých dobách různě vysoko jest na nebi, a tedy paprsky jeho mají různý sklon k rovině obzorové: paprsky Slunce vycházejícího a zapadajícího jsou s obzorem rovnoběžné; v poledne, kdy Slunce vystoupí na nebi nejvýše, sklon jeho paprsků k obzoru jest největší. Proužek třebas z plechu v prstenec stočený, na straně jedné opatřený dírkou, jíž by procházel sluneční paprsek, na straně protější rozdělený na hodiny, mohl by býti slunečnými hodinami. Že však Slunce v různých dobách ročních v poledne různě vysoko stojí na nebi, na př. nejvýše kolem 22. června, nejníže kolem 23. prosince a tedy i v jistou dobu denní, třebas o desáté hodině dopoledne, tož dírka to by musela býti posunutelnou dle datum, jež by bylo označeno na onom proužku, aspoň od desíti do desíti dnů. Ale též v různých šířkách zeměpisných Slunce v týž čas různě vysoko stojí na nebi, a hodiny takové platily by jen pro jistou zeměpisnou šířku, pro niž zkusmo byly zřízeny dle dobrých slunečných hodin. Též tyčinka kolmá na rovině svislé z pevného mosazného plechu, kteréžto rovině dáme takový směr, aby
rozšířena jsouc procházela středem Slunce, ukazovala
by hodiny svým stínem. I tato tyčinka musela by býti posunutelnou dle datum v roce. Při východu a při západu Slunce stín té tyčinky bude na svislé rovině vodorovný; čím Slunce na nebi vystoupí výše, tím úhel stínu tyčinky s vodorovnou přímkou na oné rovině bude větší, v poledne bude největší.

I délka stínu přímé holi kolmo postavené na rovině vodorovné může býti měrou času denního. Čím Slunce na nebi výše stojí, tím bude stín holi kratší, nejkratší
bude v poledne.

??

Ale v různých dobách ročních i v poledne stín téže tyče bude různě dlouhý a proto i v touž denní hodinu, třebas o jedenácté hodině. Hůl taková by musela býti prodloužilelná podle datum jisté stupnice, sestrojené buď dle výpočtu nebo zkusmo dle slunečných hodin, a muselo by býti na ní označeno tedy datum a v kterou denní hodinu značí jistá délka stínu určitou hodinu. Poněvadž délka stínu téže týče a v touž denní dobu v různých zeměpisných šířkách jest různa, i tyto slunečné hodiny platily by jen pro jistou zeměpisnou šířku. Od desíti hodin dopoledne až do dvou odpoledne výška Slunce na nebi poměrně málo se mění: určení času dle stínu tímto způsobem mohlo by býti
ovšem jen přibližné.

## Jak poznává se datum po Měsíci.

Však nejen hodiny dne označeny jsou na tom
nesmírném orloji nebeském, nýbrž i datum; bez vše-
likých měřidel umělých jen pohledem na nebo aspoň
přibližně jest možno je určili.

Dobu od úplňku k úplňku Měsíce trwi průměrně
29 dní 14 hodin 44 minut — Měsíc synodický. —
Máme právě úplněk, když střed Měsíc:: na nebi pro-
chází rovinou myšlenou “kolmo na obzoru od středu
Slunce, tedy když Měsíc jest na opačné straně oblohy
než Slunce a mi proto AR o 12 hodin větší než
Slunce. Obi-uci k nám strunu osvícenou. — Máme
nový Měsíc, když střed Měsíce prochází rovinou my-
šlenou kolmo na obzoru ku střel-lu Slunce, Měsíc má
stejnou rektascensi — AR — se Sluncem :: obrací k Zemi

78

stranu od Slunce neosvicenou. Jc-Ii právě první čtvrt,
Měsíc prochází na nebi rovinou kolmou na rovině
nahoře určené, jest od Slunce vzdálen o čtvrt kruhu,
má tedy AR o 6 hodin větší než Slunce. Je-li právě
třetí čtvrt, Měsíc opět prochází touto rovinou kolmou
na čáře vedená od Slunce k nadhlnvníku a roz-
šířenou přes pól k obzoru; má AR větší a 18
hodin než Slunce, čili od Měsíce ke Slunci směrem,
jak čítají se rektnscense, jest 6 hodin..—Přiklady:
Kdyby Měsíc, v úplňku procházel čarou od pólu k hvězdě
a Draka — AR 17 h 28 m —, Slunce mělo by AR
o 12 h menší, tedy 5 h 28 m a. bylo by 13. června.
Kdyby Měsíc, tíOVfŠÍV právě první čtvrt, procházel
čarou yedenou od pólu ku hvězdě : Panny, '— AR
12 h 57 m —, Slunce mělo by AR o 6 h menší, tedy
6 h 67 m, a bylo by asi 5. července. — Kdyby Měsíc
dovršiv právě třetí čtvrt procházel čnrou od pólu k hvě-
zdě e Oriona — AR 5 h 31 m — Slunce mělo by
AR o 18 h menší, tedy: (244-5 h 31 m)— 18 h —
11 h 31 m a bude asi 16. září. K posici Slunce b chom
dospělí v tomto případě, kdybychom k AR Š-lěsíce
připočctli 6 h — tedy: 5 h 31 m + 6 = 11h 8! m.
— Při úplňku připočetlí bychom 12 h. — (Ví-z. .Ne-
beské hodinyc).

Častým pozorováním Měsíce nebudeme míry, oč
ho denně přibývá a můžeme odhadnouti, kolik dní
jest po čtvrti, po úplňku, nebo před čtvrtí, před úplň.
kem a. dle toho určití posici Slunce a tím i datum.

Určování datum dle měsíce z pohledu na nebe
může býti jen ?. hruba přibližné.

K lepšímu porozumění na základě příkladů p0v
slouží ná5ledující tabulka.

u _ .. _ 79
„ náma?
;: : %“;— . LLŽ !

.. gs,„ “=**—***
: “„ = a „ = s * a
0 $: E_ĚT__:_.3„_._Ž__Í._
gšžág o. m É % :. gg % a
i? .. i ; * _ g a a
: ! i a: _ ' . % š % '
„ ___—E_AŽ—„ŠJ—„sJ—LŽ—Ž.
ĚLĚ514352ŠZŠĚÉ

**ET'**'**********:**=**
ggmwzssww
E . 2 = 92 81 ?R
gl š. Ž Ž 3 E š % g. .
_.íň._;_:_š_o_.i_é_w_i
ÉLÉLRŘĚÉZŠEĚ
w.“ sďf'TÚ—f—LLL
„zv—a „arms
£ _ _. .! 92 31 8
m' ** Šfš
„„ „g, .s Š ŠĚ “
E _ 8 až “s ' .

_“ __ .g. -;- .;.y_:__Š_ŠLi._7-€_
,Š._5=L„„žsžššš
15 e __g :: a H „ _ g
x ĚOQĚ—m v.: E Š ; : $ 3

__.- í—FTTMLM—

„ „ a s

% o '? v '; 3 ;“
  : a ; š ? b “3

_ _u u u_ .ŠELŠÍ.

80

K přesnému určeni datum dle Měsíce AR Slunce
by měla býti brána dle Slunce skutečného, ne dle
středního, smyšleného, jimž řídíme hotliny ukazující
čas střední. Měsíc za hodinu ve své dráze kolem
Země popojde téměř o 31 minut obloukových, za den
okrouhle o ili—l“ ]0'5', t. j. takořka o 527 minut časových
(siderický oběh), Slunce jen asi 0 3 minuty 56 sekund,
tedy 0 ne celé 4' minuty časové.

Měříme-li od oka, nepoznáme přesně, zdnž Měsíc
právě dovršil první čtvrt, pak je-li právě dOvIŠen
úplněk, a podobně i třetí čtvrt, schází-li něco málo
nebo přibylo-li ho nad ty fásy; proto datum měsíce
určíme o dva, tři dny mylně a tak bychom chybili,
i kdybychom přesuč věděli reklascensi Slunce skuteč-
ného. Komu však by záleželo na přesnějším určení
rektnscense Slunce, než jest potřebí ku poznání času
středního, tomu poslouží tabulím na str. 62 a 63, v níž
ovšem udána jest průměrná rektnscensc Slunce, neboť
skutečná rektascense rok co rok v týž den jest (: ne-
celou minutu časovou čili téměř o čtvrtinu stupně
obloukového větší. takže ve čtyřech letech rozdil činí
jeden den dráhy sluneční — rok přestupný. Tropický
oběh Slunce kolem Země (zdánlivý) trvá 365'2422 dne.
.V astronomickém kalendáři pak přesně bývá udána
rektascense na každý den roku.

Jak poznáváme roky po hvězdách.

Víme-li dobu, ve které některá oběžnice „_ lépe
vzdálenější. na př. Jupiter nebo ještě vhodnčji Snturn. —
vykoná dráhu kolem Slunce'), víme i jak dlouho

') Vl: tabulku -Plehled sluneční soustavy—. str. 96—97.

Sl
potrvá v jednom souhvězdí zvířetníku, a můžeme po-
znati, kolik roků uplynulo mezi dvěma událostmi, když
bylo poznačeno, se. kterými hvězdami oběžnice byla
v konjunkci, když ly události se sběhly. Na př. Saturn
ubíhá kolem Slunce — oběh siderický — za 29 let
a HW dní. Byl-li viděn v konjunkci s některou hvězdou,
třebas : Býka — Aldebaran — AR al h 710 m, v dobu
nějaké události a později, když jimi událost se sběhla,
v konjunkci s hvězdou :: Draka — AR 12 h 80 m —,
Saturn vykonal dráhu: 12 h 30 m — 4 h 30 m ==
8 11, tedy třetinu svého oběhu. a uplynulo času mezi
oběma událostmi třetinu 29 roků 16? dni t.j. 9 roků
a téměř 10 měsíců.

Jupiter vykoná dráhu kolem Slunce — oběh side-»
rický — za ]] r. 315 d. čili ]] r. 10% měsíce, popojde
na nebi za rok o málo více než o jedno souhvězdí
zvířelnílm. Kdyby v dobu nějaké důležité události byl
viděn v konjunkci s hvězdou :! Blíženců — Pollux,
AR 7 h 39 m. jindy v konjunkci s hvězdou 1) Vel-
kého Vozu — Benetnaš, AR 18 h 43 m, vykonal vtom
čnsednihu: 13h 43m — 7h 39m=6h4m, tedy
téměř polovici své dráhy, :: uplynulo mezi tím polovice
doby: 11 r. 815 dní čili 6 r. 11 měsíců a 10 dní.
(Theoreticky ovšem i li r. 11 měsíců 10 dní ami-
sobek doby 11 r. 315 dní). — Určení to jest přibližné,
protože planety zdaji se míli i pohyb zpáteční, jakoby
se vracely poněkud a pak opět šly svou cestou dále;
jejich AR po nějakou dobu ubývá A pak opět roste.
Kdyby poznnčenn byla současná konjunkce těch dvou
plane! s některými hvězdami a snad i konjunkce Mě-
síce a jeho frise, pak která hvězdu právě vrcholila,
daly Aby se určití rok, den i hodina, jež od té doby
uplynuly. Doby všelikých historických událostídúvno-

6

82

včky'ch určují hvězdáři dle zievů astronomických, pokud
jsou zaznamenány i dle roční doby a ze zath
Slunce nebo Měsíce.

Zatmění Slunce, zvláště úplné zatmění, kteréž pro
noob rčejný účinek na člověka neujde pozornosti, jest
důležito pro určení času. Jest to zjev řídký; gro totéž
místo povrchu zemského přihúzí se průmčrn ve 200
letech jednou.

Josue dobyv pevného Jericho, porazil u Gibeonu
knížata nmorejská v bitvě, trvající až i po západu
Slunce. Slunce se zastavilo v běhu svém.
Novověká astronomie, jež dovede na tisíce- let napřed
i nazed určití zatmění Slunce, ukázala, že zpráva.
biblická jest pravdiva: bylo úplné zatmění Slunce
k večeru, Slunce jakoby zapadlo a pak než zapadlo
skutečně, objevilo se opět.

I zatmění Měsíce býva dobrým ukazovatelem času.

Postupem bodu jarního označena jsou na nebi
tisíciletí, vzájemným postavením planet určena jsou
staletí a rokyj Měsícem označeno jest datum v roku,
vrcholícími právě. hvězdami dany jsou hodiny a mi-
nuty. — Jak důkladné jsou tyto zlaté nebeské hodiny,
jež jdou od věků do věků. nniž jest potřebí natahovnti
je a spravovati! __

Zpětný pohyb bodu rovnodennosti a důsledky.

Rovina ekvátoru zemského rozšířená až po zdán-
livé nebe protíná se s rovinou zdánlivé dráhy sluneční
v přímce uzlové; touto přímkou na nebi určeny jsou
body rovnodennosti jarní a podzimní. Od bodu jarního
směrem k východu počítají se na rovníku rektasconse,

88
na ekliptice délky posice hvězd. Kdyby Země byla úplnou koulí, průsečík ekliptiky s rovníkem by místa neměnil, že však Země jest koule sploštělá, rotační ellipsoid, tož přitažlivost Slunce na Zemi působí tak, že tu stranu polokoule, která jest obrácena ke Slunci, jako by k sobě nadzdvihovalo, čímž rovina ekvátoru s ekliptikou nehybnou protínají se v přímce otáčející se směrem od východu k západu; bod jarní i podzimní rovnodennosti na ekliptice postupuje od východu k západu. Oběžnice a zdánlivě i Slunce pohybují se směrem od západu k východu. Myslíme-li jen na zdánlivý pohyb Slunce, tož bod jarní jakoby Slunci
šel naproti; nyní ročně postupuje o 50.25 vteřin obloukových. Kdyby postup ten byl stálý, bod jarní vykonal by dráhu dokola asi za 25.800 let. Dobu tu někteří hvězdáři nazvali platonickým rokem. Ale tento postup není docela stálým a též nedal se dosud určiti tak přesné, aby spolehlivě dalo se souditi o jeho oběhu na tak dlouhou dobu.

Více než před dvěma tisíci léty bod jarní byl
v souhvězdí Skapce, od té doby postoupil Zpět téměř
o celé souhvězdí: jest nyní v souhvězdí Ryb, kolem
roku 4000 bude v prostřed Vodnáře, kolem roku 6000
na hlavě Kozorožce, kolem r. 8500 u šípu Střelce atd.

Každé z těch souhvězdí má svůj znak: i ty
značky hvězdář-i s jarním bodem posouvají zpět:
značka Skopce jest nyní v souhvězdí Ryb, značka
Ryb v souhvězdí Vodnáře atd.: dlužno tedy rozezná-
vati nyní znameni od souhvězdí. Ekliptika rozdělena
jest na 3600 čili na 12 stejných dílů po 300 stupních
počínaje od bodu jarního v souhvězdí Ryb, označeného
značkou “(“ (Sk0pce). Praví-li se na př., že Slunce
dne 21. března vstupuje do znamení Skopce, tož ne-

6'

8-1

vchází do souhvězdí Skapce, nýbrž jest v souhvězdí
Ryb v jarní rovnodennosti. vai-li se na př., že Slunce
dne 21. června vstupuje do znameni Raka, tož jest
v Blížencích, v slunovratu letním: dne 22. prosince
vstupuje do znameni Kozorožce. jest však v souhvězdí
Střelce, v slunovratu zimním. Podobně mluví se i o
pohybu Měsíce.

Bod jarní rovnodennosti tedy jde vsxřic Slunci
na jeho zdánlivé dráze v ekliptice: proto rozeznáváme
—» přenášejice skutečný pohyb Země na Slunce —
oběh tropický. od bodu jarního opět k bodu jarnímu,
366'24220 dne od oběhu siderického, totiž od bodu
v ekliptice nehybného — od stálice — až opět témuž
bodu — k téže stálíci. Ten bude přirozeně delší :
865'25630 dne. Jako 11 Země, tak se to má i u ostatních
planet s jejich oběhy; rozeznáváme oběh tropický od
siderického. Kromě toho mluvíme u planet o oběhu
synodicke'm tsoubčžném) a myslíme dobu, jež uplyne
mezi dvěma konjunkcemi Země a planety, kdy totiž
planeta má vzhledem k Zemi a ke Slunci totéž posta-
vení. Má se to tak jako s ručičkami na ciferníku našich
hodin; byla by to doba, kdy ručičky se kryjí : na př.
012 ti pak 01 h 5 m 2731“ 5. pak o 211 10 m 54% s
u (. (l. Siderieká doba oběhu ručičky ukazující minuty
vzhledem ku stálému bodu na ciferníku (12) jest hodina,
synodlckai doba oběhu jejího vzhledem k ručičce uka-
zující hodiny jest 1 h 5 m 273.. 5. Tak mluvíme
i o synotlickóm oběhu planet vzhledem k oběhu Země.
Občh Venuše tropický 22469544, siderický 22470075),
synodický ] rok 218 dní 16 h. oběh tropický jest
nejkratší, — bod jarní jde vstříc; oběh siderický jest
delši, _- bod, k němuž planeta má přijíti. jest ne-
hybný; — oběh synodický jest nejdelší, — - planeta
jakoby musila doháněti Zemi (zdánlivě Slunce),

86

Rozeznáváme i různé délky dne (str. 72 u 73).
Myslíme-li si rovinu, v níž jest zemská Osu a bod zem-
ského-povrchu, kdež stojíme — rovinu poledníku roz-
šířenu už po nebe, — tož jest dobu odjcdnoho projití
jarního bodu touto rovinou až ke druhému, den astro-
nomický. mohli bychom jej též zváti dnem tropickým :
doba od projití nehybného bodu na nebi. stálice, až ku
projití nejbližšímu, jest den siderický: ten jest o tolik
delší. oč bod jarní denně postoupí opačným směrem.
než se otáčí Země. — Tento bod postoupí za rok jen
(» 5025" n_ za den 0 013758 sekundy obloukové čili
0 0110917 sekundy časové. a jest tedy den siderický
o 0'00917 sekundy čnswé delší než den tropický. t.j.
mai na jednu stotiuu sekundy. Toho nepatrného rozdílu se
nedbá. Dobu, v níž mvinn poledníku přechází středem
Slunce myšleného v rovníku na nebi, jež pohybuje se
stejnoměrně,nž k nejbližšímu projití touto rovinou —
od kulminacc ke kutminnci — jmenujeme střední délkou
dne a dělíme ji na 24 hodiny. Délku dne siderického
obnáší téměř 23 h 66 m. Doba. od projití středu
skutečného Slunce, rovinou poledníku až nejbližšímu
projití — od vrcholeni k vrcholení, — sluje délkou
skutečného dne slunečného. Čtyřikrát do roka toto
Slunce zároveň vrcholí se Sluncem myšleným v rov.
níku: tehdy den slunečný má stejnou délku se dnem
času středního. (n-Xebeské hodiny.).

Dráha měsíční.

! rovina dráhy měsíčně protíná se s rovinou eklipo
liky v přímce —_ přímka uzlů dráhy měsíčně _._ a svírá
s ní úhel téměř 598%2 Kdyby dráha měsíčně byla
v rovině ekliptiky, měli bychom pokaždé zatmění Slunce,

%
když by byl nový Měsíc, zatmění Měsíce pak v čas
úplňku. protože by v ty doby středy Sli-nee. Měsíce a
Země byly v jedné přímce; stín, jejž vrhá Měsíc (nový),
msnhovnl by Zemi, a stín Země padal by na Měsíc,
když by byl v úplňku. — Zatmění Slunce může nastali
jen tehdy, když Měsíc (nový) přechází přes přímku
uzlů. Je—li Měsíc buď nad ekliptikou nebo pod ní,
stín jeho nezasáhne Země, i když jest Měsíc nový,
ani slin Země nepadá na Měsíc, i když jest v úplňku;
jeslit Země proli Sluncí nepatrnou. a tím více ztrácí
se Měsíc proti Slunci.

Tropický měsíc, čilí doba od konjunkce Měsíce
5 bodem jarním k nejbližší konjunkci, trvá 27 dní
7 h 43 m 4'7 5: měsíc siderický, čilí doba od konjunkce
Měsíce k nejblížší konjunkci s některou sídlící. trvá
27 dní 7 h 43 m 11'53 5. Měsíc synodický jes! doba
od úplňku k úplňku, nebo od nového Měsíce k novému
Měsíci, t. “j. od konjunkce Měsíce se Sluncem k nej-
blížší konjunkci. a trvá průměrně 29 dní 12 h 44 m 2-9 s.

] měsíčná dráha vzhledem ku středu Země není
kruhem, nýbrž ellipsnu, v jejímž jednom ohnisku jest
Země: Měsíc jednou za svého oběhu kolem Země
octne se Zemí nejblíž, v přízemí _ perigneum »- a
jednou od Země nejdál. v odzemí — npogneum. —
Doba, jež uplyne, co Měsíc přejde od přízemí k přízemí,
nebo od mlzemi k odzemí, jmenuje se měsíc anomnlí-
slícký a trvá 27 dní 13 h 18 m 37 s. Přímka. v níž
protíná se ekliptikn čili rovina dráhy zemské s rovinou
dráhy měsíčně, směr ustavičně mění, otáčejíc se směrem
od východu k západu — Měsíc postupuje od západu
k východu —; přímka ta otočí se za 6793 dní čili
za 18 roků a 218 dní, průsečík její s ekliptikou pří-

81
chází Měsíci vstříc. Doba mezi dvěma. průchody Měsíce
tímto průsečíkem čili uzlem jmenuje se měsíc dračí
a trvá průměrně 27 dní 5 h 5 m 368 vt.

Pól ekliptiky : pól rovníku (severní).

Nahoře bylo pravcno, že rovinu, v níž leží rovník
zemský. s rovinou ekliptiky protínají se v přímce, jež
slove přímkou uzlů. Sklon těchto dvou rovin
k sobě obnáší 230 27' $“; kolmice těch dvou zdánlivých
kruhů — rovníku totiž a ekliptiky — svírají též
úhel 28“ 27; B"; kolmice na rovníku středem Země jest
osa zemská, kteráž jsouc prodloužena až k hvězdám,
končila by nedaleko Poldi—ky. Kolem této osy všechen
prostor světový zdánlivé 'otáčí se vždy jednou za den
a noc. ix'olmicc na ekliptice (ve zdánlivém jejím středu)
ukazuje k pólu- ekliptiky; jest to bod na nebi mezi
hvězdami '; a d Draka, něco mailo blíže hvězdy ; než
v polovici vzdalenosti těchto dvou hvězd od sebe. Pól
tento jcst nehybný. protože rovina ekliptiky jest ne-
hybnou. aspoň vzhledem k naší soustavě sluneční.
Jinak se to má s osou zemskou. Slunce jako by nad-
zdvihovelo rovník a tedy i osu zemskou na rovníku
kolmou, osa však. jako u každého rotačního ellipsoídu,
chce podržeti stejný směr; tím povstává pohyb zpětný
nné přímky, v níž se protínají rovník s ckliptikou.
Osa zemská otáčí se kolem osy ekliptiky: kdyby
zanechúvala po sobě stopu svého pohybu. byl by to
plášt kolmého kužele, jenž by stál vrcholem ve středu
ekliptiky; konec zemské osy opsal by v 25.800 letech
mezi hvězdami kruh, hvězdy ležící v této kružnici
byly by polárními hvězdami v různých dobách. Po-

88

látku. naše; jež jest blízka této kružnici, nebyla před
několika tisíci lety polárkou a opět jí nebude. 'lhkový
otáčivý pohyb osy pozorujemei při čamrdě. při »vlkuc,
oblíbené hračce dítek.

Nebes búnč se svými hvězdami proti pohybu
zemské osy jest jako nehyhnou: kolotá—li zemská osn.
tož v různých dobách, ovšem velmi vzdálených od
sebe, 5 téhož místa povrchu zemského. na př. s naší
vlasti, bude možno viděti 'í různě daleko na jižní nebe,
a budou proto v různých tísíciletich víditelny hvězdy,
jež nyni vůbec nnd obzor náš nevystoupí. Tak v Recku,
v Malé Asii . . . za časů Homerových. asi 900 roků
př. Kr., hvčmln '; Velkého Vozu — Benetnaš ._ na
kencj voje, byla hvězdou obtočnovou a viditelne i
v dolní kulminnci; když stojí na nebi nejníže. přece
bylo jí viděti nsi ll“ nad obzorem. Tehdy byla nad
rovníkem 630 49', tedy výše než nám jest nyní hvězdou
Velikého Vozu (zadní kolo vrchni). Nyni Benetnaš jest
nad rovníkem jen 490 49'.

Souhvězdí Kříže, jehož nejjasnější a zároveň nej-
jižnější hvězdu nyni jest 620 83' pod rovníkem, může
býti viděno — ovšem až na obzoru — 5 míst po-
vrchu zemského 270 sev. šířky: před šesti sty lety ta
hvězda měla deklinace 590, :: souhvězdí mohlo býti
viděno 5 míst už 3l_“ sev. šířky, na př. z Alexandrie,
Delhi, Jemsuléma, Senghaie; nyní z těch měst této
“hvězdy není viděli a proto i ne Jižního Kříže, aspoň
ne celého.

Že přímku uzlové otáčí se opačným směrem než
zdánlivě Slunce postupuje, že tedy bod jarní o. bod
podzimní v ekliptice zpět postupují, že osa zemská,
myslíme-li si ji prodlouženu až po hvězdy, po tisíci
letech vždy k jiným hvězdám ukazuje, šest velmi dů-

ši
!ežilo pro určení doby nějaké udalosti, je-li jen nějak
poznamenáno, u které hvězdy byl tehdy hod jarní.
nebo která lwčzdn byla poldi-kou. nebo vůbec nějaké
události astronomického zjevu. ?. něhož by se dala
vypočisll tehdejší posice bodu jnrního nebo tehdejší
polarky.

Osu zemská. tedy kolmice na rovině rovníku, spíše
v této době plášt kužele kolmého. jehož vrchol jest
ve sti-cilu ekliptiky a prochází na nebi hvězdami
v kružnici, jejiž středem jest pól ekliptiky u poloměr
v úhlu 237,0 , neho vzdálenost nynějšího pólu od pólu
ekliptiky. t. j. vzdálenost naší Polárky — ač není
úplně na pólu — od pólu ekliptiky, jenž leží téměř
v polovici mezi hvězdami 8 a *; souhvězdí Draka.
Proto za dobu 25.800 let různé hvězdy budou polár—
knmi. Tím na nebi označena jsou tisíciletí. Kdyby při
nějaké události starého věku 1210 označeno, kolem
které hvězdy tehdy nebe so oui clo, mohlo by sd nyni
věděti. kolik času od té doby až po dnes uplynulo.
zvláště kdyby i byla zmínka o zatmění Slunce neho
Měsíce, pak ve kterou dobu roční a denní se událo;

Sklon osy oběžnice !( rovlně ]ejí dráhy

kolem Slunce a důsledky.

Protože rovník s ekliptikou svírají úhel téměř
28'l20, čili že osn zemská skloněna jest k ekliptice
úhlem 66V;—0 a že za oběhu kolem Slunce polohy
v prostoru světovém nemění, Slunce v různých dobách
ročních v poledne na nebi různě vysoko stoji. Cim
Slunce v poledne na nebi výše stojí, tím dříve ráno

90
vychází a později zapadá, tim den jest další; naopak
čím v poledne na nebi stojí níže, tím později vycházi
ráno a tím dříve zapadá večer, tím den jest kratší:
tím méně máme od něho světla i tepla. Tnk i hvězdy
čím pólu jsou blíže, tim déle potrvají nad obzorem. —
Kolem dne 21. prosince Slunce jest v té části ekliptiky,
kteráž pod rovníkem jest nejníže — 23%“ —, a máme
nejkrutší den. Odtud Slunce co den na nebi výše stojí
v poledne: do 23. prosince v poledne vždy níže stálo,
od 23. prosince počíná v poledne státi výše, tedy
jakoby se obrátilo: proto i doba tu jmenuje se sluno-
vratem — Sonnenwende — Slunovrat tento jest zimní.
Však po několik dní kolem 23. prosince nepoznáváme
pouhým okem, že by Slunce v poledne na nebi bylo
níže nebo výše, i dni jsou téměř stejně dlouhé, po
několik dní, jakoby Slunce stálo, pokud se týká jeho
pohybu směrem k pólu; proto po Iatinsku ta dobu
slovo solstitium — stání Sltmce. — Doba ta zajisté
jest význačna a bývala slavena od národů pohanských.
Tehdy bohové dopřávali člověku nnhlédnouti i do bu-
doueností, lidé dávali pozor na všeliké úkazy, ?. nichž
pak domnívali se uhodnouti, co bude. Zvyky a obyčeje
z lidu snadno nevymizí, přecházejí s pokolení na po-
kolení. zachovávají se tím úplněji, čím vzdělanost —
at to byla třebas i »kulturnc — méně pokročila. Po-
věry lidu. zvláště o štědre'm yečeru, o Vánocích jsou
toho důkazem — Erbenův »Stědrý vočerc.

Kolem 21. března Slunce jest v bodě jarním, totiž
v jednom z průsečíků ekliptiky s rovníkem, jest
i v ekliptice i v rovníku: den rovná se noci — rovno-
dennost jarní. Kdyby Slunce bylo vždy v rovníku,
měli bychom po celý rok den tak dlouhý jako noc,
poněvadž Slunce by nad obzorem opisovala oblouk
na nebi tak velký jako pod obzorem. ale roční časy

91
— jaro, léto, podzim a zima by se nestřídaly. Kde
Slunce vychází a zapadá, když jest v rovníku, jest
pravý východ a pravý západ.

Kolem 22. června Slunce dostoupí v ekliptice
nejvýš nad rovník: 23' 20 — tehdy za celý rok Slunce
pólu jest nejblíž, opisuje na nebi oblouk eo největší,
dni jsou nejdelší a. noci nejkratší; za celý rok Slunce
v poledne na nebi stojí nejvýše. Od 22. červnu Slunce
v poledne stojí vždy níže, jako by se bylo v postupu
směrem k pólu obrátilo. Dni počínají se krátiti. Doba
ta jmenuje se proto slunovrat - letni. Však po
několik dní před ?l. červnem a po něm ani nepozo-
rujeme. že by dne přibývalo nebo ubývalo. jakoby
Slunce stálo, pokud se týká jeho pohybu směrem k pólu:
latinsky i tu dobu jmenuje se solstitium — stání
Slunce.

Jako dobu. slunovratu zimního. tak i doba sluno—
vratu letního jest velmi důležitou a bývala slnvena od
národů. Naše ohně svatojunské večer dne 23. června
jsou zbytkem starých stavnoatí. Božstva nižšího druhu
člověku nepřejícl nabývala moci nad člověkem, a ne—
bylo radno vydávali se na jistá místa, do lesů, k vodám.
— Čelakovského »Toman a lesní panna-, Erbenův
»Vodníkc. — Pověry lidu o čarodějnicích, o čarodějné
mocí bylin natrhaných před východem Slunce svato-
janského dne mají svůj původ v dobách starých.

Nevelká část drtihy sluneční v ekliptice málo dní
při 23. prosinci a při 22. červnu jest téměř rovnobčžna
s rovníkem. a proto po těch několik dní nepoznáváme,
že by Slunce v poledne nestálo na nebi stejně vysoko.
Význam latinského slova solstitíum — stání Slunce
— jest znám i Slovanům: Srbové praví, že Slunce
v Javanj dan třikrát na nebi od strachu se poza-
stavuje.

92

Po slunovratu letním Slunce v poledne vždy níže
stoji, až dne 23. září jest v druhém průsečíku ekliptiky
s rovníkem: máme den tak dlouhý jako noc— rovno-
dennost podzimní. Odtud Slunce schází pod rovník,
dne rychle ubývá, už dne 23. prosince Slunce octne
se nejníže pod ekliptikou, ve slunovratu zimním, kdy
máme nejkratší den a nejdelší noc.

Spojíme-li body slunovratů obloukem jdoucim přes
půl. obdržíme kolur slunovratů, spojíme-li body
rovnodennosti jarní a podzimní obloukem jdoucím přes
pól, máme kolur rovnodennosti. Těmito dvěma
oblouky hvězdné nebe dělí se na čtyři čtvrti, kvadranty,
rovníkem pak na dvě polovice severní a_iížní; i eklip-
tikou hvězdné nebe na dvě polovice se dělí, na severní
a jižní ; ovšem s jinými póly. Kruh rovnoběžný
s rovníkem. procházející bodem slunovratu letního
+230 27', slove obrntník Ruka; kruh rovnoběžný
s rovníkem, procházející bodem slunovratu zimního —
—230 27'—»- slove obratníkem Kozorožce. V krn-
jinách ležících mezi +230 27' severně a 230 27' jižně
od rovníku Slunce do roku po dvě doby v poledne
kolmo stojí nad obzorem, předměty kolmé, jako domy,
věže . . nedávají stínu v poledne. To pásmo nazývá
se horkým.

Kruhy rovnoběžné :; rovníkem, vzdálené od pólů
230 27' nebo od rovníku 660 33'. slevou k r u h y p o l ti r a í,
severní a jižní. Dostoupí-li Slunce v ekliptice bodu
nejnižšího pod rovníkem, kolem 22. prosince —
—230 27' -— na severní polokouli v šířkách od 660 33'
vůbec nevyjde; dostoupí-li nejvyššího bodu v eklip-
tice nad rovníkem, kolem 22. června1 Slunce v těch
krajinách nezajde. Ovšem přiblížíme-li k lomu světla
ve vzduchu, věc poněkud se mění, tak že hranice 690 33'

98
není přesnn, obyvatelům Země o něm severněji Slunce.
když mrímc nejkratší den, přece na obzoru se ukáže,
jako nám ještě svítí,i když vlastně jest již pod obzorem.
Taktéž i v krajinách o něco níže než 660 33' sev. šířky
Slunce za slunovratu letního nezapadá.

Co děje se na severu za našeho slunovratu zim-
ního. to děje se ve stejných šířkách — při —660 33' —
na jižní polokouli za. našeho slunovratu letního: co
děje se na severu zn slunovratu našeho letního, to
děje se na jihu zn našeho slunovratu zimního. Když
na severní polokouli den jest nejdelší, na jižní jest
nejkratší; když na severní polokouli noc jest nejdelší,
na jižní jest nejkratší. Pásma na severní i na jižní
polokouli mezi obratníky a kruhy polárnimi slovou
pásmu mirnú: severní a jižní mirne pásmo; krajiny
ležící severně od kruhu polárního tvoři studené
pásmo se ve rn i. krajiny ležícíjižně od kruhu jižního
polárního činí studené pásmo jižní.

Tuto různost a změna ročních časů nn povrchu
Zemském jest podmíněna tím, že osa zemská skloněna
jest k rovině ekliptiky úhlem 660 33'. Kdyby stála
kolmo na ekliptice. paprsky sluneční dopmlaly by na
rovníku ustavičně kolmo, nebylo by tam jaksi úlevy
nd nesnesitelného vedra; rostlinstvo i živočišstvo by
tím trpělo. V krajinách severních teplota byla by po
všechen čas příliš nízkou, opět v neprospěch orga-
nické přírody. Povrch zemský neměl by lolik výhod
zo Slunce. co jich má nyní. Kdyby osn zemská bylo.
v rovině ekliptiky, Země též by od Slunce neměla
těch výhod tolik, co jich má skutečně.

Planety otáčejíce se kolemlsvých os, budou při-
r02eně rotačními cllipsokly; dráhy jejich s rovinou
dráhy zemské svírají úhel brzy větší. brzy menší,

94

však celkem zůstávají blízko dráhy Země naší. Onen
bod, od kterého dráha zemská vystupuje nad ekliptiku,
jmenuje se uzel vzestupný; ten bod, od kterého
schází pod ekliptiku, sluje uzel sestupný.

I u planet „čili oběžnic — jako u naší Země —
rovina rovníků jejich s rovinou dráhy se protínají;
přímka uzlů jako u Země bude otáčeti se směrem
opačným než koluje planeta kolem Slunce. Změna
ročních časů bude tím patrnější, čím větší jest úhel
sevřený rovinou rovníku a rovinou dráhy planety. —
Viz tabulku »Přehled soustavy sluneční-. ... Rovník
Jupitera skloněn jest k rovině drahy jeho úhlem pouze
30— čili osa jeho tvoří s rovinou dráhy úhel 870,
stojí tedy téměř kolmo na ní, a proto změna ročních
časů na Jupiteru bude nepatrnou. Kdyby tam byli oby-
vatelé na rovníku, měli by Slunce v poledne po celý
rok téměř v nadltlavniku; v poledne nikdy by nestálo
od nadhlavníku dále než o tři stupně. Na obou tučnách
střídavě po dobu našich šesti let (ne úplných) Slunce se
neukáže a opět po šest let s obzoru, nesestoupí, ale
Slunce nevystoupí výše než tři stupně a neklesne níže
pod obzor více než o tři stupně. Kdyby na Jupiteru
byly jako na Zemi vzduch, voda . . . organismy, tož
by tam.nemohlo býti té rozmanitostí jako na Zemi naší.

Osy Merkuru :: Venuše na rovině jejich drah stojí
taktéž téměř kolmo, a nebude tam patrných rozdílů
ročních časů. Osa Saturnova k rovině jeho dráhy má.
sklon 600; proto rozdily ročních časů budou na Saturnu
značnější než na Zemi.

Misto popisu soustavy slunečné sloužižtabulkanastr.
96 a 97, obsahující věci důležité, jež vztahují se k velikosti.
vzdálenosti, hmotností, tíže a pohybu oběžnic. - Věru,
mnoho jich patří pod vládu našeho Slunce, pomyslíme-Ii.

95
ke již přes 510 malých oběžnic jest objeveno v pásmu
vezi Mertem a Jupiterem. Kolem Slunce kolují ještě
velmi četná Vlasatice a velké houfy. mraky malých
těles nebeských — kosmického prachu, létavic, pově-
tz'oňů čilí meteorů.

## O velikosti naší soustavy slunečné.

O velikosti rozměrů v naší soustavě slunečné poučují nás sloupce čísel v tabulce v přehledu soustavy slunečné, avšak sotva dovedeme si představiti tuto velikost; naše síly duševní na to nevystačují. Dovedeme představiti si jen takové velikosti, vzdálenosti, jež jsme semi změřili; jak velikým jest Slunce naše, neumíme si představiti.

Již velikost naší Země, již obýváme, vyniká nad všeliké pomyšlení. Vystoupíme-li na pahorek, s něhož dokola vidíme na 25 km daleko, tož povrch zemský jest aspoň 260.000 krát toliký jako plocha kruhu, jehož poloměr obsahuje 25 km. Kdo nevykonal cesty kolem Země, velikosti její sobě nepředstaví.

Jak veliké jest Slunce, jehož krychlový obsah jest 1,284.000 krát větší než obsah zemský! Do náprstku prostřední velikosti vejde se asi 4.000 makových zrnek,
do litru okrouhle asi milion zrnek vysušených. Abysme aspoň jakés takés ponětí sobě učinili o poměru velikosti Slunce, nikoli o jeho velikosti samé, vysypme litr a třetinu litru maku na veliký bílý stůl, rozdělme zrnka tak, aby nebyla na sobě a pozorujme, co jest zrnko jedno proti tomu množství zrnek maltových!

96
Přehled naší
Í Punk. . Kvychloh) .
I sauna-w mm; ' Nm, : “u“ ' Hmou nutnou!
"er . pvů- 1 mil-ni uuu-i
* roman .:qu „' "“"7 mnou nwm!
v M temu'm "ú" “m“— :::-0 : vody
„ [ ' l " ! !.múým . umim —
. , * 1 ' . '
! Slunce © l1336199010851 Imm: 'msmou' 322.800 1-11
Merkur ; ' 4.8!6 0377 (m 005 I 004 ' 4-85
QDobmpo'm '; ' „
Venuše Q [; Ilam u-mlal 088 * 11-83 ' 0-78 ' 6-34
(Kmnpanh ! . l ' :
Země 5 19.765 100 ; 1-00 ' 1-00 , 1.00 j 5-56
' s měnícomml ] ' ' ' ' _
3.482 wm “0745 nm 3.6 ! 3-56
1
Aslorpld "1 ' ' ] I
Em.-1 , . _ . „
Mars d 1 6.745! 0529 (mu i ma “ 011 , 4-11
' (Smrwncžšg ' . I ' 1
s ? mém“ Piamo planetuid. name—id počtem přes 670 pn'uněm
„ “meho .
1 _ !
Jupiter 9+ 143.757 „11-27 121-2 ' meu-7 ' 008 I 1-30
(Krnlmnocx * .
sc ': měsíci * ' I ; ,
Saturn h '. nam-5- 9—31 nws I sur. ' tv! _ u—so
411mm“ “ . ,
- s 9 měsíci : ' i - .
Uran f MM'oT J'M 20'5 . Íll'EI » lň ' 092'
(Ncbcšfnnh'n ' ' J ]
1 so 4 měsíci j .
-Noptun Y! n.m.“ 4-31 1 11111 Í om ' w * 1-13.
(Vodu) ' _»
i ! měsícem; _ _ , '
360 mm. měří průměr modré plochy mnpy a znázorňuje průmir
Povrch mmsky jest as: 260.000kn'1 větši než plocha kmhu,

91
soustavy slunečné.
W. .
» mmm. minul! . „_4; =““ : ,' u. mnm
*=: . umu Warum: .; 'EŠŽŠ : “3:27“ ' om . mm mm
, „...—..._.._..._,__ _,_l —'-'—'—'
:“:Y ' umu . wu! lšgšš' “'““ yourJŠm' 5:3! Pitt
" r *“ nn.—. IŠŠeĚ ; “““, „m! ="“—
v . : ' . '
17-35. _ , - : - „254.6'.,h' — Šanci — w
, l : ' ' ' I
— oss. “';-.ĚH'“ . 23: . o-asal' 81-9 cl. mm d.ll'95l m !
I r : . , i ' *
, ooo _' 108-3. 106-7' 957 * 0123: u; »? 'maoma.: ;; ! 23
„o;-5.40! . 1 .. A
. u . 1m... 146-2. 1 123h “m'a-ma., 3-3, 39  
, : 1486 » . si s ' , ,
. (He 884.115'5km.odumč1 127mm manny 0.9 01 *
,   ' 4am1l-ss'43mn—aas:
1 20921. lvuóťi- .47; 1459 ' ,““"m'nel ;
17- A I , - I *
l ' „
, 0'69 ' 947-6. 205—4. rm ' 1-52 '24 unum. 321.13 1.7! 59
l ' l
, 220.5 . '51 , - j '
nd 12 km. — Viktorin — až 804 km. — . Ceres - -. mnků kosmického
nmovných vlasallc.
: 2—24 If 810463359: $? ' 5-20 lm. na m "llr.315d.i 37 i m *
l 7 ' , l . I .
; „ I , o . -
' ( * 0
' ass Mamma-an mo w wu 'thQQm 20r.l67d.i al * m
nm; » 1190 ' ' '
' » ' ; ' . . ' :
l .
' 0-9 lm'mm-g' 8182 . 19-2 : . 184 t. u.! u l na
. ! sam-a . 2570 . ' . ; :
. , 1 . I
: .. lmgěaťws; ŽŠ? ' sms ; .. '_mr.mu.', m ' nes
. 7- I *
l l 1 I I
c . ,
slunce na rovnlku. »
jnhož poloměr má délky 26 km.
7

98

To jest poměr velikosti Země proti Slunci; Země mizl
a neni téměř ničím. tn Země, jejiž velikost si dovede
představili jen ten. kdo by ji obešel pěšky. kdyby
to bylo možné! Kdyby někdo ušetdenně 35 km.
vykonal by cestu kolem Slunce za 3-10 roků, kolem
Země by obešel zn 3 roky 45 dní.

lx'rychlový obsah Slunce zhruba obnáší 14 trillionů
krychlových kilometrů. Zmenšíme-li měřítko tak, že by
krychlový kilometr znázorněn byl zrnkem makovým.
tož Slunce obsahuje tolik krychlových kilometrů. kolik
makových zrnek by se vešlo do jámy 9.410 m dlouhé.
tolikež Široké i hluboké. Kdo si to představí?

Mnjíc tolik hmoly. Slunce silněji přituhuje k sobě
tělesa na svém povrchu. Tělo naše nn Slunci bylo by
28 krát těžší než nn Zemi, síla naše nestačila by
udrželi tělo na nohou: museli bychom býti sestrojení
jinak :: přizpůsobeni ke hmotě Slunce.

Jak nemůžeme sobč předstih-ití velikosti Slunce.
tak nemužeme sobě předstnvíti ani jeho vzdálenosti od
naší Země: ta vynikii nad naši chápavost, jen poměr
vzdálenosti jakžtakž si představujeme, nebo lépe řečeno.
znázmňujeme. Střední vzdálenost Země od Slunce
ohmiší I48'6 milionů kilometrů. Měsíc jest vzdálen
384.415 kilometrů. Kdyby rychlovlak urazil za hodinu
70 kilometrů, dojel by k nám se Slunw za 241 roků
ustavičně. jízdy; dělová koule letic rychlosti 500 m
za vteřinu. doletěln by se Slunce k nám až za deset
let, 3 měsíce a 9 dní. Nejvzdálenějši dosud známá
oběžnice Neptun jest třicet n. půlkrát dále od Slunce než
naše Země. Dělová koule vystřelená rychlosti 500 m za
vteřinu doletěla by za víc než 313 let k Neptunu, vlak je—
doucí 70 kilometrů za hodinu. urazil by tu cestu za
7.245 roků. Jakkoli rozměry v naší soustavě slunečné

99
nám jsou nepředstavitelny. neobsažitetny naši obraze.
tvornosti, tož přece opět mizí proti vzdálenostem stálic
od Slunce našeho, od naší soustavy slunečné. Zmi-
zornime-li vzdálenost Země od SlunCe přímkou 4 cm
dlouhou, tož nejbližší stálice u nás viditelná, Sirius,
byla by téměř 22 km. Polárka 109 km daleko. Střední
vzdálenost Země od Slunce jest 148'0 milionů km,
Sirius jest 542.000 krát, Polr'trkn 2695300 krát dále od
něho; a což teprv jiné hvězdy! Dlužno poznmncnati'.
že vzdálenosti hvězd stálic. pokud vůbec mohly býti
počítány. počtem asi 50. ba snad jen 19, nejsou určeny
přesně. údaje i značně se různí.

Podobně nemůžeme si předstm'iti množství prám,
jakou vykonává Slunce jen na umi. Všechen vzrůst.
všechen život organický. všechen život na zemi svůj
původ hore ze Slunce, Kolik pn'tCe jest obsaženo ve
vodě zdvižené do vzduchu jako páry. kteréž srážejí
se :! padají na zemí jako déšť. jimž napt'tjeji se prn-
mcny ! Vodu nespotřebovnnú odtéká v potocích a řekách;
jen Labem z Cech odtéká ročně průměrně 10.000 mil.
krychlových- metrů vody. Pn'tci můžeme převésti na
teplo a teplo na práci, takže 424 jednotek prace, totiž
práce, kterou vykom'tme, zvedneme-li 424 kg na metr
vysoko, dn jednotku tepla. t.j. tolik tepla, jimž by
litr vody ohřál se o jeden stupeň. Ocel vyrábí se ze
železa. Ve velké hruškovité nádobě, zvané »konverterc,
tekutým železem prohání se moený proud vyhřátého
vzduchu: teplota v nádobě stoupá až na 2000 stupňů.
Nahýbánim konvent,-ru pak tekutina se vylévá: jeji
oslňující záře může se porovnávati jen se záři slu—
neční. Zhavé tekuté železo samo o sobě jsouc pozo-
rováno, září až oslňuje. na př. hledíme-li naň, když
vypouští se ; poco. ale proti tekuté oceli, vylité
?. konvertoru jest tmavohnědá a vypadá jako černá.

7.

100

ka'va proti bílému šálku; jest to účinek protivy, kon-
trasku. Platina taví se nejen dána jsouc do této ocele
tekuté, nýbrž i držena jsouc nad ní ve vzdálenosti
metru; tolik tepla vyzařuje! Však měřením shledáno,
že stejně velká plochu povrchu slunečního v téže době
vyzáři tepla nejméně 87 už 100 krát tolik. Čím výše
stoupá temperature čili teplote tělesa, tim výše stoupá
počet vln, s kratšími v'chvěvy, způsobujících světlo.
Je-li teplota Slunce vyšší než teplota tekuté ocele, bude
světlo jeho mnohem mocnější než světlost ocele tekuté.
Langley měřenlm zjistil, že stejná plocha povrchu
slunečního v téže době vyzúří nejméně 5.800 krát tolik
světla jako tekutá ocel.

## O teple slunečním.

Jaká jest teplota Slunce, není dosud určeno, neboť neznáme ještě zákona, jak souvisí teplota tělesa s vyzařováním tepla za teploty neobyčejně vysoké. Různí badatelé různě ji udávají, však v tom se shodovali, že teplota Slunce prý není menší než 20.000°. Naproti
tomu Dr. J. Scheiner na observatoři v Postupimi dochází k výsledku, že teplota Slunce na povrchu jest mezi 6000° a 8000° C. Plocha jeden čtvereční centimetr velká, sotva taková co nehet na palci, v místech nad Zemí, kde teprv počíná vzduch, kolmo postavená na paprsky sluneční, zachytí za minutu tolik tepla, co by postačilo, aby jim tři gramy vody ohřály se o jeden stupeň. To se jmenuje sluneční konstantou (stálou veličinou). Vzduch pohlcuje asi třetinu toho tepla.

Země za minutu obdrží od Slunce 3.581 billionů kalorií čili takové množství tepla, jimž by se ohřála

101

o jeden stupeň spousta vody, kteráž by vyplnila 3.581 krychlových kilometrů; za rok činilo by to 1583 trillíonů kalorií a dostačilo by, aby jim roztála ledová kůra tlustá 464 m, obalující celou zeměkouli. Údaje tepla Sluncem vyzařovaného různí se značně u přírodozpytců; teplo Zemí přijaté páčí se i na takové množství, že by kůra ledová dle jedněch 31 m, dle jiných až 50 m tlustá za rok roztála; proto i ostatní údaje jsou pouze odhady, jež mají pro sebe jen pravděpodobnost.

Ačkoliv toto množství tepla, kteréž Země ročně přijímá od Slunce, jest nám nepředstavitelné, tož přece všechno to teplo jest pranepatrnou částí tepla, jež Slunce vůbec vyzařuje do prostoru světového. Země z tohoto množství tepla zachytí asi jen 2.200 miliontou část. Množství tepla Sluncem za minutu vyzářeného jest tak veliké, že by jím spousta vody pětkrát až sedmkrát tak veliká jako naše Země ohřála se o jeden stupeň. Kdyby Slunce bylo z nejlepšího kamenného uhlí, jehož kilogram, když shoří, ohřál by 7.600 litrů vody o jeden stupeň, tož by prý shořelo za 3.560 let,
vydávajíc denně tolik tepla, co by se ho vyvinulo spálením tukové koule uhlí, jak velká jest naše Země. Dle jiných shořelo by teprv za 26.000 let.

Jak dlouho již Slunce hoří, nevíme; jsou to miliony roků. Thomson myslí, že Slunce nezáří déle než 20 milionů let a že bude ještě zářiti pět až šest milionů roků.

Kdo dovede říci, jak dlouho může ještě zářiti, povážíme-li, že novými objevy o záření hmot starší názory se převracejí. Kdo dovede představiti si tu práci, kterouž Slunce již vykonalo a ještě vykoná!

102

Kdyby Země obklopena byla hmotou nějaké plynové povahy (nemyslím na vzduch, jímž skutečně jest obalena) a obal ten sahal mnohem dále než k Měsíci, lomil paprsky světla i tepla k Zemi, tož Země by zachycovala mnohem větší plochou paprsky sluneční, než obnáší její největší průřez; pak by teplota i světlost Slunce nemusela býti tak velkou, a též množství tepla vyzařovaného Sluncem mohlo by býti menším než nahoře uvedeno, ale bylo by vždy ještě toliké, že žádná lidská obrazotvornost není s to, aby si je nějak představila.

A co bylo by říci o délce času? Staří naší Země není dosud určeno, ale jmenují se miliony let; jak dlouho ještě potrvá, nikdo neví. V prostoru světovém spatřujeme vývin těles nebeských, jejich vyspělost, sešlost a zánik jako ve světě organickém; Země jest sice ohromné těleso, ale proti Slunci téměř mizí. Čím asi bude její stáří proti staří Slunce? Čím bude stáří naší soustavy slunečné proti stáří oné soustavy sluncí, jejíž částí jest soustava naše! Asi tím, čím jest stáří stromu proti Stáří lesu. A čím bude stáří opět této soustavy proti stáří kosmického světa námi viditelného? A jest to již všecko co vidíme? Čím bude teprv stáří Země proti stáří světa viditelného? I ty miliony let jejího trvání mizí. Dle nynějších názorů theorie Kantovy a Laplaceovy (však činí se proti ní námitky) země původ vzala ze Slunce — byla ohnivá — a opět do něho se vrátí — vychladlá, zčernalá —

»Děj Země je krátce jen vyprávěn,
jak píseň se krátce skládá;
vylétla jiskřička z plamenu
a zčernalá zpět zas padá.«

Jan Neruda.

Našich historických šest tisíc let jest jen okamži-
kem. O, věčnosti, zdaž tě netušímeř Kym by neotřásla
výpověď Kristova: »Nebe u země pominou, ale slovu.
má nepomínoulc
Však obratme se opět ke Slunci, nejsme s ním
hotovi. Tak ohromné vydáni tepla Slunci nějakým
způsobem aspoň částečně musí býti nahrazováno. To
může se dítí rozmanitým způsobem. Teplo spalováním
vyvozené stačilo by poměrně jen nn krátkou dobu.
Hlavní zdroj tepla pro Slunce může záležeti ve změně
energie pohybu v teplo. 'I'lučeme—lí kladivem na ko-
vadlinu,i kladivo i kovndlina se oteplí, Energie pohybu
soustředěná v kludivč v tom okamžiku. kdy má se
dotknouti kovaclliny, přechází v energii jiného způsobu,
v teplo, když pohyb celku byl nerezem zrušen. Ná-
rnzem ocele na křemen. vyvozuje se teplo, jež dostačuje.
aby pranepatrné částečky otíené se rozežhnvěly a byly
viditelnými jako jiskry. I.itr vody pohybující se rychlostí
lll m za vteřinu má energie za jednu kalorií, ohřál
by se totiž o jeden stupeň, kdyby nárazem na pevnou
plochu pobyb se zrušil a energie všechna proměnila
se v teplo. To jest ovšem dosti malo: ale energie roste
se čtvercem rychlosti. tak že bude čtyřikrát. devětkrát
. . . stokrát. deset tisíckrát . . . větší. bude-li rychlost
tělesa v pohybu dvakrát. třikrát . . . desetkrát. stokrát
větší. Množství tepla nárazem vvvozeného roste pak
jako energie. Kdyby litr vody pohyboval se rychlostí
9200 m za vteřinu. nárazem jeho by se vyvodilo teplu.
již okrouhle 10.000 kalorií. tedy více než dá kilogram
nejlepšího uhlí. Na povrchu zemském člověk silami,
jakýmiž vládne, tak velkých rychlostí nedocílí. ale
v prostoru světovém tělesa pohybují se rychlostmi
nepoměrně většími. Země naše urazí za tveí'inu asi

104

80.000 metrů! Metcory. malá tělíska - o nichž dále
bude řeč — pohybují se, přicházejíce do ovzduší zem—
ského. rychlostí 15.000 až 75.000 metrů za vteřinu.
Kdyby tělísko takové bylo jeden kilogram těžké a
všechen pohyb jeho byl odporem vzduchu zrušen.
tedy by energie pohybu jeho téměř 11V., milionů až
286% milionů kilogramometrít proměnila se v energii
způsobu jiného — v teplo, jímž hy ohřál přes 70.000
už 1.768.UOO litrů vody ojet-len stupeň: tolik teple by
postačilo. aby těleso jím proměnilo se v páru.

Dopadnjí-li tělesa. různé velikosti do Slunce. tož
dopadají ohromnými rychlostmi, u jejich energie po-
hybu mění se v teplo. Na Zemi tělísek nebeských,
těch kosmických prášků. — z nichž některé i tisíce
kilogramů váží, — dopadá ročně množství nad pomy—
šlení veliké, nn Slunce ohromně velké, možmi že
dopadů jich opět teprv ohromné množství, jež pozbý-
vnjicc energie pohybu. přivodí teplo.

To může býti jen časti náhrady teplu Sluncem
vyzářeného. Aby tím způsobem nahrazováno bylo
Slunci všechno teplo vy.—udané do prostoru světového.
bylo by potřebí, aby na každý čtt'crečný metr povrchu
slunečního každou vteřinu dopadly 3 gramy hmoty.
čili za rok usi šestnšcdesátina hmoty Země. Přitažlivost
Slunce sahá chile než přitažlivost naší Země; kdyby
tolik hmoty bylo v oblasti sluneční, muselo by i na
Zemi dopndnti nepoměrně více meteorů. než skutečně
dopadá. hiumson vypočetl, že by podáním těch mele
oru povrch Země se ohřál až po bod varu. l'nk Slunce
by nabývalo příliš hmoty a tím i přitažlivosti: oběh
Země byl by kratší a dalo by se poznnti již za rok,
že jest kratším; avšak ze zpráv Číňanů a zatmění
Slunce spíše dá se souditi, že doba oběhu Země se
zvětšila.

1%
Slunci muže teplo vysálané byti nahrazováno též
xznršt'ováním se. Smčstnáme-li hmotu na menší prostor.
Hhřít'á se: peníze vvpnduvajici ?: lisu, v němž se razí.
jsou horké: vzduch naihlc sražený na malý prostor
ohřeje se. až i hubkn křesnci v něm se vzujme.
Kdyby pnmčr Slunce v roce zmenšil se u 75 m,
v_i-vodila) by se tolik tepla, že by všechno teplo vyzn-
iovuné se hradilo. Se Země by zmenšení Slunce mohlo
spí-zorovnti se sotvn za 10.000 let: nbsuhnmlnť by
při vzdálenosti Země od Slunce l—lt—I'G milionu kilom.
:tsi jednu obloukovou vtciinu. — Však za pět milionů
let, kdyby všechnu tenlo smršťnvánim se Slunce mělo
býti hrazeno. jukž vypočetl Newcomb. průměr Slunce
by se musel zmenšiti o polovici. [ m prin-dě jest
nepodobno. Siemens myslí. Že všechen prostor své-
tový just vyvlnčn pammi u plyny nud míru zředěnými,
Že tyto vymiřm'unvm teplem slunečním se rozkládají
v prvky. otáčením se Sluncí.- kolelu csy ilo polnrnich
krajin jeho proudí, na povrchu slunečním se spalují
(slučují) a silou odstředivou v krajinach rovníkových
opět do prostoru světového jsou vrhány. Sluricc tedy
z prostoru-světového na točnách svých hmotu jako
by sszílo do sebe a na rovníku _ii ze sebe chrlila.
Myšlenku tu měl dříve již slavný Descartes: jsou to
jeho víry v prostoru světovém. — 1 proti domněnce
Simensově jsou závažné námitky. Teplota povrchu
slunečního mohla by býti dle Siemensu nanejvýš 30000.
Náhradu tepla může se dili i dopadaíním těles na
Slunce i jeho se smršťoviiním i ještě jiným způsobem.
třebas dosud neznámým. —» Jest i zde pole pro objevy
ještě široké.

106
O světle slunečním.

Však Slunce jest zdrojem i světla. Nejsvětlejší
světlo. jež íysikové dovedou vyvoditi, jest tak zvané
světla Drummondovo a mocnější tohoto světlo elek—
trické obloukové lnmpy: dle Foucaulta a Fizcaua
Slunce jest 1-16 krát světlejší než světlo Drummonduvo
». čtyřikrát světlejší než elektrické světlo obloukm'é.
Dle Bouguern :: Welles-tunu dalo by 60.001) svíček
normálních ve vzdálenosti jednoho metru tolik světlu
jako Slunce. dle novějších výzkumů by těch svíček
muselo býti 85.000.

Je-li pmmen světla dvakrát, třikrát, desetkrát.
tisíckrát vzdálenější. světlo jeho jest čtyřikrát. devětkrát.
stok-mt. milionkmt slabší: bude-Ii [494me mÍlÍOI'lkl'til
dále — jako umi—e Slunce — bude světlost jeho také
149.0!NJ,UUU.UUU krát slabší: má-li však dáli tolik světla
co 61mm) nebo 85.000 svíci ve vzdálenosti jednoho
memu, :( olmmu-li Slunce nahradili svícemi, museli
bychom nn jeho místu dnti 1.330 kvadrilliunu nebo
1.890 kvadrílliunú'svíček, t. j.- číslo 1.330 nebo 1.890
so 24 nullnmí. Cžslu utkat-u, lépe řečeno mnohost
takovou, sobě.- přudsmviti nedovede žádný lidský duch.
Vezmeme-li přlbllžl'lf: průměr těchto čísel, obdržíme
1.600 kvudrilliunů. Za srovnat-aci svíčku slouží svíčku
z velrybího tuku [U“ gr těžká. 7. níž shui-í za hodinu
7-77 gr. Obyčejný hořák plynový ztráví za hodinu půl
krychlového metru dobrého plynu; na místě Slunce
bylo by potřebí asi 100 kvadrillionů takových plynových
světel, aby daly tolik světla, kolik ho máme od Slunce.

Světlost těles žhavých na zemi tím jest větší, čím
teplota žhavého tělesa jest vyšší: dn tekutého železa

107
sotva můžeme hleděli, tekutti ocel, jejíž teplota přesa-
huje 2000 stupňů tepla, nás oslňuje; světlost její —
jak již nahoře bylo zmíněno „ převyšuje světlost
železa roztaveného, že toto vedle ní jest jako černá
knva v bílém štítku. Langley porovnával množství vy-
zuřeného světlu slunečního s vyzářeným světlem tekuté
ncele a našel, že čtverečný metr povrchu slunečního
vyzařuje více než 6.300 krát více světla než stejná.
pluchu tekuté ocele. Dle toho teplota Slunce by musela
hytí ohromná. Však není dosud známo, jak přibývá
zářivosti s teplotou, přesahuje-li meze nám známé.

Ale nevšechno vyzářené světlo přichází až na
povrch zemský, mnoho pohlcuje vzduch, plyny a páry
vodní. ve vzduchu obsažené: čím tlustší vrstvou
vzduchu světlo prochází, tím více se ho ztrácí ve
vzduchu, t. j. když Slunce není vysoko nad obzorem:
mno. večer, v zimě i při deváté hodině ranní a při
třetí odpoledne. Slunce ucházející a zapadající, taktéž
í Měsíc a hvězdy vycházející a zapadající jsou zbarveny
du červena, poněvadž paprsky jejich procházejí tlustou
vrstvou vzduchu, naplněného nad obzorem vodními
parami. jež popouštějí světlo hlavně červené. Zdaji se
nam býti též mnohem většími a dále, než jakými se
nam jeví. když vystoupily výše. Hledime k nim přes
mnoho předmětů za sebou, dle této mnohosti ceníme
vzdálenost: čím více vidíme předmětů mezi sebou
a věcí. kterouž pozorujeme, tím vzdálenější zdá ae
bytí. Věc čím jest vzdálenější. tím zdá se býti menší:
když pak předmět. ač jej vidíme velmi vzdáleným,
piece proti jiným předmělum. jejichž rozsahy známe,
vidíme tak a tak velikým. tož jemu ihned přisuzujeme
poměrnou velikost 11 vidíme jej i velikým. chdíme-li
ke Slunci nebo k Měsíci vysoko na nebi, nevidíme

ICB

mezi sebou a těmito tělesy žádných předmětů, zdají
se nám býti blíže a přisuzujeme jim menší rozsah.
Měřidly bychom se přesvědčili, že průměr Slunce vy-
cházejícího jest toliký jako průměr Slunce vrcholícího.

Že Slunce nebo Měsíc v úplňku, když vycházi
nebo když zapadá. zde se byti širší než vyšší, jest
lomem světla v ovzduší naplnčném parami; jim též
se děje, že vidíme je dříve, než vystoupi nad obzor
a vidíme je, i když sklesly pod obzor. Odchylka pa-
prsku od přímé čúry tím jest větší. čím tlustší vrstev
probíhá: tedy na obzoru. Paprsek s vrchního kraje
Slunce nebo Měsíce vycházejícího vyše jest nad
obzorem než paprsek s pravého a levého kraje, a pa-
prsky s těchto krajů opět výše než paprsky s kraje
spodního; účinek toho jest. že Slunce a Měsíc, když
vyeh'ázejí nebo když zapadají, zdaji se býti širší než
vysm.

Světlost Slunce v prostředku jeho jest největší,
ke krajům svčtlosti jeho ubývá: Pickoring shledal. že
světlost na okraji Slunce činí něco více než třetinu
svčtlosti uprostřed. Příčinou toho just, že paprsky při-
cházející z okraje k nam musí projití tlustší vrstvou
sluneční atmosféry, v níž bývají pohlcovany než pa-
prsky vycházející ze středu povrchu slunečního. Kdyby
Slunce nemělo atmosféry — obal jeho jest ovšem
jiného druhu než jest atmosféra Země _ světlost jeho
dle Pickcringa byla by 4'l, krát větší než je skutečně.
Různé autority udávají různé množství světla pohlce-
ného atmosférou sluneční: da se jen souditi, že Slunce
bylo by mnohem světlejší, kdyby nemělo atmosféry,
též mělo by jinou barvu: Langley soudí, že Slunce
bylo by modré.

109
0 skvrnách na Slunci.

Oku neozbrojenému Slunce zdá se býti obyčejné
beze vší skvrny. Ovšem v různých dobách víděny na
Slunci skvrny i pouhým okem, první pozorovatelé
jich soudili, že jsou to oběžnice procházející před
Sluncem. I—Iledime-li však na Slunce jasné mocným
dalekohledem,“ vidime povrch jeho zrnitý; obyčejnými
dalekohledy spozorujeme na Slunci od času k času
tn menší, tu větší množství skvrn černých drobných
i větších. Některé z nich trvají delší dobu, měsíce.
jiné mizí v málo dnech a všelíjnk se mění; častější
jsou v krajinách rovníkovýóh než v i krajinách od
rovníku vzdálených. lllc těch, které trvají dlouho,
možno poznnti. že Slunce otáčí se kolem své osy,
protože skvrna taková zjeví se na okraji. postupuje
!( prostředku a mizí na kraji protějším. Slunce otočí
se kolem své osy asi za 25']. dne. V nejnovější době
odvozeno ze spektroskopického pozorování. že části
Slunce blízko rovníku otočí se kolem osy za 24-37 dne,
v šířce 750 za 33'66 dne.

Kdyby průměr skvrny obníišcl jen obloukovou
vteřinu, skvrna jevila by se našemu oku jen jako
malinká tečku, sotva spatřítelná, však na Sluncí by
průměr jeji obnášcl 700 km a plocha jeji hy měřila
385.000 čtverečních km. Tak nepatrnými skvrnami
Slunce jest jako poseto; hvězdáři jmenují jo průlinky
— póry. Tv, jež hvězdúři jmenují skvrny, mnjí průměr
mnohem větší: byly pozorovány skvrny průměru až
302", tedy na Slunci skutečný průměr její měřil
211.400 km a měla plochu asi 69 krát větší než jest
povrch zemský dne 6. září r. ISBD. Skvrny na Sluncí,

110

jejíchž plocha rovná se povrchu zemskému, nejsou
řídké, ba i takové často se vyskytují. jejichž plocha
jest už. devětkrát větší než povrch zemský. Někdy
objevují se celé skupiny skvrn, kolem skvrny větší
a všechny jako by měly jen jednu společnou ohmdu.
Roku l847 povstala taková skupina. jejiž průměr se
zmenšil 5014. na 16. červnu s 668" na 474“, zmenšil
se tedy v těch dvou dnech o 136.000 km, denně 0
68.000 km. kdežto největší rychlost zemské vichřice
obnáší 3000 km za den, to činí z hruba 35 m za
vteřinu; avšak rychlost. s jnkou zmenšoval se průměr
skupiny skvrn. obnášela 787 m. byla tedy asi 23 krat
větší. větší ještě než rychlost dělové koule. S větší
ještě rychlostí vzrostl průměr velké skvrny r. 1850:
se 4. nn 5.'zítří vzrostl s 93" na 302". přibylo ho
140.000 km, a její plocha vzrostla s 3.300 milionů na
35.100 milionů čtverečních km; rychlost tn obnáší
téměř 1700 m za vteřinu!

Podoba skvrny samé být-tí obyčejně kruhovité:
když vzniká nebo zanikli. nepmvídelná. Na pohled
skvrna jest černá nebo tmavá. íúdro jakoby „licen ně-
jaké propustí. obklopeno jest pole-stínem méně tmavým
paprskovitým, jakoby skltíclnl se z vláken vinicích na
způsob křovin na okraji jícnu propasti, jnsnější jest
blíže zrna. temnější blíže fotosféry: na okraji polostínu
obyčejně jest viděti jasnější ohraničení, nepravidelné
pruhy větší jasnosti, nenepodobné vločkítm pěny.
plovoucím na vodě pod vodopádem: to jsou pochod'nč.

Hledíme-li na skvrny mocným dalekohledem, velmi
často vidíme podobu jejich závitovitou, spirálnou, jako
by to byly ohromné kyklony na povrchu slunečním.

Od času k času bývá. různé množství skvrn
viditelne: hvězdář-i poznali. že jsou jístá— období. v nichž

lll
skvrn vyskytá se nejvice, pnk jich ubývá, až jes! jich
nejméně: po té jich opět přibývu a sice rychleji než
_ilCh ubývalo. Zniímavo jest. že jeví se v mm jakási
pravidelnost, pcriodíčnost: průměrná. dobu. obnáší
Il'l, roku. však může býti delší nebo i kratší. Kromě
těchto období krátkých jsou ještě období další, hlavní
po 220. 110 a lil-5 letech, kdy skvrn nn Slunci zvlášlě
mnoho se vyskytuje.

S největším množstvím skvrn na Slunci vyskylují
se na Zemi nejvetší poruchy magnetické. — tak roku
I859 dne 1. zni-i nastala magnelícká bouře na celé
Zemi. že nebylo možno telegrufovmi, podobné i dne
31. října r. 1903 telem—nfovuli nebylo možno ve velké
čásli Evropy. ., největší severní záře a největší množství
vody — povodní.-.

Tak celková povaha. povětří opakuje se po lll) už
112 letech. Větší množství vody obyčejně vyskytuje
se po maximum skvrn na Slunci. Tak možno poznnti.
že skvrny na Slunci. poruchy magnetismu zemského.
severní záře a občasné povodně mají společnou při.
činu komnickou snad v obdobné změně postavení
nběžnic k sobě a ke Sluncí. — Přecházi-li velká skvrna
nn kotouči slunečním a zjeví-li se současně porušení
zemského magnetismu. jes! znamením, že na skvrnč
tě a v jejím okolí dějí se neobyčejné úkazy. výbuchy
na Slunci. . . .: jestliže však přechod takový není
provázen porušením zemského nmgnctismu, přechází
přes kotouč sluneční skvrna nebo skupina skvrn ve
stavu klidu. Tnk soudí Tacchini.

Jak již pověděno, kolem polostínu skvrny zjevují
se na Sluncí ještě jasnější mísm, jimiž polostín jest
ohrann; zvláště však na okraji slunečním viděny bý-
vaji jako vyvýšeniny hmoty světlejší nad okoli ujme-
nují se pochodně (lukule):jejich rozměry jsou různá.

112

někdy ohromné, měří délky “LOGO—30.000 km a plochy
jejich jsou větší než i největší díly naší Země. Jest
to vyvýšenú hmota povrchu slunečního; vyčnívá-li
jen jednu obloukovou vteřinu nad své okolí, měří
výšky již přes 700 km, jest pak 76 krát vyšší než
Himaluja.

l tyto pochodně vynikají někdy náhle a zanikají,
jsou svědky prudkých výbuchů a bouří na Slunci.
R. l850 dne 1. září pozorován náhlý vznik takovych
dvou pochodní v rozsáhlé skupině skvrn; pochodně
trvaly jen pět mírnit a urazily za tu dobu 56.000 km,
tedy 186 km za vteřinu!

Jakým jeví se okraj Slunce.

Za úplného zatmění Slunce spatřuje se na okraji
Měsíce — jenž nám zakrývá Slunce — světlá zář
paprskovitá, jest to koruna. Část vnitřní jest ban-y
růžové a jmenuje se chromosférn, pod ní jest f oto-
sféra sluneční. Z chromosféry jakoby vytryskovaly
paprsky žhavých plynů a obklopují Slunce jako vysoké
plameny velkého požáru větrem zmítané; to jsou
protubernnce. Jejich barva bývá růžová až do
tmavn, též línlowi, žlutavá i bílá. černě vroubená.
Nezřídka vyčnívají nad kraj Měsíce.až 2' ba 27; ba
i až 4', tak že vyšlehují do výše 87.000. 108.000 až
i 173.000 km. Secchínlu zdálo se, když r. 1860
pozoroval zatmění Slunce, jakoby za Měsícem celé
Slunce bylo v plamenech. Roku 1880 dne 30. srpna
byly pozorovány protnberance paprskovitě, kteréž od
11 h do 12 h 45 m dosáhly výše 8'4', !. j. 364.000
km a téhož roku dne 7. října vystoupila protuborance

118
až do výše 13', t. j. 563.000 km. Jsou-li to výbuchy
plynů : cbromosféry, tož dějí se rychlosti několika
set kilometrů za vteřinu. Jaké sily pak pracují na.
povrchu slunečním! Cornu myslí, že protuberance
nejsou tak pohybem, vymršťoxénim hmoty — plynů —
jako spíše zjevy světelné po způsobě záření elektri-
ckého podobně jako za' severni záře. Však vidme
chromosfe'ry a prombemncí mají společné čtyři světlé
linie. z nichž tři náleží vodíku, čtvrtá hmotě, jménem
helium, již na zemi dosud nenalezeno. To by bylo
důkazem, že proluberance jsou vyvržené plyny z chro-
mosféry. výbuchy vlastní byly by ve vrstvě pod chro-
mosférou, jimiž by plyny chromosféry, zvláště vodíku,
vrhány byly do atmosféry sluneční.

Čímž by byly nejmocnější kyklony a výbuchy na
povrchu zemském proti kyklonům a výbuchům na
Slunci!

.. i
i

0 podstatě Slunce :: úkazů na Sluncí — skvrny,
pochodně. proluberancc — hvězdáři stejně nesoudi;
jedni pokládají skvrny za otvory ve fotosféře, jiní
myslí, že skvrny jsou ve fotosféře, jiní Opět myslí, že
skvrny náleží ovzduší slunečnímu. Slunce že jest ohnivá.
koule o žáru co nejvyšším, obklopená ovzduším po-
dobně jako Země. jen z Jiného chemického složení,
že v ovzduší slunečním mistni schlndnulí způsobuje,
že tvoří se mraky, ovšem jiného druhu než jsou v našem
vzduchu a tak že povstávaji skvrny. — Věc není dosud
řádně vysvětlena. — Rozeznáváme jádro Slunce, hmotu
asi žhavou, tekutou, obalenou plyny: fotosférou, chro-
mosférou, korunou, ovzduším. Prvky, z nichž skládá
se Země, jsou i v tělesu slunečním.

8

114
Přltažllvost Slunce.

Ještě i jinak Slunce působí na Zemi: přitahuje ji
k sobě a tím se stává, že Země od Slunce nad jistou
míru nemůže se vzdáliti. Kdyby přitažlivost pominula
a Země od Slunce přece neměla se vzdáliti, bylo by
potřebí, aby Země ke Slunci připoutána byla dráty
tlustými, jak jsou dráty telegrafní a na každý čtvereční
centimetr polokoule zemské připadal jeden. Síle. tato
drží rovnováhu síle odstředivé, kterou těleso — zde
Země — kroužící kolem jiného snaží se vzdáliti od
toho středu — od Slunce. Kdyby pominula přitažli-
vost mezi Zemi a Sluncem, Země vzdálila by se do
prostoru světového; kdyby pominula odstredívost,
Země by spadla do Slunce.

## Měsíc.

Údaje o Měsíci: jeho velikost, hmotnost, oběh . . . obsahuje tabulka soustavy sluneční.

Po Slunci pro obyvatele zemské nejdůležitějším tělesem nebeským jest Měsíc a jmenujeme jej vždy hned vedle Slunce, říkáme na př. Slunce, Měsíc a hvězdy. Pro svoji skutečnou velikost nezasluhuje místa hned vedle Slunce, nýbrž jen pro svoji zdánlivou velikost, tou člověku imponuje: lidé po zevnějšku — dle zdání — posuzují, a svět obyčejně netáže se po skutečné velikosti; ten jest mu větším pánem, kdo umí objeviti se ve větším lesku, třebas vypůjčeném.

115

Měsíc proti Slunci není téměř ničím. Krychlový obsah Slunce 64 milionkrát větší než krychlový obsah Měsíce a hmoty Slunce má 25,800.000 tolik jako Měsíc; kdo by to po zevnějšku jeho tušil? A přece Měsíc jest po Slunci nám hvězdou nejdůležitější. To činí
jeho blízkost. Ať byl nepatrným proti Slunci, nám koná služby velmi platné: jest hvězdou básníků, zamilovaných; tisíce tisíců lidí k němu vzhlížejí hledajíce útěchy a klidu mysli; on jest lampou noci, vůdcem nočních chodců a zbloudilých. Postaví-li se i nepatrný sluha mezi nás a pána, může nás zbaviti pohledu jeho
obličeje — a přízně, a tak i Měsíc postaví-li se mezi Zemi a Slunce, zastře nám jeho tvář — zatmění Slunce — a způsobuje lidem zděšení — jak víme z dob dřívějších. Dobře praví české přísloví: »Lepší dobrý soused než vzdálený přítel«. V praktickém životě nehledmo tak na velikost lidí jako na to, zdaž nám mohou prokázati nějakých služeb anebo mohou-li nám škoditi, třebas byli nepatrnými proti jiným. Ale přestaňme uvažovoti a zabývejme se Měsícem jakožto tělesem nebeským.

Za úplňku pouhým okem pozorujeme na Měsíci větší i menší místa méně osvětlená; lidská obrazotvornost sestrojila si na Měsíci různě podoby i lidskou tvář, muže s otepí dříví, dvě hlavy lidské, mužskou a ženskou, jež se líbají (kukátkem hleděno), sv. David s harfou, Jidáš, antilopa aj. a rozumí se, že přibásnila
k těm tvarům patřičný text.

Správný názor o Měsíci, že jest vlastní těleso nebeské s kopci, horami, rovinami a údolími, měl již starý Anaxagoras, onen mudřec, o němž Sokrates pravil, že mezi svými vrstevníky byl jako bdící mezi snícími, jenž pravil, že v chaos světový, hmotu netvámou,

8*

116

pořádek uvedl rozum, νους, bytost rozumová, kdežto materialisté starověcí, tak jako materialisté naši, tvrdili, že náhodou ze hmoty svět se vytvořil. Podivno, že právě ti veleduchové, kteří ve vědě nejpřísnější, v mathematice (i ve fysice) vyznamenávali se neobyčejnou bystrostí ducha, zavrhují domněnku, že by svět byl mohl povstati náhodou ze hmotných částí pohybujících se všemi směry; k těmto mužům náleží Pythagoras, Descartes, Newton, Leibnitz, Gauβ a j.

Pohledíme-li na Měsíc dalekohledem, třebas jen obyčejným, jaký najdeme v každém lepším fysikálním kabinetě středních škol, zmizí všechny postavy vytvořené obrazotvorností, a oku udivenému zjeví se množství hor tvarů prarozmanitých, nejvíce však okrouhlých, nálevkovitých jako otvory čili jícny sopek vyhaslých velikostí prarúzné, rozsáhlé roviny větší i menší. Kdo by myslil, že pohled na Měsíc dalekohledam jest nejzajímavějším za úplňku, mýlil by se: tehdy Slunce svítí z plna i do údolí a prohlubin jako u nás, když Stojí na nebi, osvětluje i dno hlubokého údolí; ale když teprv vycházi, nebo když zapadá, osvěcuje nám nejvyšší vrcholy, v údolí jest tma nebo šero, stíny kopců jsou dlouhé. Pomslíme-li, že na Měsíci není šera, pološera jako na naší Zemi nýbrž jen buď světlo, kam Slunce svítí, nebo čirá tma beze všech přechodů, tož snadno pochopíme, že tam, kde
Slunce vychází nebo kde zapadá, tedy kraje Měsíce na té straně, kde Měsíce přibývá nebo kde ho ubývá, kde schází, zvláště budou se odjímati od části neosvětlené. Proto Měsíc pozorovati dalekohledem zajímavější jest když roste, od úzkého srpečku až jest plný, a zase když schází. Každý den viděti jest na okraji jinou část, kteráž jinak vypadá, když tam Slunce vychází, jinak osvětlena jsouc zplna.

117

Pokud Měsíc jeví se jen jako srpek, vidíme i ostatní jeho povrch k nám obrácený jako tmavošedou plochu, průměru menšího než jest Měsíc plný; za zvláště příznivých okolností dalekohledem i mnohé podrobnosti poznáme na této tmavošedé ploše. Toto osvětlení pochází od paprsků, jež s povrchu zemského se odrážejí na Měsíc a odtud opět k nám. Dle toho, se které části Země ty paprsky byly odraženy, i osvětlení strany Měsíce od Slunce odvrácené bývá různé. Že světlý srpek zdá se býti okrajem většího tělesa než jest ostatní tmavá část Měsíce, jest optický klam: plochy těles světlých, zářících zdají se býti širšími, většími než
plochy těles stejné velikosti, ale méně osvětlených nebo tmavých. Zjev ten fysikové nazývají irradiací.

Měsíc za jednoho oběhu kolem Země otočí se jednou kolem vlastní osy a proto k nám obrací vždy tutéž polovici; známe proto jen polovici Měsíce k Zemi obrácenou; že však osa Měsíce se kyvá, ovšem ne mnoho, můžeme od času k času poznati něco více než polovici povrchu jeho.

Na povrchu zemském máme moře, zálivy, pevniny, země, hory, jezera, bařiny . . . u každé má své jméno; tak i na Měsíci, ač tam není skutečných moří, naplněných vodou, jezer, bařin, zálivů; hvězdáři rozsáhlé plochy rovinám se podobající, pojmenovali jmény moří, jezer, zálivů, bařin: Moře nektaru (nápoje bohů), Moře úrodnosti, Moře klidu, Moře nepokojů, Moře jasna, Moře mračen, Moře bouří, Moře dešťů . . .; pohoří jsou tam jako: Alpy, Kavkaz, Apenniny, jsou to celá pásma vysokých hor, pak hory s okrouhlým krajem jako jícny sopek a to všech možných rozměrů: průměr některých obnáší až 90, 130, 200 km a nad to, a hloubka 3.000 až 5.000 m; kraj některých vyčnívá

118

jako vysoký násyp čili val nad ostatní rovinou až do 5.000, 6.000 m a výše, tak že hledíce s okraje do jícnu, měli bychom hloubku 10.000 a více metrů pod sebou; naše nejvyšší hory dány na dno takového jícnu nedosahovaly by do vrchu. V jícnu, jehož průměr obnáší 90 km, měly by místa všechny budovy, lidi a zvířata, všechna města a dědiny, jež jsou na povrchu naší Země, ovšem musely by býti v patrech nad sebou, a nevyplnily by ten jícen ani k výši ostatního povrchu Měsíce; oč kroje vyčnívají nad povrch Měsíce, zůstalo by prázdno. Jícen sopky Etny na ostrově Sicílii má v průměru jen něco přes kilometr.

Výška hor nad ostatní povrch Měsíce jest až 8.960 m; hvězdáři znají dosud více než 100 hor vyšších než 3.500 m. Nejvyšší hora povrchu zemského, jakž jindy se myslilo, Everest v Himalajském pohoří, jest 8.850 m vysoká, její výška obnáší 720tý díl zemského poloměru, nejvyšší dosud známá hora na Měsíci, 8.850 m vysoká, měří 200tý díl poloměru měsíčního; jsou tedy hory na Měsíci poměrně mnohem vyšší než hory na Zemi.

Hor s kruhovitým krajem, jako jícny sapek, viděti poněkud lepším dalekohledem na 2.000, nyní vidí jich hvězdáři dalekohledem, jehož čočka má 16cm v průměru, přes 32.000. — Pohled na Měsíc dalekohledem jest překvapující, na mnohých místech Měsíc vypadá
dírkovitý jako švýcarský syr.

Každé místečko má své jméno nebo aspoň číslo. Na Schmidtově mapě Měsíce jest 546 jmen, z těchto jest 501 jmen osob, a sice znamenitější jícnovítá pohoří jsou pojmenována dle mužů zasloužilých o astronomii, o vědy přírodní vůbec, o mathemntiku, jako Koperník, Tycho, Kepler, Herschel, Archimedes, Aristoteles, Plato . . .. Pythagoras, Thales . . .

119

Nebe rozděleno jest mezi bohy a polobohy, mezi zvířata a obludy; mužům ducha a práce zbyly jícny vyhaslých sopek — jest-li vůbec tak povstaly — na Měsíci, nepatrném prášku v prostoru nebeském: kdo chce život zasvětiti vědě, nechť nespoléhá se na skvostný úděl!

Kromě »moří« a hor na Měsíci viděti jest ještě »brázdy«; trhliny, většími dalekohledy viděti jest jich nyní již přes 350, některé z nich jsou až i 300 až 500 km dlouhé, jež pronikají i vysoké okraje jícnů ač jsou obyčejně přímočáré. Na Měsíci dobrými dalekohledy možno pozorovati rozsahy 2 až 1 km; jak
ohromné asi jsou tyto brázdy, trhliny!

Velmi podivné jsou kromě toho přímočaré pruhy zvláště světlé, jež vybíhají paprskovitě od větších pohoří kruhovitých, na př. od Tychona, Koperníka a j. Vyvýšeniny, pohoří to asi nejsou, poněvadž nevrhají stínů, když tam Slunce vychazí.

Jak povstala na Měsíci pohoří kruhovitá, vypadající jako obruby jícnů sopečných, o tom hvězdáři různě soudí. Že jícny průměrů poměrně malých jsou jícny vyhaslých sopek, v tom souhlasí, různí se však ve výkladě o jícnech rozměrů velikých; jedni hledají
původ jejich kromě Měsíce, snad padání hmoty na Měsíc, dokud nebyl stuhlým, nýbrž ještě plastickým jeho povrch; jiní myslí, že vznik jejich má příčinu v sopečné činnosti Měsíce v dobách jeho chladnutí.

Stříkáme-li do tuhnouci malty ze sádry nebo z kuroviny větví omočenou v této maltě, povstávají až nápadně podohné tvary, jakéž vidíme na Měsíci. Pokusy takové provedl pan Ant. Zajíček, stavitel v Kroměříži: fotograňe těch tvarů osvícených s boka
vypadají jako fotogratle častí povrchu měsíčného.

120

Světlo Měsíce jest odražené světlo sluneční, povrch jeho nadmíru drsný odráží asi tolik světla, co u nás hlína; Zöllner udává, že množství světla Měsícem odraženého obnáší 1/619,000 světla slunečního. Měsíc i teplo své má od Slunce, z tohoto pohlcuje
sám největší část a odráží málo. Množství tepla, jež odráží se za úplňku, má se ku množství tepla, jež Slunce nám posílá, jako 1:82.600.

Při zatmění Měsíce — možném jen za úplňku — jest nejlépe poznati, jak Slunce zahřívá Měsíc. Citlivá měřidla odraženého tepla od Měsíce ukazují, že teplota na Měsíci klesá již, dokud oko ještě ani nepoznává ztemnění Měsíce, když totiž Měsíc vstupuje do polostínu Země. Za úplného zatmění klesá temperatura na Měsíci nadmíru rychle, tak že v krátké době rozdíly teploty na Měsíci převyšují rozdíl teploty horkého pásma na Zemi proti teplotě za naší nejkrutší zimy. To děje se v několika málo hodinách: jaký bude rozdíl teploty na Měsíci za dlouhé, téměř patnáctidenní noci proti teplotě téměř patnáctidenního dne! Lord
Rosse určil rozdíl teploty na Měsíci za úplňku a za tmy (když jest nový měsíc) na více než 300°. Jedni praví, že teplota na rovníku měsíčním za dlouhého dne stoupne až na sto, ba dvě stě stupnů, jiní páčí ji jen na 50° C; za dlouhé noci klesne prý až na teplotu prostoru světového, 273° pod nullou; aspoň na pólech Měsíce prý jest tak nízká teplota; tomu však jiní odporují. Kdož ví pravdu, a podívat se tam nepodaří se tak snadno jako panu Broučkovi u Sv. Čecha.

Každým způsobem rozdíly v teplotě jsou takové, jakých na Zemi neznáme; kdyby na Měsíci i ostatní podmínky života organického byly, jak je shledáváme na Zemi, těch rozdílů teploty by nesnesly, kdyby po-

121.

dobaly se organismům zemským; na naší Zemi život organický pohybuje se obyčejně v mezích mezi 0°, 50° až 100° C.

Kromě tepla a světla podmínkou organického života na Zemi jest vzduch a voda. Až dosud není zjištěno nade vši pochybnost, že by na Měsíci byl vzduch a voda; kdyby Měsíc měl nějaké ovzduší, jevilo by se to v lomu světla na okraji Měsíce při zatmění Slunce, též by to bylo znáti na tom okraji Měsíce, kde ho přibývá, nebo kde ho ubývá; zde však není pozorovati přechodu z úplného světla v úplný stín, v čirou tmu. U nás máme již světlo, když Slunce ještě jest pod obzorem, a máme ještě světlo, když
Slunce bylo již zapadlo, přechod ze tmy do světla jest nenáhlý: to jest účinkem lomu světla v zemském ovzduší. Naše oči by těch příkrých změn úplné tmy a plného světla na Měsíci ani nesnesly. Neni-li tam vody, nemůže tam býti mraků, deště, rosy, není tam
červánků. Oblohu se Země vidíme modrou; též vzdálené hory na obzoru mají barvu do modra; ta modrost pochází od vzduchu. Pokud hledíme skrz vrstev vzduchu nepříliš mocnou, té modrosti nepozorujeme, jako bychom nepozorovali velmi slabého zbarvení vody v ploské nádobě se stěnami sobě blízkými.

Třpytění se hvězd pochází od pohybu, chvění se vzduchu, jakž můžeme jakési chvění se vzduchu pozorovati za parného dne nud polem, zvláště nad horkou plochou písčitou nebo nad žhavou plotnou sporokrbu. Na Měsíci nebe jevilo by se černé, hvězdy pak jako ostré bílé tečky beze třpytu. Tam by nebylo viděti krásné duhy ani třpytivé rosy. Vzduch jest u nás prostředím zvuku, na Měsíci zvuku není; tam by nebylo řeči, zpěvu, hudby; vše jest tem němo a ticho

122

jako v hrobě; roztrhne-li se parnem slunečním skála, ani to nezapraskne, tam ani dělové rány nebylo by slyšeti u samého děla. Na Zemi ještě lidé musí si lámati hlavu, aby našli prach nejen bez dýmu, nýbrž prach, pušky a děla, jež by střílely bez hluku. — A nechť jen nikomu se to nepodaří, sic by lidem brali již i chléb z úst na nová děla, pušky a prach!

Úchvatným způsobem líčil Neruda v XVIII. básni svých Kosmických Písní, jak by to bylo na Měsíci. — Zajisté již mnohému snílku napadlo přání bydleti na Měsíci, — ale i kdyby jinak člověk tam obstál, pojímala by ho hrůzu a přál by si býti na Zemi v tom nejposlednějším koutečku. Tak mnohé věci jen z daleka jsou vábivými, a míváme často přání nesmyslná, pocházející z neznalosti a z nezkušenosti. Ať se nám
vede jakkoli, buďme rádi, že jsme na Zemi; však nikdy nebylo tak zle, aby nemohlo býti ještě hůře, jakž Šibeniční humor praví.

Není-li na Měsíci vzduchu, vody, takového mírného střídání světla a tepla jako na Zemi, tak že by tam naše organismy neobstály, tož tím není řečeno, že tam života vůbec není; smíme jen souditi, že tam není
takového života. jaký jest na Zemi; podobně dá se souditi o možném životě organickém na jiných tělesech nebeských, totiž že tam může býti život, třebas jinaký než na Zemi.

Na některých místech povrchu měsíčného, zvláště nedaleko středu, dobrými dalekohledy lze pozorovati změnu barvy od první čtvrti až do úplňku a poslední čtvrti, tedy v době, kdy tam se dnem zároveň nastává i léto a přestává s nocí zima, až opět se světlem i den i léto přestává a počíná zase zima. Změny ty prý nemohou pocházeti od pouhého osvětlení a spíše

128

prý jsou účinkem i světla i tepla jako u nás rostlinstvo. Byť na Měsíci nebylo pozorovatelného množství vzduchu a vody, přece prý tam může býti aspoň stopa vzduchu a vody, kteréž postačí k vývinu nějaké vegetace, třebas rozdílné od vegetace naší. Ovšem pak něco jiného by
bylo, kdyby dalo se dokázati, že vládnou ve všem prostoru tytéž přírodní zákony jako na Zemi, že organický život vůbec jen za takových podmínek může vznikati jako na Zemi. Když by dalo se dokázati, že na Měsíci těch podmínek není, co na Zemi, bylo by i dokázáno, že tam není organismů.

Zdaž povrch Měsíce ještě nyní se mění: povstávají-li nové hory, nové sopky, brázdy — nyní nedá se ještě najisto určiti; změny ty musely by býti veliké, aby mohly býti spozorovány. Jestli ob čas snad spozoruje se nová trhlinka nebo nový jícen — ovšem sotva spozorovatelný — tož mohou toho býti příčinou lepší podmínky viditelnosti, což záleží na
nástrojích, na jakosti ovzduší pozemského a osvětlení, ba i na pozorovateli samém, takže snad předměty velmi nepatrné jindy zraku unikají.

Tělesa nebeská mají ohromné stáří, a celá tisíciletí proti tomu stáří jsou sotva okamžikem; celkové změny těles nebeských dějí se v dobách, jež člověk nedovede si představiti. Proto absolutně jest možno, že Měsíc po milionech let bude míti povrch proti nynějšímu pozměněný, však tvrditi toho nemůžeme. Ale to jest pravděpodobno, že malé změny i nyní dějí se na Měsíci. Ohromné změny v teplotě v mezích snad
až 300 stupňů od sebe vzdálených mohou působiti stahování, když chladnou a roztahují se ohříváním; tak i naše skály se drobí. To děje se již na povrchu zemském, kde rozdíly teploty nejsou tak veliké. I kdyby

124

na Měsíci nyní děly se výbuchy, jakými Vesuv svět děsil a nedávno Mont Pelée na ostrově Martinique, tož přece by sotva mohly býti účinky, změny povrchu měsíčného, pozorovány se Země; výbuchy ty dějí se na Zemi poměrně zřídka, ač Země není tak schladlá jako Měsíc. Nejlepší znalec Měsíce nové doby, Julius Schmidt, bývalý ředitel hvězdárny v Athénách, jenž přes 40 let zabýval se pozorováním a studiem povrchu Měsíce a vydal nejlepší mapy měsíčné, pozoroval změnu na jícnu kruhovitého pohoří, Linné zvaného. Dle toho dalo by se souditi, že vnitřek Měsíce ještě jest asi žhavým jako vnitřek naší Země.

## Přitažlivost Měsíce.

Měsíc má sice poměrně málo hmoty. 1/80 hmoty zemské a jen 1/25,800.000 hmoty Slunce, ale že přitažlivost hmot roste se čtvercem, jak vzdálenosti jejich od sebe ubývá, takže hmoty sblížené sobě na polovici, desetinu, setinu, tisícínu . . . původní vzdálenosti přitahují se navzájem silou čtyřikrát, stokrát, deset tisíckrát, milionkrát . . . . větší, přitažlivost Měsíce na hmoty
povrchu zemského jeví se značně i vedle přitažlivosti Slunce 392 krát vzdálenějším. Poněvadž běží zde jen o rozdíl, čím jednotka hmoty středu Země od Měsíce vzdálenějším méně jest přitahována než jednotka
hmoty na povrchu obráceném k Měsíci; tož rozdíl tento jest větší pokud se týká přitažlivosti Měsíce, než pokud se týká přitažlivosti Slunce; proto kdyby povrch Země všechen byl pokryt mořem, voda nad
tou částí Země, kde Měsíc vrcholí, čili kde prochází

!%

poledníkem, by se vzedmula; na straně protější též,
protože střed Země Měsíci bližší než část povrchu od
Měsíce odvrácená, mocněji jest přitahován' než tato
u proto zůstává pozadu, tedy od středu se vzdaluje.
Toto stoupání vody mořské nad obyčejnou výšku
hladiny jmenuje se příliv, klesání pod ni sluje
ndliv; oboje pak slují slapy mořské. Kdyby Měsíc
hyl stálici na nebi, od přílivu k přílivu neb od odlivu.
k odlivu by uplynulo dvanáct hodin (méně 4 minuty);
vody by na jistém místě po šest hodin přibývalo
a opět po šest hodin ubývalo. Ale Měsíc ve své dráze
kolem Země směrem od západu k východu popojde
denně 0 50 minut a 28 vteřin časových, proto od
vrcholení k vrcholeni jeho projde doba 24 hodin,
50 minut a 28 vteřin, od přílivu k přílivu uplyne tedy
12 hodin, 25 minut, 14 vteřin.

Pro všeliké překážky, stavějíci se volnému pohybu
vody v odpor, dle místních okolnostíěpříliv na různých
místech neděje se v tu dobu, kdy M síc nn tom místě
vrcholí, nýbrž opozďuje se mnohdy i o několik hodin;
doba, o kterou příliv později se dostaví než kulminace
Měsíce, nazývá se dobou přístavní (Hufenzeit, čtablis-
sement); pro Dublin obnáší 11 h 12 minut, pro Londýn
1 h 58 m, pro Lissnbon 2 h 30 m.

Je-li přitažlivost příčinou přílivu a odlivu moře,
bude příliv znnčnější za novoluní, bude to příliv
zvýšený (Springílut). Kdyby i některé oběžnice,
Měsíc i Slunce byly na téže straně Země, příliv by
se tím ještě- zvýšil.

Falb dovozuje, že přitažlivostí Měsíce dějí se
poruchy v rovnováze i v kůře zemské i v ovzduší.
Takové dni, ve kterých shluknou se okolnosti tak,
že tato síla zvláště jest velká, jmenuje kritickými a

1%

dle množství těch okolností, rozeznává kritické dní
prvého, druhého a třetího řádu. Zemětřesení, sopečná.
činnost jest závislá pry' hlavně na Měsíci a vyskytuje
se v kritické dni; podobně všeliké bouře na povrchu
zemském, kyklony. bouře na mořích.

Dobu přílivu v přístavě musi znáti plavci: velké
lodě za odlivu by nemohly přistáti, kde voda není dosti
hluboká.. Čím more jest větši, volnější, čím blíže rovníku,
tím příliv jest značnější a pravidelnější; čím blíže k toč-
nám, tím jest menší. Výška přílivu měří se od nešnižšl
hladiny za odlivu a jest dle místních okolnosti :- zná;
v St. Malo, francouzského města na skalnatém ostrově
při ústí řeky Illy do průlivu La Manche, obnáší 6 m.
Ve Východní Indii a na březích tropické Ameriky,
voda za přílivu stoupá vysoko, bývá-Ii zvýšena vlnami,
stoupá až o 30 m, v St. Malo často o 15 m. Kde
pobřeží jest nízké, voda je zaplavuje a odtékajíc ne-
chává všelika' zvířata mořská na suchu: kde pobřeží
jest vysoké a skalnaté, voda proti němu se vzpírá.
Splakovánlm všelikých organických látek hnijících
Měsíc prokazuje pobřežním kmiinám vzácné služby:
chrání je od nákazy.

Dráha Měsíce kolem Země jest ellípsa, a proto
Měsíc není vždy stejně daleko od Země: tím i výška
přílivu nebude stejnou, připadne-li novyr Měsíc na dobu,
kdy Měsíc prochází přízemím, bude výška přílivu
nejvetsi.

Však i vzduch velmi pohyblivý podléhá podobnému
působení Měsíce a Slunce jako voda, ale my žijeme
na dně tohoto hlubokého moře vzduchového a nepo-
znaváme toho působení jako na dně hlubokého moře
stoupání a klesání vody za přílivu n. odlivu sotva se

127
pociťuje. Pravidelné denní stoupání, klesání tlakomčru
nepochází od působeni Měsíce jako příliv a odliv.

Známe polovici povrchu Měsíce k Zemi obrácenou
a něco nad to, jaký jest Měsíc mt druhé straně, nevime;
nevíme též. jsou-li tam lidé nebo vůbec bytostí rozu-
mové. Jsou-li, tož jsou jiného druhu než lidé na Zemi.
Pravděpodobnost, že by tam byli lidé, jest příliš malá.

Merkur.

Z oběžníc dosud známých Merkur — Dobroptin
:: — Sluncí jest nejbližší. (Viz tabulku soustavy slu-
ncčni). Hmoty má jen asi 0136 hmoty zemské. Po-
něvadž dráha jeho jest velmi elliptickú, í vzdálenost
jeho od Slunce jest různá: v přísluni 45,710.000 km,
v odsluni 693001100 km, Zemi přiblížili se může až
na 76 milionů km a vzdálítí se může od ní až na
220 milionů km: střední rychlost jeho pohybu v drt'tze
kolem Slunce obnáší 47'6 knt za vteřinu; rovina
jeho dráhy skloněna jest k rovině dráhy zemské ze
všech planet nejvice: 70. Oku lidskému s Merkura
Slunce by mělo průměr téměř třikrát, plochu téměř
devětkrát větší než se Země. Poněvadž dráha Merku-
rova kolem Slunce leží uvnitř dráhy zemské, nemů-
žeme vídětí ho nikdy na protější struně od Slunce:
totiž na východní stmnč, jcoli Slunce na západě, nebo
na západní straně, je-li Slunce na východě: on vzdáli
se nejdál 290 od Slunce-, proto může býti viděn pou-
hým okem buď jen na západě věčer, nejdéle lh, ho.
diny po západu Slunce, nebo jen na východě ne více
než 1'], hodiny před východem Slunce.

128

U nás — říkáme — jest řídkým hostem na nebi,
obyčejně mizí v paprscích slunečních. Že nad obzor
vysoko nevystoupi, pozorujeme ho skrze tlustou vrstev
vzduchu vodními parami Viet: méně naplněnou; proto
zdá se nám býti červennvý: vždyť i Slunce i Měsíc
i hvězdy. pokud jsou blízko nad obzorem, bývají
zbarveny do červena. Kdyby mohl vystoupiti vysoko
na nebi a býti viditelným pozdě v noci. zářil by jasněji
než nejjasnější hvězdy. Nejsnáze u mis může se
pozorovuti asi 'A hodiny po západu Slunce a sice na
jaře, však není ho viděti každý den, i když jest jasno,
nýbrž jen tehdy. když — od nás hleděno — není
příliš blizko Slunce: období víditelnosti.tn-á asi deset
dní. Operním kukátkem možno viděti Merkura snadno
po západu nebo před východem Slunce. dlužno jen
věděti přibližné jeho místo. l'ak není tak vzácným
hostem na nebi, jak se praví.

Dobrými dalekohledy možno viděti Merkura i ve
dne. Jako Měsíc zjevuje se v různých podobách čili
fasích. i Merkur jeví se jako srpek. roste :! ubývá ho
dle toho, kolik z polovice jeho Sluncem osvícené
obráceno jest k Zemi. Má ovzduší nsi lG-T krat řidší
než Země. Dřívější hvězdaři soudili, že otáčí se kolem
své osy — jakž jindy vůbec se Soudilo — za 24 hod., nyní
však slavný Hehiaparelli chce tomu, že Merkur otočí
se kolem své osy jednou za svého oběhu kolem Slunce,
totiž za 88 dní. pak by obracel ke Slunci celkem usta—
vičně tutéž stranu jako k nám Měsíc. Pro svou blíz-
kost Sluncí přijímá sedmkrát až desetkrat více světla
a tepla od Slunce než stejná plocha naší Země: teplota
i světlost na osvícené polovici byla by pro náš orga-
nismus nesnesitelnou; na druhé straně byla by stálá
noc, osvícená nepatrně jen paprsky slunečními odra-
ženými od Země a hvězdami: teplota ovšem mohla

!%
by tam býtijen velmi nízká. Poněvadž dráha Merkur-ovo.
icst ellipsa :; velkou výstředností, tož od přísluní jeho
už k odsluní a zpět bývá osvětlena a otepleno od Slunce
více než polovice jeho povrchu a sice měřeno v šířce
22749, a jen 132130 mělo by věčnou noc, tedy asi třetinu
povrchu Merkurova. Protinožci této třetiny by měli
ustavičně světlo a teplo, těm ostatním by Sluncožje-
vomlo se na nebi po nějakou dobu, měli by den;
neviděli by pak Slunce opět pojistou dobu, tehdy měli
hy noc. Jest-li osa Merkura stojí téměř kolmo na jeho
dráze, kdyby zn oběhu kolem Slunce Merkur jen
jednou se otočil kolem své osy, Slunce téměř nepo—
hnutč by tam stálo na nebi, toliko v době 88 dni
vykonalo by zdánlivou dráhu na nebi jako kývadlo 28'70
na východ a na západ. Různost osvětlení u. tepla na
Merkuru jest větší než u nás na Zemi. my bychom
tam neobstáli. Mluví-li se 0 velkých protivách tepla
“ světla na oběžnicich proti světlu a teplu na naší
Zemi, tož se předpokládá, že ostatní okolnosti jsou
jako u mis: ale teplo, na př. na straně Merkura ke
Slunci obrácené muže býti mírněno ovzduším jiného-
způsobu než jest ovzduší naše a může býti přemi-
leno jim na stranu od Slunce odvrácenou prouděním,
jako my topíme ohřátým vzduchem jinde, než jest
pec vzduch ohřívající; tn protiva tedy nemusí býti tak
velká. za jakou ji pokládáme. Organismy. jakž i dříve
bylo zmíněno, mohou býti ještě jiného druhu. přizpů-
sobené okolnostem na různých tělesech nebeských,
jako my přizpůsobeni jsme okolnostem na Zemi.

Jest pravděpodobno, že v prostoru mezi Sluncem

a Merkurem krouží velké množství malých tělísek —

meteoritů: hvězdář—í pilně hledají i planety možné v tom
prostoru, ale s jistotou dosud neobjevili žádných.
__“ e

lm
Venuše.

Venuše') (čili Krasopuní Q) zjevuje se pouhému
oku jako velmi jasná hvězda buď na západním nebi.
jest večernicí nebo na nebi východním, kdy je dennící:
je-ii v největším lesku, možno ji viděti i ve dne, i o
polednrich; dělníci v lese ze stinného mista ji často
viděli. Poněvadž její dráhu leží uvnitř dráhy zemské,
nikdy nelze ji viděli na nebi na protější straně od
Slunce, avšak vídáme ji mnohem dále od Slunce než
Merkuru, může! — pozorována jsouc se Země —
vzdálili-se od Slunce až na 48“ východně, je-ii ve-
černicí, nn 48“ západně, je-li dennicí čili jitřenkou.
Jako Měsíc a Merkur i Venuše má své změny; v dn-
lekohlcdě jeví se jako Měsíc, když teprv roste, srpek.
anebo když ho ubývá; lesk její jest největší, jeví se
nám vzdálena od Slunce 390 východně nebo západně.
1 ostatní od Slunce neosvícenou část Venuše možno
spatřiti dobrým dalekohledem; poněvadž na té straně,
kde jí přibývá, světlo ve tmu přechází poznenáhlu, —
den, soumrak, noc — dlužno soudili, že Venuše má
ovzduší a to asi hustší než naše Země: též jako na
Měsíci vnitřní okraj srpku není rovný, nýbrž víc.-:
méně hrbolovitý, zubníý; proto tam budou vyvýšeniny,
hory.

O tom hvčzdáři nestejně soudí, zdaž otočí se
kolem své osy jednou za svého oběhu kolem Slunce,
totiž za 2247 dne, nebo asi ve 24 hodinách jako naše
Země; nejnovější pozorovatelé :! hvězdáren velmi

') Pokud číselných Mcjů ovulálonouti, velikosti . . so lýkd,
vl; tabulku musm: sluneční.

181
příznivé polohy praví, že Venuše otáčí se kolem své
osy ve 24 hodinách, jiní však mají za to, že otočí
se jednou kolem své osy, co oběhne kolem Slunce.
Jestli se otočí jednou za 24 hodiny, na Venuši by
střídaly se den i noc jako na Zemi; ale střídání
ročních časů mohlo by býti jen pranepatrné, poněvadž
má se za to, že osa Venuše stojí téměř kolmo na
její dráze, jakž z bílých skvrn na protějších koncích
průměru Venušina hvězdář-i soudí: skvrny ty dají se
mysliti jako led a sníh nn točnách naší Země. Má-Ii
Venuše měsíc nebo měsíce, dosud není známo. Venuše
velikostí, nutnosti a v mnohé jiné příčině zdá se po-
dobati Zemi a možná, že jsou-li tam organismy, jsou
podobny organismům na Zemi, protože stejné příčiny
mivnjí stejné následky.

Třetí velkou občžnicí jest Země, jež nám jest
známějši_než všecky ostatni planety a proto ji po-
mijíme. Ciselné údaje o Zemi viz v tabulce.

M : rs.

Čtvrtá oběžnice jest Mars (Smrtonoš d'). Pou-
hému oku jeví se jako hvězda jasná první velikosti a
jest barvy červené. Poněvadž může Zemi přiblížili se až
na 57 milionů km a vzdáliti se od ní až na 396 milionů
kilometrů, bude i v tak různých vzdálenostech od
Země míti různý zdánlivý průměr 3%“ až 26'!,", tedy
různou velikost a. jasnost. O rozměrech jeho tělesa,
o hmotě, o jeho dráze, oběhu . . poučí nás tabulka.

Mars jest oběžnice velmi zajímavá a v hvězdo-
sloví důležitá; slavný Kepler v Praze 2 Merta odvodil

a.

189

důležité zákony, jež ve fysíce známe pod názvem
»:aikony Keplerovy-. Dráhy oběžníc jsou ellipsy,
v jejichž společném jednom ohnisku jest Slunce.
plochy. jež rudíus vector — přímka od středu Slunce
ku stí-edu oběžnice — ve stejných dobách opíšu, jsou
si rovny; čtverce doby oběhů mají se k sobě jako
třetí mocniny středních vzdáleností od Slunce. Koperník
svým objchním, že Slunce jest středem, kolem něhož
Země naší: — a ostatní oběžnice — obíhají. stojí na
rozhraní nové doby ve hvčzdářství, od chlcrn datuje
se opět nová doba, pokud se týče přesného výpočtu
o drahách oběžníc, zákonů jejich pohybu a vůbec po-
hybu těles nebeských v prostoru světovém. Koperníkcm
a Keplercm svět dověděl se. že jest a jakým jest
pohyb těles nebeských, Newtonem dověděl se, proč
ton pohyb jest takovým, jnkým jest: objevili zákon
o přitažlivosti hmot čilí gravitaci.

Mars otáčí se kolem své osy ve 24 h 37 min.
a asi 22-65 vt., tmi tedy tlL'll na Martovi o něco více
než o půl hodiny déle než den nn Zemí: osa této
oběžnice skloněnn jest k rovině její dráhy čili k eklip-
tice úhlem 65085 osa zemská sklončna jest k rovině
dráhy zemské úhlem 660 33'. rozdíl činí jen l“ 25'.
babou otáčky kolem osy řídí se den a noc, sklonem
osy k rovině dníhy řídí se roční časy; poněvadž
rozdíly jsou nepatrny, a že nn Martovi jest ovzduší
ll vodn —. anebo aspon tckntinn podobná — Proto i
střídání se dne a noci. střídání se ročních času bude
podobný jako na naší Zemi, ovšem, že ty roční časy
budou tiun trvati déle. poněvadž dobn oběhu lt-lartovn
kolem Slunce trvii okrouhle 687 dm. Kolem severní
a jižní tačny nn Martovi jest pozorovaní velké bílé
skvrny jako v krajinách poliírních na naší Zemí, kdež

lBB
jest »věčnýc sníh a led. Je-li nakloněna severni polo-
koule Martova ke Slunci, čili kdyžlna severní polokouli
jest léto, bílá skvrna jest menší, sníh a lecl odtály;
když jest na jižní polokouli léto, bílá skvrna kolem
točny na této polovině jest menší; jest tam léto,
na severní polokouli jest zima, a bílá skvrna tam
opět jest větší. Vypozoruváno. že spousty sněhu a.
ledu, rychle tajíce, způsobují ohromné povodně, o jejichž
velikosti člověk sotva dovede učiniti si ponětí.
Zajímavým a zároveň záhadným zjevem na. Martovi
jsou pruhy. ba celá sít' pruhů na povrchu Martově,
tak zvané »kanályc. jež mají tvar pravidelný, rovno-
čárý a nejsou zakřivené, jak řeky. Astronomové již
nakreslili mapy povrchu Manot-n, pojmenovali jedno-
tlivá místa jmény moří a pcvnin: na těch mapách
»knnalyc tvoři hotovou Síť. Jest však si pamatovali,
že zdrovoň, najednou »kanály- ty nikdv nejsou vi-
ditelny všecky. nýbrž v různých dobách různé :: nich,
že zvláště jich bývá více, když taje sníh a led, že pak
některé z nich se zdvojují. Sířky maji nejméně 30 km,
sice by nebyly vidite-Iny, některých šířka obnáší i sta
kilometrů: délka jejich obnáší od 500 km až i čtvrtinu,
ba i třetinu obvodu Martova. Viditelny jsou jen nej-
většími dalekohledy s hvězdáren, kde?. mají čistý
vzduch a to jen jako jemné nitky sítě pavučinové.
Ustí vždy do nějakého jezem nebo do moře; o
celém povrchu Martově rozprostírá se sít' těch'»kanalgc
pravidelně rozdělených. To zavdalo příčinu k domněnce,
že ty .kanály. jsou dílem umělým, dílem rozumných
tvoru, obyvatelů té hvězdy. Možná. že na Martu jsou
obyvatelé, ale tvrcliti na jistotu se to nemůže. Mars
jest menší než naše Země: není pravděpodobno, aby
nějací lidé tam vykopali kanály nejméně 30 km široké,

184

tolik jich a délky i několik tisíc kilometrů! Dalekohledem ještě nedá se rozhodnouti o tom, vždyť i města, jako jest náš Londýn, Paříž . . nejlepšími dalekohledy na Martovi ještě by nemohla býti spozorována, ostrovy jako Kreta, Cypry . . teprv za příznivých podmínek viditelnosti mohly by býti tak sotva spatřeny. Flammarion docela tvrdil, že Mars jest obydlen. Dokázati nyní ještě se to nedá. 

## Planetoidy čili asteroidy.

Položíme-li vzdálenost Merkura od Slunce rovnu 4 jednotkám délkovým, obdržíme pro střední vzdálenosti hlavních planet od Slunce jakousi pravidelnost až na nevelké odchylky:

											Skutecně
Merkur	0.3 + 4 = 0 × 3 + 4 =		4,		4.0
Venuše	1.3 + 4 = 2^0 × 3 + 4 =		7,		7.5
Země	2.3 + 4 = 2^1 × 3 + 4 =		10,		10.3
Mars	4.3 + 4 = 2^2 × 3 + 4 =		16, 	15.7

Jupiter 16-3 + 4 = 2^4 × 3 + 4 =	52,		53.7
Saturn	32.3 + 4 = 2^5 × 3 + 4 =	100,	98.6
Uranus	64.8 + 3 = 2^6 × 3 + 4 =	196,	198.8
Neptun	103.3 +						313		zde již nesouhlasí

Mezi Martem a Jupiterem jeví se mezera, což spozoroval již Kepler, a od té doby hvězdáři tušili, že v té mezeře dlužno hledati oběžnici dosud neznámou; hledáno úsilovně, až dne 1. ledna 1801, tedy v prvý den devatenáctého století, hvězdář Piazzi spozoroval

186
v uouhvězdí Býka hvězdu 8. velikosti, jež nebyla na
mapě strilic; ovšem nevěděl ještě, zdaž jest to nová
oběžnice, později však byla. pozmina a dáno jí jméno
Ceres; pak byly nalezeny nové oběžnice a pojmeno-
vány-. Pallas, Juno, Vesta, Hebe, Iris, Flora. Čím více
dalekohledy byly zdokonalovány, čim lepši methody
pozorovací byly vynalózány, čím bedliveji hledáno,
tim více jich se objevovalo, takže jména z mytho-
logie řecké a římské nestačila a nebyla též pohodlná:
patolízalství pak u astronomů modernich, zabývajících
se věcmi nebeskými, nemělo míli místa: proto
astronomové až na některé, kteří nemohli potlačíti
jakéhosi avitismu v sobě, uzavřeli, že hvězdy nemají
býti jmenovány jmény panovníků, knížata dali jim
numera čili čísla: Ceres = ], Pallas =— 2, Flora -= 8 . .
Již jich maji přes 570, každým rokem přibývá jich
průměrně 20—30.
Místo asteroid jest hlavně v pásmu zvířetníka,
v ekliptice: sklon jejich drah k ekliptice má obyčejně
jen málo stupňů; ale též některé mají sklon až 20",
ha asteroida Kleopatra až 26“ 26': . doba oběhu mezi
třemi až devíti lety, střední vzdálenost od Slunce
21675 až téměř 4'2625 poloměru zemské dráhy. Jako
by prostředníkem, přechodem od Země k Martovi
nalezena asteroidu Eros, jenž svou drahou sahá až do
dráhy Země, již ze všech asteroid nejvíce se přiblíží.
Velikost asteroid jest velmi rozmanita. Ceres má
v průměru 804 km, Agatha 13 km. Však idalekohledy
již nestačí, aby jimi mohly býti viděny hvězdy tak
malé, jako jest na př. Agathe, a tu výborné služby
koná fotograťle. Dalekohled, jehož čočka, objektiv, má
krátké ohnisko, postaví se tak, aby viděny byly hvězdy
v pásmu ekliptiky; strojem hodinovým dalekohled tak

186

se otáčí, aby v poli znrném ustavičně byly tytéž
hvězdy stálice. jakoby nepostupovaly od východu
k! západu, čili jakoby Země neotáčela se kolem své
osy. Tam kde povstává obraz hvězd, dá se citlivá
desku fotografická a nechá se dle okolnosti jednu,
dvě i více hodin ve stroji. Když pak se vyvine obraz.
tu hvězdy stálice dle svých světlostí jeví se jako více
méně zřetelné body na desce, oběžnice pak jeví se
jako čáry. kratší. pohybují-li se pomalu nebo jsou-li
vzdálenější, delší, pohybuji-li se rychleji nebo nejsou-li
tak vzdáleny. Stopy některých oběžnic jsou tak ne-
pntrny, že mohou býti spozm'owiny jen drobnohledem
na desce. a tak věda hvčzdářská dospěla v ty konce.
že hledá tělesa nebeská již lupou, drobno—
hledem. Jest to podivno, ale pravda přece. Do pro-
storu mezi Mortem a Jupiterem patří ještě četné vla-
sntice čili komety, pnk mraky kosmického prachu.
O těchto předmětech bude ještě řeč.

Jupiter.

Největší všech planet jest Jupiter, Ix'ralomoc 2.
má sám více hmoty než. všecky ostatni oběžnice
dohromady. tuk že by stal se pánem soustavy, kdyby
někam zmizelo Slunce: v mechanice nebes rozhoduje
množství hmoty jako v praktickém životě množství
majetku: kclo více má, tomu svět víc se koří . . . .
»Žebrota. nuzotn patří do kouta.:

Jupiter “Si ještě neztuhl jako naše Země. nie má sotva
ještě slabé světlo vlastni ; jasnost jeho větší než hvězd
první velikosti pochází od Slunce Pro velkou vzdálenost

18?
průměr Slunce by oku lidskému s Jupitera jevil se
pětkrát menším než se Země. Poněvadž rychle se otáčí
kolem vlastni osy. Za necelých 10 hodin jeví se Značně
sploštčlým. Bod rovníku Jupiterova urazí za vteřinu
12.640 m, bod naší Země jen 464 m.

Na rovníku jest viděti pruhy a skvrny, kteréž
v mocných dalekohledcch jeví se velmi složitými, mění
se rychle. což jest znamením, že na Jupiteru v ovzduší
bývaji velké bouře: pozorováno r. 1676, že pruhy
a skvrny í'ítily se směrem od východu na západ
ohromnou rychlostí 49 km za vteřinu. Vše nasvědčuje
tomu. že častými výbuchy z vnitra látka dostává se
na povrch, čímž vznikají pruhy a skvrny, a že na
místě výbuchů vyvíjejí se místní páry.

Osa Jupiterovu skloněna jest k rovině jeho dráhy
úhlem 570 a stojí tedy skoro kolmo na ní: proto
změna ročních časů nn Jupiteru bude jen nepatrnou.
Kdyby na rovníku byli obyvatelé. měli by Slunce
v poledne téměř po celý rok (Jupiterův) v nndhlnvníku,
tři stupně od nadhlavniku by se nikdy neuchýlilo. Nn
točnách střídavě byl by po dubu našich asi šesti roků
den a šesti roků noc: na točmich za dne by nikdy
nevystoupilo und obzor výše než tři stupně a za nocí
níže než tři stupně by pod obzor nekleslo.

Zdnž jest na Jupiteroví ovzduší tukové jako na
naši Zemi, nebo podobné jako okolo Slunce, či jiného
ještě druhu. nikdo neví : od ovzduší puk závisí osvětlení
a ohřívání Sluncem. a co opět z toho plyne: též nevíme
nic jistého o teplotě povrchu, jaký by tam mohl
býti život: jen to víme. že naše organiSmy by na
Jupiterovi neobsttily.

Již obyčejným dalekohledem naších fysikálních
kabinetů možno viděti čtyři měsíce Jupiterovy, jež

188
jsou buď v rovině jeho rovníku nebo aspoň od ní
valně se neuchyluji; roku 1.892 nalezen byl ještě pátý
měsíc, Jupiteru velmi blízký, tak že nejmocnějšími
dalekohledy může býti pozorován. Nyní těch měsíců
napočítali již sedm. Starší měsíce číslovány dle vzdá-
leností od Jupitera [, ll, lll, IV, měsíc objevený dostal
číslo nejvyšší V, ač jest Jupiteru nejblíže. Poněvadž
měsíce ty od roviny rovníkové značně se neuchylttií,
jeví se oku v přímce rovnoběžné s pruhy na rovníku
jdoucí středem planety; myslíme-li si kolmici na této
přímce ve středu Jupitera, máme osu, kolem niž se otáčejí.
Jupiter, jako každá jiná oběžnice, vrhá za sebou
na straně od Slunce odvrácené stín, jenž jest delší
než vzdálenost jeho měsíců; proto ony za svého
oběhu vstupuji (lo toho stínu, jsou zatomňovány.
Kdyby rychlost světla byla nekonečně veliká, nebo
kdyby Země od Jupitera byla vždy ve stejné vzdále—
nosti, od zatmění k zatmění téhož měsíce uplynulo
by vždy stejně času. Pozorují-li se doby ty v čas,
kdy Země obíhajic kolem Slunce od Jupitera se vzdu-
luje, jsou vždy delšími, a v čas, kdy Země Jupiteru
se blíží, doby ty jsou vždy kratšími, než by jinak
měly býti. Součet opozdění těch. čítáno od času, kdy
Země oběhne polovici své dráhy, obnášel 992 vteřin;
muselo! Světlo v té době od Jupitera k bodu zemské
dráhy nejvzdálenějšímu proběhnouti dráhu o 40 milionů
mil delší než před půl rokem, kdy Země byla ve své
tiráže v bodě Jupiteru nejbližším, urazilo tedy za
vteřinu asi 40.000 mil čili 300.000 km. To bylo prvním
určením rychlosti světla, jež učinil r. 1676 dánský
hvězdář Olaf Rom er. Později rychlost světla určena
jinými způsoby; rozdily nejsou velké na důkaz, že
to učení jest správné.

180

Tyto měsíce rychleji otáčejí se kolem Jupitera
než náš Měsíc kolem Země: V za 11 h 57 m 22'6 vt.,
I za 17? dne atd., a proto na Jupiterovi téměř každý
den měli bychom zatmění aspoň jednoho měsíce, se
Země můžeme je pozorovati' dalekohledem.

Saturn.

Šostou hlavní občžnicí, počitajíco od Slunce, jest
S a t u r a h, Hladolct ; jest ze všech též nejzajímavějším;
pozorujeme-li ho větsim dalekohledem hvězdářským,
uvidime kolem něho světlý prsten. Pouhému oku jeví
se jako hvězda. první velikosti. Pro velikou vzdálenost
od Slunce — viz tabulku — oko naše by s něho
vidělo průměr Slunce 9'5 krát menší než se Země a,
kotouč Slunce zdál by se 90krát menší, proto i osvě-
tlení Saturnn Sluncem jest takové, že oku našemu
poledne na Saturnu by se jevilo jako náš soumrak, na
nějž nastupuje noc. Dráhu kolem Slunce vykoná za
2915 roků a potrvá v jednom souhvězdí okrouhle“ 2-5
roků. Nyni jest v souhvězdí Vodnáře blízko u sou-
hvězdí Ryb, tedy nepříliš vysoko na nebi, i když vrcholí
a obrací k nám širokou plochu prstene; pmto jest
zvláště zajimave pozorovali jej. Loď jedoucí 80 km
za hodinu dojela by na Saturnu za 5400 let, světlo
urazí tu cestu za hodinu a 19 minut.

Jako na Jupiteru, tak i na Saturnu viděti jest
pruhy rovnoběžné s rovníkem, zvláště značné jsou
u rovníku, pak skvrny; v té příčině Saturn podobá.
se lupiterovi i co se týče změn těch pruhů a skvrn:
budou těch úkazů asi stejně příčiny. Hvězdáři soudí,

140

že Saturn má husté ovzduší. Hmota jeho též — aspoň
na povrchu »— jest ještě měkká, že pak otáčí se rychle
kolem vlastni asy, asi ze. let;, hodiny, jest značně
sploštělý, jest totiž průměr jeho na rovníku větší prít-
měru od točny k točnč. — Osa Saturnova má sklon
k rovině jeho dráhy 600. tedy menší než osa zemská.
k ekliptice: proto i změny ročních časů nn Saturnovi
budou větší než na Zemi.

Tloušťka prstenu jest nepatrnn, různě hvězdriři jí
udávají: 24 km až 28 km. a hmoty prý má 'l. hmoty
zemské. Z čeho prsteny se skládají, dosud zjištěno
není, že nejsou ze hmoty pevné spojité, vysvítá z toho.
že vnitřní kraj otáčí se kolem Saturnu rychleji než
kraj vnější: u našich kolotoči: všechny body půdy.
na níž stojí »koně, kočáry, . . ,. v témž čase otočí
se kolem sloupu, osy. Pravdepodobne jest, že prsteny
skládají se z tělísek oddělených od sebe ovšem ve
velikém množství, jež dle zákonů Keplerových krouží
kolem Saturnu. Tn tělíska hustěji jsou při sobě. kde
prsten jeví se stkvělým: pro velké vzdálenosti od nás
vzdalenosti jejich od sebe nerozeznáváme ani největším
dalekohledem. Světlo jejich — od Slunce odražené —
splývá v jednotnou zář, jak světlo Svitílen vzdálené
železniční stanice, jichž pak od sebe nerozeznávúme;
tam. kde jeví se nám prsten tmavým. tukových tělísek
poměrně jest mnlo.

Saturn provázen jest devítí dosud známými
měsíci. 7. nichž největší menším jest než nejmenší
2 měsíců l—lV Jupiterových a proto mohou býti
viděny jen většími astronomickými dalekohledy. jnkýchž
ve fysiktilních knbinctech našich středních škol nebývá.
Nejmenší u Snturnovi nejbližší měsíc Mimns viděn byl
teprv 40 stopovým dalekohledem Herschelovým, jeho

IM
vzdálenost od Saturnu obnáší jen 31 poloměru Satur-
nova a jest u samého prstenu: největší a nejsvětlejší
měsíc, jenž byl proto i nejdřív objeven — roku 1655
Huygensem »— vzdálen jest 20'5, nejvzdálenější Japetus
59'6 poloměru Saturnova. Všecky měsíce až na anetn
pohybují Se téměř v rovině prstene Saturnova.

Uranus. (Hebešťanka G.)

Vilém Herschel uzřel večer mezi 10. a 11. ho-
dinou dne 13. března roku 1781 seclmistopým daleko-
hledem vlastnímu rukama shotoveným hvězdu, kteráž
jeviln se jako malý kotouč slnbě osvětlený; brzy poznal,
že jest toobčžnice — po tu dobu — neznámá. protože
stálice pro ohromnou vzdálenost jeví se v dalekoltledě
jako jasné body. Sice i jiní astronomové, jnkž později
se ukázalo, již dříve tu oběžníci viděli. ale jl nepoznali
_inkožto oběžníci.

Uran jest mk vzdálen, že loď; jež ujede zn ho-
dinu 30 km, potí-chovala by 10.840 let. než by mm
dojela; zvuk, jenž urnzi za hodinu 1.199 km, dorazil
by tam za 273 r'oxů; světlo s Urann dojde k nám za
2 h 39 m. Slunce našemu oku na Uranu jevilo by se
usi třikrát tak velké jako Venuše za největšího lesku
a neposkytovnlo by uni tolik světla. kolik ho máme
za jasné půlnoci. když nesvítí Měsíc: ovšem že i málo
tepla hy nnm poskjmvalo.

Když Uran stojí vysoko nn nebi. může býti viděn
i pouhým okem za příznivých podmínek jako hvězda
5. až 6. velikosti; jako koutouč bývá spatřen jen vět-
šími dalekohledy nu hvězdárnách. Osa jeho leží v ro.

142

vině jeho dráhy, rovník stojí tedy kolmo na dráze:
tím mizí takový rozdíl oteplení a Osvětlení Uranu na
různých místech jeho povrchu. jaký jest na naší Zemi,
nebot za oběhu kolem Slunce 84 roků '! dnů 9 hod.
22 minut Slunce přijde do nadhlavníku střídavě jak
na rovníku tak na měnách.

Rozdily teploty za léta a za zimy budou na Uranu
převeliké: trvátě nejdelší den v šířce 50“ našich 28'!,
roků, na pólech 42 našich mků. Když se mluví o te-
plotě nn planetách, předpokládá se ovzduší podobné
našemu, jest však možná, že jest ovzduší, jímž teplo
udržuje se a rozvádí jinak než ovzduším naším.

S Jupiterem a Saturnem má to společného,
že viděti na něm pruhy rovnoběžné s rovníkem, že
má krátkou dobu otočení se jednou kolem vlastní
osy v 1'3—12'5 hodinách (však není zjištěna. Hutnost
jeho jest malá proti hutnosti Země). — Viz tabulku.

Uran má čtyři měsíce, jež kolují kolem něho
v rovině rovníku a tedy v rovině kolmé na rovině
dráhy jeho: jsou pro velkou vzdálenost od nás a pro
svou malost viditelny jen mocnými dalekohledy hvě-
zdářskými jako hvězdy IQ.—14. velikosti.

Neptun. (Vodan Y)

Astronomie vydavá svědectví o člověku, že bytně
liší se od zvířete a ne pouze kvantitativně: žádné
zvíře nedovede povznésti se nad hmotu a nalití pří-
činy změn postřehovaných smysly tělesnými ve hmotné
přírodě, jak to činí člověk. Nejzajímavějším příkladem
toho jest objevení Neptune, nejzazší dosud známé

148
oběžnice našeho Slunce. V dráze Umnově jevily se
poruchy, Uran odchylovat se ode dráhy přesně vy-
počtené. To dalo příčinu k domněnce — když všechny
pokusy vysvětliti, odkud ty odchylky pocházejí, ukázaly
se býti marnými — že příčinou těch odchylek jest
těleso nebeské, dosud neznámé. Mladý tehdy mathe-
matik Leverrier v Paříži vzal na sebe nesnadný
úkol vypočítati, jak velká. jest ta neznámá planeta
a kde jest její místo. Dne 31. srpna 1846 napsal
hvězdáři Galloví v Berlíně, — jinde neměli přesných
map hvězd až 9. velikosti — že tu hvězdu mrí hlo-
datí v souhvězdí Vodnáře, u udnl její místo i zdánlivou
velikost. Ještě týž večer, kdy došla zpráva z Paříže
dne 23. září, Galle uviděl dalekohledem onu hvězdu
té velikosti (B.), jak Leverrier vypočetl a jen o
necelý jeden stupeň od místa, jak Leverrier udal.

Zároveň í anglický učenec Adams neodvísle
od Leverríern zabýval se touž úlohou a dospěl ku
stejnému výsledku. _ Tot vítězství ducha, vědy ma-
thematické, důkaz, že člověk není zvířetem, jež ne-
může za meze tělesných smyslů!

V tabulce obsaženy jsou údaje týkající se Nep-
tuna. Pouhým okem není viditelný; aby mohl bř'ti
viděn jeko kotouč :! byl rozeznatelný od stálice, ež
se jeví jako body světlé, na to musí býti dalekohled
již velký. Dalekohledem malým Neptune nelze rozcznatl
od stálíc 8. až 9. velikosti, — a těch jest mnoho.
Známe-li tedy místo, kde jest Neptun,a hledáme-líjcj
malým dalekohledem, nejsme jisti, zdnž vidíme jej
nebo snad stálici některou. Neptunu na jeho pouti
kolem Slunce, jež trvá 164 let, doprovází měsíc tolíký
asi jako Měsíc náš. Dle doby jeho oběhu a. vzdálenosti
od Neptuna. páči se hmota Neptunova asi ne Vam

144

hmoty sluneční. Planety, které mají měsíce, dají se
co do hmoty porovniivati mezi sebou nebo se Sluncem;
známe—li pak hmotu Slunce nebo některé planety
(Země), můžeme pak zvěděti i hmotu planety jiné.

Kolik planet vůbec krouží kolem Slunce. nikdo
neví, každým rokem hvězdúi—i objevuji planety nové;
čeho nelze viděti dalekohledem. ukáže fotografická
deska, a není vyloučena možnost, že najde se ještě
jiný způsob dosud neznámý, najiti oběžnice ještě
menší, než jsou oběžnice dosud známé.

Vlasatice.

ObčŽnice, zvláště velké, pohybují se v drahách,
jejichž roviny mají jen nevelký sklon k rovině dráhy
zemské čili k ekliptice; všecky pohybují se týmž
směrem — od západu k východu — jakž i Slunce
otáčí se kolem vlastní osy: dráhy jejich jsou ellipsy.
Zn jistý čas oběžnice objevují se na témž místě: dobu
jejich oběhu jest zmimu juko jejich druhy.

Oblast Slunce však hostí ješte jimi tělesu nebeská.
jež pouhému oku objevují se v dobách různých a
jejichž vzezření rozdilno jest od podoby hvězd: vy-
znumenúvují se ohonem. Roviny druh jejich nmjí
všecky možné sklony k rovině dráhy zemské od 00
už po lou“ a proto vlasatiCe putují prostorem nebeským
všemi možnými směry a mohou objev-ití se na nebi
kdekoli, kdežto planety dlužno hledali jen v pásmu
zviřetnikn. Jakkoli vlasatic pouhým okem pozoruje se
poměrně jen málo, tož pi'cce počet jejich jest veliký;
Kepler pravil. že jest vlasatic jako ryb v moři. Tolik

145
jich ovšem není, ale dějiny národů od nejstarších dob
zmiňují se téměř o 500 kometách viděným pouhým
okem. Jest povúžiti, že komety zpravidla viditelny jsou
pouhým okem, jen když na svých drahách přiblížily
se ke Slunci — k Zemi. Které v ten čas jsou viditelny
jen na jižní polokouli zemské, .ty za dřívějších dob
poznamenávány nebyly: které Slunci byly blízko, byly
nad obzorem ve dne se Sluncem a. mohly jako Merkur
býti spozorovány za příznivých okolnosti jen brzy po
západu nebo před východem Slunce. bylo-li však vten
čas počasí nepříznivé, nebyly vůbec zpozorovány;
zaznamenávány hylv vlasmice jen ty, které Zvláště
poutaly pozornost. Nyní téměř každý rok pozoruje se
dalekohledy několik vlasntic a pozoruje se na hvěz—
dármich i jižní polokoule. Kdyby staří národové byli
znuli dalekohledy a pozorovali jimi-, jak nyní se děje,
vlneatic pozorovaných bylo by mnoho a mnoho tisíc;
vždyt nyní těch vlasntic, jejíchž dráhy jsou vypočteny,
jest ke 400: z těch soustavě sluneční jistě náleži nej-
méně 18, totiž driihn jejich jest známa a doba oběhu
vypočtena: to jsou vlasatice obdobné čili periodické.
Mnohé jiné možná že též se vracejí, ale v dobách
velmí dlouhých. což dosud vyzkoumáno není. Třináct
?. těch 18 vlasatic mnjl střední vzdálenost od Slunce
a dobu oběhu větší než Mars a menší než Jupiter,
jedno větší než Jupiter, tři větší než Saturn. ale menší
než Urnnus.

Každý i přirozený úkaz na nebi, ale neobyčejný,
znvddval příčinu k pověře u lidí, kteří úkazu tako-
vého nedovedou si vysvětliti; neobyčejný zjev vlasa-
lice zvláště veliké býval lidu poslem neobyčejných
událostí: války, moru, pohrom . . . Zn času Neronova
r. 80 po Kr. objevila se Vlasatice, jež zatemňovaln

10

146

paprsky vycházejícího Slunce, jakž Senekn zazna-
menal. _l'lasatice r. 1532 mohlu i ve dne býti viděna
na nebi. R. 1618 na počátku třicítileté války viděna
byla vlasatice s ohonem 100 stupňů dlouhým, tak že
hlava komety byla již v prostřed nebe a konec ohonu
ještě sahal pod obzor a šířil se jako vějíř. Vlasatice
r. 1860 byla tak velká, že hlava její zapadala brzy
po Slunci, ale ohon 70 stupňů dlouhý byl viděn po
celou noc nad obzorem; vlasatice r. 1864 měla ohon
až 120 stupňů dlouhý. R. 1744 objevila se kometa,
jejíž jasnost dne 1. února byla větší než jasnost Sirin,
počátkem březnu zářila jasněji než Venuše v největším
lesku a mohla býti viděna i po poledni, ohon její
hvězdáři páčili na 52 milionů km; ohon komety
v r. 1811 až na 160 milionů km.

U vlnsatic, zvláště u větších, rozeznáváme tři části:
]. jádro, jež vyniká jasnějším světlem a jeví se jako
kotouč planet, jen že není vždy ostře omezen; jeho
průměr bývá. 40—45 km, zřídka přes 7-10 km. 2. Obal
mlhový »- komu — tolik co vlasy; ten u žádné vln-
sntice neschází, někdy obklopuje jádro několik mlho-
vých obalů; průměr toho obalu může obnášeti i mili-
ony kilometrů. 3. Ohon jest bezprostředním pokračo—
váním obalu, co do svčtlosti slábne se vzdáleností od
jádra vlasatice; bývá obyčejně odvrácen od Slunce,
délka jeho bývá rozličná, u některých vlasatíc schází
docela, jakož i jádro může seházeti; objevily se však
Vlasatice se dvěma i s více ohony.

Vlasatice jsou tělesa nebeská pohybující se všemi
možnými směry v prostoru světovém v drahách urči-
tých, pravidelných. Přicházejí-li na své pouti do sou-
stavy našeho Slunce, podléhají zákonum v této říši
platným; mocné Slunce dráhy jejich pozmění a budou

147
pak z pravidla parabolickými, v ohnisku paraboly jest
Slunce. Přiblíží-li se k některému většímu tělesu této
soustavy. na př. k Jupiterovi. i rychlost komety se
změní, bude z dráhy parabolické dráha olliptieká, ko-
meta již ze soustavy sluneční nemůže a zůstáva vní
jako její příslušník, bude periodickou. Tvar drahy závist
od rychlosti, s jakou kometa v určité vzdálenosti od
tělesa centrálního se pohybuje: při rychlosti poměrně
monši bude dráha ellipticka, při rychlosti větší dráha
bude parabolická, zvýší-li se rychlost nad jistou míru,
dráha bude hyperbolická; kdyby Země v přísluní měla
rychlost 42 km za vteřinu, Qráhajejí byla by parabolická,
nevrátila by se již ke Slunci. — Komety, aspoň když
k našemu Sluncí se přiblíží, a pokud mohou býtí
pozorovány, mojí dráhy takové, Že neni snadno hned
poznati. zdaž jsou jejich dráhy ellipsy nebo paraboly:
jsou-li ellipsy, jsou pak velmi prodlouženy a kometa
v příslunt má rychlost ohromnou, kdežto v odsluni
pthbuje se jen pomalu. Vlasatice r. 1843 přišlo Slunci
tak blízko, že její vzdálenost od Slunce obnášela jen
asi třetinu vzdálenosti Měsíce od Země a pohybovala
se rychlostí 1400 za hodinu čili víc než 2 miliony km,
t. ]. s rychlostí asi 562 km za vteřinu; kdyby stejnou
rychlosti se byla pohybovala — ale i v téže malé
vzdálenosti od Slunce — byla by za den oběhla okolo
Slunce asi devět a jednu tretinukrút; v odsluni však
urazila za 46 dní jen úhel ]“ a pohybovala se rychlostí
24 metrů za vteřinu: vzdálenost její cenili na l$l
poloměrů zemské dráhy. Roku 1680 kometu přišla
ke Slunci až na 013 vzdálenosti Měsíce od Země čili
na 232.000 km a vzdálila se od něho až na 426'7
poloměru zemské dráhy čili 126.800 milionů km ; Slunce
bylo by mělo pro pozorovatele na kometě v přísluni
průměru 970, v odsluni však jen 2'. Rychlost pohybu

10'

148
v přísluní byla 530 km, v odsluní však jen 4 m:
doba oběhu vypočtena na 8.810 roků.

Tu nnumne se ihned otázku, proč ty komety od
Slunce nebyly pohlceny, když k nim tak se přiblížily?
Jejich blízkostí zvětšila se rychlost v pohybu; se čtver-
cem rychlosti však roste sila odstředivá, kteráž brání
sklesnuti na Slunce; kromě toho mohou býti vzhuzeny
i sily odpudivé, když tělesu se sblíží mul jistou miru.
Vlasatice tyto maji dráhy ellipsovité, ale ty ellipsy jsou
velmi sploštčlé, v přísluní tu ellípsa, pokud z do! vy-
pozorovaných lze určití, l paraboly se nerozeznává:
postačí jen nepatrně většíoiychlost v oběhu, aby dráha
elliplická přešla v parabolickou a. též postačí i nepatrné
zmenšení rychlosti, aby pnmbola se pozměnila v ellípsu
s menší velkou osou, čímž i doba oběhu hned velice
se mění; proto u vlasatic jest nesnadno určili dobu
oběhu a Paznati. zdaž jest to Vlasatice již pozorovaná.
Největší poruchy způsobuje Jupiter, když vlnsntice
jemu se přihlíží: jeho působením stalo se, že máme
tolik již vlasatic v naší soustavě sluneční.

Tnk ?. mnohéhoredikúla, dostane-li se do ovzduší
mocného pánu. stane se poslušný občánek, jenž 7. mezi
již nevyboči!

Vlasatice zmíněné přiblížily se Slunci až na vzdá-
lenost, kemž sahají větší protuberance. tedy už do
ovzduší slunečního. Jaké síly vzbuzují se v nebeských
tělesech, jež k sobě se sblížila tak velice, kdož to ví?
Budou tu zajisté i síly magnetické a elektrické, jimi
pak vzbuzují se výbuchy. Kometa r. 1843 měla by
— se Slunce hledíc — 1160 v průměru a znkrývalu
by polovici viditelného nebe: tepla a světla přijímala
od Slunce 32.600 krát tolik jako Země, v odsluni však
přijímala tepla a světle 17.300 krát méně než Země

149
a oku jevila se jen jako hvězda průměru 15', tedy ne
větší než Venuše, ovšem ne tak světlá. V přísluní
dostávala světla a tepla 563 milionkrtit tolik jako v od-
sluni. V příslum' všechna její hmota vzplanula a není
divu, že kometa mohla býti viděna u samého krnje
Slunce; výbuchy elektrickými pnl-: hmota její byla od-
puzováun a tvořila ohon. U vlasntic vůbec pozorováno,
že ohony jejich se prodlužují. když blíží se Slunci,
zvětšuje se i jádro a obal jeho, což dlužno přičítnti
zvýšení teploty. — Zdnž ohon obrácen jest ke Slunci,
což zřídka se stává, — nebo od Slunce, nebo konečně
islrnnou. záleží na tom. z jakých látek skládá se
těleso vlasatice, zdnž převládá přitažliVost mezi hmotou
Slunce a látkou vlasntice, nebo je-li odpudivost větší
než přitažlivost; záleží i na tom, odkud ohon Vlasatice
cn se týká postavení Země, Slunce a vlnsatíce, jest
pozorován. Tvnr i sklon ohonu vlnsatico hvězdáři
v nové době vykládají ze zákonů mechaniky.

Z vidmn vlnsntic hvězdúři soudí. že plyn svítící
v nich jest uhlovodík; jsou—Ii v přísluní, jeví některé
i čáru sodíku (nntrium), jenž však opět mizí, když
vlnsmice se vzdaluje od Slunce. Tím se poznává, že
jádro vlnsutice „_ asi malé těleso pevné —.— obklopeno
jest látkami kapalný-mi a kolem všeho že jest obal
plynový.

V přísluní obal plynový mocně se roztáhne, z kn-
pnlin vytvořují se nové páry a jádro mění se v kopa.-
liny, jádro i obal nabýt-aji větších rozměrů: když se
vlnsatice vzdnlujc. chladne opět a zmenšuje se průměr
obalu. Ohon vyvínujc se teprv když Vlasatice blíží
se ke Slunci a má asi svůj původ v elektrickém nn-
pjctí; bude-li látka, z níž obal komety se skládá, stejně
elektrická s obalem Slunce, budou ty hmoty se odpu-

150

zovati, pak-li síla odpudivá větší jest než gravitace, povstane ohon od Slunce odvrácený; bude-li látka komety elektricky indiferentní nebo síla odpudivá rovnati se síle přitažlivé, nepovstane ohon; bude-li síla přitažlivá směrem ke Slunci převládali sílu odpudivou, povstane ohon obrácený ke Slunci; ovšem že možny jsou různé ještě obměny, jimiž se vysvětlují jiné směry ohonů než v čáře vedené od Slunce ke kometě a více než jeden ohon u jedné komety. To jsou hypothesy čili domněnky, proti nimž dá se namítati všelicos, ale přece nejlépe vysvětlují zjevy na kometách pozorované. Zöllner má za příčinu těch zjevů síly elektrické.

Komety lidem již mnohdy nahnaly moc strachu a nejednou byl prorokován i konec světa, jejž měla způsobiti ta neb ona kometa, že by se srazila se Zemí; i kdyby za našich dnů zjevila se kometa taková, jako roku 1858 nebo 1861, pověrčiví lidé by měli z ní velký strach. Věda jeví blahodárný účinek i tím, že nás zbavuje pověry a zla z pověry plynoucího.

Tak i v životě mnohý člověk nahání strachu, ale když bývá poznán ve svých vlastnostech, stává se neškodným.

Vlasatice mají poměrně velmi málo hmoty. Země naše prý prošla již nejednou vlasaticí, aspoň jejím obalem, nebo ohonem a nedoznala poruchy ve své dráze a v době oběhu, ani z toho setkání se nevznikly pro Zemi nijaké pohromy. Jen kdyby Země srazila se s jádrem velké komety, nastala by pohroma, že by při průchodu jádra ovzduším v málo vteřinách vyvinulo se ohromné množství tepla, jež by vše zničilo ve velkém okruhu jestě než by jádro komety ohromnou silou dopadlo k Zemi. Ale Newcomb praví, že dříve

151

by trefil slepec ptáka ve vzduchu, do něhož by na zdařbůh vystřelil, než by se stalo, aby Země srazila se s jádrem komety. Též jest si pomysliti, že by i pevné jádro komety velkým teplem mohlo proměniti se v tekutinu a z tekutiny v plyn. Jak řídká jest hmota v obalu Vlasatice, vysvítá z toho, že hvězdy i pranepatrné skrze obal mohou býti viděny a že
pozorováno to bylo u obalů, jejichž průměr byl větší než milion kilometrů!

Ještě řidší jest látka ohonu, takže jeví se jen jako jemný závoj ve světle slunečním i když má průměr několika milionů km. Je-li ohon z látky plynové, tož plyn ten jest velmi řídký: hustota naší mlhy nedá se ani porovnati s hustotou toho plynu; skládá-li se ohon z jednotlivých pevných tělísek, meteoritů, tož jsou tak daleko od sebe, že ani na krychlovou míli nepřipadá jedno. Kdyby Země procházela takovým ohonem, objevilo by se jen množství Létavic a padání meteorů. Srážka Země s vlasaticí byla by osudnou ne pro Zemi, nýbrž pro vlastníci.

A tak to bývá; když člověk nebo národ málo mocný dá se do půtky s mocným. — Těm lépe jest
vyhnouti se. Velký rozsah ještě nepojištuje vítězství; záleží na jakosti!

Vlasatic jest množství převeliké, podoba a velikost jejich též jest velmi rozmanita, podobně i rozmanité jsou zjevy u nich pozorované; jedním z nich jest, že někdy rozpadávají v přísluní ve dvě, tři i ve více částí, že i zmizí jakožto komety, a místo nich objevuje se
značné množství létavic a povětroňů. Kometa Bielova, objevená roku 1826 baronem Bielou, potomkem staročeského rodu pánů z Bělé, měla dobu oběhu 6 3/4 roku; čím dále jevila větší odchylku v době svého oběhu.

169

Začátkem r. 1846 rozpadla se ve dvě, jež od sebe se
vzdalovaly, r. 1852 byly od sebe již 378 poloměrů
zemských, ještě několikrát vrátily se, až r. 1872, kdy
měly se opět vrátiti, již nebyly nalezeny; mii se za
to, že se rozpadly. Na místě, kde měly se objevili,
bylo pozorováno velikolcpč »padtiní hvězd a létavíc-
dne 27. listopadu 1872 a 1885. Rozštěpení se vlasaticc
bylo viděno již ve staré době, jukž svědčí Senekn:
v době nově tukových případů událo se více. Vlasatict:
na svých drahách občas také narážejí na všuliké pře—
kážky. jest to působení Slunce a. velkých planet. Kdyby
vůz naložený pískem cestou tu a tam jel přes kámen,
otřásl by se, a pisek by se z něho sypal, z vozu by'
ho ubývalo a byl by roztroušen po cestě. Zhruba tak
si můžeme představili. že v prostoru naší soustavy
sluneční komety doznrivnjí všelikych otřesů, že roz-
padtiwúi se v tělíska multi, jež krouží pak kolem Slunce
jakožto mraky tak zvaného kosmického pritchu.

Létavice a povělronč.

Za jasné tmci zvláště bezměsíčnč vidííme občas
mihnnuti se jakoby hvězdičku na nebi: lid říká, Žu
to hvězdičky se čistí, že hvězdičky padají: obrazo-
tvornost pak jeho všelicos o těch »hvězdičkiichc bájí:
že člověk umřel. že jeho hvězda spadla. T y hvězdičky
zanechávají po sobě na kratičkou dobu světlou stopu,
někdy i zazáří velmi jasně, zanechávajícc po sobě
dlouhou jasnmt čáru, již jest viděti i několik vteřin,
a lid říká, že viděl draka letěli s ohnivým ocasem.

Někdy stává se, že taková »hvězdac vždy poma-
leji leti, mění barvy, že konečně se rozplyne a zmizí

158
beze vši stopy; jindy zaso rozprskne se jako raketa,
za několik minut — až 6 ivíce — slyšeti jest výbuch
jako z děla, a potom padá kamení. dva. tři i mnoho
kusů,. až. tisíce se jich počítají: to jest kamenný déšt.
Rímští letopiscové zaznamenali, že na lx'npitoliu padalo
s nebo kamení: těch dešťů kamenných zaznamenáno
i v nové době několik: r. 1790 v .luíllaku v Gaskoňsku.
r. 1803 o poledmich v Aigle,vdépertementu de l'Ome
spadlo 2000—3000 kamenů ztíží 7, bit až 85 kg na
ploše asi 15 km dlouhé a 6 km široké; r. 1898 dne
10. února o půl 10. hodině dopoledne nad Madridem.
r. 1328 dne 22. června spadlo 38 kamenů v krajině
kolem l'leskovíc a 25 u Líb0šic, r. 1808 dne 3. září
paid kamenů u Lisy v Boleslm'sku. r. 1833 25. listo-
padu pud kamenů u Blanska a 5.

Jestliže ty »hvězdičkyc jen kratince se objeví na
nebi a zmizí, jen se mihnou, říkáme jim Iétavicc,
paklí dopadnou na zem. slují pox'čtronč, ohnivé koule,
meteory, bolidy. Těch meteorů bylo nalezeno již velmi
mnoho: V museích můžeme jich viděti velké množství
všech možných velikosti, tvarů, složení a tíže: jsou
meteory jako hrách a menší, jsou též i meteory ztíží
mnoha tisíc kilogramů.

Nordenskičld našel na ostrově Disko v Ovi-
ínku nérolith, jenž byl roztržon asi v 15 kusů, 7. nichž
jednotlivé vážily 20.000, 4.300, 8.500 kg. Povětroň
ležící při Laplatč váží 15.000 kg.

(klkud jsou ty létavice, povélroně, kamenné deště?
V prostom světovém i v naší soustavě sluneční krouží
velké mraky tak zvaného kosmického prachu.
jen že prášky ty, jak nahoře jsme četli, jsou někdy
ohromné: mraky ty. mají v průměru mnoho milionů
kilometrů, kolují rychlostí kosmickou, 30 km a více

154

za vteřinu, pohybují se všemožnými směry jako vla-
satice. Jestliže Země naše prochází takovým mrakem,
některé ty »práškyc dostanou se do ovzduší, trou se
o vzduch, tim vyvine se teplo a ony zazáří. Vzduch
ve vrchních vrstvách jest velmi řídký, neklade tolik
odporu jako vzduch Zemi bližší: »prášekc takový ze
své dráhy nebývá zcela vytržen, zazáří, dokud pro-
chází tenkou jen vrstvou vzduchu a jde svou cestou
dále; to jsou létavice, jež objevují se ve vrstvách vzduchu
velmi vysokých. Jestliže však takový :prašekc nn své
dráze mii proletětí vzduchem blíž Země, pozbývá třením
se o vzduch mnoho síly, energie v pohybu, příliš se
ohřeje. Země jej přitáhne, a on dopadne na Zemi: to
jest povětroň, ohnivá koule. meteor. Povstávnjí síly,
jimiž to těleso bývá roztrženo ve dva. tři i mnoho
kusů a to již ve veliké výšce, mk že výbuch ton sly-
šetí bývá až po několika minutách, — a přece zvuk
urazí za vteřinu 333 m —, teprv pak následuje podání
kamenů. Též se stává, že velkým teplem v ovzduší
ten »prášekc se promění v plyn -— a ztratí se, nchv
padá na Zenu.

My zvime jen o těch létavicích a metoorech, které
se dostávají do našeho ovzduší, a těch jest počet nad
pomyšlení veliký: hvězdáři počítají, že do našeho
ovzduší denně se jich dostane '! až 10 milionů vidi-
telných pouhým okem; kdybychom počítali i ty, jež
viděti jest jen dalekohledem, bylo by jich snnd dvakrát
až třikrát tolik.

Létavico možno viděti za knždé jasné noci: me—
teory padly na zem za dne i za noci, z čehož vysvítá,
že dostávají se do vzduchu ustavičně: přece vídati
jest létavic poměrně nejvíce kolem třetí hodiny po
půlnoci.

165

Ty mraky kosmického prachu mohli bychom
přirovnati !( roji komárů. Kdybychom lim mjem hodili
míč, jenž by se otáčel kolem osy jdoucijeho středem,
dopadali by ti komáři nejvíce na tu část míče, kterou
míč leti napřed: tak ku třetí hodině s půlnoci jsme
na té straně povrchu zemského, kterouž Země otáčející
se kolem své osy v prostoru světovém jde napřed:
proto v tu dobu lémvic vidíme nejvíce. Tn strana
Země, kterou právě pohybuje se napřed, sluje apex.

Však i v různých dobách'ročních objevuje se
větší množství létnvic: tak od 19. do 25. dubna v sou-
hvězdí Lýry a zovou Lyriady; od 9. do 14 srpna
v souhvězdí Persea a zovou Porseidy nebo .i Slzy
svatého Vavřince na památku mučennícké smrti dne
10. srpna r. 258 toho světce, — svátek sv. Vavřince jest
dne 10. srpna; od lzl. do 15. listopadu v souhvězdí
Lva a jmenuji se Leonidy; dne 23. listopadu v sou-
hvězdí Andromedy a síuji Bielidy. O některých rojích
létavíc jesi dokázáno, že původ mají v kometlich; tak
Lyriatly povstaly 7. komety 1861 I; l'eiseidy částečně
: komety 1862 II]: Leonidy : komety 1866 I, Biolidy
z komety Bielovy. kteráž od r. 1852 se neobjevila.

Leonidy objevují se v období po 33 (n V,) letech,
tehdy Země prochází asi nejhustší částí jejich roje:
roku 1899 v noci se 13. na 14. listopadu očekávalo
se zvláště veliké množství Iétavic roje Leonid, ale
ncdostavilo se. Domnívají se, že Jupiter dráhu toho
roje pozměnil.

Jestliže uvidíme v některém souhvězdí několikrát
mihnouti se hvězdičku, dávejme pozm', odkud ony
vycházejí: prodloužíme-li si směr, jímž dvě, tři se
ubíraly směrem nazpět, v průsečíku jejich drah jest
východiště a jmenuic sc rndinčnim bodem čili

156

radiantem. Létavice takové jen zdánlivě pohybují se v drahách divergentních, rozbíhavých. Postavíme-li se na koleje přímé dráhy, zdá se nám, že koleje k sobě se sbíhají tím více, čím dále po nich hledíme; podobně i řady stromů dlouhého stromořadí zdánlivě se sbíhají:
jest to optický klam; tak jest to i s létavicemi, jejichž dráhy zdánlivě v jednom místě se protínají.

Létavice objevují se ve velkých výškách až prý 742 km a teleskopické — viditelné jen dalekohledem — ve výšce až 2.000 km (?). Dle toho vzduch by obklopoval Zemi v mnohem větší výši než obyčejně se udává 150—225 km. Byly však pozorovány létavice ve výši 7—8 km; létavice Perseidy byly pozorovány ve výšce 180—90 km. Leonidy ve výšce 156—97 km. Povětroně, jež dopadnou na Zemi, ovšem do nižších vrstev vzduchu vnikají. Ohnivé koule, bolidy, jsou jasnější meteory, pohybují se obyčejně zdlouhavě, osvětlují svým leskem celé nebe, zdánlivě i někdy většími se jeví než Měsíc a mívají za sebou ocas pomalu hasnoucí. Zjevy ty provází zhusta silné třesknutí, výbuch ve výši až 100 km a více.

Létavice, povětroně, ohnivé koule jen zjevem od sebe se různí. bytostně nikoli, jsou to tělesa nebeská; přece však se zdá, že povětroně (byť ne všechny) tím se různí od létavic, že kolují kolem Slunce v drahách hyperbolických, větší jich počet spadá na Zemi za dne, tedy na té straně, kterou Země nejde v před; létavice považují se za zbytky nebo průvodce vlasatic, o meteorech se soudí, že jsou malá tělesa nebeská, jež krouží prostorem světovým rychlostí asi 40 km za vteřinu. Věc není ještě s dostatek prozkoumána a vysvětlena.

157

Povětroně spadlé obsahují často kovy, až 98%, železo a nikel, siderity, jiné jsou kameny, aerolity. Látky povětroňů jsou tytéž, s jakými shledáváme se na Zemi, někdy obsahují i látky, jichž lučba mezi látkami zemskými nenašla; dosud nenalezena na nich stopa života organického.

O původu létavic, povětroňů, ohnivých koulí byly různé náhledy; že by byly tělesa nebeská, učenci odborníci připustiti nechtěli; povětroně spadlé v Hrašině u Záhřebu v Chorvatsku r. 1751 dne 26. května v 6 hodin večer a stvrzené svědky očitými nebyly považovány za tělesa nebeská. Odborníci dokazovali, že vznikají v ovzduší. tak přírodopisec Ondřej Schütz ve Vídni ještě r. 1790.

Pád povětroňů r. 1790 v Juillaku zjištěn byl 300 svědky, a přece akademie pařížská pokládala pád meteorů za úkaz fysicky nemožný. Bertholon litoval obec Juillak, že má tak zpozdilé představenstvo (jež dalo zjistiti svědky ten déšť kamenný). Nevěřili ani dílu, jež vydal učený Chladni o povětroni sideritu, jejž našel Pallas r. 1794 v Krasnojarsku v Sibiři, a jejž Kozáci ctili jako kámen posvátný, s nebe spadlý.

Vizme, to byla nejvyšší soudná stolice v těch věcech — a tak se mýlila! Však nikdo o tom nemluví a nezazlívá; že církev ihned nepřidala se k novému učení o pohybu Země kolem Slunce, proti kterémuž učení podáno bylo odborníky asi 70 důkazů, to bezpočtukráte ve zlém úmyslu i nyní ještě až k omrzení církvi se přičítá jako tmářství a přilhává se, kde nestačí — pravda jako v procesu Galileiho; nedbali toho, že mnoho katolických kněží náleží mezi nejslavnější hvězdáře — Piazzi, P. Secchi; že dřív než kdo jiný církev vyhlásila astrology — kdož z hvězd věštili —

158

za lidi horší travičů; že kanovník frauenburský Koperník dílo svoje věnoval samému papeži Pavlu III. s předmluvou velmi volně psanou, k čemuž by se byl neodvážil, kdyby nebyl měl k němu důvěry; že Kepleru,
evangelikovi, přáli Jesuité; že evangeličtí theologové v Tubinkách k němu nebyli daleko tak snášelivými jako Jesuité; že díla Keplerova nejdříve si povšimli Jesuilé. Jesuita Terrentius poslal z čínského města Čangčaje list do Evropy a táže se po Keplerově díle »Tabulae Rudolphinae« zvaném.

Toprv déšť kamenů v Aigle r. 1803 věcí pohnul, že učený Biot vše prozkoumal; od té doby učenci již dávali za pravdu názoru, že létavice, povětroně . . . jsou tělesa nebeská.

Pozorování meteorů jest pro hvězdáře věcí důležitou: meteory jsou ovšem jen prášky kosmické, ale mohou dáti vysvětlení o záhadách prostoru světového. Kdo by uhlídl meteor, ať podá o něm zprávu buď osobě známé, jež s věcí se zabývá, nebo do časopisu; pojmenuj místo, s něhož jsi meteor spozoroval, čas a udej v jaké výši nad obzorem, nad kterým místem — kopcem, vesnicí . . . úkaz započal a nad kterým
místem zanikl, v jaké asi výši, měřené úhlem od obzoru, jak dlouho meteor zářil, v jakých barvách, zdaž se roztříštil, rozprskl jako prskavice, raketa, bylo-li slyšeti jakou ránu a jak dlouho po rozprsknutí — tu třeba jest čekati až i pět minut. Vůbec jest potřebí
popsati úkaz co možno zevrubně. Hvězdáři jest nejmilejší, když mu udáme místo, čas, hvězdu, u které zjev počal a hvězdu, u níž zjev přestal. Obdrží-li hvězdář o témž meteoru ze dvou, neb více míst od
sebe vzdálených dobré zprávy, může vypočítati dráhu meteoru, rychlost, již se pohyboval, i místo, kde spadl.

159

## Světlo zvířetníkové.

Za čistého, jasného vzduchu vídati v měsících únoru, březnu a květnu po západu Slunce, když červánky pobledly, na obzoru, kde Slunce zapadlo, světlý kužel, jehož osa nakloněna jest k obzoru úhlem 64°, základ jeho mívá šířky 8° až 30° a výška 40° i více nad obzorem; zjev ten bývá vídán i v měsících srpnu, září, říjnu a listopadu na východě před ranními červánky; světlý ten pruh pozorovateli zdá se býti ve zvířetníku a proto i zjev ten jmenuje se světlem zvířetníkovým čili zodiakalním. V krajinách pásma horkého zvláště zjev ten vyniká a světlo zvířetníkové dosahuje jasnosti Mléčné dráhy v souhvězdí Střelce a jest viditelný po celý rok. Za okolností zvláště příznivých vídati na jaře proti světlému pruhu na západě slabší světlý pruh na východě a na podzim proti světlému pruhu slabší světlý pruh na západě ovšem v ekliptice na protějším místě od Slunce; to jest protisvit; i spoje těchto světlých pruhů ještě slabšího světla byly pozorovány a pojmenovány světelnými můstky.

Zjev světla zvířetníkového až dosud není s dostatek vysvětlen, a jsou o něm různé domněnky: jedni myslí, že elektrickými výbuchy na Slunci látka jeho ovzduší se odpuzuje, že kolem Slunce jest pak takový pruh té bitky na způsob prstene, jehož průměr jest větší než průměr zemské dráhy. Osa zemského stínu obnáší asi 1,300.000 km a má délky víc než čtvernásobnou vzdálenost Měsíce od Země; o půlnoci, kdy Slunce vrcholí dole, osa stínu zemského směřuje k oné části zemské dráhy čili ekliptiky, jež vrcholí nahoře: světlo zvířetníkové však bývá pozorováno i v této části

160

ekliptiky, nebývá tedy zatemněno stínem Země. Zkoumání spektroskopická nevysvětlilo záhady světla zvířetníkového. Podivuhodno jest, že shledána jakási souvislost toho světla jako při severní záři a poruchách magnetických s větší a menší činností Slunce, jež se jeví počtem skvrn na Slunci. jako ten počet mívá svoje maxima a minima, čili že skvrn těch bývá v období průměrně asi desítiletém nejvíce a pak zase nejméně, tak i jasnost světla zvířetníkového bývá ob čas největší a pak zase nejmenší; období to též činí asi deset let. V době, kdy na Slunci jest nejméně skvrn, zdá se, že pruh ten svítí vlastním světlem a je
jasnějším; když skvrn na Slunci jest nejvíce, pruh ten má světlo většinou odražené. Mohl by to býti tedy pruh tělísek malých, kosmického prachu, od něhož světlo sluneční k nám by se odráželo, ale pak by pruh ten měl býti v rovině rovníku slunečního, on však zavírá s ním úhel 7°.

Jiní myslí, že Země kolem sebe má ještě mlhovou látku v podobě prstenu, nebo že Země jako vlasatice za sebou vleče ohon z lehkých plynů, jako by rozšířené ovzduší; zde prý mohou i zjevy elektrické i odraz světla slunečního způsobovati světlo zvířetníkové. Opět jiní myslí, že světlo zvířetníkové má původ svůj v naší oběžnici, v Zemi, jako severní záře.

## Stálice.

Kolik jest hvězd? — Za jasné, bezměsíčné noci, hodně pozdě, tak po jedenácté hodině, v srpnu, v září, kýly bývají noci ještě teplé, položme se na znak pod širým nebem a patřme na hvězdy. Úplné

161

ticho a klid dovoluje nám pozorovali tu neskonalou krásu a velebu hvězdnatého nebe a zamysliti se do nekonečnosti světa nás obklopujícího. Nejdříve nám napadne myšlenka, kolik asi jest těch hvězdiček na nebi. Zamilovaný by si jich přál asi velmi mnoho, když slibuje milé dáti »tolik hubiček, kolik jest na nebi hvězdiček.« Mluví nerozvážně —; což jiného lze
očekávati od zamilovaných!

Čím déle hledíme na nebe, tím více hvězd na něm se objevuje, ale týž člověk nenapočítal by vždy stejné množství hvězd na témž prostranství nebeském; nebýváť vzduch vždy stejně čistý, pak stojíce na hoře nějaké, více hvězd budeme viděti než stojíce na nížině; kromě toho i zrak u různých lidí jest různě bystrý. Hvězdář Argelander napočítal pro střední Evropu 3.300 hvězd pouhým okem viditelných, hvězdář a mathematik Heis napočítal jich 5.421; ovšem měl zrak nad míru bystrý. Na celém nebi severním i jižním člověk dobrého, ač ne příliš bystrého zraku viděl by pouhým okem asi 5.719 hvězd, ovšem za příznivých okolností. Ale to jest jen pranepatrná část všech hvězd.

Z praktických příčin hvězdy dělí se na třídy čili velikosti. Hvězdy nejjasnější, jako jsou na př. Kapella, Sirius, Rigel, Pollux, Antares, Vega, Attair . . . jmenují se hvězdami první velikosti; těch jest na celém nebi 20, 11 na severním, 9 na jižním; hvězdy jako ve Velikém Vozu zadní kola, přední spodní kolo a na voji, jmenují se hvězdami druhé velikosti. Tak jasných hvězd jest na severním nebi 26, na jižním 25, dohromady 51. Hvězdy jasné jako Přední kolo (vrchní) Velikého Vozu, nebo i o něco jasnější, jsou hvězdy třetí velikosti; jsou ještě dobře viditelny za jasné měsíčně noci; těch počítá se na severním nebi 88, na

11

162

jižním 112, celkem 200. Hvězdy jasné jako ve voji Malého Vozu druhá a třetí od Pohárky počítaje, jsou čtvrté velikosti; za měsíčně noci ještě možno je viděti. Těch jest na severním nebi 277, na jižním 318, dohromady 595 nebo okrouhle 600. Hvězdy páté velikosti za měsíčné noci již sotva jsou viditelny, obyčejně mizí, ale když Měsíc nesvítí, dobře je vidíme, jsou jasny jako hvězdička znamenající přední (spodnější) kolo Malého Vozu. Těch počítá se na severním nebi 595 (600), na jižním 618, dohromady 1213. Nejmenší hvězdy viditelné pouhým okem dobrým, ač ne příliš
bystrým, jsou hvězdy šesté velikosti; za Měsíce jich nevidět. Jejich počet na severním nebi páčí se na 1919, na jižním asi 1721, dohromady 3640. Hvězd sedmé velikosti již obyčejně nevidíme pouhým okem; sice hvězdář Heis je viděl ještě, ale on měl výminečně bystré oči; takovému zraku zjevovalo by se na nebi více než dvakrát tolik hvězd než oku průměrné bystrosti.

Jak viděti, počet hvězd roste tím více, čím jest větší číslo velikosti. Co skrývá se oku prostému, objeví se oku ozbrojenému dalekohledem; čím dalekohled jest lepší, větší, tím více hvězd jím spatřujeme; vidíme pak i hvězdy na takových místech, kde pouhé oko nevidí ničeho. Všech hvězd už do desáté velikosti páčí se přes 200.000 a hvězd viditelných největšími a nejlepsími dalekohledy až do 15.5. velikosti odhaduje se 1.650 milionů! Kdyby zamilovaní každý večer dali si po stu hubičklách, vyhubičkovali by ten počet teprv za 45.176 let! − Ale to nejsou všecky hvězdy. Čeho nevidíme ani nejdokonalejším dalekohledem, zachytí ještě fotografická deska, vystavená paprskům hodinu
nebo i více hodin. Kdo by mohl spočísti všechny hvězdy! Kolik jich jest, zůstane asi lidstvu tajemstvím.

168

Mluví-li se o hvězdách, zajisté myslí se hvězdy svítící. To jsou samá slunce, přemnohé i větší a mnohokráte větší než jest Slunce naše. Kolik jest těles nebeských teprv ani zdaleka nedá se odhadovati i v prostorách přístupných oku ozbrojenému dalekohledem. Slunce má přes 570 oběžnic, velkých i malých, náleží k němu i velký počet vlasatic a mraky kosmického prachu: nic nevadí, aby každá hvězdička jako naše Slunce měla své oběžnice, vlasatice a mraky kosmického prachu. Však jsou ještě tělesa nebeská, stálice, kteréž dávno byly již pohasly, jichž nám nelze již viděti ani fotografovati, o jejichž jsoucnosti však hvězdáři přece jsou přesvědčeni. — Kolik těch jest, ovšem nikdo neví a nikdo nám nepoví.

Jakž řečeno, hvězdy třídíme dle velikosti od nejjasnějších jako Sirius až po hvězdičky viditelné již jen nejmocnějšími dalekohledy, od hvězd první až po 15.5. nebo 16. velikost. Tím není řečeno, že hvězda na př. druhé velikosti jest skutečně menší než hvězda velikosti první a větší, než hvězdy ostatních velikostí, ona pouze zdá se nám býti menší než hvězda první velikosti a větší než hvězdy ostatních velikostí; těmi třídami znamenáme pouze zdánlivou jasnost (velikost), o skutečné velikosti hvězd tím není nic pověděno. Kdyby všechny hvězdy byly, jak se zdá, celkem stejně vzdáleny (na jedné ploše »na klenbě nebeské«) a byly na stejném stupni žáru, pak by ovšem hvězdy
jasnější (větší) byly skutečně i většími. Na druhé straně kdyby všechny hvězdy byly stejně veliké a stejně zářily, byly však v různých vzdálenostech, vzdálenější by zdály se býti menšími, protože jasnosti ubývá se čtvercem vzdálenosti, takže hvězda dvakrát, třikrát, čtyřikrát. . . desetkrát . . . stokrát. . . tisíckrát vzdálenější svítila by nám jen světlem 1/4, 1/9, 1/16, 1/100 . . .

164

1/10000, 1/1000000, kráte slabším než hvězda stejně jasná ale v jednoduché vzdálenosti. Ty hvězdy však jsou v různých vzdálenostech, jsou to samá slunce různých velikostí a různé svítivosti, proto může hvězdička jakékoli velikosti, třebas i šesté i patnácté býti skutečně větší než naše Slunce, než hvězdy velikosti první.

Když ty hvězdy nejsou na jedné ploše na »klenutí nebeském«, bude nás vedle počtu hvězd zajímati jejich vzdálenost od nás a jak jsou skutečně veliké.

## Jak hvězdy jsou od nás daleko.

V prostoru světovém dlužno užívati jiných měr, jiných loktů, než jakými měříme vzdálenosti na povrchu zemském; kdybychom chtěli měřiti cestu z Prahy do Říma millimetrem, tož by ten malý millimetr byl přece ještě velikou měrou i proti milionu kilometrů, jež bychom si vzali za jednotku dálkovou ku měření vzdáleností těles nebeských. Proto hvězdáři užívají jiných měr než geografové čili zeměpiscové. Přohlédněme si ještě jednou tyto míry.

Nejmenší jednotkou délky jest poloměr zemský: 6.378 km; k Měsíci bylo by jich asi 60, ke Slunci asi 23.400. Kdybychom chtěli měřiti tímto loktem už k nejvzdálenější zmámé oběžnici naší soustavy sluneční, k Neptunovi od Slunce, napočetli bychom těch
loktů přes 700.000, k nejbližší stálici, sousedu našeho Slunce bylo by těch loktů 6.435 milionů. — Jak viděti, ten loket jest příliš malou měrou, proto hvězdáři užívají lokte delšího za jednotku a sice vzdálenost Země

105

od Slunce, 148.6 milionů km čili okrouhle 20 milionů mil. K nejvzdálenější oběžnici, k Neptunu jest těchto měr 30.5, k nejbližší stálici však již 276.000, ku hvězdě Vega v souhvězdí Lýry jest jich 1,275.000. Již z těchto velkých čísel jest viděti, že i tento loket jest příliš malou měrou pro vzdálenosti těles nebeských od sebe, třebas jest to délka, že přesahuje naše pomyšlení, představiti si ji nedovedeme a jen ji znázorňujeme. Dělová koule letící rychlostí 800 metrů za vteřinu, doletěla by na Slunce se Země za osm roků, rychlý vlak, jenž ujede za tři hodiny 170 km, dojel by se Země na Slunce za 300 let; a přece i tato délka ještě není téměř ničím proti vzdálenostem stálic od nás a od sebe.

Světlo urazí za vteřinu 300.000 km, čili by oběhlo kolem zeměkoule asi 7 1/2 krát za vteřinu. Cesta, již by vykonalo světlo za rok, obnáší 300.000 x 60 X 60 X 24 X 365.24 km t.j. 9.367.280 mil. km; dělová koule by potřebovala na tu cestu 600.000 roků, kdyby letěla rychlostí 600 m za vteřinu; rychlý vlak by k té cestě potřeboval 16 3/4 milionu roků. Světlo se Slunce k nám dospěje za osm minut a 16 vteřin, s Měsíce za 1 1/3
až 1 1/4 vteřiny; což jest tato vzdálenost proti vzdálenosti, kam dospěje světlo za rok! A to teprv jest hvězdářský loket. I celé miliony kilometrů považují se za veličiny pomijitelné, nic neznamenající, drobty, odstřižky. Kdo si to představí? A přece nejbližší soused našeho Slunce jest tak daleko, že by světlo
s něho dospělo k nám, nebo na Slunce — což jest zde jedno, teprv asi za 47 1/2 roku! Dělová koule doletěla by se Slunce na jeho nejbližšího souseda za 2,200.000 roků, rychlý vlak jel by tam přes 72,000.000 let!

Určiti vzdálenost stálice jest práce nad míru obtížná, protože jest měřiti úhly menší než jest jedna vteřina.

166

Opíšeme-li kruh poloměrem jednoho metru, rozdělíme na 360 stejných dílů, stupňů, každý stupeň na 60 stejných dílů čili minut a každou minutu na 60 stejných dílů, čili vteřin, tož taková vteřina na tom kruhu neobnáší ani půl tisíciny milimetru. Poloměr zemské dráhy, téměř 150 milionů km (148.6 mil.) jevil by se ještě menším než půl tisíciny milimetru, asi 3/4 této nepatrné veličiny. Nu takové veličiny jest potřebí velmi přesných měřidel, chyba sebe menší již výsledek měření velice mění. Proto až velikému úsilí a důvtipu teprv v posledních dobách podařilo se ovšem jen zhruba určiti vzdálenost a to jen některých hvězd stálic, asi 50ti, ba snad jen 19ti: při tom tisíce milionů km nepadají na váhu, to jsou v prostoru světovém veličiny pranepatrné. Vzdálenost hvězd udává se v rocích, jež by světlo potřebovalo, aby s nich dospělo k nám, urazí-li za sekundu čili za vleřinu 300.000 km. Údaje ty neshodují se u hvězdářů a dlužno je považovati jen za číslo přibližné. Se Siria světlo k nám by došlo za osm roků, s Attaira za 16—17 roků, s Prokyona za 11—12 let, s Vegy za 20 let, s Arktura prý za 163 roků, s Kuřátek prý víc než za 500 roků (záleží na tom, se které hvězdy v Kuřátkách). Jsou hvězdy tak daleko, že světlo s nich potřebuje tisíce let,
než by k nám dospělo. Co vidíme právě na nebi, to není již přítomnost nýbrž minulost. Kdyby hvězda byla tak daleko, že by světlo k nám potřebovalo 2.000 roků a ona shasla, již před tisícem roků, tož nám, Zemi naší svítila by ještě tisíc let. Jan Neruda v Písních Kosmických správně pěje:

»Čeho my ještě se dožili!
ba na stará kolena divy;
nám minulé vidět lze přítomně,
a mrtvy zřít jakoby živy!«

167

Hvězdy, třebas světlo s jedné na druhou než by dopadlo potřebovalo by čtyři, deset, ba sta let, mohou zdáti se nám blizounko u sebe.

Hvězdami nebe zdá se býti poseto, hvězdička na hvězdičce a přece jsou od sebe tak daleko! Vraťme se ještě k naší soustavě sluneční, co znamená její rozsah mezi stálicemi. − Mysleme si kruh, jehož poloměr by obnášel jeden metr, do středu umístěme Slunce a na obvod poslední známou oběžnici Neptuna 4.467.5 milionů km vzdáleného, tož uvnitř kruhu museli bychom umístiti ostatní členy naší soustavy, pak bychom měli v tomto kruhu celou soustavu jako ostrůvek v moři; teprv 7.000 metrů od toho ostrůvku byl by ostrůvek jiný, soustava nejbližšího slunce. Vizme, jak řídko jsou hvězdy od sebe. Jak veliký musí býti prostor, aby v něm mělo místa aspoň těch zmíněných nahoře 1.060 milionů hvězd? Však zcela jistě víme, že to nejsou hvězdy všechny! — Kam až hvězdy sahají, nikdo neví a sotva kdy který člověk se doví.

## Jak veliké jsou hvězdy?

Bylo-li nesnadno určiti vzdálenost hvězd, ještě nesnadněji jest určiti jejich velikost: poloměr zemské dráhy, okrouhle 150 milionů km, v prostoru světovém jest veličinou pranepatrnou, natož pak poloměr zeměkoule, 6.378 km, nebo i poloměr Slunce, 693.845 km. V naší soustavě sluneční setkáváme se s tělesy nebeskými prarůzných velikostí od Slunce až k malým asteroidům s poloměrem třebas jen 10 km a menším.

168

Co hvězd se týče, máme na mysli jen slunce, hvězdy totiž s vlastním světlem a budeme je srovnávati se Sluncem; toto jest tak veliké, že Země proti němu jest co ořech prostřední velikosti proti sudu, do něhož by se vešlo 182 hl vody. Náš rychlý vlak dojel by s povrchu Země do jejího středu za 4 3/4 dne, s povrchu slunečního do středu Slunce však teprv za 504 dni; dělová koule by tam doletěla s povrchu za 13 1/2 dne, do středu zemského za 2 h 57 m, zhruba tedy za 3 hodiny rychlostí 600 m za vteřinu.

Kdyby dal Slunce do takové vzdálenosti, že by světlo s něho k nám potřebovalo osm roků, bylo by jako hvězdička třetí velikosti čili jako hvězdička označující vrchní přední kolo u Velikého Vozu; kdyby bylo tak daleko, že by světlo s něho k nám potřebovalo 16 let, již bylo by hvězdičkou sotva viditelnou; kdyby však světlo s něho k nám šlo 30 let, již bychom ho neviděli pouhým okem. Z toho následuje, že stálice jsou samá slunce a přemnohá z nich jsou mockrát větší než naše Slunce. Již jeho nejbližší soused, α Centauri, jest větší, Sirius, Kapella, Kastor, Pollux, s něhož světlo k nám prý dojde za 47 let, a jest nám při tom hvězdou první velikosti, musí býti mockrát větší, podobně Polárka; a co říci o Arkturu, hvězdě první velikosti v souhvězdí jménem Bootes, jestliže světlo s něho k nám dospěje teprv za l63 let! Jsou ovšem stálice i menší než jest naše Slunce.

Není sice docela správno jasnost považovati za velikost, jakž to bylo zde učiněno; to bylo by dovoleno, kdyby stejné plochy povrchní hvězd vysílaly stejné množství světla, kdyby byly na stejném stupni žáru a z téže hmoty, což ovšem nedá se tvrdili, ale

169

zde chtěli jsme jen věděti, jsou-li ty hvězdičky menší nebo větší než Slunce a nikoli kolikrát jsou větší nebo menší. Těch, co jsou větší — a mnohokrát větší — bude nepoměrně více než menších.

## Jaký jest žár hvězd a z čeho jsou.

Teplota Slunce byla již mnohokrát určována a shledána od zkoumatelů velmi různě, až i na miliony stupňů ji odhadovali, pak na 20.000° C nyní má se za to, že jest mezi 5.000° až 8.000° C.

Hvězdáři rozdělují stálice na tři hlavní třídy. Do první čítají hvězdy, jichž teplota páčí se aspoň na 150.000° C; sem náleží hvězdy bílé, nejjasnější jako Sirius, Vega, Amir, Deneb, Regulus a j. Do druhé třídy patří hvězdy, jichž světlo jest žlutavé; jejich teplota leží mezi 8.000° a 150.000° C. Sem patří Slunce, Kapella, Arktur, Aldebaran, Polárka . . . Do třetí třídy počítají hvězdy s červeným světlem, teplota jest menší než 4.000° nebo 3.000° C. K nim patří hvězdy jako α Herkula, Ras Algethi. β Pegasa, Seat ο Velryby čili Mira Ceti . . . — O teplotě nyní soudí se dle vidma hvězd. V prostoru světovém jest temperatura nad míru nízká, 273° pod nulou; jest to absolutní bod mrazu. I mezi Sluncem a Zemí jest tak nízká lemperatura, i kdybychom od Země se vzdalovali směrem ke Slunci až po jistou vzdálenost; teplo jakoby se vzbuzovalo znova na Zemi dopadem paprsků. Kdo mluví telefonem z Prahy do Vídně, vzbuzuje vlny vzduchu u přístroje telefonu v Praze, a ve Vídni přístroj vzbuzuje vlny nové podobné vlnám vzbuzeným

170

na př. v Praze; nejde snad vlna vzduchová z Prahy do Vídně. Tak mocných vln hlas lidský nedovede vyvoditi, aby byl hlas slyšitelný z Prahy až do Vídně; podobně i Slunce není horké tak, aby hřálo až i na Zemi z příma, jako hřejí naše kamna tím více, čím blíže k nim přicházíme.

Tělesa nebeská jeví se na různých stupních vývinu, jakoby v prostoru světovém odehrávalo se to, co děje se na povrchu zemském: rostlina vyvíjí se, dospívá vrcholu vývoje, zraje, chřadne — a hyne; tak i hvězdy vyvíjí se, dospívají největšího lesku, blednou, červenají — a hasnou, jen že v dobách, s nimiž i nejdelší věk lidsky nedá se nikterak ani porovnati.

Z čeho hvězdy jsou, hvězdářům zjevuje vidmo hvězd; oni paprsky hvězdy zachycují, propouštějí hranoly ze skla, zkoumají zvětšovacími skly barvy a temné čáry vidma z nichž pak usuzují, kterých látek plyny hoří na povrchu těles nebeských. Zdá se, že v prostoru světovém ve hvězdách soustředěny jsou hmoty tytéž, jakéž nacházíme na Zemi: ovšem hmoty takové nejsou ve skupenství pevném, nýbrž aspoň na jejich povrchu hoří jejich plyny.

## Zdaž stálice skutečně stojí.

Jindy myslivnlo se, že hvězdy stálice nemají po-
hybu, ale tomu tak není: i ty hvězdičky pohybují se
rychlostmi kosmickými, t. j. urazí mnoho kilometrů
za vteřinu a pohybují se různými směry. Nám se zdá,
že sestaveny jsou ve všeliké vzorce, tvary, říkáme jim
souhvězdí a že souhvězdí tn podržují týž tvar. Sou-

171
hvězdí Velikého Vozu, Orionu atd. mělo před několika
tisíci lety podobn " tvar jako dnes, a přece jednotlivé
hvězdy v nigh rázný-ni směry. se berou kosmickými
rychlostmi. Čím to jest, že zdánlivě nemění vzájemné
polohy ani velikosti?

Postavíme-li se na tmt, vlaky podle ons uhúněji
velikou rychlosti: dívámeJi se však s nčjnk'é hory do
roviny. vlnky zdaji se jeli tím menší rychlostí, čím
jsme „od nich vzdálenější; kdybychom mohli viděti
vluk jedoucn rychlostí 10 m za vteřinu se vzdálenosti
10.000 km a patřili na něj celou hodinu. sotva bychom
poznali pouhým okem. že se hnul 3 místa a přece by
objel kolem nás v takovém kruhu již asi za 76 dní.
Hvězdy, jakž jsme viděli, vzdáleny jsou od mis tak
ohromně, že ani celý věk lidsky. ani tisíc let nepo-
stačí, aby pouhým okem mohlo býti spozorovtino, že
se hnuly 5 místo, byt pohybovaly se rychlostmi mnoho
kilometrů zn vteřinu. K vyšetření pohybu hvězd jest
potřebí velmi přesných přístrojů: ale jimi teprv vyšetří
su jen složku pohybu kolmá na zornou přímku. Pohyb
hvězd směrem od nás k hvězdě nebo naopak vyše-
třuje se z pošinutí čar quenhoferových ve vidmu
hvězd. Vidíme-li, že hvězda hnuln se s mistn — jestliže
poznnčeno bylo třebas před sto lety její “místo — tož
není to celý její pohyb, leč děje-li se kolmo na zon-nou
přímku. chdícc v noci podél přímé trati na vzdálený
vlak, nepozorujeme jeho pohybu, soudíme však, že
Se pohybuje. dle svítilny: vzdaluje-li sc vlak, světlo
slábne. blíži-li se. světlo nnbýwi větší jasnosti, ovšem
i lomoz jest silnější.

Vzájemná postaveni hvězd k sobě v souhvězdích,
ač ony třebas různými směry od sebe rychlostmi
kosmickými se pohybují, ve stu, ba v tisíci letech

I??

nemění se tak, aby zmena mohla býti poznam bez
přístrojů, ale za 50.000, 100.000 let souhvězdí budou
přece vypadati jinak; hvězdáři již nyní kreslí obrazce
souhvězdí Velikého Vozu, Kassiopee, Oriona atd., jak
budou vypadati za 50.000 nebo 100.000 let. Pohybujedi
se hvězda stálice a je-Ii středem soustavy těles ne-
beských, bere své oběžnice s sebou na pouti prostorem
nebeským, jako i Země provázena jest Měsícem na své
pouti kolem Slunce.

lSlunce se všemi oběžnicemi a jejich měsíci,
vůbec ,s celou soustavou putuje nesmírným prostorem
směrem k souhvězdí Herkula. Je-li tomu tak. tedy by
hvězdy tohoto souhvězdí měly zdánlivě se rozestu-
povati a hvězdy souhvězdí, od nichž se vzdalujeme,
měly by zdánlivě k sobě se blížiti, jako přijíždíme-Ii
vlakem ke stanici s elektrickými lampami, tyto, čím
více se blížíme, tím zdeji se býti od sebe vzdáleněj.
šimi, odjíždíme-li, zdají se státi tím hustěji při sobě,
čím více se vzdaltíjeme.

i toto zdánlivé rozestupování se hvězd souhvězdí:
k němuž se blíží Slunce se vším, co k němu náleží:
mohlo by býti spo'zomváno bez měřidel snad až za
tisíciletí. Rychlost pohybu Slunce hvězdář-i udávají od
osmi až kn patnácti km za vteřinu. .Iiné stálice mají
pohyb rychlejší, opět jiné méně rychlý.

Jaký jee! skutečný vlastní pohyb, rychlost a směr,
velmi nesnadno jest určití, protože stanovisko naše
není pevné: pohybujeme se kolem Slunce a. se Sluncem
v prostoru světovém; směr, kam Slunce spěje, přesné
určen není, ani jeho rychlost, vzdálenost hvězd není
též známe. — až na malý počet hvězd. Pokud hnutí
5 místa posuzuje se jen dle směru, úhlem, hvězdy

178
prvnich velikosti, tedy jasnější (ovšem ne všecky) jeví
pohyb rychlejší, z čehož dalo by se souditi, že celkem
jsou bližší než hvězdy sotva viditelné nebo viditelné
pouze dalekohledem.

Poněvadž všechna tělesa nebeská se pohybují,
slovo stálice zajisté“ neznememi již hvězdu, která
by stálo; suilic v tomto smyslu není. Pohybem toliko
udržují Se; kdyby těleso nebeské pozbylo pohybu,
bylo by po něm veta: Země na. př. kdyby z jakýchkoli
příčin zastavila se v pohybu kolem Slunce, ihned by
řítila se směrem ke Slunci a padla by do něho. Prav-
divá jsou slova: »Co chce žiti, at s:.- hýbá.:

Změny mist hvězd poznávají se dle záznamů, dle
map. Číňané. Indové. Egypťané již byli poznali, že
hvězdy nezůstávají nehybně na svých místech; ze
záznamů. jež pořídili Timocharis a Aristyllus, poznal
Hipparch, že hvězdy se pohybuji a pořídil seznam
hvězd na tu dobu velmi důkladný. Plolomneus též
pořídil katalog hvězd, a dle něho hvězdář Halley po-
zoroval změny ve vzájemném postaveni hvězd !( sobě;
nyní již více než při 4000 hvězdách hvězdáři poznali
vlastní pohyb.

Přesné seznnmy hvězd čili katalogy maji v této
příčině velikou důležitost, proto věnuje se jim veliká
péče. Hvězdář Piuzzi, kněz. po něm ijiní sestavili
katalog 70.000 hvězd; Argclnnder dal podnět k sesla-
vení katalogu hvězd až 9. a 10. velikosti. Od severního
pólu až k obratníku Kozorožce jest v Argelenderově
seznamu obsaženo přesně vyměřených posic hvězd
až 9. velikosti 145.000. — Ještě velikolepějšl dílo
podniknuto ?. návodu francouzského admirála Mou-
chez-a, kteréž by obsahovalo všecky hvězdy i nej-

174

lepšími dalekohledy vůbec viditelné — až po Iti.
velikost. Nebe rozděleno na pásmo, 18 hvězdármim
na celé zeměkouli přidělena jednotlivá pásma, aby je
fotografovaly. pak poSice vyměřily. Poněvadž všech
hvězd až po 16. velikost byl by velký počet milionů
a nemohly by býti kutalogísovány, tož usnešeno, aby
posice hvězd až po tl. velikost byly vyměřenya ka-
talogisovúny — však i to jest práce ohromná, neboť
i těch hvězd jest na tři miliony. Jak ohromnou prt'tci
jest vykonali, nby hvězdné nebe takto bylo ofotogm-
fováno, poznáme, uvážíme-li, že na fotografickou desku
12x12 cm možno ofotografovati dva stupně do čtverce;
celé nebe obsahuje tukových stupňů 41.000, ledvbylo
by potřebí 21 .000 desek, kdyby fotografovalo se jen
jednou. Však to nepostači, nebot nebudou fotografie
všecky podařené, musí se fotografovali aspoň dvakrát
každé místo. ba i vicekráte, neboť vadám všelikým nelze
so vyhnouti; tedy sotvn méně než 50.0% fotografických
desek jest potřebí: ale to jest teprv menší díl práce.
hlavní úkol teprv jest provésti vyměřovánim.

7. tukové práce pro vědu hvězdářskou plyne ve-
liký proxpěch. Pndnikne-li se dilo takové později. za
moc?—it. za sto let. bude se viděti. jaké změny nastaly
v posici hvězd. bude ae moci přesněji vyměřiti jejich
pohyb: ze změn vubec nnstalych bude se moci vč-
děti o hvězdách více, než, jest to možno nyní. Však
idřívc než za padesát nebo sto let budou takové
seznamy kontu't důležité služby; opčtným pozorováním
n fotografováním ukáže se, kde a kdy vyskytly se
nové hvězdy a které se změnily, neboť i ty hvězdy
nezůstávají stejnými. — Na světě všecko so měni.

176
Hvězdy mčnllvé.

Bylo již řečeno, že hvězdy i stárnou a nosnou,
že jsou na různém stupni žáru, že doznávaji tedy
zm_čn; takto mění se všechny. Hvězdnmi měnlivými
však rozumí se takové, které v různých dobách mají
různou! jasnost. což po jistých dobách se opakuje;
období tn jsou ode— dvou dní a několika hodin až celé
roky. že táž hvězda objevuje se v různém lesku, že
mění svoji vvelikostc, vlastně měli bychom říci, že
mění jasnost.. Al gol čili hvězda ;? v souhvězdí Persea
na př. měni jasnost ve dvou dnech a 21 hodinách,
že 2: hvězdy 2'2 velikosti sklesnc na hvězdu 3'5 veli-
kosti, tedy víc než o jednu třídu. Dvn dni a dvanáct
hodin just hvězdou 2-2 velikosti. puk v devíti hodinách
sklesne na hvězdu až téměř čtvrté velikosti: v nej-
menším lesku potrvá jen nei pntnáct minut. Hvězda
Mira Ceti čili a Velryby. »hvězdn podivná: mění se
v 33'G dnech 2 hvězdy l'T—f'rn velikosti na hvězdu
8—9'5 velikosti; považíme-li jen hlnvní změnu, sklcsnc
tedy 7. hvězdy druhé až na hvězdu osmé velikosti. že
pouhým oltem není ani vitlitelnn. U některých hvězd
jest změna“ pravidelna “ jiných však nc. Hvězdy měn-
livé hvězdář Pickering dle vidina rozdělil na pět
tříd: Počítá. se jich nyní již k pěti stům,- velmi mnohé
7. nich jsou hvězdy do červena zbarvené »stárnoucic.

L' všech příčina změny není dosud vysvětlena,
zvláště u těch, jež mění se pravidelně; kde změny
jsou prnvřdelné v obdobách ne příliš dlouhých, u hvězd
čcrvenavých a červených, hvězdáři soudí, že na jejich
povrchu tvoří se pevniny; hvězdy otáčejíce se kolem
osy. ukazují brzy světlejší, brzyr méně švětlý povrch

176

— a to pravidelně. Které hvězdy mění se jen v malo
dnech jako'nn př. Algol v Perseu, tam udavaji se za
příčinu oběžniCe tmavé těch hvězd, kteréž přijdouoe
do zorné přímky mezi naše oko a hvězdu. nám ji
častečně zastiňují. L' Algola skutečně i objevena taková
oběžnice tmavá: vypočtena její velikosx, hmota. vzdá-
lenost a doba oběhu.

ňvězdy nové.

lx'u hvězdxim proměnlivým náleži tak zvané hvězdy
nově. Od nejstarších dob bylo ob čas pozorováno
vzplanutí nových hvězd. Dokud nebyly známy daleko-
hledy, mohly byti zpozorovány pouhým okem jen
hvězdy nápadné, velké. což ovšem nedělo se často:
od dob, kdy vynalezeny byly dalekohledy mocné. a
co užívá se za pomůcky fotografie. pozoruje se poměrně
velmi často vzniknutí hvězd nových. — 1 to jsou
hvězdy měnlivé, ale toho řádu (dle l'ickeringn řádu
prvého), že mihla vzphmou, po nějakou, krátkou dobu
sviti, světlosti rychle pozbývají _„ již v několika mě-
sících stávají se pouhým okem neviditelný-mi. a možno
je pak pozorovali jen dalekohledem —' až některé
zmizi i ?. dalekohledů.

V souhvězdí Kassiopee dne ll. listopadu r. 1572
vzplanula hvězda jasnější než první velikosti, že i oby-
čejným lidem byla nápadnou. Hvězdář Tycho de
Brahe v Praze ji pozoroval a s ní se zabýval. proto
jmenuje se i hvězdou Tychonovou. Na začátku r. 1574
byla již jen hvězdičkou b. velikosti a v březnu r. 1574
zmizela úplně pouhému oku.

17!

Roku 1604 zazářila hvězda nová v souhvězdí
Hadonoše. světlejší než hvězdy první velikosti, počátkem
roku 1606 zmizela úplně prostému oku. Jmenuje se
hvězdou Brunoy-ského, protože tento hvězdář jí
pilně pozoroval. V souhvězdí Persea objevila se
roku 1901 dne 21. února hvězda nová třetí velikosti.
dne 22. února byla již hvězdou 'vellkostí první, dne
IB. březnu byla hvězdou jen třetí velikostí, dne 10.
dubna byla již sotva viditelnou; později mohla byti
pozorována jen dalekohledy; však mezi tím, co jí
ubývalo lesku í měnila svou jasnost. Vyskytnuti se
nových hvězd teleskopických, totiž hvězd viditelných
jen dalekohledem, není velkou zvláštností.

Hvězdy jmenuji se novými, protože se myslílo,
že skutečně byly to hvězdy nové. lidé nedovedli sí
vysvětliti tento zvláštní úkaz mezi hvězdami.

Od těch dob, co do služeb hvězdářských vstoupila
spektralní analyse. rozbor totiž světla těles nebeských
procházejícího hranolem, soudí se jinak. Zn příčinu
náhlého vzplanutí udává se bud' výbuch plynů, jímž
prolomí se kůra hvězdy již tmavé, žhnvé nitro vyleje
na“ povrch — a září. Druhá příčina může .býti, že
tmavé již těleso nebeské dopndem jiného tělesa ne-
beského se rozezhaví: energie pohybu ohromného
tělesa pohybujícího sc miminnou rychlosti promění se
při srážce v teplo, jež postačuje. aby těleso tmavé
opět zářilo. Kdyby Slunce bylo již pohnslé a Země
do něho spadln. zazářila by opět. »— Však i jiný
výklad má mnoho. bo více pravděpodobnosti pro sebe.
Hvčzdáři vyšetříli, že v prostoru nebeském jsou roz-
sáhlé mraky jemně rozdělené látky, mlhoviny, akosmickú
mračna: mnoho milionů km v rozsahu. Jestliže

12

178

tmavá již těleso nebeské (tedy neviditelné) na své
dráze vnikne do takového mraku. třebas velmi řídkého.
zmenší se odporem jeho rychlost. tím energie pohybu
ubývá. ale nastupuje oteplení. jež postačí, aby povrch
toho tělesa se rozežhavěí. a pak zhaslá hvězda září
znova. Čím jest mrak ten rozsáhlejší, tím déle bude
těleso v něm trvali. tím déle tedy bude zářiti. Záleží
pak i na směru, kterým mrak i hvězda se pohybují:
největší odpor nastane. jestliže pohybují se proti sobě,
pak novú hvezda bude i nejvíce zářítí. Nebude-li hustota
mraku vždy stejna, nastane kolísání v jasnosti nové
hvězdy. jakéž i' skutečně se vyskytuje u některých
»novýchc hvězd. Naše meteory jsou jinak tmawi
tělíska; dostanou-li se na své dráze do našeho ovzduší.
at ve výškách, kde vzduch jest náramně řídký, má
přece pro velikou rychlost jejich pohybu i v řídkém
vzduchu nastane odpor. energie pohybu častečně mění
se v teplo, a meteor září. Podobně má se to s lětavicemi.
jež nám se jen mihnou zdánlivě mezi hvězdami; Iide'
to jmenují čistění hvězd.

Též býva pozorováno, že hvězdu viditelná daleko-
hledem vzplnne a stane se jasnější, že pak její jasnosti
opět ní:-ývá. I to dá se vysvětliti : vniknutí do kos-
mického pmchu. V přístroji. kterýmž rozkládá se světlo
a tvoří se vidmn, ve spektroskopií, pozoruji se růzmi
vidme. na sebe poiožemí,a z těch se soudí. že hvězda
prochází nějakým kosmickým mrakem.

Mohla by povstali též movú- hvězda tak, že by
světlo hvězdy nesmírně daleké teprv nyni, když hvězda
se objeví, došlo k nám. Ale pak by zajisté neSVítíla
taková hvězda jen málo měsíců a nepozbývala by tak
rychle lesku a. nezmizela by za krátký poměrně čas.

179
Když tedy vznikají občas hvězdy move: a leskem
převyšují i nejjasnější hvězdy. pak zase mizí. není
proč považovali za nemožnost hvězdu jasnou, o niž
nám vypravuje evangelium o narození Páně a dle niž
mudrcnvé přišli do Betléma.
Hvězdy podvojné a pomnožné.
Díváme-li se velmi pozorně na hvězdu prostřední
ve vojí Velikého Vozu — jmenuje se Mizar »—
vidíme u ní hned malou hvězdičku v levo nahoru od
ní — zdá se nám, že jest vzdálena jen asi dva centimetry
od jasné hvězdy Mizara. Ta malzi hvězdička jmenuje
se Alkor. Kdo ji vidi, má dobrý zrnk. Však hvězdy
pouhým okem od sebe rozeznatelné. třebas zdanlive
velmi blízké, nepočítají se k hvězdám podvojným.
l):ílckohledem hvězda .l-lízar sama rozdělí se nám na
dvě hvězdičky, jednu jasnější 3. velikosti, druhou pak
méně jasnou ."). velikosti vc vzdiilemíati od sebe 14"
(lat vteřin). Podobně dvojhvězdnou jest ,? Labutě. skládá
se z hvězd 3. a ň'l, velikosti. vzdálených od sebe
34". Hlavní hvězdu je.—.t žlutá. menší jest modni.
?. velkých hvězd dvojitými jSou Aldebaran v sou-
hvězdí Býka. Rigel v souhvězdí Oriona. Sirius
v Souhvězdí Velkého Psa. Antarus v souhvězdí
Stím atd. Všech dvojhvězdi mezi hvězdami počítá se
přes 11.000 a vždy nalézají se ještě nová dvojhvězdí.
U některých pozmi se i malým dalekohledem, ze
hvězdu jest dvojitá. jestliže jednotlivé hvězdy jsou
dosti jasné a. od sebe aspoň 14". Čím jedna z nich
(nebo i obě) jest menší 9.. 10. a menších velikosti,
12'

18)

čim pak jsou sobě bližší. tím mocnějšich dalekohledů
jest potřebí, aby byly pozm'my jako dvojhvězdy: ně-
které vůbec nedaji se od sebe ani nejlepšími daleko.
hlady rozděliti a poznúvnii se jen spektroskopem dla
vidma.

Ale ne každá dvojice hvězd sobě velmi blízkých
jest i skutečným dvojhvězdím; dvě hvězdy mohou
býti za sebou v téz zorné přímce a jinak Spolu ne-
souvisí: takové dvojhvězdy slovou optickými dvoj-
hvězdnmi, ty pak, které tvoří pro sebe soustavu otá-
čející se kolem společného těžiště, jsou pravými,
l'ysíckými dvojhvězdnmi. Hvězdáři dovedou vypo-
čítati poměr hmot jednotlivých složek. dráhy jejich
pohybu, dobu oběhu. jejich vzdálenost od sebe. Dvoj-
hvězdím věnuje se zvláštní pozornost.

Ke dvojhvězdim zajisté patří i m, při nichž jedna
složka jcst již těleso tmavé jako na př. dvojhvězdi
Algola v souhvězdí Person: budou tedy mnohé
2 hvězd měnlívých dvojhvězdími.

Nezřídka stává se, že tři hvězdy jako hvězda ';
v souhvězdí Raka. čtyři i více hvězd tvoří pro sebe
soustavu: oku prostému jeví se jen jako hvězdu jedna:
takové hvězdy slují potrojné, počtverné. pomnožné,
na př. v souhvězdí Oriona. Z hvězd pom-ejných ne-
zřídka hlavní hvězda. pro sebe bývá opět podvojnou,
jako na př. dvojhvězda Rigel, ,? Štíra: hlavní hvězdu
dvojhvězdy bývá i měnlívou, jako hvězda ?) souhvězdí
Kefea; snad že bude dvojitou.

Hvězdy podvojné mají pohyb kolem společného
těžiště, kteréž leží blíže středu většího tělesa a sice
tolikrát bllžc, kolikrát má více hmoty než těleso druhé,
jež náleži ko dvojici.

l$l

Mají-li tato tělese opět oběžnice jako Slunce má
kromě jiných oběžnici Zemi, tož dráhy jejich ze. účinku
dvou těles centrálních budou jiné než dráhy obéžnic
v naší soustavě. Budou-li hvězdy potrojné . . . míti
své oběžnice, dráhy těchto oběžnic budou za působení
tři . . těles centrálních ještě složitějšími. Vypočítáváni
takových drah činí obtíže náramně, namnoze ještě
nepřekonatelné.

Jaký život by, byl na takové občžnici náležející
soustavě dvojhvězdi', trojhvězdí, kdož dovede pověděli:
oběžnici hvězdy podvojné svítila by slunce dvě, z nichž
(obyčejně) každé má jinou barvu. Mohlo by v jistý
čas vycházeli :! zapnduti obě zároveň; v jiný čas jedno
by snad vycházelo. třeba červené a všechno zbarvilo
do červena, jako když díváme se na přírodu skrze
červené sklo, a když by toto zapadlo, vycházelo by
druhé — snad zelené » a zbarvilo'hy všecko do
zelena. V jiné barvě přírodu by se jevila, kdyby svítila
ziiroveň 'obě slunce, jedni) jako slunce odpoledni,'dmhé
jako dopolední. Každé z nich mohlo by míti své
oběžnice větši, zriřicí jejich světlem, jako na př. oběžnice
našeho Slunce: Merkur, Venuše, (Země), Mnrs, Jupiter,
Saturn . . sviti světlem slunečním: každé toto slunce
mohlo by míti své vlasntice. jako je má Slunce naše
i své mraky kosmického prachu, jež jsou nám příčinou
zjevu »čistění hvězd: —'- létavic a meteorů; jaké úkazy
by tím vznikaly obyvatelům (kdyby jich tam bylo) na
takové občžnici hvězdy podvojné! A jaké teprv no
oběžnici hvězdy Pon-ojné . . . byla by možnost roz-
mauitostí a divum přírodyl Na Zemi naší jedno
Slunce vykouzluje přebohalou květenu a zvěřenu a
jinak bohatost zjevů přírodních; kdo dovede povědětí
a vylíčiti bohatství květeny, zvěřenya zjevů přírodních
— možných — za působení slunci dvou, tří . .ř

189
Shluky hvězd.

Již pouhým okem spatřujeme na nebi v některých
místech hvězdy jakoby nahromaděné u tvořící soustavy
pro sebe, nejznámější jsou asi »K u řátk ac v souhvězdí
Býka. V souhvězdí Persea téměř uprostřed mezi sou.
hvězdím Kassiopee ». jasnými hvězdami souhvězdí
Pet—sea. za jasné noci, když nesvítí Měsíc, pouhému
oku jeví se jako světelný mráček málo větší než plocha
Měsíce v úplňku; podíváme-li se však dalekohledem
s čočkou jen u cm v průměru, objeví se nám natom
mlstě nesčetné množství hvězdiček, dva shluky: světlý,
jak se nám zdálo. obláček rozdrobí se v samé hvěz-
dičky. “Podobně se to má se shlukem hvězd v sou-
hvězdí Ruka; i zde světlý obláček v dalekohledě
rozdrobí se na samé hvězdičky.

Ukaz ten snadno si vysvětlíme takto. Díváme-li
se v noci z daleka na velkou stanicí osvětlenou
elektrickými obloukovými lampami. jež jsou od sebe
padesát i sto metrů daleko, vidíme jen zář, svetlo
lamp“ splývá dohromady. Hledíme-li z blízka, vidíme
každé světlo zvlášť; podobně i v dalekohledě vidíme
každé světlo zvláště, protože dalekohledy mint vzdálené
věcí sbližují.

Těch shluků hvězd, ovšem viditelných jen daleko-
hledem. jest veliké množstvl, počítají jich do tisíců.
V takových shlucich hvězd vidíme, čím blíže ke středu
shluku, tím více hvězd, což- jest zcela přirozeno,
jestliže prostor. jejž zaujímá shluk, jest kulovitý a hvězdy
v něm by byly od sebe aspoň zhruba ve stejných
vzdálenostech: hledíme-li ke středu, budeme vidět

188
těch hvězd více v zorné přímce (za sebou), než hle-
díme-li ku kraji.

Cim dalekohled jest mocnější. tim viac takových
světlých mráčků se pozoruje a tim snáze ly mrúčky
podaří se rozdrobíti v samé hvězdičky. Počet hvězd
v takových shlucích bývá “ltd pomyšlení velký,
Vilém H e r s e h e l. jenž sobě pořídil ohromný dalekohled
zrcadlový, počítal jich na ploše sotva toliké jako desátý
dil zdánlivé plochy měsíční. aspoň 5.000: v některých
shlucích hvězdář-i páči hvězd až mi 40.000! Jsou-li
ty hvězdy od sebe tak daleko. jak daleko jest nejbližší
soused našeho Slunce od mis, že by totiž světlo :; něho
k nejbližší jemu hvězdě ve shluku dospělo už za
47, roku. jaký prostor by zmijimal jeden takový shluk,
jenž nám .se jeví na prostranství sotva lolikém co plochu
Měsíce; jak jest daleko od nás, jak daleko jest potom
jeden shluk od druhého! To zdají se býti soustavy
pro sebe se :společuým střediskem: jestliže ty hvězdy
v nich mají obežnice, a tyto. opět měsíce. tož shluk
hvězd byl by soustavou soustav.

Jestliže tukový světlý mráček dalekohledem dri
se rozložili ve hvězdičky, slove mlhnvinou optickou.

Ale všecky světlé mničky nedsdí se i nejlepšími
dalekohledy rozdrohiti v same hvčzdičky. zůstávají
i v nejmocnějších dalekohledem světlými mráčky
s jednou nebo i více hvězdami. někdy jako by se kryl
shluk hvězd s takovým nerozložitelným mničkem:
tyto světlé mráčky jmenuji se mllmvinumi skutc-
čnými čili fysickými.

184

## Mlhaviny.

Okem neozbrojeným lze viděti za jasné noci bezměsíčné v souhvězdí Andromedy nade druhou ze tří jasných hvězd tohoto souhvězdí světlé místo; v dalekohledě objeví se pak jako podlouhlý světlý mráček uprostřed hustší a světlejší, s hvězdičkou. Tento světlý mráček již žádným dalekohledem, jakéž dosud máme, není rozložitelný ve hvězdičky, jest to mlhovina skutečná, fysická. V souhvězdí Oriona, pod pasem jeho, podobně viděti jest světlý mráček, mlhovinu. Hvězdář Argelander počítá mlhovin okem neozbrojeným viditelných celkem 19, bystrozraký mathematik a hvězdář Heis uvádí 29 mlhovin viditelných ve střední Evropě.

Teprv obrovskými dalekohledy zrcadlovými, jež pořídil slavný hvězdář Vilém Herschel (největší z nich 40stopý dohotoven r. 1789), později lord Rosse r. 1845 shotovil ještě větší s délkou ohniska 17 m, se zrcadlem průměru 1.8 m, otevřen pohled do nesmírna a objeveny pravé divy v prostoru světovém k radosti a úžasu hvězdářů. Byly to zejména měsíce oběžnic, hvězdy podvojné, potrojné . . . pomnožné, shluky hvězd a mlhaviny.

Vilém Herschel objevil na tisíce nových předmětů v prostoru světovém dílem sám, později s otcem pracoval rovněž slavný hvězdář, Sir John Herschel a vydal seznam mlhavin do roku 1864 objevených počtem 5.079. Však lord Rosse, Lassel obrovskými dalekohledy objevili nové mlhaviny. d'Arrest a jiní počet mlhavin novými objevy rozmnožili takže nyní se jich zná již asi 8.000! V. Herschel sám rozeznával

185

osm tříd mlhavin: majiť mlhaviny prarúzné tvary. Všude rozmanitost, nejen v přírodě na Zemi. v nerostech, rostlinách a zvířatech, pak ve zjevech přírodních, nýbrž i v prostoru nebeském v předmětech kosmických!

Shluky hvězd nejčastěji se pozorují v Mléčné dráze a v blízkosti Mléčné dráhy; mlhaviny pravé vyskytují se zvláště v krajinách vzdálených od Mléčné dráhy, zřídka v její blízkosti: v souhvězdí Panny a Lva jest veliké množství mlhavin, podobně v souhvězdí Kštice Bereniky pod Velkým Vozem.

Co do tvaru nedají se snadno tříditi. protože my je pozorujeme jen s jedné strany, jak totiž promítají se nám na nebe, přece však jmenují se zvláštní tvary mlhovin. Planetární slují ty, jež oku ozbrojenému jeví se jako kruhovité plochy všude stejně husté, na př. nedaleko hvězdy β Velikého Vozu. Prstencovité jeví se jako prsteny nezřídka s hvězdičkou uprostřed ba i s hvězdami na vnitřním i zevnějším okraji. Kdyby mlhavina byla podoby kruhovité, jevila by se nám jako nějaká deska kruhová, kdyby byla podoby prstencovité a my na ni patřili se strany, ne kolmo na její plochu, bude se nám jeviti jako ellipsa více nebo méně sploštělá; takových mlhavin vyskytuje se velmi mnoho. Mlhaviny tvaru kulovitého, s každé strany jevily by se kruhovitými; mlhaviny tvaru ellipsoidového mohly by se jeviti kruhovitými nebo ellipsovitýmí dle toho, kterou stranou k nám jsou obráceny. Jindy mlhaviny mají podobu závitnice, mlhaviny spirálovité, ba mnohé mlhaviny, které ve slabších dalekohledech ukazují tvar ellipsovitý, v nejmocnějších dalekohledech jeví se býti spirálovitými, na př. v souhvězdí Lva, Ohařů (honicích Psů), Kštice Bereniky. Hvězdy s mlhavinavými paprsky jsou hvězdy jevící se na okraji mlhaviny, kteráž

186

jedním směrem se protahuje, někdy i rozdílným tvarem se jeví. Však i jiné podivné tvary mezi mlhnvinami jsou zastoupeny.

Jako hvězdy, rozeznávají se i mlhnviny jednoduché, podvojné, potrojné i pomnožné, ba i shluky mlhavim též změny lze pozorovali na mlhavinách. Studium mlhavin není tak staré jako studium hvězd; pokud nebylo dalekohledů velmi mocných, o mlhavinách nevědělo se téměř ničeho, až sestrojeny byly velké
dalekohledy, a do služeb astronomie pojmu fotografie a spektrální analysa v novější době, teprv studium o mlhavinách (ovšem v astronomii vůbec) utěšeně pokračuje.

Shluky hvězd a mlhaviny jsou asi nejvzdálenějšími předměty v prostoru nebeském; bylo-li nesnadno určiti pohyb hvězd, ostrých světlych bodů, pro jejich ohromnou vzdálenost, bude tím nesnadněji určiti pohyb mlhavin, jež nejsou ostře ohraničeny a mají větší rozsah, jsou-li vzdálenější než převeliká většina hvězd; ovšem pravděpodobnost nasvědčuje tomu, že i mlhaviny mají pohyb v prostoru světovém; ne-li dalekohledy a fotografie, poučí nás o nich lépe spektroskop.

Co pravé mlhaviny jsou, dojista ještě se neví, ale hvězdáři snášejí se v domněnce o nich, že jest to hmota ještě nezhuštěna v tělesa nebeská, hmota rozptýlená, z níž že teprv hvězdy se rodí; v prostoru světovém mohli bychom tedy pozorovati hmotu, z níž někdy — za miliony let — budou hvězdy; v některých viděti jest bod světlý, jako by zárodek hvězdy, nebo i více těch světlých bodů; v jiných již vývin pokročilejší, jiné na vysokém stupni vývinu, že téměř všechnu hmotu kolem sebe — někdejší mlhavinu — jako by

187

v sebe vssály a zhustily, jiné, kolem nichž není již pozorovati rozptýlené hmoty; to jsou hvězdy hotové v nejjasnějším lesku. Dříve bylo již řečeno, že pozorujeme slábnutí lesku — stárnutí — světlo hvězdy jest žlutavé, u jiných již žluté a červenavé, opět
u jiných červenavé až čeřvené, červené do tmava, jiné pozbyly již úplně vlastní záře, jsou tmavé, nesvítí, jako by byly mrtvy. Kdo představí si čas od prvního zárodku hvězdy až k jejímu vývinu a odtud až ku zhasnutí, když i tisíc let u přemnohých nepostačuje, aby byla zpozorována jen nepatrná změna vůbec, jako
bychom nepozorovali změny na věkovitém dubě hledíce na něj jen minutu!

## Mléčná dráha.

Bylo již řečeno nahoře, že V. Herschel světlé mráčky, »mlhaviny« rozložíl v samé hvězdičky; co nepodařilo se Herschelovi obrovským dalekohledem, podařilo se Rosseovi dalekohledem ještě větším, rozdrobiti ještě i některé »mlhaviny« ve hvězdičky, kterých dalekohled Herschelův nerozložil. Kdyby se někdo tázal, jakým asi zdál by se svět člověku, kdyby se octl uprostřed takové »mlhaviny«, tož byla by odpověď ta, že asi takovým by se jevil mu svět, jakým jeví se nám svět náš, kteříž jsme tež asi uprostřed takové mlhaviny. Vidíme na nebi světlý pruh nestejné šířky na různých místech a nestejně světlý, místy se dělí v pásma dvě, jež spojují se opět. Pruh ten jde po celém nebi kolem do kola. Čím důkladnějšími dalekohledy pozorujeme ten světlý pruh,

188

tím lépe poznáváme, že jsou to samé hvězdičky zdánlivě tak blízko u sebe, že svit jejich splývá v jedno jako záře mnoha světel, ale vzdálených, tak že jednotlivých již nelze rozeznávati. Čím místo některé je světlejší, tím hustěji jsou tam hvězdičky u sebe. — Myslíme-li si kolmo na onom kruhovitém pásmu uprostřed přímku, tož místa na nebi, kam přímka ta směřuje oběma konci, jmenují se póly Mléčné dráhy. Nejvíce hvězd jest v onom pásmu, ale čím více se vzdalujeme od něho k pólům, tím méně hvězd spatřujeme na nebi.

Mléčná dráha zdá se býti soustavou soustav slunečnich; Slunce se vším, co k němu patří, jest jen nepatrnou částečkou této ohromné soustavy, jejiž rozsah jest tak veliký, že světlo při svojí úžasné rychlosti potřebuje celá tisíciletí, aby dospělo od kraje do středu. Tvar té soustavy zdá se býti čočkovitým. Kdybychom
v prostoru, jenž by měl podobu ohromné čočky, myslili si hvězdy ve stejných vzdálenostech od sebe, hleděli pak ze středu směrem ku hraně ostré, viděli bychom těch hvězd tolik, že bychom jednotlivých všude ani nemohli rozeznati od sebe, poněvadž světlo jejich by splývalo: čím více bychom pohlíželi k pólům, tím méně by hvězd bylo za sebou, tím méně bychom jich viděli,
jakž to skutečně spatřujeme u Mléčné dráhy. Kolik takových soustav slunečních, jako jest naše — sluncí totiž — tvoří Mléčnou dráhu, nikdo neví.

Kdyby nám bylo možno vzdálili se na přímce kolmé uprostřed kruhu Mléčné dráhy o deset průměrů tohoto kruhu, jevila by se oku našemu jen jako kruh průměru 5.50 stupňů, ve vzdálenosti jednoho sta těch průměrů, byla by kotoučkem jen asi půl stupně v průměru, tolikým jen, jak Měsíc zdá se nám býti; pak žád-

169

ným dalekohledem bychom již nerozeznali jednotlivých hvězd od sebe, byla by to mlhavina nerozložitelná; spektroskop ovšem by ještě prozradil, že ona »mlhavina« vlastně jest shlukem hvězd.

Ve vzdálenostech ještě větších, Mléčná dráha jevila by se mlhavinou ještě menší — až i nejlepšími dalekohledy sotva by byla viditelnou jakožto světlá kulatá mlhavinka několika málo minut obloukových v průměru; tvar byl by kruhovitý. — Kdybychom se vzdalovali v přímce na pásmě Mléčné dráhy šikmé, viděli bychom ji v podobě ellipsy tím sploštělejší, čím přímka stála by šikměji k rovině pásma, a tím menší, čím dále bychom se octli od něho.

Mlhavin těch, jež vlastně jsou shluky hvězd, jakž ukazuje spektroskop, čítá se na tisíce. Jsou-li to takové mléčné dráhy, jako jest naše, kolik jest v nich hvězdm jak daleko jest od jedné ke druhé, když průměr takové mléčné dráhy mizí proti vzdálenostem mléčných dráh od sebe, ač průměry ty světlo by proběhlo teprv za mnohá tisíciletí: jak veliký jest prostor, jenž to všecko v sobě pojímá! — A ještě nesmíme říci, že jsme pronikli až na meze veškerého světa: čeho nezhlédne oko, uvidíme dalekohledem, kam nedohlédneme dalekohledem menším, dohlédneme dalekohledy mocnějšimi, čeho nevidíme i nejmocnějšími dalekohledy, zachytí ještě fotogratickti deska a to tím více, čím jest citlivější a čím déle byla exponována čili působení světla vystavena; snad najdou se ještě jiné dosud neznámé způsoby pronikati dále do prostoru svělového, a možná, že v té příčině proti časům budoucím jsme na tom stupni, na jakém byli hvězdáři dřívější proti hvězdářům nynějším. — Kde všemu jest konec, kde jsou meze, za nimiž by již nebylo hvězd?

190

»Mysli se nejvýš — a nad tebou
hvězd jako vřesných zvonců —
a kdybys byl jako slunce stár,
nedomyslíš se konců.«

Jan Neruda.

Rozumem lidským nelze obsáhnouti té velikosti světa; a přece vidíme jen část celku, jen »cíp«, a nemůžeme ani říci, kolikátou část, vidíme snad jen jakoby předsíň velkolepého paláce.

## DOSLOV.

Již zalétli jsme až v nekonečné prostory: vraťme se zpět na Zemi. Čím jest ona proli Slunci? Tolik co do obsahu, čím jest ořech prostřední velikosti proti sudu, jenž jímá 182 hl vody; čím jest Slunce proti soustavě, jejíž jest bezprostřední částí; čím ta soustava proti soustavě soustav takových slunci; čím taková soustava — mlhavinka — proti všemu, co jest nám dostupno i důmyslnými přístroji; čím to všecko proti véškerenstvu v nekonečném prostoru? A to jen po stránce velikosti, mnohosti!

Jak stará jest Země naše, nikdo neví: ať od jejího vzniku uplynuly tisíce milionů let, ať potrvá ještě tisíce milionů roků, tož proti stáří sluneční soustavy bude to přece jen chvilkou; celé trvání její jest co trvání jiskry

»vylétla jiskřička z plamenu*)
a zčernalá zpět zas padá.«

Jun Neruda.

*) Totiž vylétla ze Slunce a do něho zase padá.

192

Čím jest stáří soustavy proti stáří soustavy soustav, takové jednotky proti vyššímu celku; čím stáří takové vyšší jednotky proti trvání všeho, co můžeme ještě viděti; čím to proti věčnosti? — To po stránce trvání.

Jaká jest rozmanitost nerostů, rostlin a zvířat, zjevů fysikálních na Zemi — nikdo ji ještě nepřehlédl — co jí objeví drobnohled ve stavbě buněk; co jí bude snad ve světech naší soustavy, co ve veškerenstvu? Co síly v sopkách, jež svými výbuchy otřásají dalekým okolím, zvedají ostrovy z oceanú, co síly v pohybu celé Země jakožto tělesa nebeského, co v pohybu
soustavy sluneční kolem tělesa jiného . . .? Tak možno zamýšleti se všemi směry, všude nekonečnost. A člověk proti Zemi není ničím ani co do hmoty, ani co do obsahu prostorového, ani co do trvání, ani co do síly. — To vše jaksi po stránce hmotné.

Spatřuje-li se ve světě mikroskopickém zákonnost, účelnost, vidíme-li zákonnitost a účelnost ve přírodě pozorované smysly neozbrojenými, vidíme-li zákonnitost ve světě teleskopickém, a nedá-li se mysliti zákonnitnst a účelnost, jež by mohla povstali náhodou: jakou bude bytost, kterouž dlužno považovati za původce tak velkolepého světa? Nebude člověk po stránce rozumové proti bytosti této tím, čím jest po strance tělesné proti hmotnému veškerenstvu? Myslíme si čísla i sebe větší ale určitá, nějak představitelná v mysli, proti číslu nekonečnému — třebas nekonečnému jen vzhledem k naší chápavosti — tož všechna přece jsou pouhými nulami. — Nač potom ještě býti pyšným?

Dle těla člověk naprosto mizí již proti Zemi a to všemi směry. Země sama téměř ničím není v soustavě sluneční, proti shluku hvězd . . . . Po stránce duševní člověk jeví se býti zcela jiného řádu: duchem pojímá

198

věci tak veliké; podmaňuje sobě síly přírodní, proti nimž síly jeho tělesné mizí — pára, elektřina, třaskaviny . . .; duchem zalétá v taje nedostupné smyslům tělesným ani ozbrojeným; není omezen prostorem ani časem. Úkony duševní jsou podmíněny sice u nás i hmotou, potřebujeme totiž k myšlení mozku; ale ze samé hmoty myšlení pochopiti nelze.

Mnohost ve veškerenstvu, ať byla jakkoli veliká, nepochopitelná, přece tolik neznamená co zákonnitost, účelnost, krása . . . — v té mnohosti; — člověk si to uvědomuje a nepřestává jen na tom, co pojímá smysly: on o tom přemýšlí, hledá příčiny, od díla přichází k tvůrci.

je-li co na člověku velikého, jest to jeho rozum, jím podoben jest Bohu; poznává a tvoří, jest účasten božských blažeností, kdykoli se mu podaří poznati nějakou pravdu, objeviti některý zákon přírodní, způsobiti něco dobrého. Ovšem nemyslím slovem »rozum« jen důmyslnost a ostrovtip. Tento rozum jest naším šlechtickým diplomem, jímž odlišeni jsme ode všech jiných tvorů; s tímto diplomem smíme vkročiti v předsíň paláce Nejvyššího a podivovati se té velikoleposti Jeho děl, smíme takto skrze Jeho díla pohleděti na Něho samého, jakž toho štěstí dostalo se největším učencům, nejdůmyslnějším mužům všech dob.

Již pochopíme, proč slavný Ampère, drže se za hlavu oběma rukama, často říkal: »Jak veliký jest Bůh, jak veliký jest Bůh«; proč veleduch Newton nevyslovil jména Bůh, aby neobnažil hlavy; již pochopujeme proč velký Koperník se vyslovil: »Jestliže kdo zahloubá se myslí do všehomíra, kterak jest krásně uspořádán a řízen božskou moudrostí, kterak by neměl stálým pozorováním a takřka důvěrným obcováním

194

(se všímmírem) povznášen býti k věcem nejvyšším, aby se podivoval Tvůrci světa všemohoucímu, v Němž spočívá nejvyšší blaženost, v Němž vrcholí všechno dobré!«

Veliký Kepler, objevitel zákonů, dle nichž oběžnice pohybují se kolem Slunce, onen Kepler. jenž všechen život svůj věnoval vznešené vědě hvězdářské, napsal: »Ty otče světla, jenž světlem přirozeným budiš v nás touhu po světle milosli, abys přivedl nás ke světlu věčné slávy, děkuji Tobě, můj Stvořiteli a Pane, že jsi mi dopřál radovali se z Tvého stvoření a že jsem rozplýval se slastí nad dílem rukou Tvých!«

Slavný hvězdář Mädler napsal: »Pravý přírodozpytec nemůže býti neznabohem. Kdo tak hluboko nahlédnul jako my do dílny Boží . . ., ten v pokoře musí pokleknouti před prozřetelností svatého Boha«.

Protož končím slovy, jimiž jsem začal:

»Klečím a hledím v nebe líc,
myšlenka letí světům vstříc
vysoko — převysoko —
a slza vhrkla v oko.«

Jan Neruda.

## OBSAH.

Strana
Proslov . . . . . . . . . . . . . . . . . 3
Pohled na hvězdnaté nebe . . . . . . . . . . . ?
Původ jmen souhvězdí a hvězd . . . . . . . . ll)
Mléčná dráha . . . . . . . . . . . . . . l! a 187
Jména souhvězdí a hvězd . , . . . . . . . . . IB
Ekliptika . . . . . . . . . 49
Dráha Měsíce . . . . . . . . čl
Drihy oběžnic . . . . . . 54
Soustava ekliptikální . . . . . . . . . . . . _ _ . 55
Rovník . . . '. . . . . . . . . . . . . . . . . ňii
Soustava rovníková . . . . . . . -. . . . , . 67
Jak se určuje čas vůbec. Rovnice časová . . . . . 59
Hodiny nebeské. . . . . . . . . . . . . . . . 64
Jak poznáváme po hvězdách kolik jest hodin . . . . . 056
Rozdíl dnů . . . . . . . . . . . . . , . . . . 72
Slunečné hodiny . . . . . . . . . . . . . . . 73
Jak povrh-i so dulum po Mčslci . . . . . l . . . 77
Jak pomivn'mc roky po hrčzdich . . . . . . . . 80
Zpčmy' pohyb bodu rovnodennosti : důsledky . . . 82
Driha měsiční . . . . . . . . . . . . _ . . . . 85
Pól ekliptiky . pól rovníku . . . . . . . . . . . . 87
Sklon ony oběžnice k rovině jeji drilu: kolem Slunce a důsledky 89
o vellkoaii naši soualavy slunečné . . . . . . . . . . 96
o teple slunečním . . . . . . . . . . . . . . . ICI)
O světle slunečním . . . . . . . . . 106
0 skvrnich nl Slunci . . _ . . . . . . . „ . . 109
Jakým „loví uo okrnl Slunce . . . . . . . . . . . . 112
Přitažlivost Slunce . . . . . . . . . . . . . . . 114
Mčslc . . . . . . . . . . . . . . . . . 114
mnam: Měsíce . . . . . . . . _ . . . 194
Merkur . . . . . . . . . . . . . . . . . . IST
Venuše . . . . . . . . . . . „ . . . . m

sun-
Mm „. . . . . . . . . . . . . . . . . . . UŠI
l'Innololdy un uteroldy . . . _ . . . . . . . . . . IM
Jupiter. . . . . . . . . . . . . . . . . IM
Saturn . . . . . . . . . . . . . . . . . 189
Uranu. . . . . . . . . . . . . . . . . . HI
Neptun . . . . . . V. '. . . . . . . . . MB
Vlasulco . . . . . . . . . . . . . . . . . . . Md
Létavlco . povčuonč . . . v . . . . . . . . . IM
Svčllo zvíře—mílové . . . . . . A . . . . . . . . lw
Sinice . . . '. . . . . . . . . . _ . . _ . . 100
Jak hvězdy jsou od nás daleko . . . . . . . . IM
Jak veliké jsou hvězdy . . . . . . „ . , . . . . 167
Jaký jen “: hvčxd . z čeho jsou . . . . . WD
ldlt twice skutečně stojí . . . . . . . , . . . l'iO
Hvězdy minllvé . . . . . . . . *. . . . . . 175
Hvězdy nové . . . . . . . . . . _ . . . . . . I'm
Hvězdy podvojné : pomnoiné . . . . . . . . . . 175!
Shluky hvězd . . . . . . . . . . . . . . _ . 182
Mlhavlny . . . . . . . . . . . . . . . . . . . IM
Mléčnt dráha . . . . . . . . . . . . . . . . . 187
Doslov . . . . . . . . . . . . . . . . . . . m
0 p r a v y :
Str. :n 5. Kód: z do!- — nvmía misto mylnfa
- 24 s. . . mnm- oříšky - oříšky
» 24 m. . . . __ x . K
- 88 8. . - doh — Con-us - Corona
- 85 19. » - webu — Junony - Jun-on '
— 48 15. - » - — lovišwýn - Jovišgv
- 107 12. - - doh — propodtěji - popouštčjl
- 115 _3. . » vrchu'u 11.26.me patří Init.
- 129 8. . » v“ — Nm! mlno bisnl
» IBS 8. - » . ' — za slovo Slunce ; mino .
- 188 19. v » v — -hnily- —' průplavy
- 151 19. » » doh — Tč misto Těm

This is a digitized (scanned) book '0 hvezdach',
written by Frantisek Nabelek and published in

1 906, and as such is probably in the public
domain worldwide.

Itwas digitized by me, Miloslav Ciz, on 1 2 August
201 9. I reserve no rights that may have arised by
me scanning or processing the book and I waive
all my rights with the cca 1.0 waiver (https:ll
creativecommons.orglpublicdomainlzerol1.0l).
My intention is to keep the book completely in the
public domain.
