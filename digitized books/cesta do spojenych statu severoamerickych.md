Heslo: *„Svůj k svému a vždy dle pravdy.“*

# Cesta do Spojených Států severoamerických.

Podává

úředně schválená jednatelská firma česká

JOS. PASTOR.

**v Hamburce a v Novém Yorku.**

Tiskem dra Edv. Grégra v Praze. — Nákladem vlastním.

1891.

Leden. Unor. Březen.
Neděle 41118251 81522 1 8152229
Bondělí 5 12 19 26 2 9 16 23 2 9 16 23 30
Uterý 6 13 20 27 3 10 17 24 8 10 17 2431
Středa 7 14 21 28 4 11 18 25 4 11 18 25
Čtvrtek 1 813 22 29512 1926 5 121926
Pátek 2 91623306132027 6132027
Sobota 31017 24317 142128 7142128
Duben. Květen. Červen.
Neděle 5 12 19 26 3 1017 24 61 7 14 21 28
Bond ělí 6 13 20 27 4 11 18 25 1 8 15 22 20
Úterý 7 14 21 28 512 19 26 2 9 16 28 60
středa 1 s 15 22 29 6 13 20 27 3 10 17 24
čtvrté 2 9 16 23 30 7 14 21 28 4 11 18 25
Pátek 3 10 17 24 l s 15 22 29 5 12 19 26
Sobota 4111825 2 91623 30 6132027
Červenec. Srpen. Zářl.
Neděle 5 12 19 26 2 9 16 23 30 6 13 20 27
Pondělí 6 13 20 27 3 10 17 24 31 7 14 21 28
Úterý 7 14 21 28 4 11 18 25 1 8 15 22 29
Stí-eda 1 8 15 22 29 5 12 19 26 2 9 16 23 30
Čtvrté 2 9 16 23 30 6 13 20 27 3 10 17 24
Pátek 3 10 17 24 31 7 14 21 28 4 11 18 23
Sobota 411 18 25 1 815 22 29 512 19 26
Říjen. Listopad. Prosmec.
Neděle 4 11 18 25 1 8 15 22 29 6 13 20 27
Pondělí 51219 26 2 9162330 7112129
Úterý 61320273101724 1 6152229
Středa 71421284111825 2 9162330
Čtvrtek 1 81522295121926 910172191
Pátek 2 9162330 613 2027 4111926
Sobota 3 10 17 24 31 7142128 6121926

TODOOOOOOOOOOOOOOOOO

NORMANNIA

Doppel-Schrauben-Schnell-Dampfer

der Hamburg-Amerikanischen 

Ceškonárodní jednatelství pro přímou paroplavbu mezi Hamburkem a New-Yorkem.

Prodej parolodních i železničních lístků.

Výměna peněz.

Výplaty a t. d.

Filialní závod: New-York, 1432, I. Avenue.

**JOS. PATOR.**

Hlavní závod: Hamburg, 23 kl. Reichenstrasse.

Spediterství do zaoceanských zemí.

Bankovní závod.

Čechům, cestujíČímpřes moře mým prostřednictvím, dávám na památku knihu „České osady v Americe“ a „Kapesní slovník esko-anglický“ skvostně vázané a věnuji jim veškeré služby ochotně a úplně zdarma.

**V Hamburku**, 20. května 1891.

## Jízdní pořádek.

**Mezi Hamburkem a Novým Yorkem** přímo bez přesedání jezdí parolodě od 20. května 1891 jak následuje:

**Poznámka.** Za lodí v závorce naznačuje (exp.) = expressní, (pošt.) = poštovní, (obyč.) = obyčejnou a (nákl.) = nákladní loď.

Odjezd z Odjezd zpět '
Jméno parolodě Hamburku do z New Yorku
New Yorku do Hamburku
Normannia (exp.) . . . . . . . . 22. května 4. června
Wieland (pošt.) . . . . . . . . . . 24. » 13. »
MOFGVia (obyč.) . . . . . . . . . . 27. » 16. »
Columbia (exp.) . . . . „ . . . .. 29. » 11. »
Rugia (pošt.) 31. » 20. »
Marsala (nákl.) . . . . . . . 3. června 27. »
Fiirst Bismarck (exp.). . . .. 5. » 18. »
Geliert (pOŠiL) . . . , . . . . . .. 7. » 27. »
California (obyč.) . . . . . . 10. » 30. »
Augusta Victoria (exp.) . . . 12. » 25. »
Dania (pošt.) . . . . . . . . . . . . 14. » 4. července
Sorrento (nákl.) . . . . . . . . . 17. » 11. »
Normannia (exp.) . . . . . . . .. 19. , » 2. ,
Rhaetia (pošt.) . . . . . . ._ . . . 21. » 11. »
Bohemia (obyč.) . 24. » 14. »
Columbia (exp.) . . . . . . . “.“. . 26. » 9; »
Sueviav(pošt.) . . . . . . . . 28: » TS.-' »
Gothia (obyč.) . . . . . . . . . . 1. července
Flirst Bismarck (exp.). . . . . 3 » lil. »
Wieland (pošt.) . . . . . . . . . . 5. » " . 25, .
— “ ů

Mezi Baltimorem a Hamburkem
_ budou se konat od 1. června následující výpravy:
_._ — * , v ; Odjezd. z . Zpáteční od-
; Paroloď Hamburku do jezd z Báltim.
, Baltimore do Hamburku
Scandia . . ; .. , -——— _ ». 23. květma
G0thia................. -— 6.června
Slavonia . . . . . . . . . . . . . . . i 6. června 4. července
Scamha 20. » 18. !
Mami. Hamburkem ra Montrealem
budou se konat od 19. května následující výpravy:
v ' ' Odjezd z Odjezd
Jméno parolodě Hamburku do z Antwerp
' Montrealu do Montrealu.
Steinhóft . . . , . . . . . . . . — 8. května
Kehrwieder— . . . . . . . . . . 23. května 29. »
Gremon. . . . . . . . . . . . . . . . . 3 června. 5). června
Baumwall.......,l....... 13. » 19. „
' Převozní ceny v mezipalubí
  v bankovkách od'l. dubna '1891._
I. z. Hamburku 6.9 Nového Yorkua
Po nákladní parolodi 63 zl. r. m.
Po obyčojné parolodi 63 zl. r. m. .
Po poštovní parolodi 68 zl. 70 kr. r. m.
Po expressní parolodi 74 zl. 50 kr. r. _m..
IL Z Hamburku do Baltimore: '
Po obyčejné parolodi 63 zl, r. 111. ' '
„ , V).—JMÝŠLQM

Ceskonárodníf jednatelství
pro
přímou paroplavbu
, mezi
Hamburkem
a.
New-Yorkem.
Prodej parolod-
nich i železnič—
ních lístků.
Spe-
V , „ diterství
mena do za-
penez. oceánských
zemi.
Výplaty
„ t_ d_ Bankovní
závod.
Čechům,
cestujíČímpřes moře
mým prostřednictvím,
dávám na památku “knihu
„České osady v Americe“
a. „Kapesní slovník česko-
anglicky'“ skvostné vázané
a. věnuji jim "véškeré služby
— :oohotně a úplně zdarma.

— 3 —

## Úvod

Odbyl jsem 1863 gymnasiální školy. Na universitu nebylo peněz. Co počít? Odebrati se do Ameriky. Myšlenka zrála, až uzrála. Jak se tam ale dostat? Nevěděl jsem, kudy kam. Odebral jsem se do Prahy, předpokládaje, že tam někoho najdu, jenž by mně poradil. Po dlouhém hledání octnul jsem se u tehdejšího konsulárního agenta Spojených Států severo-amerických. Ani mi nemohl povědět, kde v Americe ponejvíce Čechové usazeni jsou. Lodní lístek ale že prý mi obstará. A také obstaral. To bylo vše. Slavná firma to nebyla a česká také ne. Platil jsem tenkráte 65 tolarů na. paroloď, která jela přes moře 23 dni. Nějakých porad nebylo. Cestování sám jsa neznalým dělal jsem chybu na chybu. A přec jsem uměl německy a byl jsem absolvovaný gymnasista. Každá chyba stála peníze a nádavkem byla k tomu ještě samá mrzutost a zlost. Kdo jaktěživ daleké cesty nekonal, hmatá ve tmě a ta věčná nejistota působí na člověka strašně. Mluvím ze zkušeností, proto doufám, že podrobným pojednáním o jízdě do Spojených států severe-amerických posloužím rodákům, již tuto cestu ponejprv konají. Poučení a spolehlivá rada uspoří trampot a. peněz. Prospěch krajanů byl stále přední snahou mou. Ač mé povolání pohybuje se v mezích pouze jednatelských, tož přec činnost má, jak lidsky tak národně, je ryzá a upřímná. Kdo by pochyboval, nechť nahlédne v mé obchodování, jež jest knihou otevřenou.

— 4 —

## Co je Amerika a co jsou Spojené Státy.

Amerika je díl světa, jako jsou díly světa Evropa, Asie, Australie a Afrika. Spojené Státy, do nichž vystěhovalecký proud z českých vlastí se ubírá, jsou jenom částí Ameriky právě jako Rakousko je částí Evropy. Uvádím v dolejší tabulce výměru a obyvatelstvo celé Ameriky, tedy všech říší, z nichž se Amerika skládá, a. čtenář ihned pozná, že Spojené Státy, ač nejen největší nýbrž i nejmocnější říše Ameriky, přec nezaujímají ani jednu čtvrtinu americké rozlohy. (K vůli porovnání má. Rakousko s Uherskem 473,626 čtvr. kilom. a 41.500.000 obvv.)

Jméno státu kilometry Obyvatelstvo

1. Spojené Státy . ,. . . 9,068.272 62,480.000

Jezerwa pobřež..vodstvo 382.971 —-

2..Brasil.ie. . . .. 8,337.213 12,933-000
3. Mexiko . .: . . . ., . 1,946292 10.448.000
4. Britská. država. . 9,221.162 6,525.000
5. Columbia. .. . '. 830.723 3,879.000
6. Argentina. . . . .) _, 2,835.9'691 13,026.000
7. Peru . . . . . . . _, 1,049.270 2,972.000
8. thle . . . . . . . 753.216 2,527.000
9. Spanělskáydržava. . . . 128.147 2,306.000
1'0.“'Boliwia ; .. . . . '1,139.255 2,308.000
11. Venezuela '. . . . . 1,639.B9.8—' 2,198.000
12. Ecuadori . . . . . 650.938 1,505.000
.13. Guatemala . . '. . 121.140 1,358.000
'14'. Haiti. . .. ,' . . . . 23.911 800.000
115. Salvador . . . .. 18.720 634.000
'16. Uruguay . . _, .“ . “.? 186.920 596.000
17. Dominiko . . . . . 53.343 504.000
18. Francouzská država. . 124.500 383.000
19. Paraguay . . . . . . 238.290 » 370.000
'20: Honduras . . . „_ . 120.480 323.000
21. Nikaragua . . . *. . 133.800 260.000
22. Costarieav. “. .. . . . . 51.760“ 214.000
23. Nizozemskádržava v. 120.451 119.000
“ 24. Dánská Západní Indie 359 34.000
Uhrnem . . 39,176.506 118,'697.000

— 5 —

## Do kterých přístavů Spojených Států severo-amerických se jezdí?

Lodě, s kterými možno do Spojených Států severoamerických z Evropy se dostati, plují do následujících přístavů: 1. do Bostonu, kam jezdí však jenom lodě anglické, 2. do Nového Yorku, 3. do Philadelphie, 4. do Baltimore, 5. do Nového Orleansu a 6. do Galvestonu. Do posledních dvou přístavů nejezdí parolodě pravidelně, nýbrž jenom občasně, když právě je dobrá vyhlídka na zpáteční náklad. Do Nového Yorku, kde se soustřeďuje skoro veškerý obchod Spoj. Států, kam tedy nejvíc zboží a nejvíc lidí z Evropy se hrne a kde zároveň pro zpáteční cestu na cestující i náklad nejsnáze počítati se může, jezdí nejvíc lodí ze všech přístavů, a co pozoruhodno, lodí největších, nejlepších a nejrychlejších, což se dá snadno vysvětliti přílišnou konkurrencí. Do Philadelphie a Baltimore trvá jízda po vodě skoro o 8 dní déle než do Nového Yorku a lodě tam jezdící nejsou nikdy první třídy. Nový York je také největší branou Spojených Států; neboť z veškerého přistěhovalectva přibude jedině tímto přístavem plných sedm osmin. Proto je jízda do Spojených Států nejlépe k odporučení přes Nový York.

## Zápisky.

Dobře udělá Čech, do Ameriky se ubírající, koupí-li si na cestu olůvko a malou zápisní knížečku, do níž vše si poznamená, kudy jel, s kým mezi jízdou jednal, kde co se mu přihodilo, co kde platil, atd. atd. Kdyby měl pak příčinu k nějaké reklamaci ohledně ošizení, ohledně ztráty zavazadel atd., jest hlavní věcí, aby udal den, místo, jména osob atd., což k zakročení bývá nevyhnutelnou věcí. Na tak daleké cestě stane se člověku ledacos.

## Přípravy.

Evropan přijda do Spojených Států s obmezenými názory o svobodě občanské zírá zprvu ve volnosti nekázanost, v umělé i vzorné pracovitosti dření,

— 6 —

v neúnavné snaze přílišnou ziskuchtivost, v bezohledné přímosti nezdvořilost a surovosť; shledá cizí zem, cizí lid, cizí řeč, cizí mrav. A v této zemi mezi tímto lidem má rozbíti svůj stan a hledati svou výživu! Zasteskne si. Rád by přemnohý obrátil se zase k drahé otčině, rád by znova započal ten snad trapný, ale přec jen uvyklý milý domácí život, rád by na vždy zřekl se vší naděje, hledati štěstí jinde — ale jest pozdě již; vyčerpané prostředky mu v tom zabraňují. Vryjí se hluboko do srdce slova: „Všude dobře, doma nejlíp“, a zasmušile hledí člověk útrapám vstříc, jež pro něho v lůně kryje doba nejbližší. Neznaje poměrů, ani jazyka, ani amerického výkonu práce, stojí tu vystěhovalec, nevěda kam a k čemu se obrátit, vydán v šanc po delší dobu všem zlobám i nehodám, které člověka v cizině obyčejně potkávají. A tato tuhá zkouška nemine i našich rodáků, nemají-li tam známých a příbuzných. Očekávají je veškeré svízele, jaké s budováním domácího krbu v daleké cizině spojeny bývají. Proto nežli k vystěhování do Ameriky se odhodláš, rozvaž dobře vše, co s tímto krokem spojeno jest. Člověk snadno se stěhuje, ale těžko se vrací. Amerika klade za příčinou svých zcela jinakých a přistěhovalci neobvyklých poměrů mnoho překážek a obtíží v cestu, jichž překonání vyžaduje nemalé ráznosti, síly a vytrvalosti. Přemnoho našich lidí chová o Americe velmi obmezeného ještě ponětí a domnívá se, že, jakmile jen se dostane přes moře, má vyhráno. Amerika jest arciť země požehnaná, ale pečená holoubata do úst tam nelítají. Naopak Amerika vyznamenává se hlavně prací a zrovna práce, všestranná, rozumná a neúnavná práce utvořila ten blahobyt, který dnes tam snad panuje.

Kdo tedy zamýšlí do Ameriky se odebrati, ať se napřed zkoumá, zdaliž pracovat umí, zdaliž pracovat může a zdaliž pracovat chce. Bez práce v Americe uživit se nelze. A víš co je americká práce? V knize „České osady v Americe“ dočteš se na stránce 139 toto: „U Amerikána jest práce pravou vášní a rostoucí z toho rozčilení se mu stalo životní potřebou. Amerikán nepracuje, jak my v Čechách práci rozumíme, nýbrž on se dře v pravém slova

— 7 —

smyslu i tehdy, kdy již jmění byl nabyl. Napřed se namahá, aby k něčemu přišel, a když k něčemu přišel, namahá se dále, poněvadž tomu uvyknul. Amerikán proto málo zažije v životě svém rozkoší a radovánek, ale velikým jest mu uspokojením, ba blažeností, vědomí, že pilností a vytrvalostí postavil se na vlastní nohy a vlastní silou že vykonal činy, na které smí býti hrdým. Celá severní Amerika zdá se býti kolbištěm práce, na němž každý počíná si tak, jako by šlo o závod, o přítrž. Úspěch působí radost i čest. Čehové ve své rodné vlasti rádi pořádají slavnosti a tance, v Americe však to brzo přestává. Nováčkové, mladíci, obdrževše sobotní výplatu, rádi si ještě zahýří v nastražených „šindykách“, ale jakmile jednou zakusili, co to jest býti v Americe bez peněz, když ku př. není právě práce, vystřizliví záhy. V Americe nic není, když kapsa jest prázdna, a každý, seznav jedenkráte tuto pravdu, snaží se, aby v kapse vždy něco bylo. A tak, jako po celém světě, točí se v Americe všechna činnost okolo dollaru, jenom že v Americe tato činnost je pružnější, poněvadž se tam více vydělá. Vyšší vzdělanost pak v Americe rovněž neplatí, není-li zároveň s dryáčnickou reklamu stále do světa bubnována. Proto Amerika není žádným eldoradem ani pro lenochy, ani pro evropské vzdělance. Jak mnohý americký senátor a vynikající politik započal svou činnost jako cídič bot neb roznášeč novin. Motyka neb lopata, ku které sáhneš, v Americe tě nezneuctí nikdy. Jenom žebrota působí nečest. Jsi-li v nouzi Amerikán ti dá práci, abys sobě chleba vydělal, ale s nemalým opovržením by tě odkopnul, kdybys opovážil se co žebrák o sousto chleba ho požádat.“

Každý Čech bez rozdílu povolání a vzdělání, vyjma snad lékaře, kněze, redaktory a hudebníky, již jedou už na jisto, musí v Americe, když se tam octne k vůli usazení neb k vůli výdělku, sáhnouti k ruční práci, chce-li se uživit. Toto pravidlo platí tak dlouho, dokud člověk neseznal poměry a dokud nepřiučil se angličině. Ale nemysliž nikdo, že snadno snad nováčkovi práci dostat. Dělnických sil, a právě těch nejhrubších, hrne se za ocean nesmírný počet. V soutěžení má pak vždy něco napřed dělník

— 8 —

ten, jenž dovede se smluvit a dřívějším čtením o Americe do poměrů vpravit. Nejtěžším jestiť počátek. Zaměstnavatel, jemuž za svrchované platí heslo „Time is money“ (tajm iz mony), čas jsou peníze, nerad přibírá „zeleného“ dělníka, aneb jak tam v Novém světě se říká „green-horna“, poněvadž nerozumě jeho řeči nemůže mu dáti žádných rozkazův a vysvětlení, i zároveň se mu zprotivuje, postaviti k práci dělníka neohrabaného, jenž jako kus polena zbytečně marní čas, čas v Americe všeobecně příliš drahocenný. Proto je a zůstane hlavní podmínkou brzkého uchycení se v Americe, znalost jazyka anglického, jakož i znalost zemských poměrů a já důrazně poukazuji našince na odborné knihy mým nákladem vyšlé, o nichž na jiném místě se zmiňuji. Až dosud spočívaly přípravy českého lidu, do Ameriky se ubírajícího, v shánění peněz na cestu a v zajišťování sobě místa na lodi. Když nadešla lhůta, vydali se na pouť, asi tři týdny trvající, ovšem bez přemrštěných nadějí, leč, co horší, s naprostou resignací, s jakousi otupělou oddaností osudu, za mořem na ně čekajícímu. Venkovští jednatelé pro paroplavbu, nemajíce Ameriky a plníce obchodní svou povinnost, namlouvají vystěhovalcům, že jen pro ně spása, když vyvolí si k dopravě loď, jimi zastupovanou. I sebe menší prozíravost vystěhovalcův, co se paroplavby a Ameriky týče, jest jim nemilá, z obavy, aby jim zákazník snad jinam neodskočil. Slepota jako by se ještě naschvál udržovala! A přec nynější doby, když už paroplavby tak pokročily v zdokonalení všeobecném, že rozdíl činí se jen v rychlosti, že ale s rychlostí stoupá cena i nesnesitelný na palubě průvan, jest jízda přes moře věcí zcela vedlejší. Jestli se našinec do Ameriky dostane o den neb o dva dni dříve či později, přes ten či onen přístav, po lodích té či oné společnosti, je zajisté lhostejno, co mu ale lhostejným není a nesmí býti, je otázka, jak se mu tam povede. Nahoře již podotknuto, že tato otázka souvisí s připraveností, s jakou člověk do Ameriky přijde. Znát poměry, aby se vědělo, jak třeba se pohybovat, znát zemskou řeč, v které možno s každým se smluvit, znát české osady po celé Unii, kam všude se možno obrátit,

— 9 —

chce-li kdo mezi krajany, a brát v poradách útočiště k českému jednateli v přístavním městě, jenž Ameriku ze zkušenosti zná a svým rodákům ochotně a věrně slouží, to jsou ty přípravy, na nichž záleží všechno. Zanedbají-li se doma a po cestě, je začátek vystěhovalcův tím krušnější a vyhlídky na zdárné pokračování jsou odložený na tak dlouho, až nováček trpkými zkušenostmi konečně vynahradí, co doma a po cestě byl opominul. Mnohdy jedinká rada, jedinký pokyn zkušeného člověka a odborného znalce má dalekosáhlých účinkův a proto se ho drž. Chceš-li ještě doma studovati moje knihy, které svým cestujíČímdávám na památku zdarma, pošli záhy závdavek a připoj po 30 kr. na frankaturu a rekomandování, a já ti je obratem pošty odešlu. Jinak je dostaneš až zde, pojedeš-li mým prostřednictvím.

V novější době nabízejí se jednatelové ve větších městech, od moře daleko vzdálených, různé národnosti i různého vyznání, ku prodeji přeplavních lístků. Počínají si s nevídanou narafičeností: jako by český cestující jenom u nich mohl a směl složiti závdavek, aby se dostal do rukou českého jednatele v přístavě, aneb jako by výhradně jen u nich český cestující našel toho pravého obsloužení. Abych věc uvedl na náležitou míru, podotýkám, že, kdo si k těmto jednatelům zadá, se mnou více do styku nepřijde, jelikožť s nimi nestojím v žádném spojení. I zdravý rozum sám povídá, že především v přístavě, na rozhraní pevné země a moře, kde Vaše první delší zastávka, potřebí Vám upřímných služeb a znaleckých rad. Co by Vám prospělo, kdybych ku př. seděl v Praze či v Lipsku? Dopisy na mne stály by tutéž práci a totéž poštovné, jako sem do Hamburku. Vydav se konečně na cestu, musil byste se u mne zastavit, snad si ještě notně zajeti, přenocovati, utráceti a odebrav ode mne lístek jeti dále. Zastávka, ostatně zcela zbytečná, stála by čas i peníze. Přibyv do přístavu, byl byste poukázán opět na sebe sám a nemoha snad se s nikým smluviti, uvedl byste se možná do rozpaků hroznějších, nežli člověk, světa málo sběhlý, předvídati dovede. Proto jednatel v přístavě a hlavně jednatel český zasluhuje přednost. Zvol si toho či onoho, jen když požívá chvalnou

— 10 —

pověst a tvé důvěry hoden jest, a nedej se pak od žádného místního jednatele na jinou firmu odvrátit, kterou snad ani neznáš.

Před odjezdem urovnej dobře všechny své rodinné aneb obchodní záležitosti, nebo přes ocean bývá urovnání nad míru obtížné a se značnými útratami spojené. Máš-li něco k pohledávání, co se teprv později dá vyřídit a co potřebuje tvého podpisu, dojdi k notáři a dej osobě, k níž máš důvěru, plnomocenství, aby za tebe jednala v tvé nepřítomnosti. Kdybys posílal plnomocenství z Ameriky, jež musí být od notáře sepsané a rakouským konsulem stvrzené, uviděl bys, že to ani tak snadno nejde a že to stojí mnoho peněz.

Dej si vystavit doma křestní list. V Rakousku, kde se slaví jmeniny a ne narozeniny, neví mnohý ani, kterého dne se narodil aneb kterého dne se narodily jeho dítky. A přec se často v životě stane, že den narození musí se udat, a pak to tvoří rozpaky i hanbu, když člověk ani neví, kdy se narodil. Ohledně pasu viz článek „Legitimace.“

Na cestu do Ameriky ber zavazadel jenom tolik, mnoho-li máš svobodno s sebou vézti. Amerikán má rád i u dělníka slušný oblek a čisté prádlo. Na to dbej. V Americe přichází změna povětří z tepla do studena často a rychle a nosí se tam nazvíce vlněné spodní košile, poněvadž vlna, třeba zapocená, tak nechladí na těle, jako plátno.

Louče se s domovem, nelouči se s ním navždy. Předevezmi sobě opět se vrátit, jakmile poměry tomu dovolí. Láska k rodišti a věrnost k svému národu nehyne nikdy. Navrácení se do otčiny je v daleké cizině utěšeným cílem pro horlivější a úspěšnější činnost. Vlast nosí se v srdci jenom jedna. K ní spěje každý: bez ní žádná blaženost. To stále v mysli měj a na vlast pamatuj.

## Zlaté knihy na cestu do Ameriky.

Krušné počátky v Americe dají se zmírnit a zkrátit, když člověk už napřed obeznámí se se zemí, do které přesídliti se hodlá, a když přiučí se poněkud anglickému jazyku, který je tam panujícím. Abys

— 11 —

o této věci mohl sobě učiniti jasnější pojem, představ si na př. Angličana, jenž by přišel do Čech, aby se tam usadil, neznaje ani českou zem, ani české zvyky, ani českou řeč. Bez peněz by byl, jak říkáme, ztraceným. V Americe je to už ovšem poněkud snažším, jelikož jsou tam české osady, kde se Čech s Čechem dorozumí. Nesmírnou proto výhodou pro českého vystěhovalce je, když již napřed ví, kde všude za mořem našincové usazeni jsou, a když vyzná se částečně v angličině i v amerických poměrech. Kniha „Ceské osady v Americe“, vyšla mým nakladem, vyhovuje nadřečeným třem účelům: přináší seznamy českých osad zaoceanských s četnými zprávami z nich, popisuje americké poměry a podává anglická cvičení pro samouky; je zlatým pokladem pro každého, kdo zamýšlí vydati se na pout vystěhovaleckou, je nejlepším učitelem, rádcem a průvodcem. Kdo ji pozorně přečte, sezná Ameriku, jakoby tam byl už alespoň 10 roků a tato znalost má pro další pokroky velikou důležitost. Sám vládní list
„Pražský Denník“ ze dne 26. května 1889 praví, že „tomu, kdo se už jednou hodlá z Čech vystěhovati, poslouží tato kniha lépe, než všichni hubařští pozývatelé loďařští, poněvadž pravdu píše o poměrech v Americe a žádných jablek těšínských nemaluje“. Ovšem třída lidu, z které české vystěhovalectvo nejvíce vychází, nedovedla ještě s důstatek poznati užitečnost četby a nedbá proto jaksi o čtení. Leč to si pamatuj každý, že v Americe bez čtení se neobejdeš a že, chceš-li tam prospívat, musíš míti všechny smysle otevřeny. Amerika tě ku čtení přinutí. Marné bylo by vzpírání se. Buď zvítězit neb podlehnout. Jenom nedbalec podléhá a nese proto sám vinu svého nezdaru. Rečenou knihu ve skvostné vazbě dávám svým cestujíČímna památku zdarma, aby měli při jízdě přes moře, která hezky dlouho trvá, co číst, aby tento čas využitkovali cvičením se v angličině a obeznamováním-se s americkými poměry. Zároveň vydal jsem „Kapesní slovník česko-anglický“, jenž čítá asi 8000 slov z obecné mluvy. Ku každému slovu připojena jest úplná výřečnost. Angličina se jinak píše a jinak vyslovuje. Proto jest udání výřečností pro samouka nezbytnou věcí. Řečený slovník

— 12 —

koná znamenitých služeb a uděluje se všem cestujícím, kteří jedou prostřednictvím buď mé hamburské neb novoyorské firmy rovněž úplně zdarma. 

## Poptávky.

Na poptávky, jak je v Americe aneb má-li se někdo do Ameriky odebrati, nemohu podati žádnou zprávu. Vypisovat americké poměry — k tomu stačí sotva celá kniha, a radit někomu, má-li se vystěhovat čili nic, není rovněž možná člověku, jenž si cizí osud na svědomí bráti nechce. Já proto dávám odpověď na poptávky čistě obchodní. Občan, činící za příčinou obmýšlené jízdy do Ameriky u mne poptávku, učiní nejlépe, dá-li si zprvu vypsati rozpočet celé cesty; při tom nechť vyjmenuje všechny osoby, tedy i děti, od nejstaršího do nejmladšího, s připojením stáří (dle roků) každé jednotlivé osoby, nechť zároveň udá místo v Americe, kde se usaditi chce, a nechť také ihned poví, na jakou paroloď, zda-li na expressní, čili na poštovní, čili jen na obyčejnou sedati si přeje. Rozpočet se mu odešle se všemi zprávami. Komu by po bedlivém přečtení a prozkoumání bylo pak ještě něco nejasným, nechť znovu píše a vysvětlení se mu podá; Každá poptávka zodpoví se ochotně a zdarma. Ale nebudiž věc odkládána až na poslední okamžik, kde pro nával práce nelze vše vyřizovati obratem pošty. Kdo si přeje míti odpověď rekomandovanou, nechť přiloží 10 krejcarů v poštovních známkách. Já své dopisy všechny frankuji a přijímám dopisy také jenom frankované.

## Poštovní zásylky z Rakouska do Hamburku.

Za jednoduché psaní do 15 grammů vahy platí se 5 kr. a za psaní, těžší než 15 a lehčí než 250 grammů, platí se 10 krejcarů. Za *korrespondenční lístek* platí se 2 kr. Korrespondenční lístek s vyplacenou odpovědí stojí 4 kr. Psaní *zálepka* stojí 5 kr. *Tiskopisy* do 50 gr. po 2 kr., do 250 gr. po 5 kr., do 500 gr. po 10 kr. a do 1 kila po 15 kr. *Zápisní* poplatek (rekomandovaní) obnáší všude 10 kr. Jak vidno, není dopisování s Hamburkem dražší, nežli

— 13 —

doma od jedné pošty ke druhé. Psaní z Čech a z Moravy dojde obyčejně do Hamburku už druhý den, a třetí neb čtvrtý den může zasýlatel očekávati odpověď. Peníze do Hamburku zaslat poštovní poukázkou lze až do obnosu 200 zl. (je-li obnos větší, upotřebí se poukázky dvě); poplatek je následující: do 40 zl. po 20 kr. a za každých 10 zl. o 5 kr. více. Peněžitá psaní, to jest psaní s udáním ceny, stojí až do 150 zl. po 30 kr., do 300 zl. po 40 kr., do 500 zl. po 60 kr. Jelikož se list s penězi musí dobře pečetit, je poukázka s menší prací spojena, a tudíž i příhodnější. Poštovní balík do 5 kilo bez udání ceny stojí z Rakouska do Hamburku 30 kr. a s udáním ceny 35 kr. Nákladní zásylky do Hamburku přijímají rakouské pošty až do 50 kilo; sazba jest tato: do 5 kilo 30 kr. a pak za každé další kilo do
vzdálenosti 100 mil (100 mil je z Prahy do Hamburku) 18 kr., do vzdálenosti 150 mil 24 kr. Při zasýlaní balíků a nákladu poštou musí se vyhotovit dvojí celní deklarace. Za telegrommy do Hamburku se platí předně stálá taxa 30 krejcarů a za druhé za každé slovo 4 kr. Telegrafické poukázky jsou rovněž možné; poplatek obnáší tolik, co stojí telegramm a obyčejná poštovní poukázka dohromady. Poštovní cenník Spojených Států.

Pošta čili poštovní úřad jmenuje se ve Spojených Státech po anglicku Post Office (Zkráceně P.0., jako v češtině p.). Jednoduché psaní až do půl unce (15 grammů) stojí po celé Unii 2 centy a do Německa a Rakouska 5 centů. Korrespondeční lístek (Postal Card) stojí 1 cent a do Německa a Rakouska 2 centy. Za tiskopisy, jež se přijímají až do 4 liber, platí se po 1 centu za každé 2 unce. Za tiskopisy do Německa a Rakouska platí se totéž. Za noviny platí vydavatelstvo po 2 centech za 1 libru.
Rozesýlá-li ale někdo jiný noviny, musí platit poštovného po 1 centu za každé 4 unce. Rekomanduje-li (aneb, jak v Americe říkají, registruje-li) se psaní, platí se po 10 centech zvlášt. Za poštovní poukázku do doll. 10 platí se 8 centů, do doll. 15
platí se 10 c., do doll. 30 platí se 15 c., do doll. 4O

platí se 20 c., do doll. 50 platí se 25 c., do doll. 60
platí se 30 c., do doll. 70 platí se 35 c., do doll. 80
platí se 40 c., do doll. 100 platí se 45 c. Co jest přes doll. 100, na to musí se vzíti poukázka nová. Poštovní úřad, u něhož se peníze skládají, vystaví stvrzenku, kterou zasýlatel musí ve psaní odeslati na toho, kdo peníze má přijmout. Adresát s touto stvrzenkou dojde pak na svou poštu a dá si peníze vyplatit. Kdo není na poště známým, musí někoho s sebou přivésti, koho na poště znají, aby dosvědčil, že to tatáž osoba, na kterou poukázka vystavěna. Kdo v Americe posýlá někomu poštou méně než
5 dollarů, může vzíti poštovní notu (Postal Note), za kterou obnáší poplatek 3 centy. Poštovní nota vyplatí se na poště každému, kdo ji předloží, beze vsech zkoušek. V Americe se mohou peníze na poštovní poukázku také posýlat do Německa přímo a do Rakouska a Uherska nepřímo přes Švýcarsko. Za 1 německou marku počítá se 24 1/4 centu zlata a za 1 švýcarský frank 19 3/4 centu zlata. Při zásylakách z Ameriky do Rakouska strhne si švýcarská pošta za svou práci ještě po 5 centimech z každého
franku. Poplatek za poštovní poukázky do Německa a do Švýcarska (čili do Rakouska a Uherska) obnáší: 15 centů za obnos až do 10 doll.; 30 c. za obnos od 10.01 až do 20 doll., 45 c. za obnos od 20.01 až do 30 doll.; 60 c. za obnos od 30.01 až do 40 doll.; 75 c. za obnos od 40.01 až do 50 dollarů. Přesahuje-li obnos 50 doll., musí se vzít poukázka nová, jelikož se na jednu poukázku nesmí poslat více než 50 doll. Náklad a peníze v obálkách americka pošta nepřijímá. Pro tento druh dopravy jsou tak zvané expressní společnosti, z nichž největší a nejznámější jsou: Adams Express Company, American Exp. Co., Pacific Exp. Co., Southern Exp. Co., United States Exp. Co., Wells, Fargo 86 Co. 

## Poštovní cenník z Rakouska do Ameriky.

Psaní za každých 15 grammů 10 krejcarů; korrespodenční lístek 5 krejcarů. Tiskopisy pod křížovou obálkou každých 50 grammů 3 krejcary. Rekomandovaní 10 kr.

## Frankovám dopisů.

Poštovné je nyní už po delší čas tak laciné, že každý, kdo list píše, jej také frankuje, a obecenstvo uvyklo dostávati a přijímati všechnu korrespondenci na poště zcela vyplacenou. Když pošle se list nevyplacený, musí adresát platiti pokutu dvojnásobného porta, což spůsobuje nemilý dojem, který opravdový dopisovatel přece vzbuditi nechce. Nefrankované korrespondence posýlají nyní už jenom anonymové, již, aniž by udali svoje jmeno, mají v úmyslu někoho pozlobit. Proto se dnes nefrankované dopisy nerady přijímají.

## Do Ameriky se přepracovat

nejde. Mnoho listů dochází, v nichž pisatel žádá, aby se vykonávaje práci na lodi buď zdarma neb velmi lacino dostal přes moře. To je úplně nemožné. Ku každé práci na lodi musí se dle zákona najímati mužstvo schopné a otužilé, jež je pod přísahou, že loď neopustí a že se s lodí opět vratí. Někdy se ovšem stane, že za vodou některý topič neb sklepník uprchne a že lodi pro zpáteční cestu schází lid, jenž je v Americe těžko k dostání. V tomto případě přijímá se cestující zdarma, když se uvolí vykonavat sebe těžší práci. Leč to platí jen z Ameriky do Evropy. V Evropě to býti nemůže. I vystěhovalecký zákon patrně zapovídá, dopraviti někoho na splátky. Kdo tedy nemá dostatečných prostředků, ať si nedělá žadnou naději, že by se přes moře dostal do severní Ameriky.

## Moře nezamrzne.

Venkovský lid se domnívá, že, když zamrzne rybník, také moře zamrzne a že se pak v zimě nemůže jezdit. Abych na četné dotazy v tomto smyslu nepotřeboval věčně stejnou dávati odpověď, podotýkám, že moře mezi Evropou a Amerikou, jež slove Atlantický ocean, nezamrzne nikdy. Výpravy parolodí konají se pravidelně v létě i v zimě. Tak vyjíždí jak po celé léto tak po celou zimu poštovní parolodě z Hamburku do Nového Yorku každou neděli a obyčejné parolodě každou středu. Jenom expressní parolodě, které pracují s příliš drahým aparátem, leží přes zimu ladem, poněvadž v zimě méně lidí cestuje, než v letě. Pro tutéž příčinu, nedostatek cestujících, odpadávají u železnic rychlíky zimního času.

## Vystěhovalecký agent.

Abych hned z prvu předešel mylným náhledům a lecjakému podezřívání, podotýkám se vší rozhodností, že lákáním k vystěhování nijak se nezabývám, což s důstatek dosvěděují knihy a spisy, mnou o Americe až dosud vydané. Naopak já veškerá lákadla zatracuji, jak se na upřímného Čecha sluší a patří. Kdyby tomu tak nebylo, musily by za ten dlouhý čas mého působení, jež nyní již dvacet trvá roků, objeviti se přec nějaké důkazy proti mému tvrzení. Leč po celou tu dobu důkaz ani jediný. U mne proto není nikdo vydán v nebezpečí falešným předstíráním vylákánu býti k opuštění svého domova. Já nejsem žádným vystěhovaleckým agentem. Jsem pro jednu německou paroplavbu výhradně jen českým jednatelem, na něhož Čech, zamýšlející cestovati do Spojených Států severoamerických, může ve svém mateřském jazyku obrátiti se dříve, nežli na cestu se vydá, za příčinou pojištění na lodi místa, aby v přístavě neuváznul a čas i peníze zbytečně neutrácel, kdyby snad parník už obsazen byl. Mám tedy co dělati pouze s cestujícími bez rozdílu, za jakým účelem přes oceán se ubírají, a s vystěhovalcem mám co dělat jen potud, pokud on je také cestujícím. Paroplavba má ovšem z dopravy cestujících užitek a poskytuje svým jednatelům procenta z odvedených peněz, ale obviňovat snad paroplavbu a její jednatele, že k vůli tomuto užitku podněcují lid k vystěhování, je čirý nesmysl. Vybízení k vystěhování podléhá v každé zemi přísnému trestu, jemuž veřejný ústav paroplavební a veřejná osoba jednatelova zajisté by neušly. Dráhy jak evropské tak americké mají z dopravy vystěhovalců rovněž užitek a dávají z odporušených osob „refakce“ čili odměnu. A proč se náhrady dávají? Zajisté ne, aby lid přemlouván byl k vůli užitku té neb oné paroplavby, aneb k vůli užitku té neb oné dráhy pustiti se do světa, nýbrž aby, kdo už jednou někam na cestu se vydá, volil k dosažení svého cíle z vícera konkurrujících doprav dopravu právě tu neb onu. Je to v našem případě soutěž o cestující, která všude se objevuje od drožkáře vzhůru až ku nejčelnějším dopravním ústavům, od obyčejné noclehárny vzhůru k nejvelkolepějším hótelům.

Trefný obrázek tohoto soutěžení podávají nádraží v rozličných krajích a zemích. V Polsku a Rusku na příklad perou se vozkové, perou se hostinští o přibylé vlakem pasažéry způsobem tak dotíravým, že člověk děkuje pánu bohu, když se z ohlušující vřavy dostane šťastně ven. Podobně tuhé soutěžení jest u transatlantických paroplaveb o pasažéry a to nejen z Evropy do Ameriky, nýbrž i z Ameriky do Evropy. Která společnost by se v řečené soutěži nezúčastnila, nic by pro ni nezbylo. Do paroplavebního obchodu zasahují ostatně ještě činitelové jiní. Jízda přes moře tisíc mil široké musí býti připravena. Cestující chce napřed vědět, jak a kdy loď jede, jaké jsou ceny atd.; on musí místo na lodi si zabezpečit. Nezbývá mu než vyjednávat. S kým? Jak? V kterém jazyku? Velké ústavy, jakými akciové paroplavební Společnosti jsou, nemohou každému cestujícímu v každém jazyku každé vysvětlení podati; jsou nuceny práci děliti buď dle zemí neb dle národností. Ustanovují jednatele. Takovým jednatelem jsem i já. S vystěhovaleckou otázkou nestojím v žádném činném spojení. Nikoho nevybízím k opuštění jeho domova, nikomu neradím, ve které zemi by se usaditi měl. V Americe jsou země, jako Kanada, Brasilie a jiné, které poskytují značné prémie za dodání přistěhovalců, já ale do takových krajin ještě nikoho nedopravil a obrátí-li se někdo na mne v této příčině,
odepřu mu svého prostřednictví, nechtě bráti na sebe žádnou zodpovědnost za dopravu do zemí, českému lidu podnebím nepříznivých. Ma doprava obmezuje se pouze na jednu zem, a to jsou Spojené Státy severoamerické (United States of America) s dvěma přístavy, Novým Yorkem a Baltimorem, kam Čechové a Moravané nejvíce se ubírají. Tato země až dosud ani krejcaru nevydala na přilákání vystěhovalců a nevydržuje žádných agentů. Vzdor tomu se tam vystěhovalcctvo ze všech stran hrne. Proč nechci rozbírat; soudný člověk ale ví, že příčiny tohoto stěhování jiné nejsou, nežli jen národohospodařské. Proto,
co se Spojených Států týče, nemůže být u rozumných lidí ani řeči o vystěhovaleckých agentech ve smyslu, jako by tito měli vlivu na původ a směr stěhování. Slyšíme-li někdy, že ten který občan do Spojených Států vylákán byl, vztahuje se to, pokud má dlouholetá zkušenost učí, vždy jen na zprávy dřívějších vystěhovalců, kteří, daří-li se jim dobře, píší svým známým a příbuzným do rodné vlasti, aby za nimi přijeli. Leč takové vybídnutí děje se obyčejně ve dobrém úmyslu a ten, kdo na vylákání trpce si stěžoval, láká snad po roce, překonav krušné počátky, tím horlivěji opět jiných. Je to agenda přirozená, proti níž se zakročovat nedá, a která zároveň objasňuje zřejmě, jak proud vystěhovalecký se udržuje a řídí. Zádný agent ho uměle nevyvolává. Je, a proto že je, musí být také paroplavební jednatel, na něhož by se vystěhovalec co cestující obraceti mohl, aby mu urovnal cestu. Zhusta se ale vystěhovalec neobracuje na jednatele jednoho, nýbrž on běhá a píše na všechny
strany, hledaje, kde by byl převoz nejlacinější. Konkurrující jednatelé snaží se přílišným namlouváním, přemlouváním, pomlouváním a vymlouváním vystěhovalce co cestujícího získat, kteréžto počínání svou přemrštěností vystupuje rádo z mezí slušnosti. Pozorovatel tvoří si pak svůj úsudek, toť se rozumí nepříznivý. Stěhování z vlasti je samo sebou jak politickým úřadům, tak všemu ostatnímu občanstvu nemilé, ba protivné, a osoby, jako dopravní jednatelé přicházející s vystěhovalci do jakéhosi styku, nemohou se už proto těšiti žádné lásce. Budiž. Cesty národní práce mohou býti rozličné.

## Volba přístavu.

Jízda po železnici v Rakousku a. Německu a vůbec po celé Evropě je dosti obtížnou, hlavně trvá-li hodně dlouho. Kdo jede dvacet čtyry hodiny po dráze, cítí se konečně jako roztlučeným. Ovšem
jsou také zastávky, kde si člověk odpočine. Největší starosti působí přesedaní a celní prohlížení zavazadel na hranicích. Nežli proto sobě přístav zvolíš, ptej se napřed, jak daleko je do přístavu a kolikráte se musí jeti přes hranice. Čím více přístav od tvého rodiště vzdálen, tím více zkusíš, tím déle budeš musit tlouci se po cestě a tím více, jak samo se rozumí, za železniční lístky zaplatíš. Některé paroplavby snižují pro země, příliš od jich přístavu vzdalené, maličkost na cenách přes vodu, poněvadž by jim tam pro drahotu železničních lístků nikdo nejel. Človek uspoří snad na lodním lístku 3 až 6 zlatých, vydá však víc než dvakrát tolik za lístky po dráze a nádavkem k této peněžní ztrátě má jenom delší trmácení cestou. Do odlehlých přístavů cestuje Čechů a Moravanů pramalá hrstka, která neznajíc poměrů dala se snadno přemluvit. A hrozno jest Čechu, mluvícímu toliko svou mateřštinou, cestovati osamoceně krajinami a po lodích, kde není krajanů, s nimiž by se dohodnul, kde není jednatelů, u nichž by ve svých nesnázích našel rady i pomoci. Podobně má se věc s celními prohlídkami na hranicích. Jsou přístavy, do nichž se jede z Rakouska jenom přes jedny hranice, jsou ale také přístavy, do nichž se musí jeti přes dvoje i troje hranice. I to ber v přísnou úvahu. Clověku, nevyznajícímu se v cestovaní ani v cizích jazycích, jsou naznačené obtíže tím obtížnějšími. Pamatuj, že ku př. z Prahy čítá se do Hamburku 680 kilometrů, do Brem 720, do Rotterdamu 1078, do Antwerp 1228 a do anglických přístavů přes 3000. Hamburk je tudíž přístavem pro Čechy a Moravany nejbližším, nejlacinějším a nejpříhodnějším. Hamburk jest zaroveň největším obchodním městem na evropské pevnině a má mnoho spojení na všechny strany. Každý vystěhovalec přibyv do Hamburku obdrží, nežli z nádraží vyjde, od strážníka adresu zdejšího vystěhovaleckého úřadu „Auswanderer—Behörde“, Passagierhalle am Grasbrook, který ochotně a zdarma poskytuje okamžité a rázné ochrany každému vystěhovalci, jenž tam osobně ohlásí, že se mu nějaké příkoří stalo. Hamburk má totiž ze všech přístavů nejpřísnější zákon na ochranu vystěhovalectva. Hamburk je také starobylé světové město a stojí našinci, jenž by cestou rád něco poznal, za podívanou. Proto tento přístav vším právem odporučiti dlužno.

Jednatelská firma česká

## Jos. Pastor v Hamburce.

1. Můj závod trvá už na dvacátý rok. Tato doba dosvědčuje, že obchodní vedení musí býti dokonalé; jinak bych se nebyl udržel.
2. Jsem stále na témže místě a stále činným pro tutéž paroplavbu.: I tato okolnost podává svědectví, že Hanburk je pohodlným přístavem, a že paroplavba mnou zastupovaná patří k prvním. Kdyby tomu tak nebylo, byl bych mezi tak dlouhým časem
zajisté usadil se v jiném přístavě, abych požadavkům svých rodáků vyhovil a pozadu nezůstal.
3. Dvacetiletá obchodní zkušenost stojí mi po boku a vím proto, jak s cestujícími zacházeti třeba, abychje ve všem uspokojil.
4. Po celou tu dobu nevyskytla se ještě ani jedinká stížnost proti mně, což nade vše patrným je důkazem, zdaliž zasluhuji důvěry čili nic.
5. Až dosud jsem já jediným jednatelem českým, jenž sám druhdy js vystěhovalcem po delší dobu v Americe žil a poměry tamější z vlastní zkušenosti poznal. Třikráte jsem již konal cestu přes moře sem i tam a dovedu proto každému poradit jakožto znalec. A dobrá rada lepší zlata.
6. Já sebral přebohatý materiál o Americe pro české vystěhovalce, a může si u mne jeden každý vypsati adresy z kterékoli české osady, jakož i adres továren a všech průmyslových závodů ve Spojených Státech.
7. Adresy, které mnozí vystěhovalci u sebe pro Ameriku mají, bývají chybny a nedostatečny. Já každému tyto adresy opravím a polohu z podrobných velkých atlasů vysvětlím.
8. Angličiny jsem mocen v mluvě i písmě. Přemnohý Čech a Češka cestuje do Ameriky ku svým přátelům na příliš vzdálenou a poněkud zamotanou adresu někde na venkově. Takovým cestujíČímdávám otevřené psaní anglické, v němž se udává cíl cesty, a v němž se každý lidumil prosí, řečené osobě k dosažení cíle býti laskavě nápomocným.
9. Abych našemu vystěhovalectvu na obou stranách moře jak radou tak skutkem přispívati a českoamerickému obecenstvu obchodní styky se mnou zároveň usnadniti mohl, otevřel jsem filialní závod v Novém Yorku pod firmou Jos. Pastor, 1432, I. Avenue, New York. Je to za hranicemi první česká firma, založená ve dvou světa dílech v zájmu české národnosti, je to nový důkaz o neúnavné a obětavé činnosti ve službě cestujícího přes moře
obecenstva českého.
10. Aby Čech, jenž do Ameriky se ubírá, poznal už napřed poměry Nového Světa, aby už napřed přiučil se poněkud angličine, která je tam řečí panující, a aby konečně věděl, kde všude za mořem krajané usazeni jsou, vydal jsem spis pod názvem České osady v Americe. Je to objemná kniha, vkusně vázaná. Zároveň jsem vydal „Kapesní slovník česko anglický“ s úplnou výřečností pro samouky, 8000 slov čítající. Obě knihy, které stály práci peníze, dávám svým cestujíČímna památku zdarma. Jsou to knihy pro vystěhovalce drahocenné, neb přispívají víc, než cokoli jiného k tomu, aby našinec octnuv se v Americe záhy vpravil se do poměrů a brzo se tam uchytil.
11. Veškeré služby poskytuji svým cestujíČímúplně zdarma. Sám už hamburský zákon zakazuje jednatelům bráti za služby vystěhovalcům prokázané jakousi odměnu a ukládá lodním majetníkům, aby své jednatele odškodňovali sami, což se děje povolením percentní náhrady z odvedených peněz. Tato náhrada je můj jediný výdělek, který při dopravě cestujících mám.
12. Mým prostřednictvím cestuje skoro na každé lodi četnější česká společnost. Tím najde u mne Čech české soudruhy a cestou se pak nestýská. Čím více krajanů jede pohromadě, tím je jim veseleji. Mezi větším počtem českých cestujících nalézá se vždy někdo, jenž německého jazyka mocen jest a jenž pak, je-li toho potřebí, dělá ostatním tlumočníka. Výhoda spočívá v tom, že se každý Čech na lodi smluví, což tím větší váhy nabývá, kdyby někdo roznemoha se na lodi potřeboval lékařské pomoci.

## Má adresa.

V Hamburku jsem nyní již 20 roků. Za tuto dobu měnil jsem místnost čtyřikrát. Nyní nalezá se má kancelář v čísle 23. Kleine Reichenstrasse. Vzdálenost od nádraží jenom asi 5—10 a od přístavu asi 15 min. cesty. Má protokolovaná firma zní jednoduše: Jos. Pastor, Hamburg. Veškeré dopisy s touto adresou správně dojdou. Adresa ulice je vedlejší. Našincové se domnívají, že by list bez udání ulice a čísla v tak velkém městě nemohl býti dodán. V tom jsou však na omylu. Pošta zná adresy protokolovaných firem velmi dobře. Pastorova adresa je tudíž snadno ku pamatování. Pak ale ať se adressa nepíše jinak, než jen Jos. Pastor, Hamburg, beze všech titulů a dalších přívěsků. V Německu poštovní úředník neumí česky a napíše-li někdo na obálku ku příkladu „v Hamburku“ místo Hamburg, už ho to mate. Do Německa nechť se píše adresa německy; z Německa do Čech a na Moravu se může psáti adresa už česky, jen když na konec připojí se slovo „Böhmen“ neb „Mähren“. Německý poštmistr pošle list jednoduše do Čech neb do Moravy, kde pak adrese už rozumí. Toto pokynutí platí hlavně mým cestujícím, kteří odtud rozlučují se ještě písemně s domovem. — Adresa mé filialky v Novém Yorku zní: Jos. Pastor, 1432, I. Avenue, New York. Při této adrese ať se ulice a číslo nevynechává, sic by dopisy nedošly. Je to mezi 74. a 75. ulicí ve středu české čtvrti. Můj zástupce p. Václav
Jarkovský má do státního ústavu přistěhovaleckého volného přístupu a cestující mohou s ním buď tam či v blízkém katolickém ústavu „Leo House“ č. 6. State Str. mluviti, aneb také s mým synem Otakarem, kteří oba dva s ochotou krajanům poslouží. Vím z vlastní zkušenosti, jak člověk je slepým vystoupiv americe na břeh.

## Pastorova pisárna v Hamburku.

Ma pisárna umístněna jest v čísle 23. v ulici Kleine Reichenstrasse v 1. poschodí. Uprostřed průjezdu v pravo vedou schody nahoru. Okna jdou na ulici a v oknech jsou nízké modré záclony, bíle popsané mou firmou. Vchod do pisárny je skleněnýma dveřma; na tmavém skle je průsvitně psaná má firma. Já sám žadný hostinec nevedu; své cestující odporučuji do Oelkersova hostince v téže ulici šikmo naproti v čísle 10. Z mé pisárny je vidět v levo na náměstí zvané „Fischmarkt“, na němž stojí vodotrysk (kašna) s vysokým pomníkem. Já vše toto uvádím, aby cestující, jenž se mnou chce jednat, přesvědčiti se mohl, zdaliž také v mé pisárně jest. Často se stává, že Čech zavlečen do jiného hostince neb do jiné pisárny, kde se buď přímo za Pastora vydávají aneb kde se snaží vzbuditi dojem, jako by u Pastora byl.

## Český paroplavební jednatel.

Z Evropy do Ameriky nelze se jinak dostati, než jen přes moře po lodi. Česko—moravská vlast žádného moře nemá, ani žádných námořních lodí. Našinec do Ameriky cestující je tudíž odkázán na cizozemské přístavy, kde se, jak známo, český nemluví. Úkol českého jednatele je proto zřejmý a svrchovaně oprávněný: má být svým rodákům tlumočníkem, prostředníkem a ochráncem. Kdo má tu úzkost a ten strach, jež náš venkovan pociťuje z neobyčejně daleké cesty přes moře a přes cizé země, kdo zná ty útrapy, jež člověk zakusí, když taková cesta spravedlivě se mu nevysvětlí a neurovná, kdo má ty rejdy, jež dějí se na vystěhovalcích, jelikož snadno pro svou neuvědomělost dají se oblafnout, kdo zná konečně tu smutnou pravdu, že Čech se svou mateřštinou daleko nedojde, a stane-li se mu po cestě nehoda, on svého práva domáhati se nemůže, nejsa mocen některé světové řeči; kde vše to v úvahu vezme, ten také dovede ocenit význam solidního jednatelství českého. Nikdo nevezme si s českými pasažéry tolik práce, nikdo nezná potřeby našeho venkovského lidu pro jízdu tak dalekou lépe, nežli český jednatel, který zároveň, je-li samostatným a požívá-li chvalnou pověst, skýtá svým zákazníkům dostatečných záruk pro správné provedení svěřených mu záležitostí. On musí, aby svým závazkům vůči cestujíČímvždy  vybavil, předně složiti kauci, a nabýti úředního schválení, a za druhé veda obchod pouze český musí přísně dbáti nejen na solidnost ve všech výkonech, nýbrž i na starostlivé obsloužení svých svěřenců, aby pro budoucnost před českou veřejností obstál a v zasloužené přízni pokračoval. Jediný případ nesprávnosti, tiskem ve známost uvedený, může českého jednatele úplně zničit, a v tom právě spočívá ta silná záruka, již samostatný jednatel český Svým rodákům skýtá. Cizorodý jednatel, jenž provozuje pomocí českých písařů-český obchod jen tak co vedlejší, nestojí k Čechu v poměru co rodák k rodáku, nýbrž vždy jen co výdělkář; kdo se naň obrátí, popřeje mu výdělek, ale mnoho služeb a národní upřímnosti od něho očekávat se nesmí; mluvte s ním, když je — neznalým Vašeho jazyka, skládejte důvěru v něj, když on důvěry k Vaší národnosti nemá. „Svůj k svému“. Platnost tohoto přísloví ukazuje se právě v cizině, kde Čech bývá opuštěným, neumí-li jinou řeč, a málo neviděným, dovede-li smluviti se s kýmkoli. Český jednatel v přístavním městě odkázán je výhradně jen na Čechy, nemaje u jiné národnosti pražádné přízně, protože je Čechem, a neměl by tudíž Čech přáti Čechovi, anať sama už národnost jednoho na druhého odkazuje? Ať tudíž nikdo nemyslí, že český jednatel láká k vystěhování do Ameriky, rozešle-li někdy své nabídky; neníť to lákáním, nýbrž pouhým upozorněním těch, jichž úmysl stěhování již upevněn jest, aby použili ku své jízdě prostřednictví bezpečného, výhodného a především vlasteneckého. Jsou to nabídky, jichž ještě nikdo nelitoval, kdo jich přijal aneb kdo jich dále jinému odporučil; neboť čelí v první řadě k ochraně a ku hmotnému i duševnímu prospěchu ubohého lidu našeho, vydaného v šanc všem zlobám a nehodám na jeho smutné pouti vystěhovalecké. I pro dobrou věc stala se bohužel nynější doby jakási agitace nezbytnou, poněvadž cizorodá konkurrence přičiněním četných agentur po česko-moravském venkově napíná všech sil k opanování celého obchodu v našich
vlastech. A může česká veřejnost vůči této tuhé soutěži zazlívati solidním firmám českým že uchá zejí se o přízeň svých rodáků? Těch samostatných firem českých je jen po skrovnu a bylo by věru k politování, kdyby časem český lid českých závodů v přístavech neměl. Komu tudíž tento spis do rukou se dostane, nechť jím nepohodí, nemá-li proň žadné ceny; snad se časem hodí pro někoho jiného, jemuž by se postoupením jeho posloužilo. Odporučováním a podporovaním českých podniků uznaně dobrých a pro rodáky, nemají-li záviseti od cizácké milosti či nemilosti, uznané potřebných, pracuje se pro zájmy české, pro zájmy národní.

## Pojišťování místa na lodi.

Na paroloď vejde se obmezený pouze počet cestujících a jakmile počet tento je doplněn, nesmí pod vysokou pokutou přijmouti se více ani človíček. Policejní předpisy k zamezení nějakého přeplnění jsou velmi přísné. Loď se právě nastavit nedá, jako snad železniční vlak přibráním několika vozů. Lodní správa je povinna úřadům 2 dni napřed oznámiti, pro jaký počet cestujících v mezipalubí hodlá ponechati místa a tento počet je pak rozhodujícím.
Ostatní prostora v mezipalubí použije se pro náklad; neboť místo na lodi jest to jediné, jež se dá využitkovat. Uřadníci jdou na loď a vyměří místnost pro
mezipalubníky ponechanou. Zákon určuje prostranství 2.85 kub. m. pro každou osobu přes 12 let starou a 1.42 1/2 kub. metru pro každé dítě od 1. do 12. roku. Určený počet se překročit nedá. Cestující, který neposlal dříve zavdavek a zůstane pak zpět, běduje a diví se, proč by se nedalo pro jednu neb 2 osoby ještě místo udělat. V posledním okamžiku nedá se více ničím hýbat. Rozdělení je hotovo a zbývající místnost naplněna nákladem, jenž pravě tolik vynese, co cestující sám; neb 1 kub. metr zboží I. třídy stojí z Hamburku do Nového Yorku dol. 7.50, což
dělá 7.50 X 2.85 = dol. 21.35 po 2 zl. 40 kr. = zl.
51.24. Z toho už vysvítá jasně, že se místo nemůže ponechávat do zásoby na nejisto, když se dá využitkovat jiným spůsobem rovněž tak výhodně. Jest tudíž potřebí, aby zasláním závdavku místa záhy se zabezpečila. Při tak daleké jízdě, kterouž cestující do Ameriky před sebou má, není radno, jen tak na zdařbůh pouštěti se do světa. Čím spíše cestu sobě předem urovnal, tím spíše vyhne se nesnázím a tím více uspoří peněz. Závdavek obnáší 10 zl. r. m. za jednu osobu přes 12 roků starou a po 5 zl. r. m. za děti do 12 let. Za kojence do 1. roku závdavek zasýlati netřeba, jelikož leží s matkou na jedné posteli
a žádného místa proto pro ně se neponechává. Tyto částky jsou nejnižším obnosem, za který se na lodi místo vůbec zabezpečuje. Vy ale můžete zaslati také více, i třeba celý převoz, chcete-li své peníze na jízdu určené bezpečně uschovat. O celý právě obnos, jenž se co závdavek sem odešle, platí se pak méně. Kdyby něco přebývala, hotově se zde vyplatí. Mnohdy se stane, že cestující po desítce za osobu doma nemá. V tom případě, aby čas zbytečně neucházel, může poslati se menší závdavek, řekněme po pětce za osobu, a já pak druhou pětku dosadím. Oč se nejvíce jedná, je, aby byla místa pro Vás obsazena. Složený závdavek poskytuje vždy přednost a dá se v pádu potřeby snadno prodloužit. Nemilé však jest, když člověk je už docela připraven a musí pak, protože v čas závdavky neposlal, doma čekat a z hotového být živ. Takové opomenutí přijde obyčejně draho. Já
proto radím důtklivě a upřímně, neváhat zasláním závdavku ani hodinu, jakmile dobu svého odjezdu můžete přibližně určit. Loď pro Vás vyberu poměrně nejlepší, což v poslední chvíli nedá se více konat. Vám pak zbude dosti ještě času k urovnání svých domácích záležitostí. Kdyby ale nadešla nutnost, odjezd odložit na pozdější dobu, zůstává Váš závdavek nezkráceně v platnosti po celý rok aneb vrátí se Vám, učiní-li důležité příčiny Vaši cestu do Ameriky nemožnou.

## Doprava dítek.

Rakouské a uherské dráhy dopravují děti méně nežli 10 roků staré za polovičku a děti, které se musí ještě nosit a které sedí na klíně svých rodičů zdarma.

Německé dráhy dopravují děti od 4 do 10 let za polovičku a děti do čtyř let, nevyžaduje-li se pro ně zvláštního místa, jedou zdarma.

Americké dráhy dopravují děti od 5 do 12 let za polovičku a děti do pěti let zdarma.

Na parolodích z Evropy do Ameriky platí se za děti od 1. do 12. roku poloviční cena a za kojence do 1. roku po 9 markách německé měny čili po 5 zl. r. m. (Plat za kojence na lodích je americký poplatek z hlavy.)

## Lodní zápisy.

Po obdržení závdavku vystavím lodní zápis, jenž má následující znění:

. Zaslauý obnos zl.......r. m. patřičně došel a ochotně sdělují
Vám, že míst...„. v .....,....,na...„,.parolodí..„........, kferá dne
189 : Hamburku přímo do Nového .Yorku vypravena bude,“pro
Vás objednána a ponechána jsou. Vy máte zaplatit

fix-o......ll (dospělé osoby) po zl.......„...„dělá žl....mr. m._

pro.....„lz (děti od 1. do 12.roku) po zl. .....dělá zl...,...r m.

pro......lo (kojenci do 1. roku) po zl. ..,...dělá 'zl...'....r.'-m._

Úbrncm za převoz přes moře, . . zl....mr. m.-

Na to souložila, jak svrchu udána zl...„..r. m.

Zbývájoštčk doplacení vrakousk. bankovkách 21. ....r 111.

Dále máte “míza: za.........,..železničn ust
: Nového Yorku do po zl.......r. m. zl........r. m.

Za odeslaná předem do Hamburku zava—

, zadla....._...._.., kousky z zl.......r. m.
Úhrnem . . . . . zl....,..r. í'n.
Kviíance. ) Podpis.
Zbytek zl.......r. in. doplacen
v Hamburku, dne....,........

Hořejší formulář cestujícího poučuje jak se osoby na lodi dle ceny třídí. Cena řídí se dle stáří. Radno proto při obsazování míst udati jméno a příjmení, stáří, zaměstnání a bydliště každé jednotlivé osoby, jelikož jest toho potřebí k vystavení lodního zápisu a k zhotovení seznamů, jež předepsány jsou zákony hamburskými i americkými. Rovněž radno udati místo v Americe, kam se pojede, aby cena železničního lístku jíž napřed vepsána býti mohla k vůli ušetření času při výpravě zde a pak aby cestující již

také napřed věděl, co platiti bude. Každý cestující musí lodní zápis v mé písárně v Hamburku předložit, aby se všechny platy v něm poznamenat a kvitovat mohly. Pak se mu opět vrátí, aby vždy věděl, mnoho-li platil a za co. Kdo by se s lodním zápisem nechtěl nosit, aby snad ho neztratil, může si ho opět ke mně odeslat poštou a já ho uschovám, až majitel sám se dostaví.

## Rozvrh železniční jízdy evropské.

S lodním zágisem odešlu cestujícímu zároveň
rozvrh jízdy do. amburku, tak zvanou „maršrutu“,
která. mu poví, kterým směrem se má. ubírati, , kdy
z domova vyjetí, kde co platiti, kde se přesedá,
kde na hranicích celní prohlídka. se odbývá atd.
Podle této „maršruty“ nikdo nezbloudí a bezpečně
se do Hamburku dostane; neb obsahuje současně
podrobnou železniční mapu z Rakouska do Hamburku
a zároveň také kousek plánu města Hamburku, na
němž jsou naznačena nádraží, má písarna, Oelkersův
hostinec'a přístaviště „Passagíerhalle auf dem Gras-
brook.“x Chce-li „šetřit peníze a použít Německem
třídu čtvrtou, která je o celou polovičku lacinější,
nežli třída třetí, tož sezná. rovněž z „maršruty“, kde
a kdy a za co čtvrtá. třída jezdí. Tím současně zví,
mnoho-li na cestu do přístavu bude potřebovat ra-
kouských a německých peněz, kteréž poslednější si
buď už doma neb na hranicích může zaopatřit. Kdo
by však k výměně doma neměl příležitosti aneb kdo
by se obával býti cestou ošizenu, může k závdavku
přiložití na výměnu po desítce za celou osobu, po-
jede-li čtvrtou třídou, a po 20 zl., pojede-li třetí
třídou, a já, mu poštou odešlu německé marky dle
denního kursu, ovšem v nevyplaceném listě, jenž
stojí 30 krejcarů. Z těchto částek vybyde cestujícímu
ještě něco německých peněz na útratu, což nikterak
neškodí, jelikož po cestě den a noc trvající, vždy
něcobuď k jídlu neb pití zakoupiti se uzdá. a k zou-
fání to bývá, když člověk v cizozemsku nemá. v kapse
domácích eněz a rakouských mimo směnárnu bráti
každý se zacha. Sestavování tak dokonalých „marží-'
rut“, jakých já svým cestujíČímposýlám, vyžaduje

mnoho času a mnoho bedlivě práce a proto nikomn

„maršruta“ dříve—se nepošle, dokud závdavkem své

místo nezajistil a takto mým cestujíČímse“ nestal.
Prevozm ceny.

Převozní ceny udávám v příloze nnpřed. Ceny
často se mění, hlavně udávají-li se v rakouské měně,
ažiu podléhající. Ceny neurčuje jedna paroplavební
spolecnost sama o sobě, nýbrž všechny dohromady.
Na sjezdu usjednotí se, jaké ceny každá společnost
smí amusí bráti. Čím vyšší cena, tím lepší loď, aneb
tím laciněji lze se do dotyčného přístavu dostati;
čím nižší cena, tím horší loď aneb tím více člověk
utratí, než se do přístavu dostane. Věc je urovnána
tak, aby žádná paroplavba nebyla v přílišné výhodě
proti jiným. Nikdo nesmí ceny snížit bez“ svolení
ostatních; Na uchylky od pravidel uložena velká po-
kuta. Laciné cepqy může vyhlašovat každý, poněvadž
nejsou vysoké. eboť i na expressm' lodí i se stravou
neplatí se víc než 1 krejcar za kilometr. O snížených
neb lacinějších cenách nemůže však býti nikdež ani
řeči. Všechno má svůj háček. Já proto nízkými ce—
nami nikdy k sobě nelákám. Mně hlavně záleží na
spravedlivém obsloužení svých cestujících, na vše-
možném zkrácení a zmírnění neumlčitelných obtíží
s lodní dopravou v mezipalubí. Pravdou jest a pravdou
zůstane, že věc nejlacinější bývá také tou nejhorší.
Přes moře jeti v mezipalubí, uenívžádná rozkoš, ať
už se jmenuje loď tak či onak. Čímdříve moře se
přerazí, tím dříve se mukám uděla konec. Jenom
v tom a pak zároveň v úspoře času spočívají výhody
rychlolodí. Ostatní je všude všechno stejné a k vy-
brání nikde nic není. Mezipalubí je mezipalubí; je
to poslední třída, tak asi jako na železnicích čtvrtá
a kde tě není, třetí třída. Třetí třída je všude skoro
stejna na všech železnicích a mezipalubí, je všude
skoro stejně na všech parolodích. Pohodlí začíná
teprv v druhé a první třídě. Nádhera námořního
paláce neprospěje tomu, kdo schoulen bydlí v nej-
zadnějším, koutě. Na co největší třeba klásti váhu,
je, aby Čech, ' jenž cizých jazyků nezná a světem
málo ještě prošel, svěřil urovnání svých cestovních

záležitostí osvědčenému českému jednateli v přístavu.
Jenom ten může mu při tak daleké jízdě prokázati
platných a vzácných služeb, chráně ho před škodou
a před zbytečným utrpením.

Slevovám.

Vícero jinorodých firem uchází se pomocí českých
písařů o české vystěhovalce, nabízejíce laci nad laici
na nejvýbornějších rychlolodích, při čemž se však
nejmenuje ani loď, ani přístav, ani den odjezdu. Hazí
se páté přes deváté. Obyčejné lodě vydávají se za
poštovní, poštovní zavexpressní atd. Zamotávají věc
co možná. nejvíce. Ceský venkovan, omamen lací
a podmaštěnou řečí úskočného výdělkáře, sedne rád
na vějíčku. Pošle žádaný závdavek. Leč na neštěstí
přišel závdavek prý o den pozdě a laciné lodě jsou
prý už obsazeny. Nezbývá. prý nic, nežli sednouti
na lodě dražší. Vystěhovalec si dělá, trpkých výčitek,
že zasláním zavdavku příliš otálel, ale po skutku
zla rada. Uspokojí se a je jeho. Tak se chytají hejli.
Dobrodineček přislíbuje ostatně ještě za spolucestu-
jící, které mu odporučí, až po 8 zl.; jelikož však
tuto náhradu příslibuje za spolucestující každému
cestujícímu, neobdrží žádný nic. Do nebe vynášené
poštovní rychlolodě bývají často staré haraburdí a bůh
sám ví, kdy a z kterého přístavu odjedou. Vystěho-
valec čeká a čeká, utrácí a utrácí, konečně pošle se
dále. Zelezniční lístky do přístavu obstarají se mu
za. vyšší cenu a za dopravu zavazadel počíta se daleko
více, než co sazba obnáší. V přístavě nastávají nové
trampoty, nové výdaje. Na lodi jede kdo ví jaké,
s přesedáním kdo ví kde a tluče se mořem kdo ví
jak dlouho. Avšak i za Oceanem má ho dobrodinec
ve svých rukou. Musí vystoupiv z lodi ještě na ně-
kolik dní do hostince, jehož majitel je s dobrodincem
ve Spojení. Ostatní vystěhovalci jedou ihned dále, kam
jejich lístky po amerických železnicích znějí. On ale
musí čekat, až mu hostinský zaopatří lístky na tratě
oklikami lacinější za. cenu nej dražší. Dobrodinci plynou
takto z jednoho lístku do kapsy 2 až 4 dollary a. zisk
hostinského, jenž počítá, za osobu nejméně 172 dol-
laru (3 zl. 60 kr.) za den, bývá rovněž nemalým.

Není-li vystěhovalec pevně rozhodnut, kde se usadí,
přemluví ho dobrodinec do nezdravých a. sibiřských
krajin, kde se platí za dodání jedné duše 5 doll.
Utrapy jsou k nevypsání, kterých vystěhovalec sed-
nuvší žvástavým lákadlům na. vějička snášeti musí
jak na penězích, tak na. tělei na duchu a. proto
nechť každý dbá varovného hlasu: „Láce nad láci,
peníze darme“ Nářek je pak velký, ale vystěhovalec,
stydě se za vlastní hloupost, že se tak notně dal
napálit, spílá jen všeobecně: „jeden šíbal větší než
druhý“, a obecenstvo nerozeznavá takým spůsobem
poctivé jednatele od nepoctivých. Poctivý jednatel
má ceny pevné. Nežádá ani o krejcar více, ani méně,
nežli paroplavební společnost sama. Slevovati pod
rukou, je zakázáno pod pokutou odejmutí jednatelství
pro nesprávné vedení. Společnost i úřadové přísně
nalehají na to, aby každý cestující stejnou platil
cenu. A poctivý jednatel také sleviti nemůže; neb
kdyby slevil, slevil by svá procenta, jež jemu skýtá
paroplavební společnost jakožto náhradu za jeho
práci. Jest to ku př. u mne ten jediný výdělek, z kte-
rého vydržuji sebe ikancelář. Kdybych se tohoto
výdělku oprávněného vzdal, musil bych se uchylovati
od pravdy a zabočiti v jiné směry neoprávněných
praktik po spůsobu shora. naznačeném. Lapaním
pouhého vzduchu nenasytí se žaludek. Kdo dává, co
nemá dát. bere, co nemá brát. Kdo v zdánlivý pro-
spěch Váš postupuje výdělek, jedině oprávněný z ob-
chodního řízení s Vámi, prozrazuje, že mu tento vý—
dělek nestačí. Raději ho obětuje, aby vyzískal větší.
Přináší obět ku přilákání —— kořisti. Bohužel dává
se snadno lapit lidská lehkověrnost, drzou lží zmá-
tena, příteličkováním podkuřováná a úlisným chvá—
stem omámená. Na rtech med, v srdci jed. Mým
heslem jest: „Svůj k svému a vždy dle pravdy“.
Pravda jest toliko jedna. Pravdu žádáte, tu ji máte.
Já se neřídím starou zásadou, že svět chce být kla-
maným. Co by bylo platno, vyličovat jízdu po lodích
za rozkoš, sebe za nějakého generálního ředitele,
bohatého bankéře aneb malovat jiných ještě strak
na vrbě. Vám bych lhaním neprospel a sám bych se
snižoval. Cesta přes moře v mezipalubí je a zůstane
vždy jakousi obtíží. Rovněž co rozumný muž snadno

poznáte, že přílišné vychloubání své věci a hanění
věci cizé, zvučné nabízení láce a. bůh ví jaké všechno
liché tlučení na. buben páchne jarmarečnictvím. Já
co skromný člověk nehonosím se, že lodě.mnou za—
stupované jsou rychlejší, lacinější a zařízenější, nežli
jiné; nevypínám se, že u mne budete lépe alaciněji
obsloužen, nežli u jiného: já. Vás toliko svatosvatě
mohu ubezpečit, že jiné lodě (vždy uporovnání cen
k druhulodí, zdali expressní, poštovní či“ obyčejné)
nejsou nikterak rychlejší, _ ani, lacinější, ani zaříze—
nější, nežli jsou naše, a že někdo jiný neobslouží
Vás lépe a. laciněji, nežli já.. Tato záruka Vám snad
postačí; větší ani nelze dáti. Tyto řádky dobře po-
suzujte, kdyby ještě někdo přišel k Vám a jel se
přemlouvati Vás na stranu jinou. Venkovských agentů
je všude hojnost pro firmy různé národnosti různého
vyznání v různých městech. Každý z nich k vůli
vlastnímu zisku svého pána chválí, at už je chvály
hoden čili nic, každý z nich svého soka haní, at už
je hany hoden čili nic. Vy ale nepochybíte, setrváte-li
se mnou při hesle: „Svůj k svému a. vždy dle pravdy“.
Povrehní rozpočet jízdy ku př. z Prahy do
Chicaga.
I. Nejlevnějším qrůsobem:
Z Prahy do Hamburku dráhou s upo-
třebením v Německu čtvrté třídy r. 111. zl. 10.—
Z Hamburku do Nového Yorku po
obyčejné parolodi . . . . . . . . „ „ „ 57.—
Z Nového Yorku do Chicaga oklikou
přesNorfolk. . . . . . _ .. „ „ „ 28.50
r. m. zl. 95:50
II. Nejdražšim způsobem:
Z Prahy do Hamburku veskrz III.
třídou pro všechny vlaky, tedyi
rychlíky. . . . . . . . . . . r.m.zl. 17.—
Z Hamburku do Nového Yorku po
expressní rychlolodi . . . . .. „ „ „ 68.—
Z Nového Yorku do Chicaga přímo
dráhou ...,....,...„ „ ,? 31.—
I'. na. zl. 116.—

Za děti od 5. do 12. roku počítá. se veskrz po-
lovička; za děti od 1. do 5. roku na lodi polovička,
na americké dráze nic a na rakouských a německých
železnicích, jsou-li děti malé, také nic; jinak přes
4 leta na drahách v Rakousku a Německu (viz článek
„Doprava dítek“) polovička. Za kojence do 1 roku
nepočítá se na dráhách nikde nic a na lodí po
5 zl. r. m.

Vzdálenosti.

Z Prahy do Hamburku . . . . 670 kilometrů.

Z Hamburku do Nového Yorku 6.576 kilometrů.

Z Nového Yorku do Chicaga . 1.605 kilometrů.

Dle toho přijde jízda _ _ „

za 1 kilometr 3333733 ŠŠŠ—iii?
na evropských drahách beze stravy 1.47 2.54
na amerických drahách beze stravy 1.77 1.93
na parolodi přes moře se stravou 0.86 1.03
Vedlejší útraty po cestě.

Jízda, dejme tomu, z Prahy do Chicaga trva nej-
rychlejším způsobem
z Prahy do Hamburku . . ._ „ . . . . . .lden;
pobyt v Hamburce . . . . . . . . . . 2 dní;
Z Hamburku do New'Yorku. . . . . . . .8 dní;
pobyt vNeW-Yorku. . . . . . . . . . . 1 den;
z New-Yorku do Chicaga . . . . . . . ._._2 dni;

14 dní.

Že se za 14 dní něco utratí, vyrozumíva se samo.
Mírně počítáno, možno útratu odhodnout na! osobu
při jízdě
z Čech a z Moravy po dráze za jídlo a pití,

někdy i přenocování, převoz z jednoho

nádraží na druhé . . . . . . . . . . zl. 1.—
v Hamburku v hostinci za byt a stravu ve

III. třídě, 90 kr. denně, za 2 dni . . . „ 1.80
nápoje 25 kr. denně, 2 dni . . . . . . . „-—.50
nalodipivozasdní . . . . . . . . .;!i—

Snešeno zl. 4'30

_ Přenešeno zl. 430
v Novém Yorku 1 den přes noc . . . . . „ 2.50
„ „ nějakou zásobu potravy na
dráhuna2dni. . . . . . . . . . ...»„ 1.—
10 skleniček piva po “5 0:50 a, aneb místo —
piva nějakou kávu či polévku . . . . .. „ 1.20
zl; 9.—
Přuezd do Hamburku.-—

Dle zákona a policejních, předpisů nesmí nikdo
čekati na vystěhovalce uvnitř nádraží, kde se sleza
z vbzů. Jenom hostinští, mající úřední konoessi k ve-
dení vystěhovaleckého hostince, smí čekat u nádraží
venku 11 hlavních vrat, kde sez nádraží vychází na
ulici. Uvnitř je strážník v uniformě s helmou, jenž
vystěhovalce přijímá a dle hostinců, na které ukážou
lístky, třídí. Napřed musí ostatní obecenstvo s vlakem
přijedší býti z nádraží ven. Pak jde strážník s vy—
stěhovalci do jednoho hostince patřícími na ulici ven
a vyvolá jmeno hostinského, jenž své cestující přijme
a. odvede. Strážník se vrátí a přivede vystěhovalce
zase jiné do jiného hostince patřící atd. Firma Josef
Pastor odporučuje svým cestujíČímhostinec Fran—
tiška Oelker'se v kleine ReichenstraSse č. 10 naproti
své písárně. Jinak ale může každý ubytovati se, kde
se mu líbí. Nikdo se nenutí. Hostinec není sice velký,
ale patří k těm nejlepším a nejspořádauějším a leží
po ruce. Kdo se v tomto hostinci ubytovati hodlá,
at vycházeje z nádraží drží lístek hostinského v ruce
vzhůru a volá jmeno Oelkers. Okolo nádraží bývá
0 pasažéry tahanice; tomu at se každý Čech vyhne,
nechce-li býti zavlečen. Kdo jede prostřednictvím
Pastorovým, obdrží s lodním zápisem kousek plánu
města Hamburku, kde jsou naznačena nádraží, ho-
stinec Oelkersův, písárna Pastorova a přístaviště.

Hostinec v Hamburku.

Až přijedete do Hamburku, můžete se, nežli
budete sedat na loď, ubytovati v hostinci Frant.
Oelkerse v ulici Kleine Reichenstrasse čís. 10. Je to
slušný, čistý a laciný hostinec a leží šikmo naproti
mé kanceláři. Z hostince ke mně budete mít proto

pouze přes ulici. Jeho listek při vystupování z vozu
na zdejším nádraží zastrčte si za klobóuk či za če-
pici nebo jej držte v rukou, aby strážník, jenž ještě
uvnitř vystěhovalce dle hostinců třídí, věděl, kam pa-
tříte. Vycházeje “z nádraží na ulici uvidíte v pravo
i v levo celou řadu čekajících hostinských. Tuvolejte,
abyste byl úplně jist, že přijdete na pravou adresu,
ještě jednou hlasitě jmeno Oelkers a. mějte jeho lístek
buď za kloboukem aneb ho dlžte rukou vzhůru, aby
viditelným byl. Tím bude hostinský Oelkers naVás
upozorněn a ujme se Vás. Hostinec v stěhovalecký
má tři“ třídy. Byt se stravou za 24 hošny stojí v I.
třídě 3% marky, v II. tř. 2% matky a v III. třídě
1% marky. Pouhý byt beze stravy stojí za 24 hodiny
v I. tř. 1% marky, v II. tř. 1% marky a vIII.třídé
=V4 marky. Za děti od 1. do 12. roku platí se pole—
vička a děti do 1 rokuruhostěny jsou zdarma. Za
dovežení zavazadel z nádraží do hostince počítá. se
až do 50 kilo- po » 50 trojnících a od 50 až do 100
kilo po 1 matce (1 marka platila při vydání tohoto
spisu 57 krejcarů). Tatáž sazba platí za dovezení
zavazadel z hostince na loď. Hostinský musí cestu—
jící v čas doprovoditi na loď.
Zprávy () přijet! lodi na misto.

Cestující vystoupivše za mořem na břeh nemají
pro samou novotu aneb pro starosti a trampoty oby-
čejně buď ani chuti, buď, ani času, buď ani příleži-
tosti, dáti svým lidem. věděti, že šťastně přes vodu
sejdostali. Doma nastává, pak obava, zdah'ž loď ne—
potkalo nějaké neštěstí atd. Tomu dá se předejití,
kd'yž cestovatel udá v mé pisárně adresy osob, jimž
se zpráva má podati. Jakmile lod' přirazí, dojde o tom
telegram a'já pak podám frankovaně všem, jichž
adresy za tím účelem u mne zanechany, písemní
zprávu, že parolod' ta a ta, s kterou N. N. odejel,
dne toho a toho, tu a tu hodinu šťastně do přístavu
toho a toho se dostala. Za podání těchto zpráv ne-
vyžaduje se žádného platu.

Svobodný náklad.
1. Na rakouských drahách. Na c. k. rakou-

ských státních drahách, kde je ,pro dopravu osob
zavedena pásmová sazba (viz tuto), není žádný svo-
bodný naklad. Na ostatních dráhách je na 1 lístek
v I., II. a III. třídě povoleno 25 kilo a % lístku
12 kilo zavazadelního nakladu zdarma-, '.00 je přes
váhu, za to se počítá za každých 10 kilo—a za každý
kilometr %, krejcaru. Dále se platí manipulační po—
platek 7 kr. vždy za každou dráhu a 5 kr. kolek,
když zavazadla přesahují zdarma povolenou váha.

2. Na uherských drahách. Na uherských
státních dráhách, na uherské severozápadní draze
jakož ina Košicko—bohumínské draze není žádný
svobodný náklad, jelikož nyní _na těchto dráhách
panují pásmové sazby (viz tyto). Rakousko-uherská
státní dráha v Uhrách počítá za každých 10 kilo za
každý kilometr Vm kr. Ostatní drahy thrachmají
sazby jako dráhy rakouské.

3. Na německých drahách, asice na saských
a pruských, je na 1 lístek I., II. a III. třídy povo—
leno 25 kilo nákladu zdarma a na %, lístku 12 kilo.
Za převahu se platí za každých 10 kilo a za každý
kilometr 1Artrojníku (Pfennig). Na lístek IV. třídy
není žádný svobodný naklad, než jen cočl'ověk může
s sebou vzíti do vozu.

4. Na parolodích přes moře neplatí jak při
naklade tak při zavazadlech žádná vaha, nébrž jen
míra. Na lodi se úředník neptá, jak jsou zava—
zadla těžká, nébrž pouze jak jsou velká. Na 1 jízdní
lístek povoleno % krychlového čili kubického metru
zdarma ana V2 lístku V4 krychl. metru; kdo má přes
míru, musí doplácet dle tarifu: 50 německých marek
za. 1 knbický metr, což je asi 174 marky za jednu
kostkovou stopu.

5. Na amerických dráhách mavystěhovalec
na 1 celý lístek 150 amerických liber nakladu svo—
bodno, což je asi 68 kilo, a na % lístku 75 liber
(34 kilo). Co je přes váhu, platí se ku př. z Nového
Yorku do Chicaga (960 anglických mil: 1.645 kilo-
metrů) dle tarifu: 1 dollar 95 centů za 100 liber (čili
asi 4 zl. 65 kr. za 45V3 kila).

Míra beden a kufrů.

N a lodi se nepočítá náklad dle Váh , nýbrž pouze
dle míry a rozměr beden či kuůů najdye se, násobí-li
se v centimetrech délka výškou 3. to co vyjde náso—
bí-li se ještě šířkou. Kostkový metr je jeden million
kostkových centimetrů a půl metru je půl millionu
čili 500.000 kostkových centimetrů. Uvedu příklad.
Bedna by byla 102 centimetry dlouhá, TO centimetrů
široká a 70 centimetrů vysoká; tož by v těchto roz—
měrech obsahovala 102 X 70 : 7140 )( TO : 499.800
kostkových centimetrů, tedy ještě o 200 méně, nežli
zdarma povoleno jest. Kufry a bedny nepotřebují míti
snad rozměry, jak hořejší příklad udává; mohou
míti délku, šířku i výšku jakoukoliv. Já chtěl po-
dati pouhý návod, dle něhož doveda svá zavazadla
sám si změřiti předem již každý zví, bude-li musit
dopláceti čili nic. Na daleké cestě vyžadují zavazadla
nemalé opatrnosti. Bedny akufry buďtež pevny, aby
se nerozbily častým překládáním a házením po cestě,
buďtež zavírány na dva silné zámky uvnitř, v pravo
i v levo, an se visací zámek snadno urazí a budtež
k tomu ještě obvázány dobrým provazem na kříž,
aby se někde těžším udeřením nerozsypaly.

Znamenáni kufrů a beden.

Každý kufr a balík, každá bedna a taška atd.,
jež pošlou se napřed buď poštou neb drahou na
adresu Jos. Pastor No. 23 Kleine Reichen-
strasse, Hamburg, nechť se znamená dvěma pí—
smeny a, jednou číslicí. Za písmena nejlépe třeba
volit začáteční písmena jmena a příjmení vlastníkova
a za číslici číslo domu, v kterém vlastník posledně
bydlel. Majitel by se jmenoval ku příkladu Václav
Čech a bydlel by v čísle 31.„Svou bednu neb kufr
dal by proto znamenati: V. O. 31. Kdyby měl zava—
zadel více, řekněme čtyryv kusy, dal by pvrvní zname-
nati, V. O. 31, druhé V. O. 32, třetí V. O. 33, čtvrté
V. O. 34. Kdyby pak o svých zavazadlech, na mne
pan Václav Čech psal a. je jednoduše V. 0. 31—34
jmenoval, vím ihned, která zavazadla to jsou amno-
ho-li kousků. Napíše-li však skoro jeden každý na

kufr jenom adresu Pastorovu, pak mam zde mnoho.
kufrů a beden se stejnou adresou a rozeznání spů—
sobí obtíží. Kdo proto dá. znamenat sva zavazadla
jak uvedeno dle svého jmena a příjmení a dle čísla
domu, může spoléhat, že budou vždy snadno k roze—
znání a že on sám označení čili markování svých
beden a kufrů nezapomene. Dejme tomu, že bykufr
Václava Čechu, označený V. O. 31 někde uvaznul a
že by se musil hledati. Podle znaku se lehko vy-
najde, ale dle popisu, že je černý, velký, malý atd.
těžko; neb v magacínech leží často na sta kufrů
a beden.
Zasýlání nákladu.

Zde děje se nejvíc chyb. Proto tento článek
doporoučím zvlášť Vaší pozornosti co nejbedlivější.
Chcete-li jeti do Hamburku veskrz třídou třetí, máte
na jeden celý lístek jenom 25 kilo nakladu svobodne.
Za každých dalších 10 kilo platíte něco přes 2 zl.
Vy byste měl ku příkladu přes vahu 71 kilo. Draha
už počítá, 80 kilo, musil byste za ně nejméně do-
plácet 2 X8:zl. 16, kdežto kdybyste poslal jeden
metrický cent do Hamburku 14 dní napřed na mou
adresu co obyčejný naklad (německy Frachtgut),
platil byste pouze 5 až 6 zl. Posledním spůsobem
uspoříte už na. 100 kilech víc než 10 zl. a máte pak,
když jedete, se svým nákladem méně starostí. Ná-
klad, který jde zároveň s Vámi, musíte na hranicích
v celnici dát prohlédnouti, sice by zůstal zpět a. ne
šel by dále, kdežto naklad, napřed poslaný, nepro-
hlíží se nikde, až v Americe. V nákladním listě ne-
udávejte své věci jinak než jen „Effekten“ aneb
„Passagier-Effekten“, což znamená věci pro osobní
potřeby určené, jako šatstva, peřiny, prádlo, obuv
atd. Casto se udává. ku příkladu „peřiny“, „peří“,
na které je zvýšena sazba, což je zbytečné. Prohlí—
žení působí obtíží; musíte zajít do celního sálu, mu-
síte svůj recepís odevzdat nosičům, kteří na tento
rece is zavazadla z vozů přinesou, musíte je otevřít,
a když celní úředník snad všechno rub na rub pře,
házel, zase zavřít a od nosiče, jehož dobře pamatovati
si třeba, si pak dat recepís vrátit zpět, tot se rez-

umí po zaplacení malého zpropitného za vykonanou
pro Vás službu. Mate-li úmysl k vůli uspoření peněz
použíti Německem třídy čtvrté, která stojí právě
jenom polovičku třídy třetí, pamatujte, že na lístek
čtvrté třídy nepovoluje se žádný svobodný'náklad,
než jen to, co člověk může s sebou vzíti' do vozu.
Rovněž nezapomínejte, že zavazadla, jež s sebou ve-
zete a jež jste dal na váhu, jdou vždy jen tak da—
leko, jak daleko zní Váš lístek, a že tolikráte vžd

znova musíte je dát na váhu, kolíkráte svůj lístek
po cestě obnovujete. Pozor dávejte na reoepis, bez
něhož se zavazadla nevydají. Kdo ztratí recepis,
ztratí obyčejně zavazadla. Dle mé dlouholeté zkuše—
nosti učiníte nejlépe, když těžší věci 14 dní napřed
zašlete, pravím 14 dní napřed, aby byla jistota, že
zde budou, nežli_Vy přijedete. Skladník na dráze
Vám za malou odměnu vše vyhotoví. Rychlozboží
(Eilgut) potřebuje do Hamburku asi týden, je ale
ještě jednou tak drahé jako náklad. Náklad se musí
posýlat na mou adresu: Jos. Pastor, No. 23, Kleine
Reichenstrasse, Hamburg, a ne na jinou adresu. Ně-
kdo, boje se o svá zavazadla, pošle je na vlastní
jmeno. Pak zůstanou ležeti na dráze, spůsobí vysoké
skladné (tak zvaný „Lagergeld“), majetník přijeda
sem musí je hledati, musí se pasem legitimovati,
aby dráha se přesvědčila, že mu patří, musí celní
prohlídku obstarati, do přístavu je dopraviti atd.,
kdežto uschování nákladu, který se pošle na mou
adresu napřed, děje se u mne úplně zdarma, byť by
třeba i celé měsíce zde ležel. K vůli sazbám v zlaté
německé měně * posýlají' menší stanice naklad do
Hamburku nevyplacené. Já takový naklad vyplatím,
u sebe-uložím,——k lodí dopravím a nepočítám cestu-
jíČím' ani o_ krejcar více, než jen co hotové výlohy
obnášejí. Každému, kdo si přeje, předloží'se ná-
kladní list na důkaz, mnoho-Ii dovoz do Hamburku
obnášel. Za dovoz z Hamburského nádraží k'lodi
platí“ se u mne 1 marka 10 trojníků za,—kus a 40 troj-
níků za celní 'průvod do svobodného přístavu, cel-
kem- tedy 1 marka 50 trojníků čili asi 86 kr. Kdyby
se však někomu z jeho zavazadel něco ztratilo po
cestě, neberu za takovou ztrátu žádnou zodpovědnost
já, nýbrž vždy jen zasýlatel sám.

Dpozděné náklady.

Komu by náklad předem do Hamburku zaslaný
v čas nedošel, a. on proto bez něho cestu přes moře
nastoupil, musí se hlásiti u mě filialky Jos. Pastor,
1432, I. Avenue New York, na. niž náklad s ně-
kterou pozdější lodí se odešle s dobírkou vzešlých
výloh. Paroplavební společnost čítá za dopravu ta-
kového nákladu, když se povolená. na. jízdní lístek
míra nepoužila, 1 dollar za každý kus, ať je“ velký
at malý, těžký či lehký. V Novém Yorku čítá se
rovněž za celní manipulaci, za dovezení na. dráhu
atd. jakási částka.. Cestujícím, jimž by zavazadla
zde uvázla, a kteří po Americe jedou dále na západ,
možno raditi, aby z Nového Yorku volili směr přes
Norfolk, a aby, než odjedou, oznámili věc v mé
tilialce a udali tam číslo lístku. Pak by odeslaná za
nimi zavazadla šla. také v Americe (do 150 liber
amerických na. 1 lístek) zdarma.. Nežli však se
z Nového Yorku dotyčný náklad pošle za. majitelem,
musí byti dříve všechny naznačené dobírky a. výlohy
u mě iilialky .zapraveny. Patrnoť, že opozděný ná.-
klad působí mi obtíže a. majiteli značné útraty. Proto
káždý dbej, aby náklad se neopozdil.

Kupovam převozních lístků v Americe.

Mnoho amerických Čechů vyplácí svým známým
a příbuzným jízdu do Ameriky. Každý z nich si
přeje, aby jeho lidé byli správně vypraveni, dobře
obsloužení a patřičně na tak dalekou cestu poučeni.
Tomuto přání bude vždy vyhoveno, koupí-li se pře-„
vozní lístky u'mé ňlialký v Novém Yorku, jejíž
adresa. jest: Jos. Pastor, 1432, I. Avenue New York.
Ale pozor! Já. v Americe žádných místních a. ven-
kovských jednatelů nemám. Kdo tudíž chce použiti
mého? prostřednictví, musí přímo dopsati na udanou
adresu do Nového Yorku. Vyplatí-li se cesta u ji-
ného jednatele, třeba. přes Hamburk, nejde doprava
skrze moje ruce, a já, s cestujíČímdo žádného styku
ani písemně ani osobně zde nepřijdu. Proto nechť
se peníze na převoz pošlou všude, kde se vyžaduje
mého prostřednictví, na. mou ňlialku do Nového

Yorku, která každému na předcházející poptávku
ochotně a zdarma bližších podá zpráv. Naše ceny
nejsou vyšší nežli jinde. Ovšem dělá. mnohému kra-
janu zasýlání peněz poštou částečných obtíží, ale
toť přec jen nepatrnou je maličkost-í vůči žádoucímu
pro něho cíli, aby osoby, jimž cestu vyplácí, a na
nichž mu proto také zajisté záleží, přišly předi za.
mořem do rukou českého jednatele jak v národním
tak v obchodním ohledu s důstatek osvědčeného.
Tito cestující obdrží odevmne na. památku zdarma
pěkně vázanou knihu „Ceské osady v Americe“
a „Kapesní slovník česko-anglický“ a občanu ame-
rickému, jenž jim jízdu vyplatil, podá se odtud
zpráva, na kterou loď sedati budou, a kdy asi je
očekávati má. To jsou vše malé pozornosti, malé
ochotnosti, které ale našemu lidu za mnoho stojí,
a o nichž jinde se neví. Zásluha nespočívá v do-
pravení, nýbrž v připravení cestujícího. Objednat
a vystavit přeplavní lístek, dovede každý chlapec;
ale dát vystěhovalci patřičný návod na. celou cestu,
obstarat mu všechny služby spravedlivě, výhodně
a úplně zdarma, ošetřovati a ujímati se ho ve všech
potřebách až na místo a jednati s krajanem vždy
co upřímný krajan, k tomu už patří zralá povaha
zkušeného muže, jenž ve svém poyolání zaujímá
vyšší stanovisko, k tomu už patří Čech, jenž jest
Čechem s tělem i s duší, a jenž s chutí a horlivostí
pracuje, aby plnil úkol národní. — Rovněž vřele
odporučnji americkým Čechům do Evropy cestujícím
ku koupení převozních“ lístků svouíilialku v Novém
Yorku, která. každému ochotně a správně poslouží.
Kdo z těchto cestujících u mne v Hamburku se za-
staví, tomu zabezpečuje se zase zde jakákoli služba
dokonale a úplně zdarma. Každý Čech, uposlechnuv
tohoto odporučení, najde u mne mnohý prospěch,
který by mu jinde snad ušel. Mimo jiné obdrží zde
skvostné vázanou knihu „Ceské osady v Amervice“,
kterou může vzíti s sebou na památku do _Čech.
Jest mi vždy nemalým potěšením, když mne zde
Amerikán navštíví, a když mne o poměrech své
nové vlasti zpraví. Sbírám zkušenosti starých Ame-
rikánů, abych stále po ruce měl látku poučlivou pro
nováčky, jimž, když už jednou se svou otčinou se

rozloučili, přáti a dle možnosti přispívati musíme, aby
za mořem prospívali. Avšak těším se takové ná.-
vstěvě také co bývalý Amerikán, nemoha se zbaviti
touhy po hovoru o státech, v nichž i já dlouhá. léta
strávil, o tamních Ceších; v jichž kruhu jsem ob—
coval a působil. I zde, platí přísloví: „Svůj !: svému
a vždy dle pravdy.“ Čech Čechovi proukaže.platněj-
ších služeb, nežli sebe správnější jinorodec, a má.
firma. za výdělek z prodeje lístku lodního plynoucí,
zříkajíc „se každéhovedlejšího zisku, poslouží s nej-
větší-ochotou ve všem. každému zdarma. Něco za
neco.
Výměna peněz.

Na cestě do Ameriky budete mít co dělat se
dvojími penězi, Vám sna ještě neznámými, a sice
s německými a americkými. V Německu jsou marky
a v Americe dollary. Jedna marka (německy Mark)
má 100 trojníků (německy Pfennig) a jeden dollar
(anglicky Dollar), má. 100 centů (angl. cent vyslov
sent). Kulaté stojí 1 dollar asi 2 zl. 40 kr. Kurs
rakouských peněz čili tak zvané ažio mění seo ma-
ličkost ovšem co den, ale já. uznávám zadobré, zahy
o věci Vás uvědomit, abyste sám uměipočítatimikde'
ošiditi se nedal. Měnit můžete. kde chcete, jenom
dbejte, abyste měnil u firmy známé a solidní, která
by Vám nic falešného do ruky nedala. Já. obstará.—
vám ovšem také výměnu peněz cestujícím, kteří mne
o to výslovně požádají, nevnucuji se ale, jelikož vý-
měnu konám z pouhé ochoty, beze všeho zisku, při-
hlížeje stále k tomu, aby našince, jenž mně urovnání
svých cestovních záležitostí svěřil, byl ve všem ob-
sloužen spravedlivě. Až po dnes, po celých 20 roků,
nemůže nikdo, jenž mým prostřednictvím doAme-
riky vypraven byl, říci, že by u mne byl o jediný
haléř zkráceným. Na to ale upozorňuji, aby si nikdo
vyměněné peníze nedával ani po cestě ani v Ame-
rice přepočítávat neznámými lidmi,'kteří z řemesla
se lichometí hlavně k vystěhovalcům, aby se jich
zdánlivě ujali. Bývají to obyčejně krajané. Po dlou—
hém přítelíčkování nabídne se úlisník konečně pře-
počítat peníze, zda-li nestalo se ošízení. Buď pak
něco schází aneb se najde nějaká, falešná, bankovka,

která se dovedným způsobem za dobrou bankovku
vpašovala. Podvodník takový ihned obviňuje pocti—
vého směnárníka, aby sám zůstal čistým. Důvěra.
proto neznámým lidem se nesmí dávat cestou ihned.
Do Německa, neberte s sebou žádných stříbrných
peněz, nýbrž jen papírových. Na. každém stříbrném
zlatníka tratí se o 3 krejcary více než na papírové
zlatce, a na, drobných tratí se ještě o jeden krejcar
víc. Při výměně dejte sobě všude dít účet, abyste
věděl, kde, kdy, co a jak jste měnil. Buďte opatr-
ným; neb jakmile peníze přijmete _a z kanceláře vy-
jdete, ztrácíte veškeré nároky, kdyby náhodou vý-
měna. nebyla, správná. Jenom omyl, z účtu vidný, dá,
se napravit. Jeden zlatý zlata jsou zrovna 2 něme-
cké marky. Německé marky a americké dollary je
zlatová měna, Při vydání tohoto spisu (prosinec
1890) byly kursy na celé krejcary přibližně tyto:
1 německá marka :_zl. 0—57 r. m.
1721. r. 111. (100 :'57: 1'75):m. 1-75.
1 americký dollar : 111. _4-20.
1 „ r „ (4-2o:1-75_—_2-40):_—z1. 2-40.
1 zl. r. m. (1'75z4'2020'416):d01. 041%.
Nejvyšší cena amerického dollaru je v létě, když
je po něm silná, poptávka, m. 420 až m. 4-22, nej-
nižší v zimě m. 4'18 až m. 420. Chceš-li vědět,
mnoho-li dostaneš marek a dollarů za rakouské ban-
kovky, vezmi do ruky některé Pražské noviny aneb
dojdi na nejbližší telegrafní úřad, kde se kurs něme—
cké marky dozvíš. Tímto kursem počítej dle hořej-
šího návodu a máš to.

Vystěhovalecký úřad v Hamburku.
(Auswanderer-Behórde in Hamburg).
„Ochrany a rady dostane se vystěhovalcům

bezplatně v kanceláři vystěhovaleckého úřadu v Ham—
burku, Passagierhalle auf dem Grasbrook, jenž vše-
dního dne od 9. do 6. hodiny otevřen jest.“ Toto
upozornění ve všech řečích rozdává. policie všem vy-
stěhovalcům, jakmile do nádraží hamburského při-
jedou. Komu by se tedy, ať v čemkoliv, nějaké pří-
koří stalo, obrat se na výše jmenovaný úřad, jenž

se tebe vždy ochotně a rázně ujme. Cesky se tam
smluvíš také. Jeho kancelář nalezá se v budově,
jmenované Passagierhalle auf dem Grasbrook (čti
pasažírhalle auf dem grázbrók), u vody, kde se sedá.
na loď. Kdo ode mne s lodním zápisem obdrží žele-
zniční mapu s rozvrhem jízdy do Hamburku, najde
na této mapě také kousek plánu zdejšího města,
kde budova „Passagierhalle“ naznačena jest. Dle
plánu tam každý snadno může treňt.
Legltlmace.

V Rakousku je říšskou ústavou, tedy nejvyšším
zákonem, zabezpečeno volné pohybování a přesídlo-
vání z místa na místo. Cestovati se může proto úpl—
ně svobodně bez pasu a. beze vší legitimace nejen
v Rakousku a Uhersku, nýbrž i v celém Německu.
V Americe se nikde ani pasu ani jiných legitimací
nevyžaduje. Samo sebou se však rozumí, že opatrný
člověk na cestách nezapomene na legitimační listinu,
kterouž by se dovedl vykázat, kdyby někde pro ně-
jaké podezření. zastaven byl. Kdo chce domov na.
čas opustit buď za svým povoláním, aneb k vůli ná.-
vštěvě, aneb k vůli vyhledání práce atd., nesmí; mu
obecní úřad odepříti domovský list a. pracovní knížku.
Politický úřad musí každému občanu; jenž na cesty
vydati se hodlá, uděliti pas, když prostřednictvím
obecního představenstva. o to požádá. a předepsaný
kolek zapraví. Mladíci, kteří si svůj assent ještě ne-
odbyli, mohou ovšem obdržeti pas toliko do času,
kdy k odvodu dostaviti se mají. Vše to ale týče se
pouze cestujících, tedy lidí, jež jen na nějaký čas
se vzdalují a opět domů se vrátí. Něco jiného, jedná—li
se o vystěhování do ciziny. Vystěhovati se znamená.
tolik, jako odejít za zemské hranice a více se nevrá-
tit. , Vystěhovalcům, pro žádné zločiny soudně nestí—
haným, musí vydati se pas beze všech průtahů, ne-
jsou-li vojančině podrobeni a mají-li zaplaceny daně
a. vojenské taxy. Toliko příslušníkům armády může
povolení k vystěhování se do ciziny před ukončenou
služební povinností uděliti sám jen říšský ministr
vojenství dle clánku 64. rakouského branného zá,-
kona, dle něhož rovněž povolení těm, kdož posud

nedostavili se ku odvodu, závisí od rozhodnutí mi-
nistra zemské obrany, a uděliti se může, jen když
také rodičové se vystěhují. Kdo jede ,do Ameriky
a není německým poddaným, tomu při sedání na loď
postačí legitimace jakakoliv, at to pracovní knížka,
domovský list, křestní list, pas neb cokoli. V pří—
stavě se nikdo neptá příslušníků cizé zemi, zdaliž
jsou cestujícími aneb vystěhovalci. Za to ale má, na
rakouských hranicích (3. k. policie právo, žádati od
skutečných vystěhovalců patřičných legitimací na
důkaz, že státním povinnostem vyhověno jest. Pra-
videlný pas, znějící do Ameriky, je vždy nejlepší
legitimací, a radí se proto každému, aby se jím za-
opatřil, nežli na cestu zaoceánskon se vydá, ať pak
už je vystěhovalcem čili jenom cestujícím.
Sedám na loď.

Nikdo nemysliž, že, jakmile přijedeš do přístavu,
můžeš z nádraží přímo odebrali se na loď. Na loď
se sedá v určitý čas. Napřed ale musí býti vše
uspořádáno. K tomu patří: 1. Všichni cestující musí
býti u paroplavební společnosti v knihách zaneseny,
2. lodní lístky, jež jsou velkého formatu, obsahují-
cího dlouhou dopravní smlouvu, podepsanou ředitel—
stvem a cestujíČímsamým, musí být vystaveny,
3. podrobný seznam všech cestujících musí se před-
ložit úřadům, které pak vyšlou komisary, aby místo
pro cestující ponechané vyměřili, zdaliž je postači—
telným, aby prohlédli zasoby, potravin, pitné vody
atd., jež jsou zákonem na každou osobu předepsány
a 4. teprv když vše shledáno v pořádku, vystaví se
parolodi pas. Toto urovnání vezme při pilné práci
úplné dva dny, mezi kterým časem také veškerá
zavazadla dodána býti musí na přístaviště k změ-
ření a je proto nutno, aby cestující, kteří mají místa
závdavkem zajištěna, dostavili se do přístavu 48
hodin před odjezdem lodi. Jinak by se loď ani vy—
pravit nemohla. V určitou hodinu povinen je ma-
jitel každého vystěhovaleokého hostince své hosty
dovésti na přístaviště, kde se odbývá.: 1. lékařská
prohlídka, aby nikdo s nějakou nakažlivou nemocí
nedostal se na loď ; lékař dá na lodní lístek své ra-

Dvoušroubová expressní parolodi

zítko; 2. dále stojí úředník, jenž prohlíží lodní lístky,
zdaliž na nich lékařské razítko se nalézá; 3; dále
stojí 3 až 4 policejní úředníci, prohlížející pasy hlavně
za tou příčinou, aby nikdo nemohl uprchnout,“ kdo.
je stíhán zatykačem a aby z německé říše neutíkali
mladíci ve vojenských letech; 4. u přístaviště na
vodě leží loď menší, spojená s přistavištěm dřevě—
nými schody; u schodů stojí 2 kontrolor-ové, kteří
odtrhnou od lodního lístku kupon a na loď tolik
osob'propustí, na kolik lístek vystaven jest; proto
se rodiny musí držet pohromadě. Tato malá loď do-
veze cestující k velké lodi, nakterou opět přestoupí
a která. pak je zaveze až na místo. ' Na velkělodi
stojí mužstvo, které cestujíČímukazuje, do. které
místnosti mají se odebrat. Třídění děje se dle lístků:
rodiny mají lístky bílé, svobodní mužští mají zelené
a svobodné ženské mají žluté. Tyto tři druhy cestu-
jících jsou na lodi odděleny. Veškerá zavazadla jdou
s cestujícími s malou lodí na velkou, kde se pak
mezi jízdou po Labi k moři (z Hamburku se jede
po Labi ještě asi 18 mil, skoro 6 hodin k moři)
urovnávají. Každý cestující, jenž má'menší kufry,
at:-si svá zavazadla dá donést kn své posteli; kde
se umístní buď pod postelí aneb v některém koute-
čku, kde právě místa jest. Větší kufry a. bedny uloží
se jinde, ;ale majitel si —z nich napřed může vzíti, co
potřebuje. Proto dej každý pozor na. své věci, když
se počnou na velké lodi ukládat a rovnat.
Parolodě.

Společnost, kterou j a zastupuji, jmenuje se „Ham-
burg-Amerikanische Packetíahrt—Actien—Gesellschaft“.
Je to jedna z největších paroplaveb na. světě, majíc
přes 40 obrovských lodí (malé lodě víto'nepočítaje).
Její personál obnášel koncem minulého roku 2897
osob. Její loďstvo vykonalo za minulý rok 1,634.290
,mil cesty, což je asi tolik jako 75krát okolo země.
(Objem celé zeměkoule je 21.600 námořních mil
a 1 námořní míle:1855 metrů). Že od tak roz-
sáhlého podniku nelze očekávat, aby jeho ředitelstvo
a. představenstvo, jež zákonitě oprávněno jest firmu
podepisavat , umělo také česky k vůli rozumění

českým dopisům, je na bíledni. Přec nemůže nikdo
podepsat právně dopis, jemuž nerozumí. “Proto je
ustanoven český jednatel, jakožto prostředník “mezi
společností a-"českým obecenstvem. Není to tudíž
žádný zbytečný poběhlík aneb vtíravec, jenž by jako
upír na vystěhovalce čekal a je lákal. Německá
paroplavba ho ,musí mít k vůli českému obecenstvu
a české obecenstvo ho“ musí mít k vůlí německé
paroplavbě. Kdybych já'zde nebyl,'byl by zde někdo
jiný, a kdo ví, jestli by někdo jiný českému obe-
censtvu poskytoval tak platných služeb, jako já. Ale
k věci. Já dopravují po trojích parolodích třikrát
za týden, a sice po obyčejných každou středu, po
poštovních každou neděli a po expressních každý
čtvrtek. Obyčejná. paroloď urazí cestu za 14 až 15
dní, poštovní za 11 až 12 dní a expressní za 7 až
8 dní. Je-li nepříznivé počasí, může se jízda. pro-
dloužit u obyčejných parolodi až o 3 dny, u poštov-
ních až o 2 dny a u expressních až o 1 _den. _ Je-li
mlhavo, smí se jen poloviční rychlostí jeti. Mezi-
palubní cena je na obyčejné parolodi o 20 matek
a na poštovní o 10 marek lacinější, nežli na ex-
pressní. Expressni lodě jezdí ale pouze od jara do
zimy, a majíce po dvou šroubech s dvojím strojem,
jsou nejrychlejšími a nejbezpečnějšími na celém
světě. Aby člověk mohl si učiniti o takové lodi malé
ponětí, uvedu, že je ku příkladu expressní parolod'
„Fin-st Bismarck“ 502 stopy dlouhá, 571!2 stopy ši-
roká a 38 stop hluboká.. Stroje pracují silou 14.000
koní. Do lodi se vejde 10.700 krychlových metrů.
Je to na. celé evropské pevnině největší loď. Stroje
váží 20.000 centů. Loď má tři komíny, každý 12 stop
široký; má 9 kotlů, každý 17' 3" dlouhý a_15' 14"
široký. Uhlí vezme loď na jednoduchou cestu tam
270 vagonů po 200 centech:540.000 centů, což se
rovná. denní spotřebě 6.750 centů. Mimo 2 hlavní
hnací stroje má loď ještě 40 menších samostatných
stroji na pumpování, skládání a nakládání, na elek-
trické osvětlení atd. Elektrických světel má loď 900.
Sroub má. 3 křídla, každé v průměru 18 stop, jeho
celý objem dělá. 509 stop, a toto mocné těleso otáčí
se za minutu “all-krát. Rychlost je 21 námořních mil
za hodinu aneb 38'95 kilom. čili 575 zeměpisné míle.

To je r chlost osobního vlaku. Na palubě to proto.
notně čičí. Tím,. že expressní parolodě mají dva
od s ebeoddělené stroje a dva šrouby, jsou
chráněny před nepředvídanými nehodami na moři,
působenými zlomem vesla. neb hřídele. V případě, že
by jeden stroj se porouchal, jest druhý stroj s to,
parolod' pohybovati ještě rychlostí obyčejných par-
níků. Každá. rychlolod' má. 11 nepromokavých
oddělení (šaty) beze dveří, podlaha lodě jest dvo-
jitá, tak že potopení se lodě jest úplně nemožné,
kdyžby nárazem na skálu neb kolisí s jinou lodi byl
spůsoben otvor. Mužstva. je na lodi kulaté 250,
a. sice 1 kapitán, 6 důstojníků, 1 správce, 1 lékař,
20 námořníků (matrosů), 155 strojníků (skládajících
se z 1 vrchního inženýra, 1 druhého inženýra, 5 tře—
tích inženýrů, 2 čtvrtých inženýrů, 14 assisteutů, 9
vrchních topičů, 54 topičů a 60 dělníků pro přivá—
ženi ke kotlům uhlí atd.), 6 kuchařů, 6 kuchyňských
pomocníků, 2 pekaři, 1 řezník, 1 cukrář, 1 holič
a 70 stewardů (posluhovačů čili sklepníků). První
kajuta má 107 pokojíků po 2 pevných postelích
a. v pádu potřeby se mohou v každém pokojíkuještě
2 postele přidělat; tož je 428 postelí. Druhá kajuta
má'GO pokojíků s 206 postelemi. Mezipalubí čítá
v pěti'odděleních 765 postelí (každé takové oddě—
lení má. opět “malá. oddělení po 8, 12 až 16 poste-
lích). Je-Ií lod' úplně obsazena, přechovává v sobě
1650 lidí. To už je celé městečko. —— Poštovní a oby—
čejné parolodě jsou ovšem menší, ale zařízení na
nich není horší. Jedou pomaleji. Poštovní má. rych-
lost 14—15 namorních mil za hodinu ». obyčejná.
11-_—13. Nejmenší rychlost parolodě dělá. pořád ještě "
asi 3 zeměpisné míle za hodinu, což není tak málo.
U mne má. cestující dostatečný výběr, a lodě mnou
zastupované nejsou předčeny až dosud na žádné
straně ani zařízením, ani rychlostí, ani lácí; krátce:
lepší dopravy přes moře není nikde. Německé paro-
lodě jsou to jenom, které já. zastupuji, a sice přímo
jedoucí, tedy bez přesedání. Kdo se přihlásí, at ur-
čitě řekne, na. jakou loď si přeje sedat, zdali na ex—
pressní, či poštovní anebo obyčejnou. Každá z nich
má. jinou cenu.

Rychlost lodi. »

Ředitel zahraničné pošty Spojených Států (Super-
intendent of Foreign Mails) uveřejňuje zprávu, sáha-
jící od 30. června. 1889 do 30. června 1890, dle níž
byla poštovní doprava mezi EvropouaAmerikou nej—
rychlejší po expressních parolodích Hambursko-ame—
rické akciové paroplavební společnosti. Druhé místo
zaujímá anglická. paroplavba „Inman Line“, třetí
místo angl. paroplavba „White Star Line“, čtvrté
místo „Severoněm Lloyd“ v Bremách, páté místo
angl. paroplavba „Cunard Line“, šesté místo francouz.
paroplavba „Compag Générale Transatlantique“ atd.

Mezupaluhl.

Loď jest rozdělena na patra. Dolní patro, 25 stop
vysoké, sáhá až ke hladině mořskéa určeno jest pro
náklad. Druhé patro, 8 až 9 stop vysoké, jest tak
zvané mezipalubí a leží nad patrem pro náklad apod
patrem, kde umístěny jsou kajuty. Do kajuty vedou
s hora. schody jedny a. do mezipalubí schody dvoje.
Na lodích žádných kajut nemajících bývá mezipalubí,
alespoň jeho větší část, v horním poschodí. Na lodi
se každý prostor musí využitkovat a jsou proto po-
stele, jak v kajutě, tak v mezipalubí vždy dvě nad
sebou, každá, dle zákona nejméně 1.83 metru dlouhá
a 0.50 metru široká.. Taková, postel slouží buď pro
jednu osobu přes 12 let starou aneb pro dvě děti
mezi 1. a 12. rokem. Vmezipalubí obdržíte na postel
nový Slamník a novou slaměnou podhlavnici, ale o
přikrývku musíte se postarati sám. Za. přikrývku
slouží obyčejně přivezené s sebou peřiny, neb kou-
pená deka, neb kožich a. svrchník. Jídlo se donáší
ve velkých nádobách pro skupiny 10 až 16 osob.
Proto radno, aby se cestující české národnosti na
lodi drželi pohromadě. Náčiní k jídlu, jako misku,
lžíci, nůž, vidličku, láhev na vodu k pití, která jenom
dvakrát denně se rozdává atd., dostane každý ce-
stující na lodi zdarma. Mezipalubí je vzimě topené. Mí—
sta jsou nejlepšív středu lodě, kde se kolíbání nejméně
pociťuje. Nechť proto našincové při vstupování na loď
hledí býti předními. Kdo dřív přijde, ten dřív mele.

* Kouření je dovoleno toliko nahoře na palubě. Pivo je

k' dostání za 50 trojniků velká. aza 30 trojníků malá.
láhev. Dvakrát denně, ráno a odpoledne, prohlíží
lékař s kapitánem neb prvním důstojníkem veškeré
místnosti v mezipalubí. Kdo by se cítil nemocným
neb kdo by měl nějakou stížnost či nějaké přání,
může se pri této příležitosti hlásit. Lékařská služba
jakož i podané na lodi léky jsou zdarma. Nižší na—
mořní mužstvo bývá, jak to jich povolání samo sebou
už nese, poněkud drsné a cestující budiž proto co
možná shovívavým aústupným; leč snrovostinetřeba
si dátlíbit a kapitán ihned zakročí, když se mu to
oznámí. Kapitán a první důstojník jsou na lodi vrch-
ními pany; na ně třeba se obrátit, když se o něco
mimořádného jedna. Oba. bývají pravidlem _vlídnými
lidumili, když se jim věc neb prosba zdvořile před-
nese. Jen nezalezat do kouta! Býti mírným, ale skálo-
pevně stati na svém pravu, vede k cíli.
Strava na lodi v mezipalubl.

Strava k nasycení dostatečná podává se tukr" út
denně a sice: 1. ráno: Káva aneb čaj a pšeničný
neb žitný chleb s máslem; 2. v poledne: patřičně
připravený teplý oběd, pozůstávající střídavě z těchto
jídel: čerstvé neb nasolené, neb naložené i jiné ho—
vězí maso, nasolená neb uzená vepřovina, usušené
ryby neb slanečky, vždy s patřící k tomu zeleninou,
hráohem, čočkou, bobem, kroupami, rýží aneb něčím
z mouky; 3. večer: káva neb čaj, pšeničný neb žitný
chleb s máslem. K upravení naduvedených pokrmů
jest společnost povinna vzíti na loď pro každouosobu
na každých 10 dní: 1. soleného hovězího masa, čer—
stvého neb v krabicích naloženého hovězího masa
aneb jiných buď čerstvých neb naložených druhů
masa dohromady 1450 gramů, 2. solené vepřoviny
700 gr., 3. slanečků 3 kusy, 4. pšeničného chleba
3600 gr., 5. másla 300 gr., 6. čerstvých bramborů
4000 gr., 7. pšeničné mouky 720 gr., 8. hrachu 500 gr.,
9. bobů 300 gr., 10. krup 180 gr., 11. rýže 360 gr.,
12. švestek 180 gr., 13. kyselého zelí 270 gr., 14. sy-
rupu a cukru, každého 100 gr., 15. kávy 100 gr.,
16. čaje 20 gr., 17. octa 0,17 litru, 18. solí 120 gr.
Vody patří každé dospělé osobě denně 2 litry pře-

kapované a 41itry nepřekapované. Ku zlepšení stravy
může se npotřehiti mimo bramborů též kapusty, řípy,
mrkve a jiných zelenin, za to však mohou přiměřeně
odpadnouti luskoviny. Pro nemocné a děti nalezá se
na lodi mimo naduvedenou zásobu ještě zvláštní zá.-
soba červeného „vína, cukru, saga, ovesní mouky,
ln'upice, čerstvého—neb kondensovaného mléka. Tato
strava.—je- zakonem předepsána pro všechny „lodě.
Paroplavební společnost, “kterou ja zastupuji, vydala
loňského roku na potraviny pro jednoho mezipalub-
nika bez ohledu na stáří za každý den '1 marku
177z trojníku průměrně, což jest asi 67 krejcarů.
Za tuto částku dalo by se při 600 lidí průměrně na
každé lodi přec něco kloudného ustrojit. Ale není
tomu tak. Zrovna. na stravu v mezipalubí bývají nej-
trpčí stížnosti. A kdo je toho vinen? Lodní správa
ne, nébrž různé okolnosti, které se nedají odstranit.
Na lodi jest místnost vždy obmezena, i kuchyně je
proto malá,. Pro 500_1000 lidí může se vařit jenom
ve velkých kotlích. Vaření děje se nejvíce jenom
parou. Pára sama dá. už jídlu jiné vzezření a jinou
příchuť. Mezi cestujícími 'sou všechny národnosti
evropské zastoupeny a každá národnostje jiné stravě
zvykla. Pro každou národnost zvlášť vařit se však
nedá. Jednomu strava chutná a druhému se proti-
vuje. K tomu přidruží se ještě následkem mořské
nemoce přílišná podrážděnost žaludku a stesky jsou
pak všeobecné. Tato vada panuje na všech lodích
celého světa. Málo kdo byl až dosud »; mezipalubní
stravou spokojen, vyjma snad severní Němce, kteří
této stravě jsou uvyklí. Strava sama v sobě je dobrá.
Ja konaje před dvěma roky cestu do Ameriky okusil
jsem tuto stravu pravidelně každý den a mohu říci,
že mi chutnala. Ovšem jsem užseveroněmecké ku—
chyni, kde se obyčejně všechno vaří— vjednom hrnci,
také uvyknul. Zde je jídlo, ku př.“ naložené maso,
knedlíky a hrušky neb suché švestky, neb maso,
brambory a zelí,_ všechno vařené v jednom hrnci,
pochoutkou a v Čechách komu by napadlo tak vařit.
To jsou rozdíly, s kterými se musí počítat a na které
upozorniti mám za svou povinnost.

Mořská nemoc.

Kolíbáním lodě na vlnách mořských dostane'ce
stojící, jenžhoupání dobře nesnese, poznenahlado
hlavy závrať, která má. v následek dávení. Tot moř—
"ská nemoc. Nebezpečnou není; neb jakmile člověk
houpání uvykne, čili jakmile utišením moře loď ko—
líbatí se přestane, čili jakmile člověk vystoupí z lodě
zase na pevnou půdu, je ihned zase zdráv jako ryba.
Na co ale upozorniti dlužno, je, že onemocněním,
byt sebe nepatrnějším, obyčejně zprotiví se lodní
strava, poněvadž každý se domnívá., že přišlo mu
špatně (po nenavyklé stravě a nikoliv následkem hou-
pání 10 e. Cestujícímu znechutí se lodní strava tak,
že ji nemůže ani vidět. V tomto případě bývá dobře,
má.-li cestující něco potravy u sebe, na kterou je na-
vyklý a o níž ví, že mu po ní nebylo nikdy špatně.
Pak konají dobrou službu domácí potraviny, jako
bochník dobře vypečeného chleba, uzené maso, ně-
jaká, jitrnice, suchary, čerstvé ovoce, suché švestky,
housky a rohlíky, česnek, sůl a pepř na. oukrop,
zapražka na polévku atd. Vařící voda v kuchyni se
dostane, když se o to požádá. a když je ktomu čas.
Z potravin, at se bere s sebou na přilepšenou jen co
se udrží, co neplesniví.

Přiraženi lodě k břehu.

Jakmile loď přirazí do Hoboken, stát New—Jersey,
jenž leží naproti a ew-Yorku přes řeku, a v přístavišti
se upevní, sejdou napřed kajutníci a pak mezipalub-
nící. Současně vysa i se na přístaviště veškerá za-
vazadla. Postavte se ke svým zavazadlům a otevřete
je. Přijdou pak celní úřadníei a prohlíží, zdali mate
něco k vyclení. Po prohlídce napíše celník křídou na
víko, že věc odbyta. Pak můžete opět zavřít. Zůstaňte
ale tak dlouho 11 zavazadel státi, až přijde člověk
a připevní na kufr či bednu plišek s řemínkem. Každý
řemínek má dva stejné plíšky. Jeden se na bedně
upevní a druhý da se Vámýdo ruky. Je to Vaš re—
cepis na bednu či kufr, na který se v statnim ústavě,
kam Vas zavezou, Vaše věci opětvydají. Pak smíte
odejít. U přístaviště stojí mala loď, na kterou se

prohlídnuté bedny a kufry nakládají. Na tuto loď
musíte ívy sednout. Ona zaveze všechny mezipalub-
níky do státm'ho ústavu, kde každého zapisují. Do-
kud v tomtoústavu meškáte, jste chráněn, jelikož
tam žádná. holotapřístupu nemá, jenom hoStínští
po _vykonané'praci se tam pu'stí.,Kdo nechce do „ho-
stince aneb nemusí, at nechodí. Js'ou na. evropské
poměry příliš drahé. Pro katolíky“ od'poručuje se „Léo
House“, jehož oznamka' podána na jiném místě. Jak-
mile vyjdete z ústavu ven, buďte óp'atřným, neb se
to tam číhající lůzou jen jen hemží.

Ustav pro ochranu přistěhovalectva

v Novém Yorku.

Kdo by neznal Castle Garden (čti kesl gardu),
jehož branami prošlo na milliony evropských vystě-
hovalců? Byl to ústav vydržovaný státem novoyor-
ským pro ochranu oněch poutníků, kteří v.Novém
světě hledali novou vlast. S tímto ústavem spojena
byla nemocnice pro přistěhóvalce a útulna na ostrově
Wards Island, jakož i kancelář gro obstarávání
služeb, práce a zaměstnání vůbec. Casem zahostily
se v něm různé nešvary, jež měly v následek,— že
lonskeho roku ochranu přistěhovalectva převzala
ústřední vláda, která. dala. stavěti nové budovy na
ostrůvku Ellis Island (čti Ellis ajlend) v přístavu
novoyorském, kde přistěhovalci budou vysazovati se
na pevnou půdu, až dohotoveny budou, což; jak se
proslýchá, stane se již letošního jara. Zatím skládají
se přistěhovalci v Novém Yorku v budově zvané
„Berge Office“ (čti bárč offis), kde se přistěhovalec
zapíše.v Při zápisu kladou „se přistěhovalcům násle-
dující otázky: Jak se jmenujete? Jak jste stár (á)?
Jste ženatý či svobodný? (Jste vdaná. či svobodná. ?)
Při vdaných s dětmi bez manžela: Kde jest Váš manžel?
Udejte jméno a stáří každého dítěte? Kam a ke komu
jedete? Máte železničnílístek aneb dostatek peněz,
abyste se dostal (a) až na místo? Kdo zaplatil Vám
cestu do Ameriky? Byl jste doma v nějakém chudo-
binci neb ve vězení? Bylgjste někdy odsouzen pro

zločin? Trpíte aneb trpěljste někdy na. nemoc, která
by Vám překážeti mohla uživiti sebe a svou rodinu?
Jaké jest vaše zaměstnání? Při zodpovídání těchto
otázek, necht. každý ví, že se zde jedná o vypátrání
osob, jimž přístup do Spojených Států je zakázán.
V státním ústavě, ať se ještě jmenuje „Berge Office“
aneb již_„Ellis Island“, mají také zástupcové všech
dráh své stolky, aby přistěhovalce, kteří mají želez-
niční lístky koupeny už v Evropě, vypravili dále.
Evropská. poukázka se vymění za lístek ten pravý.
Který úředník ti doručí lístek, tomu vydej také v Ho-
bokenu obdržený plíšek na. tvá zavazadla, jež 0-
dobně plíškem, jako u velké lodě říditi se husou
až na místo, kam tvůj lístek zní. Pak musíš nový
obdržetii plíšek, neb dva, tři, kolik právě kousků za-
vazadel máš. Kdo jede dál a neodevzdá plíšek na
zavazadla. u velké lodě, s kterou do Ameriky přijel,
obdržený, tomu jednoduše zavazadla, zůstanou v stát—
ním ústavě ležeti.

Kdo se chce v Novém Yorku několik dní zdržeti,
může tak učiniti, pak ale ať svou poukázku na že-
lezniční lístky nevyměňuje, až v ten čas, kdy hodlá
jeti. Kdo bič) však železniční lístek, to jest poukázku
na. něj, v vropě— koupený neměl, a chtěl jeti dále
promluv napřed se zastupcem mé íilialky a kup
si lístek u něho. Jakmile v státním ústavě želez-
niční zastupcové seznají, že nemáš železniční lístek,
budou se o tebe prát. Ale nedej se! Vystěhovalecké
lístky jsou asi o třetinu lacinější. Dle zákona pova-
žuje se člověk za vystěhovalce jen, dokud je ještě
v Evropě; v Americe je už přistěhovalcem a musí
platit cenu obyčejnou. Proto uspoří peníze vystěho—
valec, jenž nucen jest teprv vAmerice koupiti želez-
niční lístky, koupí-li je v mé íiliálce, která všechny
spády zná. Přímo u železnic vystěhovalecké lístky
za sníženou cenu obdržeti nelze. Moudrému napo-
věz — —-

Komu je ve Spolených Statech pristup za—
kázán ?

1. Pauperům. Pauper je latinské slovo a zna-
mená'po česku chudý a po anglicka poor (čti púr).

Angličan ponechal ve své řeči slovo „pauper“ (čti
popr), jež má ale význam jiný; toto slovo naznačuje
chuďasa. takového, jehož musí obec živit, poněvadž
bud neumí, neb nechce neb nemůže pracovat. Spo-
jené Státy severoamerické neuzavírají se proti chu-
dým přistěhovalcům, jak z hořejšího slova „pauper“
mylně se vyvozuje, nébrž proti žebráotvu, proti po-
valovačům; tulákům a světákům, které mu tam dříve
obce evropských států na vlastní útraty posýlaly,
aby se jich zbavily. Chudým smí vystěhovalec býti,
jen když je pracovitým, aby dovedl si vydělat svého
chleba a nestal se záhy břemenem obecným. Každá
skoro evropská obec má osoby žebrotě neb úplné
ničemnosti oddané, o které se musí postarat-, takové
sešlé osoby nazývá Amerikán „paupery“, a nepři—
jímá jich, když se mu tam pošlou. Zdravý pracovitý
člověk, byť by byl sebe chudším, je posud stále ještě
vítaným.

2. Mrzákům. Sem patří slepec, hluchoněmý,
na rukou neb nohou tak poškozený, že mu to v práci
vadí, blbec, potřeštěnec neb Šílenec atd.

3. Churavcům. Sem patří lidé churaví, o jichž
uzdravení se pochybuje, jakož i ti, kdož nějakou
nakažlivou nemocí stíženi jsou. Kdo tam přijde ne-
mocným, rozstonav se teprv po cestě, ten se přijme
a dá se do nemocnice, odkud vyjde jakmile se
uzdraví.

4. Dělníkům v Evropě najatým. Spojené
Státy se svou vyvinutou svobodou chtějí, aby k nim
přicházeli jenom svobodní občané, a žádní otroci.
Dělníci a řemeslníci, najati od Amerikanů v cizo—
zemsku, nejsou připuštěni, když v dotyčném oboru
domácích dělníků lze dostat. Jenom tenkrát dělník
v cizozemsku najatý se připustí, jedná-li se o zave-
dení nového průmyslu a nejsou-li pro tento nový
průmysl domácí dělníci k dostání. Každá. jednotlivá
osoba aneb společnost, která by dělníky najímala
v cizozemsku a do Spojených Států dovážela proti
zákonu, smí býti zažalována na zaplacení pokuty
1000 dollarů za každý jednotlivý případ.

5. Zločincům. Kdo v Evropě spáchal sprostý
zločin, at si pak; už trest odseděl čili nic, nemá býti
připuštěn.

6. Těhotným svob odným ž en š tin ám,
které samy, tedy bez rodiny, do Ameriky 'se ubírají,
aby: doma vyhnuly se hanbě, je rovněž přístup za-
mezen, jelikož ženská. v jiném stavu je práeetn'e—
schfápna a při porodu některé obci za obtíž obyčejně
u a ne.

1) 7. Starci neb stařeně přes 60 roků, při-
jedou-Ii do Ameriky samotny, beze svých rodin, aneb
nemají-li v Americe příbuzných, již by se zavázali,
starati se o ně a ošetřovali je až do smrti.

8. Dětem pod 13 roků starým, přijedou-li
do Ameriky bez průvodu osob dospělých.

Hořejší pravidla.(vyjma 4. a 5.) vztahují se vždy
jen na osoby, osamoceně cestující. Stěhuje-li se ro—
dina aneb jeden neb více práce schopných členů
rodiny, k níž by některá z naznačených osob zá-
vadnýeh patřila, projde bez námitek, poněvadž se
předpokládá, že rodina či rodinný člen postará se
o takovou osobu, aby veřejnosti nebyla obtíží.

Každá přistěhovalá rodina může ohledně nad—
uvedených věcí brána býti pod přísahu.

' Vláda Spojených Států ukládá zodpovědnost lo—
děm vystěhovalce přivážejíČímza dopravu závadných
osob. Vůdce lodi,. jenž by do Ameriky takových osob
přivezl, smí b'ti trestán vězením až do 6 měsíců a
pokutou až do 1000 dollarů, a loď musí všechny
osoby, jímž přistání zabráněno, vzíti a dopraviti
zpět. Proto se už řešené osoby ani na loď k dopravě
do Ameriky neberou.

Zelezmčni doprava v Americe.

Z Nového Yorku jde na západ více dráh. Tak
vede ku př. do Chicaga šest hlavních tratí. Vystě—
hovalei dopravují se zvláštními vlaky, které obyčejně
bývají přeplněný, a musíce se vyhýbati “všem jiným
vlakům, dlouhými zastávkami velmi pomalu jedou.
Zeleznični Společnosti jsou usjednoceny tak, že jedna
po druhé vypravuje vlak, jak je toho otřebí. Vy—
stěhovalec proto předem neví, po které dráze pojede,
neb on jede po dráze, která právě je na řadě; —
Jede-li Se na západ, může se z Nového Yorku jeti
také oklikou přes Norfolk ve statě Virginia; lístky

jsou o 1, 2 až 3 dollary lacinější. Při tomto směru
jede se z Nového Yorku po parníku do Norfolku
asi 24 hodiny a odtamtud pak po dráze. Tento směr,
ač mjíždkou, má výhody tyto: Na lodi je. strava
zdarma a čeká-li se v Novém Yorku, platí- se cestu-
jíČímnáhrada za den po dollaru. Příliš mnoho vy-
stěhovalců tímto směrem nejezdí a zvláštní vystěho-
valecké vlaky se proto nevypravují. Vystěhovalci
jedou pak vlaky. expressními, mají daleko lepší po-
hodlí a přijedou na místo právě tak brzo, jako by
jeli přímo vlakem vystěhovaleckým. —'Mnoho stanic
v statě Michiganu, Wisconsinu, Minnesotě a v Se-
verní Dakotě mají také spojení s Novým Yorkem
pomocí parolodi po jezerách, ovšem jenom v létě,
když jezera nejsou zamrzlá. Lístky tímto směrem
jsou o 4 až 10 dollarů lacinější. Leč jízda je zdlou-
havá, s častým přesedáním spojená. a pro drahotu
potravin a jídel na jezerních parnících, na nichž
stravování zdarma není, poměrně se značnými útra-
tami spojená. — Z Baltimore na západ je železnice
asi o 1% dollaru lacinější nežli z Nového Yorku,
za. to ale jízda po moři do Baltimore mnohem déle
trvá nežli do Nového Yorku a uvaží-li se, že se do
Baltimore platí o 6 zlatých více, nežli po obyčejné
parolodi do Nového Yorku, tož žádný peněžní pro-
spěch nekyne z jízdy přes Baltimore k vůli uSpOření
při železničních lístkách, nebot cestující ušetří na
americké železnici ovšem průměrně 3 zl. 60 kr., ale
za lodní dopravu (vezme-li se stejný čas s poštovní
parolodi do Baltimore a s obyčejnou do Nového
Yorku) platí zase o 6 zl. více. — Co se jízdy z No-
vého Yorku do Texasu týče, jsou dva směry: přes
Norfolk 8. přes Nový Orleans. První je dražší, ale
lepší a. rychlejší. Parník jede do Norfolku čtyřikrát
za týden: v úterý, ve středu, ve čtvrtek a v sobotu
a z Norfolku jede se po dráze ještě 37, dne. Do
Nového Orleansu jede parník ale jenom jednou za
týden: každou sobotu. Jízda trvá 6 dní a z Nového
Orleansu po dráze P[, dne. Kdyby člověk přijel do
Nového Yorku v neděli, musil by skoro celý týden
na vlastní útraty čekat a neuspořil by proti raž-
šimu směru přes Norfolk nic. Mou zásadou nebylo
nikdy, cestující lichým křikem, úmyslným zatajová-

ním a dovedným matením lákat a jejich nevědomost
pak pro 'sebe- vykořisťovati, nýbrž mou- upřímnou
snahou stále bylo ajest, rodáky do Ameriky _se ubí-
raiící ve všem pončovatí, aby vlastním posuzováním
poměrů, .spíše'zachovali svobodnou vůli ve svých roz-
hodnutích“ 'pro tu neb onu věc. Vol si tudíž směr
káždý sám. —— Cestující, kteří z amerického přístavu
jeďoú'dáie na západ, mohou si u mně zaplatiti že-
lezniční'lístky skoro do všech stanic Spojených Států
a Kanady.. Ceny pro, vystěhovalce jsou asi o jednu
třetinu lacinější ». doprava děje se zvláštními vystě-
hovaleckým'i vlaky. Tím uspoří si cestující nové sta-
ro'sti'a zdržování v americkém přístavě, .i má zá-
roveň výhodu to, že i sesvy'mi zavazadly zdarma
na nádraží dopraven bude.“ Na amerických dráhách
platí _Se 'z'a děti od 5. do 12. roku polovička a děti
do 5 let jedou zdarma. Také v Americe netřeba
stáří'dít'ek dokládat křestními listy, an v tom kon—
troláí-Žjs'Oudí pouze dle oka. Vystěhovalec má dále
na amerických železnicích svobodného nákladu na
1' celý lístek 150 liber, což je na českou váhu asi
68 kilo a na poloviční lístek 751iber, čili asi 34 kilo.
Kdo může, at raději jede dále, kde panuje menší
žálidněnost a. kde proto zaměstnání, hlavně pro třídu
nádennickou, spíše je k dostání. U vody zůstanou
všicci, již nemají peněz, těch je mnoho, mnoho,
a o práci bývá proto nouze. Kdo by železniční lístek
u mne nekoupil a přece po železnici dále chtěl jeti,
udělá dobře, když v Novém Yorku dojde do mé
ňliálky, která. je s to, opatřiti mu lístek laciněji, než
kdyby si ho kupoval sám. Americký zákon dělá totiž
rozdí ve slově vystěhovalec a přistěhovalec. Vystě-
hovalcem je člověk, dokud je ještě na cestě do Ame-
riky, ale jakmile vAmerice vystoupí na pevnou půdu,
je přistěhovalcem. Železniční lístky za sníženou cenu
smí se vydati jenom vystěhovalcům a ne přistěho-
valcům. Já však mám s jednou dráhou smlouvu, že
mně vydá. lístky na západ za ceny emigrantní. Na
to buďtež rovněž novoyorští Čechqvé upozorněni,
kteří chcete odebrati se na. západ. Uspora je skoro
celá třetina.

Mějte se na pozoru po cestě.

Svým cestujíČímuvádím ve známost; že po“ celém
říěmecku nikoho nemám, jenž by mne'zastnpov'al.
Kdo proto za, mého zástupce po cestě se vydává„
činí tak neoprávněně. Na. nádraží číhají na; nezku—
šené vystěhovalce všelijací náhončí všelijakých iii—'em.
Mějte se před nimi na. pozoru, byť“ by i česky mlu-
vili. Nesvěřujte' jim žádné“ peníze, nechoďte nikam
s nimi a. nedávejte jim žádné adresy svých známých
a příbuzných, kterých chtějí jen zneužítí. Nenechte
se od nich vyptávat. Zde dějí se v skutku podivné
rejdy. Přijde krejánek, stojící ve službě cizorodého
agenta, na. nádraží, kde se přesedá, a přitulí se s lí-
čenon nevinností a, upřímnosti k vystupujíČímz vlaku
Čechům, vystěhovalcům, kteří jsou z daleka k pc-
znání. Lidičky, vy jedete do Ameriky? Ano. Na.
koho pak jedete? Na. N, N. To jedete velmi draho;
u mne jste to mohli mít o celou desítku lacinější.
(Cestující se zalekne a. je zle; zlomyslně nasazená
mu štěnice nedá více pokoje.) Kdo pak Vám to'řídil?
Pen N. N. v N. (Krajánek si adresu zapíše a. druhý
den již letí list se skvělou nabídkou do Čech neb
na Moravu, aby pan N. N. pro. jeho světoznámou
firmu pracoval e. vystěhovalce mu odkazoval; neod-
poví—Ii neb odpoví-lí záporně, chová. krajánek dosta—
tečnou dávku podlosti v sobě, aby pana N. N. udal
úřadům, že prý vybízí k vystěhování a. šikuje lidi
do Ameriky; pan N. N. má pak zbytečné potaho—
vání.) Kdo pak se od Vás chystá. ještě do Ameriky?
Ti a ti. (Krajánek dychtivě zanáší adreSy do tobolky
a. druhý den Ietí dopisy na ně, že prý mu ten a. ten,
který se osobně přesvědčil, že u krajánka. je všechno
lepší a lacinější, nežli u jiného, adresy udal, aby
mohli vyhnouti se nebezpečí, padnouti do rukou ji—
ných, jako ten a ten; náš venkovský lid, nic zlého
netuše, uvěří takovým fajnovým naraňčenostem a už
sedí na. vějičce.) Koho pak máte doma.? Rodiče,
bratry, sestry, švakry, strýčky atd; atd. Vystěho-
valee se zpovídá a krajánek píše a píše. Za. několik
dní obdrží řešení občané od krajánke listy, že prý
jejich syn, dcera, zeť, švakrová, synovec atd., ho
požádali, aby jim dal vědět, jak po krajansku se

jich ujmul :; yšeqhna dobrodiní jim prokázal a a ja.-
kou vděčností ubíral še dále, jenom litujíce žalostně,
že ne jeho, néhrž cizím prostřednictvím musí cesto-
vati atd. Rodiče atd. jsou potěšeni, že někdo v ci-
zině ge jejich lidí tak obětavě ujal a. dušují se na
hodnost dotyčného pána. po vsi a okolí, odporuču—
jícello každému. A tento hodný pán je darebák
od kontirlšdo jens největší vychytralosti a prolha-
ností dovede “k sobě lákat lid, ten zajisté ví, proč
tak dělá.. Proto pozor, pozor a. ještě jednou pozor
na takové ptáky. Ku postrašení žádá mnohdy tato
čelázdka od „cestujících “pasy a vyhrožuje policií. Ne-
bqitezse. V Německu se Vám nic nestane. Naopak,
zavolejte na obtěžující Vás dotíravce Vy strážníka
auvidítei jak hbitě dobrodineček vezme do zaječích.
Po [cestě „neberte žádných odporučujících lístků, ne—
dbejte žádné Vám bez požádání dávané rady, střežte
před neznámými spolucestujícími své kapsy a v Ham—
burku na. nádraží držte se hostinského, jehož lístek
Vám byl mnou zaslán. Příznivá pro šíbala. okolnost,
že vystěhovalec odjede brzo dále a. více se nevrátí,
aneb že je příliš ostýchuvým, nevědomým, lehkověr-
ným a řeči nemalým, poňouká právě nesvědomité
lidí,.kwxužitkováuí této vzácné kořisti až bůh brání.
Tři čtvrtě vystěhovalců jsou venkované, a zrovna na.
venkovany má prohnaný šíbal změřena. Buď proto,
kdo do Ameriky jedeš, po cestě opatrným.

Vystěhovatecký dům v Novém Yorku

„Leo House“, čís. 6. State Street,
katolický ústav blízko přístavu naproti státní úřa-
dovně „Barge Office“.

Duchovní správce: P. J. Reuland.

Byt za den a za. noc se stravou 1 dollar; nocleh
beze stravy ]]4 dollaru. „

Tento „dům možno Gechům, kteří by v Novém
Yorkumusjli přenocovati, odporuěiti pro jeho čistotu,
pořádnost a "dobré zaopatření. Jeptišky posluhují,
ale kouření, pivo a kořalky jsou vyloučeny.

Tento ústav je vydržovaný katolictvem a kdo
se v něm ubytuje, může býti ubezpečeným, že se mu
tam žádná krívda. nestane.

Smlouvy mezn Spolenýml Státy americkymi
a Rakousko.Uherskem.

Dle smlouvy ze dne 3. července 1856 vydají se
spravedlnosti všechny osoby, které, jsouce obviněny
ze zločinu vraždy, neb útoku vražedného, neb moř-
ského lupičství, nebo žhářství, neb loupeže, nebo
padělání listin, Aneb hotovení čili v oběh dávání fa-
lešných peněz, at ražených mincí neb papírových,
nebo zpronevěřeni peněz veřejných, spáchaného v ob—
vodu pravomocnosti jedné strany, vyhledávati budou
útulek aneb nalezeny budou v území strany druhé.
Zločiny neb přestupky povahy politické jsou výslovně
vyloučeny. Dle smlouvy ze dne 5. září 1870 považuji
se občané rakousko—uherského mocnářství, kteří ve
Spojených Státech nejméně 5 let nepřetržitě bydleli
a mezi tím časem naturalisovanými občany Spoj.
Států se stali, od vlády rakouské i uherské za občany
americké a také tak se s nimi jedná. Prohlášení
úmyslu, stati se občanem Spoj. Států, neplatí za na-
turalisaci. Naturalisovaný občan Spoj. Států, do—
pustil-li se před vystěhováním se v Rakousku a
Uhersku nějakého trestního skutku, může soudně
stíhán a potrestán býti, vrátí-li se do původní vlasti
své, vyjma nenastaloli mezitím promlčení (Verjáhrung)
nebo odpuštění zločinu neb přestupku. Zejmena bý—
valý občan rakousko-uherského mocnářství, jenž jako
občan americký vrátí se do původní vlasti své, pod-
léhá vyšetřování a trestu dle zákona rakouského neb
uherského pro nevyplnění povinnosti vojenské 1. vy—
stěhoval-li se, když byl při odvodu branců již co
rekrut ke službě ve stálé armádě povolán, 2. vy-
stěhoval-li se, když sloužil u praporu aneb měl do-
volenou na určitý čas, 3. když maje dovolenou na
čas neurčitý aneb počítán jsa k záloze nebo k zem-
ské obraně, vystěhoval se povolán byv do služby,
aneb když veřejné povolání jej k dostavení se za-
vázalo, aneb když byla vypukla válka. Naproti tomu
bývalý občan rakousko—uherského mocnářství natu-
rahsovaný ve Spojených Státech, jenž při vystěhování
se nebo po něm přestou il zákony o povinnosti vo-
jenské skutky neb zanedbáním jiného spůsobu'nežli
v techto třech odstavcích jest uvedeno, nemůže při

návratu do původní vlastisvé ani k pozdější službě
vojenské býti povolán ani pro nesplnění povinnosti
vojenské vyšetřován aneb trestán býti. Rakousko-
uherský vystěhovalec, jenž se stal občanem Spojených
Států, nesmí po návratu do původní vlasti své nucen
býti ku vstoupení do dřívějšího státního svazku, ale
také se mu beze všech průtahů má. původní občan-
ství udělit, když o to sám požádá a občanství ame-
rického se zřekne. Dle smlouvy ze dne 27. srpna
1829 vydají Spojené Státy uprchlíky z rakouských
lodívalečných a obchodních, když utečenec patřící
k lodnímu mužstvu, opustí loď v některém americkém
přístavě zakotvenou.
Americké penize, míry a váhy.

V Americe se počítá. na dollary: 1 dollar má
100 centů a platil dne 1. ledna 1891 asi 2 zl. 40 kr.
rakouské měny. Dollarové znaménko jest $. Ku př.
10 doll. 35 centů naznačuje Amerikán takto: $10.35.
—- 1 americká, míle má 1.760 yardů, 1 yard má 3 stopy
a 1 stopa má 12 palců. — 1 americká. míle rovná. se
1'61 kilometru. 100 yardů jest 91 metrů a 1.000 ame-
rických stop jest 305 metrů. — 1 americká čtvereční
míle jest 640 akrů a rovná, se.2'59 Dkilom. — 1 akt
má. 43.560D stop a rovná se 045 hektaru. -— 160 akrů
jest zblíženě asi 11272 rakouských jiter. —— 1 bušl
(obilní míra) má 4 pecky (vyslov peky). 1 pek má
8 quartů a 1 quart má 2 pinty. — 1 bušl rovná se
35'241 litrů.-— 1 libra americká má, 16 uncí a 1 unce
má 16 drachem. 1 americká. libra rovná se 045% kilo
čili 1.000 amerických liber jest 453W2 kilo. — 1 tuna
má 2.000 liber. — 1 gallon (míra tekutin) má 4 quarty
a 1 quart ma 2 pinty. 1 gallon rovná se 3'7854 litru
čili 1000 gallonů jest 3.785 litrů. —— 1 sud mouky
váží 196 liber:88'9 kilo. —- 1 balík bavlny váží
477 liber brutto a 440 liber netto.—— 1 sud petroleje
obsahuje 40 gallonů:151'4 litrů a 1 sud piva 31
gallonů : 117-3 litru. —— Staří lidé počítají ještězhusta
po staru: 100 yardů jest 117 vídeňských loktů; 100
rakouských mil jest 471 anglických mil; 100 měřic

šlest 135 bušlů; 100 akrů jest 707, jiter: 100 amer.
'ber jest Belgrak. liber. _Konečně ještě registered
tun (při nákladu hlavně na. lodích) :: 42 krychlových
(kostkových) step: 1'189 kub. m.
Krejcarová pásmová sazba na e. k. rakou-
ských státních dráhách.
o ' 086% "'a _'Osbba ,
Š: Kilometr Ršfšěol 152%“ Š KiÍomreťr Riga“ (331
“" er. č.1č.kolk. “* mne—, Yčrkblk.:
? III. “ 111. '  III. III.
1 1— 10; —.15 1—10 15I301—350 5.25 3.50
2' 11— 20; —.30 —.20 16 351—400 6.— 4.—
3 21— 30% __.45 _.30 17Í4o1—459- 6.75 4.50
4 31—— 40 —.607 —.40 18š451—500 7.50 5,—
1 5 41— 50 —.75 .50 19 501—550 8.25 » 5.50!
Q 6 51— 65 —.98 —.6'5 20L551—600 9'.—— 76;—
; 7 66— 80 1.20 —.80' 21601—650 9:75 6:50
0 :; 81—1oof 1.50 1.—- 22 651—700310";50-— 7.—
1) 91101—1'25 1.88 1.25 23 .701—750 11.25 7.50
1.10 3126—1569! 2.25 1.50 24 751—800 12.— 8.—
„111151—1751 2.63; 1.75 25 801—850 12.75 8.50
312 1175—2003 3.751 2.— 26851—900 13.50, 9.—
„wšem—250x 4.- x 2.50 270901—9501 14.25! 9.50
314 251—300; 415033 a..—* 23951-10011 is.—E 10.—
Lístek H. třídy stojí 0 polovic více a lístek
I. třídy stojí ještě jednou tolik. Svobodný náklad
není žádný. Za každých 10 kilogramů a za každý
kilometr platí se 1:5 krajana; tak by stál kufr 55 kilo
těžký do vzdálenosti 300 kilometrů 6X300:180'0XV5
: 3 zl. 50 kr.
Vdovec má jm pašování srdce. _— Dře vrbu koza, kozu vlk. vlka.
sedlák, Sedím židi, žida. pán, pána advokát a advokáta. čert. _-
Bůh lid ml, My seslal. — Bojácného snadno zasuašíš. ——
Dlouhý ]:er cia m krátkou. — Co ze srdce nejde, v srdci se
najme. -— Lidé i po mam přicházejí o rozum. — Co komu po-
tom, že kmotra sedan. Simonem. — Hadr onuci haní, obadva
sami. —- Naposmšwiůy také docházívá. — Moudré ucho nedbá
na. hloupé řečí. — Ciň ty, co máš ; lidí nech mluv-ití, co chtí; po—
kaj bude. -— Ipsu, vtip s:. unus chyť se. _— Otec, matka vyučili
mluva! a svě: mlčaaí. — Mčmím člověk mnoho řečí odbude. ——

Pásmová sazba na uherských státníci! dra-
hách, na uh. severovych. dráze a na koslck-o-
bohummske dráze.
W
Pásmo Kilometry "na.—rychlícícm sob. vlacích

:;II. tř. (Blíží—JIL tř. [III.tř.
I. ( 1— 25 m) . . 4., 0,50 0,30 0,40 0,25
II. ( 26— 40 „_ ). 51,00 0,60 0,80 0,50
III. ( 41— 55 „ ).'. 1,50 0,30, 1,20 0,75
IV. ( 56— 70 „ ).- . 5,00, 21,210. 1,60 1,00
V. ( 71- 85 „ ). . —2,50. 1,50 2,00 1,25
VI. ( 86—100 „ ). .,3,00 1,80 2,40 1,50
VII._(101_—115“„ ). .„ 3,50 2,10 2,80, 1,75
VIII. (116—130 „ ). „( 4,00 2,40 3,20 2,00
IX_. (131—145 „ ). . 14,50 2,70 3,60 2,25
X. _(146—160 „ ). . (5,007 3,00 4,00 2,50
XI. (161_175 „ ) . . 5,50 3,30 4,40 2,75
XII. (176—200 „ ). .!l 6,00 3,00 4,80 3,00
XIII. (201—225 „ ). .;, 0,50 4,20 5,30 3,50
XIV. (226 avíce „ ). . „ 7,00 4,80 5,80 4,00
Na. jízdní lístky žádný svobodný náklad. Za
jedenkus zavazadla. ne přes 50 kilo těžký platí se
až do 55 kilometrů 25 kr., do 100 kilometrů 50 kr.
a přes 100 kilometrůrl zl., za každý kus přes 50
kilo těžký dvojnásobná. a. přes '100 kilo těžký čtver-
násobná cena; tak by stál kufr vážící přes 100 kilo,
jenž by šel přes 100 kilometrů, 4x 1 zl. 24 zl.
České časopisectvo ve Spoj. Státech.
I. Denníky:
1. „Svornost“, 150 W. 12. “Street. Chicago, III.
2. „Chicagské Listy“, 352 W. 18th. Str. Chicago, 111.
3. „Newyorské Listy“.-1368 Avenue A, New York.
4. „Hlas Lidu“, 436 E. 72nd Str. New York.
5. „Clevelandské Listy“, 1158 Broadway, Cleve—
land, O.

II. Třikrát týdně vycházi:
1. „Volanstň 56 Humboldt Str., Cleveland, O.
III. Týdenniky:

1. „Slavie“, Racine, Wis.

2. „Slovan Americký“, Iowa City, Iowa.

3. „Pokrok Západu“, 1211 S. 13th. Str., Omaha, Neb.
4. „Hlas“, 1625 S. 11th. Str., St. Louis, Mo.

5. „Dennice Novověku“, 1198 Broadway, Cleveland, O.
6. „lýova Doba“, Schuyler, Neb.

7. „Čechoslovan“, 566 Centre Ave. Chicago, II].

8. „Svoboda“, La Grange, Fayette Co., Tex.

9. „Slovan', Bryan, Brazos Co., Tex.
10. „Amerikán“, 150 W. 121111. Str., Chicago, II].
11. ,Amerika“, 352 W. 18th. Str., Chicago, III.
12. „Domácnost-.“, 372 Milwaukee Str., Milwaukee,

W'is.

13. „Pravda.f, 670 Throop. Slr., Chicago, Ill.
14. „Duch Casu“, 150 W. mth. Str., Chicago, Ill.
15. „Svobodný Občan“, 1158 Broadway, Cleveland, O.
16. „Katolické Noviny“, 17th. Str. 85 Centre Ave., Chi-
cago, Bl.
IV. Měsíčník.
1. řádu)! Americký“, 126 W. Taylor Str., Chicago,

Hořejší adresy postačí. Na dopisy z Rakouska
třeba, ještě dodati North America.

Mzdy ve Spojených Státech.

[ Služka. děveěka, má. měsíčně 10 až 15 dollarů,

l se stravou, bytem a prádlem. Služky jsou vždy hle-

! dány, jelikož americké děvče na službu nejde a při-
stěhovalkyně také brzo zameričí a záhy služby se
vzdává..

Sičky, švadleny a ostatní dělnice v továrnách mají
od 4 dollarů nahoru dle schopnosti a dovednosti,
týdně beze stravy a beze všeho. Práce není vždy

1 k dostání a musí se hledat.

Nádennik má 1 až 172 dollaru denně beze stravy.
Práce se musí vyhledávat často dosti dlouho.

Pacholek, oráě u farmera (rolníka), má, 14 až'20
dollarů měsíčně s celým“ zaopatřením. Služba se
najde časně z jara., když se práce v polích otvírá,
vždy, ale často farmer najímá pouze až do zimy,
aneb za prosinec, leden a únor nic neplatí, nechaje
oráče u sebe toliko za byt a stravu.

' Remeslník, dělník, mívá denně II]2 až 3 dollary
beze stravy, dle síly a šikovnosti.

Nejspíše v Americe možno nováčkovi najíti práci
na jaře. Zima. je tam.tužší, nežli v Čechách a “ír—ace
stavební a- rolnická, zastavena; dělník stavitelský
a rolnický vyhledává práci pro zimu jinou trno—
váčkovi“ proto málo zbývá,. Nejpřednějším' pravidlem
jest, ehopiti se práce té, která nejdříve se naskytne.
K vybírání je později pořád ještě času dost.

Jak je draho ve Spojených Státech,?

Hotová. obuv a hotové šatstvo a prádlo, jež vše
se vyrábí továrnicky v tak ohromných množstvích,
že v Evropě ani zdáníov tom není, mají skoro už
tytéž nízké ceny, jako v Čechach; Jenom zakázková
práce, t. j. dát si něco ušít dle. míry, přijde vysoko.
Svobodný dělník jde obyčejně do strávního domu
(boarding house), kde má. byt a celou stravu od 3
do 4 dollarů týdně. Rodina si najme malinký byt,
jenž stojí průměrně 5 dollarů měsíčně. Byty jsou
bez kamen, jež si nájemm'k musí koupit sám. Dobrá
kamna železná pro vaření stojí 8 až. 12 dollarů.
Maso 6 až 12 centů libra, z jaké právě časti hovada.
Mouka, sud, 200 liber, 372—5 dollarů. Pivo v ho—
stinci 5 centů sklenice (asi 7, litru). Káva. 8 až 10
liber za 1 dollar. Cukr 15 až 116 liber za 1 dollar.
Sádlo 5 centů libra. Máslo 15 centů libra.— » Sýr
9 centů libra.. Vejce 20 centů tucet. Brambory, bušl,
50 centů. Zelí, hlávka, 5 centů.

Kde Čechové usazeni jsou.
Ve čtyrech ročnících „Ceskýeh Osad v Ameriee“ při-
nešeno bylo již 1550 míst v Americe, kde Čechové
usazeni jsou, s četnými adresami. Doplňujíce seznamy
tyto uvádíme zde opět 120 míst nových, kde našin-
eové bydlí a připojujeme po jedné adrese, na, kterouž
možno se obrátiti, kdoby sobě přál bližších zpráv.
K vůli snažšímu přehledu udáváme při okresích
sever (okres:County, čti khaunty,
zkráceně Co.) polohu ve státě
az 8 80 či v territoriu, jež dělíme na.
' ' ' 4 devět částí dle vedlejšího
“g __“_ Š obrazce. 0. znamená. tudíž po-
v .= , . ..
_g— 2- StT- "- _o lohu vychodni, gv.:_]1hový-
„ l——— :“ chodní, j.: jižní, jz.=jiho—
“ jz, ! j. ' je. západní, z. :západní, sz. :
severozápadní, s. : severní,
jih se.: severovýchodní a stř.:
střední. Ctu-li ku př. dole
první jmeno osady, vím ihned, že Hazen je místo
a pošta. v okresu Prairie, který leží v střední části
státu Arkansasského.
Arkansas:
Hazen, Prairie Co. (stř.) John Kocourek.
Judsonia, White Co. (stř.) F. C. Doubrava.
Marche, Pnlaski Co. (M.) F. Sokol.
California:
Lawrence, Santa Clara. Co. (z.) John Janata.
Colorado:
Atwood, Logan Co. (se.) J. G. Svidenéký.
Breekenridge, Summit Co. (stř.) A. Fendrich.
Cheyenne Wells Bent Go. (iv.) J. R. Wašiee.
Connecticut:
Hadlyme, New London Co. (iv.) Frank Fiala.
Waterbury,New Haven 00. (7'.) V. Mrazík 13 Phoenix
Ave.
Illinois:
Hettiek, Macoupin Co. (stř.) Jan Hajný.
Murphysborough, Jackson Co. ( j.) Fr. Hrabík.

Indian Territory:
Frisco, Oklahoma Co., Frank Selement.
Noble, Oklahoma 00., J. Bezdíček.
Iowa:
Beaman, Grundy Co. (stř.) Adolf Zejšek.
Diagonal, Ringgold Co. (i.) Jaroslav Záruba..
Ferguson, Marshall Co. (stř.) Fr. Dobrkovský.
Indiana.:
Knox, Starke Co. (sz.) Vaclav Fana.
Kansas: _
Agenda, Republic Co. (s.) Vaclav Stěrba.
Inka, Pratt Co. (j.) Fr. Dvořák.
Jennings, Decatur Co. (sz.) Mat. Kašpar.
Fancy Creek, Clay Co. (s.) Jos. Lippert.
Margaret, Lincoln Co. (stř.) V. Doležal.
Seward, Staiford Co. (j.) Karel Moravec.
Silver Lake, Shawnee Co. (v.) Ant. Chnran.
Stockton, Meade Co. (jz.) J. Hrabě.
Massachusetts:
Athol, Worcester Co. (stř.) Fr. Hrbek box 571.
Holyoke, Hampden Co. (z.) Karel Huleš.
Michigan:
Garden, Delta Co., (sz.) Václav Skarda.
Shetland, Leelanaw Co. (sz.) Jan Kalina.
Wilson, Menominee Co. (sz.) Fr. Týra.
Minnesota:
Brainerd, Crow Wing Co. (stř.) Fr. Sýkora.
CazenOVia, Pipestone Co. (í.) J. Syrový.
Chowen, Hennepin Co. (stř.) John Zahálka.
Heron Lake, Jackson 00. (já-) J. Bartoš.
Kettle River, Pine Co. (v.) St. Vavřík.
Lakefield, Jackson Co. (jz.) Václav Soukup.
Mulford,s Siding, Le Sueur Co.v(j.) Jan Vojta.
Myrtle, Freeborn Co. (j.) Jan Cervuý.
Pipestone, Pipestone Co. ( jz.) J. Zabokrtský.
Vesta, Redwood Co. ( jz.) Jos. Jedlička.
Vining, Otter Tail Co. (z.) Karel Stibal.
Missouri:
Wellsville, Montgomery Co. (v.) Mat. Falta.

Montana:

Blakeley, Yellowstone Co. ( jv.) Fr. Dvořák.
Livingston, Park Co. (j.), Wm. Hrůza.

Nebraska:
Adaton, Sheridan (sz.), Frankl Cílek.
Aliance, Box Butte Co. (sz.) Jandera dz Krajíček.
Beatrice, Gage Co. (ja)—J; V. Roubal.
Beemer, Cuming Co. (sv.)“Filip Zelenka.
Belvidere, Thayer Co. (j.) Jan Sršeň.
Benedict, York 00. (stř.) Jan Němec.
Bladen, Webster Co. ( j.) Jan Petráš.
Boheet, Platte Co. (stř.) James Krásnický.
Colon, Saunders Co. (v.) Jos. Králík.
Elmer, Hayes Co. (jz.) Jakub Lang.
Esther, Dawes Co. (sz.) Jan Makovský.
Florence, Douglas Co. (v.) J. F. Drábek.
Gilead, Thayer. Go. ( j.) J. F. Komrs.
Gretna, Sarpy Co. (v.) Jan Košarek.
Kramer, Lancaster Co. (v.) Jos. Leták.
Majors, Buffalo Co. (stř.) Jos. Dušek.
Marsland, Dawes Co. (sz,)e'Ant. Havlíček,
Menominee, Cedar Co. (se.) Fr. Zavadil
Lanham, Gage Co. ( jv.) W. Ftípil.
Wimberg, Butler Co. (v.) Jan Marušák.
Sprague, Lancaster Co. (v.) Jos. Vlasák.
Springfield, Sarpy Co. (v.) M. Stava.
Strickland, Hayes Co. ( jz.) Jan Teplý.
Tecumseh, Johnson Co. (jv.) Kazda Bros.

New Jersey:
Bayonne, Hudson Co. (se.) Ed. Jušták 27. William Str.
Boundbrook, Somerset Co. (stř.) Josef Fr. Soukup.

New York: v
East Venice, Cayuga Co. (ati—.) Fr.Skudera—.
Middle Village, Queens Co. ( jv.) Fr. Drahorad.

N. Dakota:
Cayuga, Sargent Co. Frank Dushbabek.
Forest River, Walsh Co. Miss Katie Urban.
Genesee, Sargent Co. (v:) Fr. Ryba.
Mandan, Morton Co._Jan Kahovec.
Mapleton, Cass Co. Stěpán Cernohous.
Voss, Walsh 00. J. F. Bárta

Ohio:
Ironton, Lawrence Co. (j.) J. Fišer.
Jewell, Deňance Co. (sz.) Jiří Steífel.
Nottingham, Cuyahoga Co. (sv.) 0. H. Dvořák.
Oregon:
East Portland. Multnomaln Co., (sz.) J. G. Krejčík.
Kent, Wasco Co. (s.) Jan Skarek.
New Era, 'Clackamas Co. (sz.) Fr. Spulák.
Pennsylvasia:
Beaver Meadows, Carbon Co. (v.) Jakub Hora.
Blackburn, Westmoreland Co. (jz.) F—r. Vitouš.
Brownfleld, Fayette Co. (jz.) Tom. Holub.
Forty Fort, Luzerne Co; (v.) Iran—Brůha;
Grapevílle, Westmoreland Co. ( jz.) Václav Havel.
Tarentum, Allegheny Co. ( jz.) Josef Velek.
S. Dakota: v
Bridgewater, Mc. Cook Co. Martin Zák.
Buňalo Center. Buffalo Co. Fr. Procházka..
Crow Creek, Buffalo 00. Václav Havlík.
Kolda, Hand Co. F. J. Kolda, poštm.
Sioux Falls, Minnehaha Co. Jan Ceška.
Tripp, Hutschinson 00. J. Kučera.
Vienna, Clark Co. M. Knadle.
Texas:
Del Rio, Val Verde Co. (ji) Jos.„Janko.
Monaville, Waller Co. ( jv.) Jan Repka.
Ray Ville, Parker Co. (s.) Ant. Bábek.
Smithville, Bastrop Co. (stř.) Jan Hříbek.
Springdale, Cass Co. (sv.) Leopold Kubica.
Yorktown, De Witt Co. (j.) Josef Boháč.
Virginia: „
Church Road, Dínwiddie Co. ( j.) B. Sedivý.
Newvílle, Prince George Co. (jz.) Alois Kadlec.
Washington:
Colfax, Whitman Co. (v.) Fr. Smolík.
Farmington, Whitman Co. (U.),„JOS. Hájek.
Seatle, King Co. (z.) Rev. F. J. Šimoník.
Big Wausaukee, Marinette Co. (sv.) Jiří Kustka.
Wisconsin:
Blenker, Wood Co. (stř.) Jos. Beránek.
Dancy, Marathon Co. (s.) Frank Procházka.

Dodgeville, Jowa Co. (j.) Jakub_Starý.

Florence, Florence Co. (se.) Fr. Sperl.

Lancaster, Grant Co. ( jz.) Jakub Vaněček.

Marshfueld, Wood Co. (stř.) Bart. Truhlář.

Merril, Lincoln Co. (s.) Josef Sládek.

National Home, Milwaukee Co. (v.) Jan Teplý.

Neillsville, Clark Co. (sz.) Jan Kubát.

Saint Francis, Milwaukee Co. (jv.) Jan Pavlů.

Seneca, Crawford Co. ( jz.) Tom ;Stluka.

Silver Lake, Kenosha' Co. (jv.) F1). Navrátil.

Stangelville, Kewaunee Co. (o.) Jos. Sladký.

West Salem, La Grosse Co. (z.) Tony Neuman.

Whiteňsh Bay, Door Co. (se.), W. 6% C. Mashek.
Národnl jednota sokolská ve Spolenýeh

Státech.

Výkonný výbor je v Chicagu, Ill.: Ant. Kolman,
starosta, Tomáš Gresltnaměstek, Karel Jiran, ta-
jemník, F. Kaplan, pokladník; adresa na jednatele:
P. V. Sisler, 334, W. 11tll Str., Chicago, Ill. Národní
jednota sokolská skládá se z následujících tělocvič-
ných jednot: __

Chicago, 111. Cesko-Americký Sokol. Adresa: F. Ka-
špar, 537 Blue Island Ave.

Chicago, 111. Plzeňský Sokol. Adresa: Otto Chvalov—
ský, 601 Centre Ave.

Chicago, Ill. T. ]. Sokol. Adresa: J. E. Zárobský,
204 W. mh St._

Chicago, Ill. T. ]. Geohie. Adresa: Petr Chnraň, 47
Emma St.

Chicago, 111. T. ]. 8. Praha. (Town of Lake.) Adresa:
T. Vonášek, 4736 Wood St.

St. Louis, Mo. T. 1. S. Sokol. Adresa: Jan Tříska,
1867 So. 111.11 Str.

Milwaukee, Wis. Věrni Bratři Sokol. Adresa: P. A.
Vaněček, 686 Gth St. .

Omaha., Neb. T. ]. Sokol. Adresa: J. A. Hospodský,
care of Pokrok Západu.

South Omaha, Neb. T. ]. Sokol. Adresa: Jos. Sínkule,
Cor. 24 85 L. Str.

New York, N. Y. “Tél. led. Sokol. Adresa: Tom. Be-
neš, 1072 First Ave.

New York, N; Y., 8. ]. 'Svornost. Adresa: Fr. Kará-
sek, 953 Tinton ave. near 163rd Str. ' '
Long Island City, N. J. T. .I. S. Fuegner. Adresa:
V. Vančura, 59 Debeyoise Ave. '
Cleveland, 0. T. .I. S. Čech. Adresa: Jan Prošek, 3

Burwell _Ext. „
Baltimore, Md. Sok. led. Blesk. Adresa: V. J. Simek,
Cor. Broadway 85 Barnes Str.
Detroit, Mich. Těl. led. Budivoj, Adresa: J. Janeček,
698 Antonine Str. , _ ,
Cedar Rapids, Iowa. S. led. Tyrš. ' Adresa:—"K:fMoj-
žíš, 190 12th_Avenue.
Westfield, Mass. T. ]. Sokol. Adresa: V. Březina,
97 Meadow St.
Přistěhovalectvo Spolených Státu.
Rika se, že, čím čilejší národ, tím vetší jeho
pohyb. Přihlédneme-li k statistice, shledáváme, že
přijelo dle úředních výkazů roku 1889 do Spojených
Států 518,518 vystěhovalců, již dle zemí" dělí se
takto: (Přidáváme zároveň počet celého obyvatelstva,
jednotlivých zemí, abychom věděli, na mnoho-li oby-
vatelů jeden vystěhovalec přijde. Viz_ tabulku str. 74.)
Do Spojených Států přichází tudíž poměrně
k obyvatelstvu nejvíce vystěhovalců 1. z Irska (1
z každých 75, obyvatelů), 2. ze Svedska a Norska
(1:95), 3; ze Skotska (1:150), 4. z Dánska (1 : 216),
5. z Anglie (1:291), 6. z Německa (1:402), 7. ze
Svýcar (1 :429), 8. z Italie (1:585), 9. z Rakouska
(bez Čechů a jižních Sloganů) (1:597), *_10. z' Nizo-
zemska. (1:719), 11. z Čech a Uher (1:780), 12.
z Polska (1 : 1101), 13. z Ruska. (1 : 1740),"14.'z Fran—
eie (155255) Washingtonská kanoelář'počíta Čechy
a Uhry 'pro sebe. Mezi Uhry jsou ale, jelikož Maďar
se nestěhnje, jenom Slóváci, Chorvaté va Srbové.
z. Ruska “stěhují se jenom židé a němečtí a čeští
kolonisté. Z Italie stěhuje se' mnoho“ lidí také "do
jižní Ameriky a z Anglie, Irska a Skotska do Ka-
nady, Australie atd. Čechové předčí v pohybů svém

pouze Polsko, Rusko a Francii. O Francii se ví, .že
se přežila; její počet obyvatelstva. více neroste,
nýbrž ubývá. I v hořejších číslicích jeví se zajímavý
obraz národního života.
počet počet celého 1 vystě-
přistěbo- země, odkud přijeli obyvatelstva 33231312
“Imm dotyčné země obyv.?
77,168 Anglie . . . . .. 22,704,613 291
71,761 Irsko . . . . . . 5,402,759 75
23,588 Skotsko . . . . . 3,558,613 150
106,924 Německo . . . ' 425000900 402
6,869 Francie . . . . . 36,102,921 5,255
16,585 Ě Čechy a Uhry. . . 13,000,ooo 780
25,080 1 Ostatní země rak. . 24,882,712 597
37,333 , Rusko . . . . . 65,ooo,000 1,740
5,902 ' Eolsko . . . . . 6,500,000 1,101
65,949 Svédsko a Norsko . 6,410,495 95
8,756 Dánsko . . . . . 1,893,ooo 216
5,457 Nizozemsko . . . 3,924,792 719
47,422 Italie . . . . . . 27,7%,475 „585
6,619 Svycarsko *. . ;. '. 2,840,977 429
12,305 ; Ostatní země . . . —- '—
Dběanstw Spojených Států.
Dodatek 14. k ústavě americké praví: „Všecky
osoby ve Spojených Státech narozené anebo natura-
lizované, a pravomocnosti jejich poddané, jsou ob-
čany Spojených Států, jakož i státu, v němžto „sídlí—.“—
Indiáni ve svazku kmenovém žijící“ a daně neplatící,
nejsou občany. Dítky narozené v oizozemsku, jejichž
otcové byli však tou dobou občany americkými, jsou
po zákonu též občany Spojených Států., Qizozemkyně,
která se provdá za občana amerického, považována
jest za' občanku; též může cizozemkyně sama nabytí
občanského práva. dle zákona o naturalizaci. *Dítky
cizozemců, které za neplnoletosti své přijdou -_ do
Ameriky se svými rodiči, stávají se občany pakli
otcové jejich nabudou úplného občanského práva.
Však prohlásí-li otec toliko úmysl svůj. občanem se

státi (t. j. vyzvedne-li si toliko první listinu), tím
děti jeho občanského práva nenabudou, nýbrž musí
samy občanský list si opatřiti. Tak zní rozhodnutí
soudův. Občané přijati, meškající v cizozemsku, mají
totéž právo na ochranu osoby a majetku se strany
vlády americké,“ jako občané rození. Povinnost pre—
sidenta jest vytknuta následovně: „Kdykoli by ozná—
meno bylo presidentu, že některý občan Spojených
Států bezprávně zbaven jest svobody pod právomocí
některé cizí vlády, budiž povinností presidenta ihned
požádati od oné vlády příčiny toho uvěznění; a přijde-li
na jevo, že jest křivé a ku porušení práv amerických
čelící, president ihned požádej propuštění občana
toho na svobodu, a pakli by na žádost tuto pro-
puštění bylo nerozumné dlouho odkládáno nebO'ode—
přeno, president použij takových prostředků, ne—
znamenajících činy válečné, jež za dobré uzná,
aby propuštění toho docílil; a vše]iké podrobnosti
a jednání v té příčině buďtež co nejřive možno pre—
sidentem sděleny kongresu Spojených Států.“ Cizo-
zemec čili přistěhovalec, stane-li se naturalisací ob-
čanem Spojených Států, požívá týchž práv, jako ob—
čan rozený, toliko nemůže státi se presidentem ani
místo-presidentem Spojených Států. Zákonné předpisy
o nabývání práva občanského ustanovují toto: Cizo—
zemec chtějíoí občanem se státi musí nejméně dvě
leta před svým připuštěni—m prohlásiti přísežně u ně-
kterého soudu Spojených Států aneb u některého
soudu jednotlivého státu Americké Unie, jenž soudní
zápisy vede, že má poctivě v úmyslu státi se občanem
Spojených Států a odříci se věrnosti a příslušnosti
kterémukoli cizozemskému panovníku nebo vrchnosti,
jmenovitě pak tomu panovníku neb státu, jehož jest
tou dobou poddaným. Když pak nejméně po dvou
letech žádá za druhou listinu čili občanství, musí
mimo to prohlásiti, že buď nikdy neměl titul dědičný
a nepatřil k žádnému řádu šlechtickému, anebo iná—li
titul takový, že se ho odříká a že podporovati bude
ústavu Spojených Států. Při tom musí dokázati dvěma
svědky, občany americkými, že bydlel nejméně pět
let stále ve Spojených Státech a nejméně rok ve
státu neb území, kde soud dotyčný se nalezá,.že se
po ten čas choval co muž mravného charakteru,

dbající o zdar ústavy a dobrý pořádek i blaho Spo-
jených Států. Cizozemec neplnoletý, jenž bydlel ve
Spojených Státech po tři poslední leta před svou
plnoletosti (která tam nastává v stáří dokončeného
21. roku) a trvale zůstal v zemi až do té doby, kdy
žádost za občanství soudu předloží, může přijat býti
za občana po pětiletém přebývání ve Spojených Stá-
tech, počítaje v to ona tři leta nedospělosti, aniž by
byl dvě leta napřed vyzvednul první listinu. Prohlásiti
musí pod přísahou a ku spokojenosti soudu dokázati,
že po dva roky právě prošlé bylo jeho skutečným
úmyslem státi se občanem Spojených Států; ostatně
musí učiniti zadost předpisům o udělení občanského
práva v obyčejných případech. Plavec, který pro-
hlásí úmysl státi se občanem, a napotom slouží tři
leta na lodích amerických, může býti za občana při-
jat, prokáže-li se čestným propuštěním. Kdokoli dá
se k vojsku Spojených Států a napotom čestně pro-
puštěn jest, může občanem se státi, dokáže—li, že
přebýval ve Spojených Státech jeden rok před svým
přihlášením se o právo občanské a že charakter jeho
jest zachovalý. Přistěhovalec, chtějící vyzvednouti si
první listinu, to jest, učiniti prohlášení úmyslu, státi
se občanem Spojených Států, může jen dojíti k pí—
saři (klerkovi) soudnímu, kterýž vyplní tištěný blan—
ket dle požadavku zákona a vezme žadatele pod
přísahu. Prohlášení se zapíše a žadatel obdrží do-
svědčení čili „první list“, který si musí uschovati.
Však ztratí-li jej, může dostati od klerka za poplatek
75 centů pověřený Opis. Když přijde čas, že může
se občanem státi, musí dojíti ke klerkovi některého
oprávněného soudu a své dosvědčení či první list
předložiti. To se státi musí, když soud zasedá. Klerk
vyhotoví udání obsahující hořejší fakta, které potom
žadatel s dvěma svědky svými odpřisáhne a podepíše;
k nařízení soudce vystaví mu klerk dosvědčení, že
byl přijat za občana, kteréžto dosvědčení jest na—
potom právním důkazem jeho občanství. Ztratí-li ho,
může dostati od klerka nový pověřený opis, poněvadž
celé jednání do knih zanešeno jest. Zemře-li cizo—
zemec, který byl si vzal první list, dříve nežli sku—
tečným občanem se stane, jsou vdova a sirotci po-
važováni za občany, když složí přísahu předepsanou.

Žádný cizinec ze státu, s nímž Spojené Státy ve
válce jsou, nemůže za občana býti připustěn.
Největší města Spejených Států.
Dolejší abecedně srovnaný seznam podává města
ve Spojených Státech, Jež dle loňského sčítání lidu
měla. přes 20.000 obyvatelů. Aby nasmec o velikosti
těchto měst měl Jasnějsí pojem, _ nvádím zde za, mě—
řítko několik známých měst SJJBJIGll obyvatestvem.
Tak čítá, Vídeň 1,103.857 obyv., Praha o sobě s VOJ—
skem 177.026 9. s předměstími Karlínem, Smíchovem,
Vinohrady a Zižkovem 275.871, Brno 73.771, Plzeň
38.883,'Budějovice 17.886, Chrudim 11.886, Tábor
7.413 a Čáslav 6.000. Písmeny za. městy JSOU. obvyklé
zkráceniny Jmen amerických Států; tak se k zná-
mému Chicagu vždy přlpOJllJe Ill., což znamená stát
Illinois; neboť jsou tam ještě 2 Chicaga a sice Jedno
ve Státě Kentucky a druhé ve státě 01:10.
Město Obyvatelsvo r. 1890 Město Obyvatelsvo r. 1890
Akron, O. . . . . . . 27.702 Columbus, O. . . . . . 90.398
Albany, N. Y. . . . . . 94.640 Council Bluffs, Ja. . . . 21.388
Allegheny, Pa. . . . . . 104.967 Covington, Ky. . . . . 37.775
Allentown, Pa. . . . . 25.183 Dallas, Tex.. . . . . . 38.140
Atlanta, Ga. . . . . . . 65.414 Davenport, ja. . . . . 25.161
Auburn, N. Y. . .“ . . 26.887 Dayton, O. . . . . . . 58.898
Augusta, Ga. . . . . . 33.150 Denver, Col. . . . . .106.670
Baltimore, Md. . . . .434.151 Des Moines, Ja. . . . . 50.067
BayACity, Mich. . . . . 27.826 Detroit, Mich.. . . . .205.669
Binghampton, N. Y. . . 35.035 Dubuque, Ja. . . . . . 30.147
Birmingham, Ala. . . . 26.241 Duluth, Mínn. . . . . . 52.725
Boston, Mass. . . . . . 446.507 Elizabeth, N. ]. . . . . 37.670
Bridgeport, Ct. . . . . 48.856 Elmira, N. Y.. . . . . 28.070
Brockton, Mass. . . . . 27.278 Erie, Pa. . . . . . . . 39.699
Brooklyn, N. Y. . . . .804.377 Evansvile, Ind. . . . . 50.674
Burlington, Ja. . . . . 22.528 Fall River, Mass. . . . 74.351
Buffalo, N. Y. . . . . .254.451 Fitchburg, Mass. . . . . 22.007
Cambridge, Mass. . . . 69.837 Fort Wayne, Ind. . . . 35.349
Camden, N. Y. . . . . 58.227 Fort Worth, Tex. . . . 20.725
Canton, O. . . . . . . 26.328 Galveston, Tex. . . . . 29.118
Charleston, S. C. . . . 54.592 Grand Rapids, Mich. . 64.147
Chatannoga,Tenn . . . 29.100 Harrisburg, Pa. . . . . 42.120
Chelsea, Mass. . . . . 27.850 Hartford,-Ct. . . . . . 63.180
Chester, Pa.. . . . . . 20.107 Hempstead, N. Y._ . . 23.512
Chicago, Ill.. . . . .1,096.516 Hoboken, N. ]. _. . . . 43.561
Cincinnati, O. . . . . .296.309 Holyoke, Mass. . . . . 35.526
Cleveland, O,. . . . . .261.546 Hot Springs, Ark. . . . 71.115

Město Obyvatelstvo r. 1890 ! Město Obyvatelstvo r. 1890
Houston, _Tex.. . . . . 27.411 Poughkeepsie, N. Y. . . 22.836
Indianapolis, Ind. . . . 107.415 Providence, N. ]. . . .132.043
Jersey City, N. ]. . . . 163.987 Pueblo Colo. . . . . . 28.128
Joliet, Ill. . . . . . . . 27.407 Quincy Ill. . . . . . . 31.478
Kansas City, Kan. . . . 38.170 Racine, Wise. . . , . . 21.022
Kansas City, Mo. . . . 200.000 Rcading. Pa. . . . . . 58.626
Kingston, N. Y. . . . . 25.000 Richmond, Va. . . . . 80.838
Knoxville, Term. . . . 2l.131 Rochester, N. Y. . . . 138.327
Lancaster, Pa. . . . . . 32.447 Sacramento Cal.. . . . 26.272
Lawrence, Mass. . . . . 44.559 Salem, Mass. . . . . . 30.735
Leavenworth, Kan. . . 21.613 Salt Lake City, Ut. . . 45.025
Lincoln. Neb. ; . . . . 55.491 San Antonio, Tex. . . . 38.631
Little Rock, Ark. . . . 22.396 San Francisco, Cal. . .297.990
Los Angeles. Cal. . . . 50.394 Savanah, Ga. *. . . . . 41.762
Long Island City N. Y, 30.396 Seranton, Pa. . . . . . 76.897
Louisville, Ky. . . . .161.005 , Seattle, Wash . . . . . 43.914
Lowell, Mass. . . . . . 77.005 ( Sioux City, ja. . . . . 37.862
Lynn, Mass.. . . . . . 64.596 ! Somerville, Mass. . . . 31.546
Macon, Ga, . . . . . . 22.698 ! South Bend, Ind. . . . 21,786
Manchester, N.*H. . . . 43.938 [ Springfield, Mass. . . . 44.164
Memphis, Tenn. . . . . 64.586 Springfield, Ill. . . . . 24.852
Milwaukee, Wisc. . . .204.150 Springlield, Mo. . . . . 21.842
Minneapolis,Minn. ._ . .164.789 Springlield, O. . . . . 32.135
Mobile, Ala.' . . . . . 31.822 St. ]oseph, Mo. . . . . 95.587
Montgomery, Ala. . . . 21.790 St. Louis, Mo... . . . .460.358
Muskego, Mich. . . . . 22.668 St. Paul, Minn. . . . . 133.157
Nashville, Term. . . . . 76.399 Syracuse, N. Y. . . . . 87.877
Newark, N. J.. . . . .755.876 Tacoma, Wash. - - . - 85.885
New Bedford, Mass. . . 40.705 Taicuton, Mass. . . . . 25.389
Newburg, N. Y. . . . . 23.263 Terre Haute, Ind. . . . 30.287
New Haven, Ct. . . . . 85.981 Toledo, O. . . . . . . 82.652
New Orleans, La. . . . 241.995 Topeka, Kari. . . . . . 31.893
Newport, Ky. . . . . . 24.038 Trenton, N. I. . . . . . 58.488
New York. . . . . .1,513.501 [ Troy, N. Y. . . . . . . 60.605
Norfolk, Va.. . . . . . 34.986 l Utica, N. Y. . . . . . . 44.001
Oakland. Cal. . . . . . 48.590 Washington. D. C. . .230.000
Omaha, Neb. . . . . .139.526 ' Wheeling, W. Va. . . . 35.052
Oshkosh, Wis. . . . . . 22.752 Wilkesbarre, Pa. . . . 34.787
Oswego, Nl Y. . . . . 21.826 Wilmiiigton, Del. . . . 61.437
Paterson, N. ]. . . . . 78.358 Wichita, Kau. . . . . . 23.335
Peoria, Ill. . . . . . . 40.758 Worcester, Mass. . . . 84.533
Petersburg, Va. . . . . 53.317 Yonkers, N. Y. . . . . 31.945
Philadelphia, Pa. . . . ELM-1.864 Youngstown, O. . . . . 33.199
Pittsburg, Pa. . . . . .238.473 York, Pa. . . . . . . . 20.849
Portland, Me, . . . . . 36.608 Zanesville, O. . . . . . 21.117
Portland, Oreg. . . . . 35.861
. . , ,a
Obyvatelstvo Spolenych Statu.

Dle úřadního sčítání obnášel počet obyvatelstva
severoamerické Unie, vygma Indian Terntory,Alasku
a Indiany v reservacích, dne 1. června 1890 celkem

62,480.540 duší proti 50455583 roku 1880. Dle toho
zvýšil se počet obyvatelstva za posledních 10 let
0 12324757 nebo 241!2 procenta. V témže čase
(1880—1890) se přistěhovalo do Unie 5,246.613 osob;
obnáší tedy přirozený vzrůst obyvatelstva Spoj- Států
něco přes 7,000.000 duší. (Unie má. rozlohy 9,212.27OD
kilom., tudíž přijde ne celých 6 osob na 1D kilom.,
kdežto Čechy s rozlohou 51.9.5613 kilom. 85,436.000
obyv. čítají na 1G kilom. 104 duše).

Počet obyvatelstva jednotlivých států u porov-
nání s obyvatelstvem r. 1880 je nyní následující:

1890 1880

Alabama . . . . . . . . 1,508.073 1,262.505
Arizona . . . . . . . . 59.691 40.440
Arkansas. . . . . . . . 1,125.385 802.525
California . . . . . . . 1,204.002 864.694
Colorado . . . . . . . . 410.975 194.327
Connecticut. . . . . . . 745.861 622.700
Delaware . . . . . . . . 167.871 146.608
D. of Columbia . . . . . 229.796 177.624
Florida . . . . . . . . . 390.435 269.493
Georgia . . . . . . . . 1,834.386 1,542.180
Idaho . . . . . . . . . 84.229 32.610
Illinois . . . . . . . . . 3,818.536 3,077.871
Indiana . . . . . . . . 2,189.030 1.978.303
Iowa . . . . . . . . ' 1,906.729 1,624.615
Kansas. . . . . . . . ,. _1,423.485 996.094
Kentucky . . . . . . *. . 1,855.436 1,648;69O
Louisiana . . . . . . '. 1,116.828 939.946
Maine . . . . . . . . . 660.261 648.936
Maryland. . . . . . . . 1,040.303 934.943
Massachusetts . . . . . 2223.407 1,783.085
Michigan . . . . . . . . 1,089.892 1,636.937
Minnesota . . . . . . . 1.309.017 780.773
Mississippi . . . . . . . 1,284.887 1,131.597
Missouri . . . . . . . . 2,677.080 2,168380
Montana . . . . . . . . 131.769 39.159
Nebraska. . . . .' . . . 1,056.793 452.402
Nevada ,. . . . . . . . 44.327 62.266
New Hampshire. . . . . 375.827 346.991
New Jersey. . . . . . ' 1,444.017 1,131.116
New Mexico . . . . . . 144.862 119.565
New York . . . . . . . 5,981.934 5,082871

1890 1880
N. Carolina. . . . . 14517340 1,399:75o
N_. Dakota, . . . . . . 182.425 36.909
Ohio . . . . “. . . . '. . 3,666;719 3,198.062
Oklahoma -. . . . . , . v61.701
Oregon . . „. . -. . . . . 312.490 174.786
Pennsylvania—. . . . ». *. 45348574 4,282.891
Rhode Island , . . . . . »— 345.243 276.531
5. Carolina . . . Z . . ., »1,14_.7.15=1 995.577
S.»Dak0ta' . . . . . . ._ 327.848 98.268
Tennessee . . . . . . . 15763123 1,542.359
Texas . . . . . . . . . 2,232.22O 1,591.749
Utah . . . . . . . . . . 206.498 143.963
Vermont . . . . . '. . . 332.205 332.268
Virginia . . . . . . . . 1,638.911 1,512.565
Washington . . . . . . 349.516 75.116
West Virginia . . . . 760.448 618.457
Wisconsin . . . . . . . 1,683.697 1,315.497
Wyoming. . . . . . . . 60.589 20.789
Americké drožky.

Drožku jmenují v Novém Yorku Hackney-coach
(čti hekny khóč). Ceny: až do 1 míli (angl.) 50 centů
a za.. každou další půlmíli 25 centů. Ve Philadelphii
mají drožky tak zvané Hansem eabs (čti hensom
khebs) jen pro dvě osoby; cena 65 centů za hodinu.
Hansom—cab je dvoukolový kočárek s kozlíkem pro
kočího vzadu. V Baltimore jsou: One-horse cabs
hansoms; k nádraží či od nádraží platí se za osobu
25 centů a za hodinu 75 centů,. (One horse; čti von
horszjeden kůň.) Ve Washingtonu: Hackney-car-
riages-(čti hekny kherridžez) s párem koní, jako
v Praze ftakr; ceny “za. 1 neb 2 osoby za 1 míli
dol. 1'—; za každou další osobu 50 centů; na hodinu
dol. 1-50. One-horse cabs: ceny: pro 1 neb 2 osoby
za hodinu 75 centů; za, malé dovežení ve městčw25
centů za každou osobu. V Chicagu: Hackneyvoarria-
ges;_za jednu osobu z nádraží na nádraží 25 centů;
zanl osobu až do 1'míli 50 c., do 2 mil dol. 1 apřes
2=míle dol. 1'50; za. každou další osobu 25 c.; za

děti mezi 5. a 14. rokem polovičku; na čas: za první
hodinu dol. že ;za. každou dálší hodinu doI. 1— —
Pak jsou v Chicagu drožky zvané Gurney cabs a Han-
som cabs, jichž ceny jsou: za dovežení 25 c. za
každou osobu aneb 75 centů za hodinu za dvě osoby.
Dodávání poštovních zásilek;

V obvodu poštovní správy Spoj. Států se děje
dodávání poštovních zásilek pomocí listonošů “po—
měrné v nepatrné míře a sice dnešního dne pauze
ve 7154 větších městech. U všech ostatních poštov—
ních úřadů si musí adresáti sami svoje psaní'a zá-
silky—z pošty odebírat a iv městech, kde pravidelné
dodávání listonošem je zavedeno, se stává, že ze-
jména větší obchody si svoje žásilky samy z pošty
vyzdvihují. U poštovních úřadů nalézají se ve stěně
mezi kancelářskou místností a předsíní pro obecen—
stvo (chodníkem) umístněné přihrádky pro takové
osoby, které pravidelně svoje dopisy z pošty si ode-
bírají a za používání těchto schránek určitý popla—
tek složili, jenž obnáší na malých poštách 1 doll.,
na prostředních (v městech přes 5000 obyvatelů)
1—4 doll. a v největších městech až 15 doll. ročně.
Taková poštovní schránka nazývá se lock box (lók
bóks) aje tak zařízena, že poštovní úředník ji má
s otevřenou stranou po ruce, aby pohodlně do ní
mohl zásilky házet, kdežto abonnent jí může na chod—
níku svým klíčem otevřít a co pro něho poštou při-
šlo, si vzít. Je-lí schránka expeditorem uzavřena
uvnitř, nemůže ji abonnent z venku otevřít a. musí
pak vejít do úřadovny. To se stává, když dojde zá—
silka neb psaní s nějakou dobírkou neb rekoman-
dovaně, ab adresát uvedl napřed věc do pořádku.
Některé schránky jsou také šuplata, rovněž se zám»
kein k otevření z venku, které pak Ameúkán nazývá
„drawer“_ (dráer). Nejvíc schránek ale je bez klíče;
jsou to číslované přihrádky uvnitř, celé skleněné,
tak že abonnent z venku' hned vidí, jest-li v jeho
přihrádce něco leží neb ne. Leží-li tam něco, jde
k otevřenému oknu, udá expeditoru číslo přihrádky,

která sevjmenuje jednoduše „box“„ a on mu to vyda.
Tím se Čechům vysvětluje, co na adresách znamena
box, ku příkladu 872:'totiž adresát ma na poště
předplacenou schranku čili přihrádku pod číslem 872,
do níž vše se vloží, co naň dojde a tam zaroveň tak
dlouho zůstane ležet, až si adresát pro to bud' sám
dojde neb pošle. Na poštách, kde donášení neníza-
vedeno, musí se adresati, kteří nemají své vlastní
schránky, o dopisy pravidelně u manipulujícího úřed-
níka hlasit. K _yž do 4 neděl o dopisy nikdo se ne-
hlásí, upozorňuje se na ně veřejnou vyhláškou na
poště někde vyvěšenou aneb také inseratem „: novi-
nách. Dojde-li ktomuto prostředku, přirazí še'vzešlé
útraty, které za jednotlivé psaní 1 cent. neemí pře—
sahovat, k portu, které adresát nahradí při vydání
listu. Všecky poštovské zašílk'y'a dapisy, které by
vzdor veřejnému “ohlášení zase do %! neděl se ne-
odebraly, zašlou se s připojením jednoho opisu vy—
věšeného návěští neb exemplaru onoho čísla-novin,
v němž byly uveřejněny, do Washingtdnu tak zva-
nému Deadletter office (dédlétr “ofňs) úřadu mrtvých
psaní, kde se vše otevře; jest-li pak ještě adresát
se nevynajde aneb. alespoň zasilatel, jemuž by se věc
vratila, vyndá se veškerý cenný obsah, jenž veřejnou
dražbou se prodat, a ostatek se zničí.
Ceske kmhy clu nepodlehaji.

010 na knihy ve Spojených Státech je dle za-
kona Mc. Kinleyova_25 procent, ale pouze „na knihy
v jazyku anglickém. Cizojazyčné knihy, jako české,
francouzské, německé atd. jsou hořejším zakonem
ode cla osvobozeny. Zásluha patří českým redakcím,
které roku 1888 podali kongresu společnou žádost
o zrušení cla z knih cizojazyčných.

Lži ač svět projydeš, ale nazpět se. nevrátíš. '— Lež, ačkoli snídá,
zřídka obědvá, nikdy téměř nevečeří. — Byli jednou tři bratří:
jeden lhal, druhý kradl a třetí visel. —- Jazykem nemlat'; cheeš-li,
11ij se cepuv. — Lenoch se strhal, že nechtěl dvakrát jíti. —
Bůh praví: Pomáhej si sám., pomohu tobě i já. _ Kdo večeře ne-
hledá, toho večeře nenajde. — Bez práce se člověk ne.—nají. —

Časový rozdil.
.páŘ" právě tu dobu; ..u'kazuji hodin
Kd ' ' h d' '. . *
yz “5:32 o m ve 11 Ham— IV Novém v v San
P“ “8 v Praze ., ., j — _ v Omaze _
1 Vldm burku ' Yorku Chxcagu Franolsku
v Praze . . . . 12 pol. 12.05 odp. 11.42 dop. 6.06 ráno 5.11 ráno 4'88 ráno 2.52 ráno
ve Vídni . . . . 11.52 dop. 12 * pol. 11.„ dop. 5—53 ráno 5.03 ráno 4.30 ráno 2.44 ráno
v Hamburku . 12.13 odp. 12.2„Íiodp. 12 pol. 6.24 ráno 5.„ ráno 4.52 ráno 3.10 ráno
v Novém Yorku . 5—54 odp. 6..02 odp. 5.„ odp. I2;pol. 11.05 dop. 10.32 dop. 8.45 ráno
v Chicagu . . . . 6.43 _več. 6.56- več. 6.51 več. 12.55 odp. 12 pol. 11.27 dop. 9.41 ráno
V Omaze . . . . "(.22 več. 7.80 več. 7.4 več. 1-23 odp. 12.33 odp. 12 pol. 10.m dop.
v San Francisku . k 9.8 več. 9.16 več. 8.50 več. 3.34 odp. 2.19 odp. 1-45 dop. 12 pol.

V Praze „vychází Vslunce () 8 minut později nežli ve,Vídni, o 18 minút dříve než v Hamburku, o 5 hodin 54 minuty
dříve, než v Novém Yorku, () 6 hodin 484 minut dříýe, než v Chicagu, o 7 hodin 22 minut dříšre než v Omaze
a o' 9 hodin 8 minut dříve než v Saxi Francisku. Dejme tomu, že by se v Praze *! poledne o 12., hodině něco stalo;
což by ihned telegrafovalo se do Ameriky. Telegram nepotřebuje více než 1 hodinu času a zpráva o věci, kteráž sc.
udála v'Pra'ze v poledne byla by téhož dne, již v 7 hodiň ráno v Novém Yorku, v 6 hodin 'ráno v Chicagu, v 5112
hod. ráno v-Omaze a ve 4 hod. ráxio v San Francisku, ageb neberouce ohled na čašový rozdíl, mohli bychom říci,
že v Ameriee telegrafem dozví se evropské události 0 5 “až 8 hodin dříve, nežli se staly.

Budoucnost české národnosti v Americe.
Z knihy »Za oceanema od Dra. Štolby.

„ Dejme tomu, že přijede doAmeriky mladý muž
z Čech, jenž pln je zápalu pro vlast, kterou byl
právě opustil, a těžce toho nese, že žíti musí tak
daleko od ní. Umíní si, že zůstane věrným jejím
synem, že bude mu vlast místem poutnicky'm, do
něhož odebeře se vždy po několika letech, aby osvěžil
zase city, jež víží jej k jeho národu, a že stane se
útulkem je o zase, až nashromáždí si v Novém Světě
tolik, aby mohl doma žíti bezstarostně. Vrhne se do
života s plnou chutí ». pracuje. Při tom však vpra-
vuje se zároveň již do sociálních poměrů, jež jsou
daleko svobodnější a volnější nežli poměry naše do-
mácí, vžije se zvolna do nich a vžije se do nich tak,
že, poněvadž jsou skutečně daleko přirozenější nežli
poměry naše, myslí, že jinde jiné ani býti nemohou.
Zároveň působí na něj politická. váha státu, v němž
žije, a čilý jeho politický život, jenž proudí tak bo-
hatě ve všech vrstvách, takovým spůsobem, že s pý-
chou má se za příslušníka velkého toho státu. To
nikterak nevadí však posavadnímu národnímu jeho
vědomí, a tím méně, poněvadž v politickém životě
nové jeho vlasti nikdy se nic nevyskytne, co by vě—
domí toto uráželo aneb i jennepříjemně se ho do-
týkala. Neboť každý občan cítí se především Ame—
ričanem a může se jím cítiti, aniž by staré vlasti
své tím ublížil. Nyní nastane doba, kdy mladý ten muž
zarazí si svůj vlastní krb. Vyhlédne si dceru z české
rodiny, zařídí sobě úplně českou domácnost a těší
se, že uštědří-limu osud dítek, vychová. je co zdárné
dítky daleké vlasti. Přání jeho se vyplní; dítky se
dostaví a vychovávají se pak zcela česky. Láska
k daleké vlasti vštěpuje se jim do srdce, a otecs ra—
dostí vzpomíná. oněch dob, až děti ty uvede do vlasti
a ukáže jím onu zemí, z nížto pochází. Vše daří se
z počátku dobře. Avšak poměr ten se zkalí, když
dítky počnou scházeti se s dítkami jinými, hlavně
ale když počnou choditi do školy. Zde naučí se hravě
angličině, jižto slyší všude a na všech stranách, a
zde vypravuje se jim stále o velikosti ». slávě Ame-
riky. Dítko vžije se velmi snadno do obého a již je

ztraceno. Ono mluví sice s rodíčemi česky, avšak
jakmile přijde mezi dítky a třebas by to i byly dítky
české, mluví anglicky; ono slyší sice, jak otec blouzní
o daleké vlasti, ale nechápe, jak možno blouzniti
o tak nepatrném kousku země, jenž vedle Ameriky
úplně mizí, o němž není v jeho zeměpisných knihach
ani zmínky, a o němž mimo rodinný dům neslyší
nikdy ani sleva. Dítě jest nyní česky vychovaným
Američanem. S bolestí pozoruje to otec, a chce to
napraviti. Sebere rodinu a vypraví se na cestu do
Čech, 'doufaje, že rodná. jeho země vzbudí v dítkách
jeho tentýž zápal, rjaký panuje pro ni v_srdci jeho.
Avšak zmýlil se. Dítkam, jest tu vše cizí; ony ne-
slyší nikde ani slova té angličiny, již měly doma
stále v uších; ty představy, jež nadšené popisy otcovy
v nitru jejich vzbudily, nesrovnávají se také se sku-
tečností; města v té otcové vlasti mizí proti městům
americkým; vše jest malé, tak nepatrné proti Ame-
rice, a dítkám jest teskno, stýská se jim po — do-
mově. Avšak nejen to. I otec k syému zděšení musí
se přiznati, že necítí se více v Čechách tak doma,
jako před odchodem svým. Vžil-se v Americe do
poměrů zcela jiných, myslil, že musí býti všude tak,
a nyní najde v tom nejzamilovanějším koutku celého
světa život, jenž dotýká. se ho téměř až mrazivou
rukou. Miluje svou vlast, ale přichází k poznání, že
—nemohl by v ní více žíti, jest věrným jejím synem,
ale — odchází přece zase a odchází skoro rád. Děti
vrátí se do Ameriky, porovnávají dalekou tu otcovu
vlast se svou vlastí, a. jsou rády, že jsou opět tam,
kde se narodily. Proces průpravní je ukončen, a dítky
našeho, pro vlast hořícího krajana, jakož i jejich
budoucí potomstvo jest pro nás ztraceno. Pokolení
třetí jest jíž čistě americké. Těžce to nese krajan
naš, avšak není pomoci. Jestit tak nejen'u Čechů,
nýbrži u všech ostatních národností. Přistěhovalí
Němci pořádají sice „Siegesfesty“ a zpívají „Wacht
am Rhein“- -.— ale dítky jejich již cítí se úplně Ame-
ričany, třebas by dovedly mluviti řečí rodičů svých,
třebas by i němectví své v popředí stavěly, a daří
se jim i rodičům, když vrátí se do vlasti, právě tak,
jako onomu Čechu. Jak u jiných, tak i u nich za-
viňují to poměry. Kdo vžije se do volných poměrů

amerických, ten nedovede se více vpraviti do těžkých

poměrů evropských. Pozoroval jsem to u všech kra.-

janů i neln'ajanů, kteří navštívili po delším pobytu

Evropu. Nechť sebe více lnuli k svému dřívějšímu

domovu, přece cítili se v něm tam stínění, že spě-

chali zase jen zpět.
Cís. král. rakouští konsulové
ve Spoj. Státech a v Kanadě.

N o vý York: 33 Broadway: Theodor A. Havenmayer,
generální konsul; dr. Antonín šl. Palitschek,
konsul; Otta P. Eberhard, kancelista.

Boston: Artur Donner, konsul.

Philadelphia.: L. Weatergaard, konsul.

Baltimore: Ch. A. Martin, konsol.

Richmond: Ludvík T. Borchers, konsul.

Mobile: V. F. Stoutz, konsul.

Nový Orlea ns: Baron Arnošt z Meysenburku,
konsul.

Galveston: C. F. Prehn, konsul.

San Francisco: Rudolf Hochkoder, konsul.

Milwaukee: Arnošt z Baumbachu, konsul.

Chicago: H. Claussenius, konsul; Ed. Claussenius,
vicekonsul.

Pittsbur g: Max Schamberg, konsul; princ Alois
Solms—Braunfels, vicekonsul.

St. Louis: Ferdinand Diehm, konsul.

Louisville: Theodor Schwarz, konsul.

0 h arle s t o n: Karel Witte, konsul.

Mo ntr e al: Edvard Schultze, konsul.

St. Johns: J. H. Thompson, vicekonsul.

Halifax: V. H. Hart, konsul.

Dospělý muž chce pravidla, ženy a děti výminky._—— Čím menší

vlast, tím větší láska. k ní. -— Kde je dobrý mrav, je také dobrý

zvyk a pěkný. obyčej. — Chudáku schází mnoho, lakomci- is'ecko.

— Kdo jinému jámu kopá, sám do ní padá. — Kdo mnoho zapne

čne, málo skončí. : Lépe zlomit ruku nežli vaz. —— Raději se

obrať, nežli bys bloudil. — Ve svátek má čert: nejlepší žně. —

Na rychlou otázku pomalu odpověz. —- Každé radosti sedí kus ža-

losti na zádech. — Raději utrp sám, nežli ublížíš jinému. —

Hlavni pravndla stručně opakovaná.

1. Kdo chce cestovat do Spojených Států severo-
amerických, přeptej se dříve písemně, co celá. cesta.
až na. místo bude stát. Důstatek peněz musíš mít,
neb v cizině ti nikdo nic nedá,; ani nepůjčí.

2. Záhy pošli si na. pojištění místa lodního zá-
vdavek. Neodkládej až na. poslední chvíli. Kvapky
mívají kapky.

3. Svá. zavazadla pošli 14 dní napřed na. mou
adresu go náklad, pravím, 14 dní napřed.

4. Ctyři dní před odjezdem lodě musíš se z do—
mova vydati na. metu. Po cestě nedůvěřuj nikomu,
koho neznáš. Penize dobře uschovej a,buď na ně
opatrným. Můžeš je poslat i poštou napřed na mou
adresu, což nejbezpečnějším.

9. Zavazadla, jež povezeš s sebou, musíš dát
na německých hranicích v celnici prohlédnout. Tato
zavazadla jdou vždy jen tak daleko, jak tvůj želez-
niční lístek zní. Kolikráte po cestě nový lístek ku»
puješ, tolikrát; musíš svá zavazadla. znovu dávati
na váhu.

6. Na metu napeti“ si něco německých peněz,
které budeš Německem potřebovati.

7. V Bambi-me jdi jenom do hostince mnou tobě
odporučeného. Z něho nenech se nikým na, nějaké
táčky vyváděti, af. to krajan či cizinec, když ho osobně
neznáš. K ukrácení chvíle jsou v hostinci vyložený
různé česko—americké noviny.

8. V Novém Yorku obrať se na, mou ůlialku,
která ti„ ve všem upřímně a, spravedlivě poslouží.

9. Cemuž dobře nerozumíš, raději se u mne ještě
jednou poptej. Několik řádků dovedeš napsat a. 5 krej-
carů poštovného můžeš obětovat.

10. Uděláš-li chybu7 neseš toho vinu sám a sám.
Veďe—Ii se ti dobře, jsi pry chlapík, vede-Ii se ti zle, jsi prý ne-
řád. —— Spěcháš-li předhoníš bídu, loudáŠ-li se, předhoni bída tebe.
— Strach bývá větší než nebezpečí. -— Když mlčíš, nebude ti od-
porovat nikdo. — Nejvíc starostí spůsobuje si člověk sám. — Kdo
hudbu objednal, ať ji zaplatí. — Bez obrany tě snadno každý„po—
razí. _- Hodná žena je hlaženou v blažení své rodiny. — Cim
menší společnost. tím lepší zábava. -— Boháč má svou víru v tru—
hle.-—— Co má jít k “srdci, musí přijít od srdce. -—Není růže bez trnu.

Veřejné hlasy.

Na doklad, že co český jednatel plním úkol
více vlastenecký než obchodní, mohl bych předložiti
celé haldy díkůvzdání a odporučení od rodáků, mým
prostřednictvím přes moře dopravených. Leč to by
vedlo příliš daleko. Myslím, že postačí, uveřejním—li
hlasy pouze dva: jeden od jednotlivce, českého spi-
sovatele, který jel v kajutě, a druhý od celé sku-
piny „cestujících v mez'ipalubí.

Ceský spisovatel p. Pavel Albieri, redaktor ame-
rického denníku „Svornost“ vycházejícího v čísle 150
W. 12th Street v Chicagu, zmiňuje se v delším článku
ze dne 8. března 1890 o tomto předmětu mezi jiným
takto:

„V Hamburku vedla mne moje cesta do hostince
Frant. Oelkerse číslo 10. Kleine Reichenstrasse, kamž
jsem byl p. Pastorem odporučen. Hostinec tento jest
sice jen vystěhovaleckým, ale byl jsem opravdu pře-
kvapen vykázaným mi pokojem, jaký bych byl ne-
obdržel ani ve mnohém hotelu první třídy, jmeno—
vitě ne za ty peníze, an jsem platil v první třídě za
byt i stravu pouze 3 marky 50 feniků denně. Obdržel
jsem velmi pěkný, na hotel snad až příliš pěkně a
vkusně upravený pokoj v prvním poschodí, na ulici,
skvoucí se čistotou. Ohledně obsluhy pak musím
doznati, že jsem nikdy nepoznal v hostinci lepší.
Majetníci jsou bezdětní, zámožní manželé, kteří drží
hostinec více již ze zvyku a pro všeobecné dobro
než pro výdělek a mají jen radost ze slušných asa-
žérů. Pořádek je tam vzorný, lidé velice srdeční,
vlídní, ochotní a soucitní pro každého, kdo nastu-
puje dalekou, nebezpečnou tu cestu. Musím dáti
průchod pravdě, že jest podobný vystěhovalecký dům
skutečným dobrodiním pro většinu našich krajanů,
kteří jsou sem . Pastorem jenom ku značné výhodě
doporučovani. Egan J. Pastor získal si skutečně o
české vystěhovatelstvo zásluh nepopíratelných! Na-
vštívil jsem jej v kanceláři, naproti hostinci, hned
jakmile jsem se zpamatoval a upravil po mrzuté
noční cestě a nalezl jsem v němmuže, kteaý musí
hned v první chvíli vzbuditi úplnou důvěru v každém.
Bývalý česko-americký žurnalista, znající důkladně

poměry zde i za Oceánem, usadil se právě tam, kde
může znalost svou nejlépe použiti, ve prospěch ro-
dáků svých, spěchajících do Nového světa, o němž
obyčejně mají falešné jen představy a po případě
i zcela žadné. Jsa českým jednatelem zdejší veliké
společností, která vládne asi čtyřiceti velkolepými
parními loďmi v ceně mnoha a mnoha. milionů, pou-
žívá vlivu svého obětavě po devatenácte dlouhých
již let, aby dosáhl pro krajany své vždy výhod co
možná největších. Maje provisi od společnosti, ne-
beře od vystěhovalců žádného poplatku a jest ochoten
vždy a každému posloužiti, jak jsem viděl sám na
sobě a jsem úplně přesvědčen o podobném i vůči
jinému. Za čtyřdenního pobytu svého v Hamburku
snažil jsem se povšimnouti si co nejvíce, poznati'co
nejlépe vystěhovalecké poměry, pro nastávajícího
česko—amerického novináře tak důležité a. mohu říci,
že měl jsem příležitost i za. ten krátký čas dosti na-
hlédnouti do nich. Ve prospěchu pak všeobecném
sdělují ze zkušenosti své, že Ceši do Ameriky jedoucí
mohou prostřednictvím p. Pastorovým velice získati.
Jediná. jen výčitka spadá na p. Pastora vtom směru,
že příliš málo vystupuje ve veřejnosti, že příliš málo
agituje. Mluvil jsem sním o tom a obdržel jsem za
odpověď, že velkou reklamu činiti nemůže, an to
mnoho stojí a musil by to pak od těch, kdož jedou,
dát si zaplatit! Jest to tedy opravdovou povinností,
aby byl aspoň vřele odporučovan a známost o něm
šířena, an koná. vůči českým vystěhovalcům dílo sku-
tečně národní, které nesmí býti podceňováno ajemuž
nebude také pokládáno žadné nízké výdělkařství,
kde měl příležitost p. Pastora osobně seznati._ Nej-
záslužnějším, ale také po zásluze posud nejméně
oceněným, podnikem p. Pastorovým jsoujeho „Ceské
Osady v Americe“, měsíčník pro české osadníky a
jmenovitě pro naše vystěhovalce do Ameriky velmi
prospěšný. Jednotlivec podnikl zde práci skutečně
velikou, sestaviv první důkladnější adresář český pro
Ameriku, výhoda to, již nejlépe posoudí jmenovitě
ten, kdo jede sem, nemaje nikoho známého jakož
i mnohý v Americe usazený obchodník. Adresář ten
jest již po léta doplňován .s uznaníhodnou bedlivostí
a důkladností. Dále podává. v „Osadách“ velmi dobrá.

ponaučení ohledně amerického života, statistická data,
líčení poměrů, anglická, cvičení pro samouky atd.
snaže se, jak jen možná, podati vše co nejsprávněji,
nejpravdivěji, čímž časopis jeho uchází výtce, že by
snad chtěl vypisováním nějakého pozemského ráje
do Ameriky lákati, aby jen mělzkaždé dušičky zisk.
Varujet docela lidi nezkušené před takovýmto kro-
kem, líčí obtíže, ale jest též stale dobrým rádcem
těch, kdež se již jednou k tomu odhodlali. Mně sa-
motnému prospěly „Osady“ velice, byvše mým pouč-
ným čtením po celou dobu plavby. Bohužel nedostává
se krajanu našemu té podpory a ohledu, jakého by
zasluhoval, jmenovitě v Čechach, kdež z neznalosti
věcí jest velmi málo odporučovan a naopak podpo-
rovány na úkor jemu firmy cizácké. Výhoda těch,
kdož se naň obrátí, pakjest samozřejmáanelze toho
ani dosti odporučiti. Ceský vystěhovalec nalezne
v něm muže poctivého, který jej uvaruje mnohého
vydání a mnohých nepříjemností, cesta z Hamburku
jest asi stejně drahá. jako odjinud, lodě stejně dobré
—- pokud mohu souditi dle oné, kterou mi bylo jeti,
docela pohodlné, s dobrou stravou, která se na ham-
burských lodích zvláště chválí, a konečně poskytne
p. Pastor každému rady ipomoci, daje mu potřebná.
poučení, adresy a pod. Jeem přesvědčen, že činím
jen dobře, snaže se jej odporučiti, vždy a všude podle
zásluhy jen, přeje mu úplného, lepšího ještě zdaru
a dlouhého působení v důležité úloze, v níž by byl
těžko nahraditelným pro svou znalost poměrů a pro
poctivé, české jednání. Tlumočím tím věrně mínění
své! Stěhování jest nutným u nás zlem, nuže hleďme,
aby zlo to bylo co možná nejmenší, když je podstou-
piti již musíme a zde je příležitost dána !“ (Poznámka:
Měsíčník „Ceské Osady v Americe“ pro nedostatek
účastenství zaniknul. Ročníku čtvrtého dal jsem vy-
tisknout větší náklad, jenž se nyní cestujíČímzdarma
rozdává. J. P.)

Kdyby člověk věděl, že je obrazem božím, byl by jím. —— Co nás
mrzí, to nás drží, a co ješt milo nám, to nechce ]: nám. — Mou-
drým ten se darmo zove, kdo neumí radit sobě. — Děravého mě-
chu nenadmeš a hlupce nepoučíš. — Komu není 53 hůry dáno.
v apatyce nekoupí. — Obyčejný rozum v neobyčejné míře nazý-
váme rozšafností. — Trpělivost je ctnost ale jen do jistých mezi.

Z mých cestujicich podaly dvacet tři osoby do
veřejnosti následující

osvědčení:

„My nížepsaní vystěhovalci jeli jsme do Ameriky
prostřednictvím svého rodáka, pana Josefa Pastora
v Hamburku, a byli jsme s jeho dopravou a s jeho
obsloužením tak dalece dobře spokojeni, ale ani ve
snu by nám nebylo napadlo tyto řádky psati, kdyby
nám na lodi a pak v Novém Yorku a při jízdě do
Chicaga nebyly bývaly ohledně jednaní pana Pastora
otevřeny oči. My myslili, že pan Pastor nepočíná, si
ani hůře ani lépe ve svých jednatelskýchpovinno—
stech, jako každý jiný jednatel, a nebyli bychom—si
věci dále povšimli, kdybysme se nebyli sešli s ji-
nými Čechy jak na lodi tak v Americe, s nimiž jsme
se smluvili, jak každému se vedlo a jak každý byl
obsloužen. Poroynávali jsme všechny jednotlivosti
a teprve tímto-porovnáváním nabyli jsme přesvěd—
čení, že pan Pastor nejedna s vystěhovalci co zisku—
chtivý obchodník , nýbrž co pečlivý otec, poctivý
a svrchované svědomitý lidumil a horlivý národovec.
Tuto pravdu dosvědčil nám každý americký občan,
s nímž jsme o panu Pastorovi mluvili, což je už do—
statečný důkaz, an by se p. Pastor tak chvalné po-
věsti “v Americe netěšil, jakou zde všeobecně požívá,
kdyby věc se neměla tak, jak udávame. My se totiž
na lodi sešli ještě s jinými Čechy, kteří jeli jiným
prostřednictvím, a v Novém Yorku sešli jsme se
opět s Čechy, kteří přijeli z jiných přístavů a s kte-
rými jsme jeli společně do Chicaga. Měli jsme proto
tu nejlepší příležitost všechno vyzvědět, a výsledek
byl tento: Kdo jel dle rozpisu p. Pastorem cestuji—
címu zaslaného, dostal se do Hamburku bezpečně
beze všeho strachu a bloudění , a uspořil naproti ji-
ným asi 8 zlatých. Kdo poslal své kufry a bedny,
jak pan Pastor každému v dopise radí, 14 dní na-
před —co obyčejný naklad, uspořil na 100 kilech asi
10 zlatých a neměl po železnici žádných starostí- se
svými věcmi. Mnoho Čechů tahalo všechen svůj na-
klad sebou a měli velikých nesnází 'a'trampot až
k nevypsani a naplatili se jako mezkové. My byli
odporučením pan-a Pastora ubytování v hostinci ma-

lém, ale slušném, a platili jsme po 90 kr. ve třetí
třídě a, po 1 zl. 50 kr. ve druhé třídě denně za'noc-
leh a stravu, kdežto jiní v jiných hostincích platili
daleko víc a nebyli tak vlídně obslouženi jako my;
hostinští jim k tomu ještě vnutili všelijaké zbytečné
tretky, jako deky, rum, ocet, mýdlo, kapky atd. za
drahéňpeníze, tak že vydali mnoho peněz, kdežto
nam hostinský nevnucoval pranic, bezpochyby rovněž
nařízením pana Pastora, tak že„ jsme neplatili než
jen za byt a stravu a zajisté ani o krejcar více než
kdybychom snad přenocovali v některěm hostinci
v Čechách. My měnili u pana Pastora peníze; on
nám počítal, že musíme dát za. 1 americký dollar
“2 zl. 45%3 kr.; my chtěli smlouvat a byli bysme ho
skorem rozhněvali; on povídal, že nebere užitek
žadný; my mu ovšem nevěřili, až jsme se od 5 olu-
cestujících dozvěděli, že musili platit za 1 (follar
2 zl. 50 kr., 2 zl. 52 kr. i také 2 zl. 54 kr.; někteří
měnili v Praze za. 2 zl. 53 kr. Tu teprv jsme po—
znali, že nám p. Pastor dal dollar o 4, 6 až 8 kr.
laciněji nežjiní bankéři. I převoz jsme měli laci-
nější; o mnoho to ovšem nebylo, ale lacinějším byl.
Tolik můžeme říci, že se rozpočet, kter' nám pan
Pastor zaslal, na krejcar shodoval až do Chicaga,
kdežto u jiných neshodoval se od 10 do 30 zl.,
když jsme pjočítali celou cestu z Čech do Chicaga.
Nejvíce se Ceši neplatili tam, kde Se jim jízda za
nejlacinější vydávala.

Jednomu vystěhovalci, jenž si také s p. Pastorem
dopisoval, jenž ale pak se dal přemluvit jinam, že
prý je to lacinější a lepší, přišla jízda o 42 zl. draže,
než jak počítal ajelikož měl pouze 40 zl. na útratu,
musil pro 2 zl. zastavit u jednatele svůj kufr! Nas
chtěli doma různí jednatelové, jinak pořádní a dů—
věryhodní mužové, od p'. Pastora odvrátit, ale jak
jsme byli tomu rádi, že jsme jinak nejeli. Těmto pánům
upřímně radíme, aby lidem do Ameriky cestujícím
odporučovali především pana Pastora, chtějí-li se jim
zavděčit. Pan Pastor nás dal na expressní loď, kteráž
nás dovezla z Hamburku do Nového Yorku za týden,
kdežto ti, co jeli přes jiné přístavy také o rychlo-
lodích, potřebovali přes vodu 10 až 14 dgn. Cesta.
mořská není právě příjemná. Nám na štěstí brzo

utekla a již osmý den jsme byli v Novém Yorku,
kde jsme přenocovali v kulaté budově, jmenované
„Castle Garden“. Poslechli jsme p. Pastora, jenž—nas
napřed varoval, nechoditi v Novém Yorku do. žáde
ného hostince, kdežto mnozí “iní Ceši měli odporu-
čení na hostinské a dali se od nich odvésti. To jim
ale přišlo hezky draho; dollary jen lítaly, kdežto
my v Castle Gardenu přespali zadarmo. Když jsme
na americké železnici tak vsecko zpytovali, tu litovali
ostatní, že nejeli prostřednictvím p. Pastora, jenž
nám ke všemu ještě dal na památku pěkně vázanou
velkou knihu a sice ročník IV. jeho „Ceskych osad
v Americe“, v níž jsme po cestě četli a seznali ame-
rické poměry. Také jsme se z té knihy učili anglicky,
a dostali jsme také ještě česko—anglický slovník,
který nám při kupování potravin a nápojů v Americe
znamenitě posloužil. Jak nyní ze zkušenosti víme,
jsou obě tyto knihy pro novácky v Americe učiněným
zlatem, ač jsme z počátku o ně příliš mnoho ne-
dbali, a můžeme tvrdit, že stojí pro každého vystě-
hovalce za sta a tisíce, kterému na tom záleží, aby
se do amerických poměrů brzo vpravil, na čemž,
jak každý uzná, právě všechno záleží. My'doznávame,
že všickni ostatní vystěhovalcj, at jeli kteroukoli
stranou a jakýmkoli prostřednictvím, zaviděli nám
skvělý dar řešených dvou knih od pana Pastora a li-
tovali velmi, že nejeli prostřednictvím jednatele jako
my, nebo by byli tyto dvě nad míru užitečné knihy
také obdrželi darem a byli by bývali o hromadu pe—
něz zištnější. My se zmiňujeme o této záležitosti
hlavně z dvoupřičin: předně, aby vystěhovalci vě—
děli, co to za člověka ten pan Pastor, jenž mnoho
hluku nenadělá; imyjsme zpočátku byli na. omylu:
on nás arcit vlídně přijal, poučoval nás o Americe,
těšil naše manželky, které měly z jízdy strach, ale
nechtěl o slevení ani slova slyšet, když jsme počí-
nali smlouvat. Kdyby nám byl převozné o pětku vy—
dražil a pak dva neb tři zlaté slevil, aneb kdyby
nám byl za dollary počítal po 2 zl. 50 kr. a byl nam
pak po dvou krejcařích slevil, byli bychom, bývali
rádi a velice spokojeni, poněvadž jsme my Čechové
smlouvání z domova uvykh', ač bychom byli bývali
předc ošizení; že ale pan Pastor žádal na nás ceny

tak nízké, že z nich skutečně nic sraziti (se nemohlo,
to nám, upřímně řečeno, v neznalosti věcí nebylo
vhod, ale když jsme se přesvědčili, jak poctivě a své—
domitě se 's námi jednalo, tu máme za svatou povin-
nost, dáti to do veřejnosti, a prosíme české listy za
otištění, aby náš lid do Ameriky se ubírající'věděl,
na. koho se obrátiti má, nechce-li přijíti 'ku žádné
škodě, chce-li jeti po dobrýchlodích a chce-li obdržeti
na cestu knihy, z nichž se přes moře tolik naučí,
jakoby v Americe alespoň už deset let trávil,

-A za druhé myslíme, že i celé české „obecenstvo
má, býti upozorněno na muže, kterého právem pro-
hlásiti dlužno za otce, řítele a ochránce českého
vystěhovalectva, o jehož Ihmotné i duševní blaho on
se stará. všemožně a vytrvale, už o dlouhou řadu
let, At se jenom' uvaží, že, jede-li (fo Ameriky ročně
z Čech a z Moravy 8000 osob a každá osoba ušetří
prostřednictvím p, Pastora dle naší zkušeností asi
25 zl., české vystěhovalectvo ročně 0 dvakrátstotisíc
zlatých více do Ameriky přiveze, a že předběžné
poucování českých vystěhovalců o Americe, které
pan Pastor ve svých spisech lidem zdarma podává,
v hmotnosti ještě asi desateronásobně ceně se rovná.
Toho at si naše obecenstvo dobře povšimne, a čeští
subagenti a dohazovači po českomoravském venkově
at utíchnou pomluvami niěimž než jen nahým chle—
bařstvím čili tak zvanou konkurencí vyvolanými proti
muži, jenž pro dobro vystěhovalců více koná nežli
kdokoli jiný. My _proto českým a moravským vystě-
hovaleům s dobrým svědomím odporučnjeme pana
Pastora. jsouce na základě vlastní zkušenosti pře-
svědčeni, že lepšího opatření a lepších výhod na žádné
jiné straně není a také býti nemůže.

V Chicagu, 28. června 1889.

Následují podpisy.

OBSAH.

Str.
Úvod.3
CojeAmerikaacojsouSpojeněStáty............4
Do kterých přístavů Spojených Států severo-amexfických se j ezdí? 5
Zápisky.....................-..,_.,.'5
Přípravy .zw5
Zlaté kníhyna cestu do Ameriky. . . . . . . . '. . i . .10
Poptávky.........................._12
Poštovní zásylky z Rakouska. do Hamburku. . . . . . . . . „12
Poštovní cenník Spojených Států . . . . . . . . . . . . . . .13
Poštovní cenník z Rakouska do Ameriky . . . . . . . . . . .14
Frankovánídopísů......................15
DoAmerikysepřepracovat.................15
Mořenezamrzne.......................15
Vystěhovaleckýagent....................16
Volbapřístavu......................_.v.18
Jos.?astorvHamburce................'.*..20
Máadresa 22
PastorovapísámavHambul-ku................22
Český paroplavebníjednatel . . . . . . . . . . . . . . . . .28
Pojišťovánímístanalodi.....“..............25
Dopravadítek........................26
Lodnízápisy ...27
Rózvrh železniční jízdy evropské . . . . . . . . . . . . . . .28
Převozníceny........................29
Slevování..........................30
Povrchní rozpočet jízdy ku př. z Prahy do Chicaga. . . . . . . 32
Vzdálenosti.........„...............83
Vedlejšíútratypocestě...................33
PříjezddoI-Iambxuku..................'..34
Hostinechamburku....................34
Zpráv-yopřijetílodinamísm................35
Svobodnýnáklad 85
Mirabedenakufrů.....................37
Znamenáníkufrůabeden..................37
Zasýlánínákladu ..38

Str.
Opozděnénáklady........A..............40
Kupování převozních lístků v Americe . . . . . . . . . . . „40
Výměnapeněz.........................42
Vystěhovalecký úřad v Hamburku . . . . . . . . . . . . . .43
Legítimace.............'.............44
Sedánínaloď........................45
Dvoušřoubová expressní parolod' (vyobrazení) . . . . . . . . „46
Parolodč ...47
Rychlostlodí .50
Mezípalubí ............;........,...50
Stravanalodivmezipalubí . . . . . . . . . . . . . . . . .51
Mořskánemoc.....................L..53
Přimženíloděkbřehu....................53
Ústavy-o ochmnn přistěhovalectva v Novém Yorku . . . . . . 54
Komu je ve Spojených Státech přístup zakázán? . . . . . . . 55
ŽelezničnídopravavAmeríce . . . . . . . . . . . . . . . .57
Mějtesenapozorupocestě . . . . . . . . . . . . . . „60
Vystěhovalecký dům v Novém Yorku »Leo House:; . . . ._ . .61
Smlouvy mezi Spojenými Státy americkými a Rákousko-Uherskení 62
Americkčpenízc,míryaváhy . . . . . . . . . . . . . . . KGS
Krejcarová pásmová sazba na c. k. rakouských státních dráhách 64
Pásmová sazba. na uherských státních drahách,“ ná uh. severo-
vvých. dráze a na. košicko-bohumínské dráze . . . . . . . . .65
Ceské časopisectVo ve Spoj. Státech . . . . . . . . . . . . .65
Mzdyve Spojených Státech . . . . . . . . . . . . . . .66
Jakje draho ve Spojených Státech? . . i . . . . . . . . . .67
Kdečechovéusazenijsou . . . . . . . .. . . . . . . . ...68
Národní jednota. sokolská ve Spojených Státech ' . . . . . . .72
Přistěhovalectvo Spojených Států . . . . . . . . . . . .73
ObčanstvíSpojenýchStátů. . . . . . . . , . . . . . . . . „74
Největší města Spojených Států. . . . . . . . . . . . . . . .77
ObyvatelntvoSpojenýchStátů . . . . . . . . . . . . . . . „78
Americkédrožky 80
Dodávání poštovních zásilek . . . . . . - . .' . . . . . . . .81
České knihy clu nepodléhají . . . . . . . . . . . . . . . . .82
Časovýrozdílš...........-...........483
Búdoucnost české národnosti v Americe . . . . . . . . . . . . 84
Cís. král. rakouští konsulové ve Spoj. Státech a v Kanadě . . . 86
Hlavní pravidla stručně opakovaná . . - . . . . . . . . .87
Vcřejnéhlasy......................„.88
Osvědčení..........................91

## Josef Pastor v Hamburku.

Podobizna tato sleduje účel, aby po cestě a v přístavě nikdo nedal se másti různými náhončími, kteří k vůli přimámení našeho lidu rádi i za jiného se vydávají; neboť již často se stalo, že cestující přijedše do Ameriky dommívali se, že vypraveni byli mým prostřednictvím aneb že prý u mne bydleli, kdežto ani v mé pisárně nebyli a u mne bydleti nemohli, jelikož já sám žádným hostinským nejsem, nýbrž pouze paroplavebním jednatelem.

s SEFF; *AST _ 3
Ě *: —„ O fécýw *— , O 5
Ě _ 1 =: f'- jJ * a : Š
„Ě 33 ŠÍJ : $ ?? J'WČ 13 Š
g = x ' ., „ l : Š
- Ě 5 ; " [ 9 “55 $“? » ' ;
b: : © “*f, v „Miš—.lt *? “*
“ Š “! " ©“ 2“ ŘÍM —wi'i “Ý %
ga ' .? „$$$ AN © x a
š » a * v w *'5 Wa ] ) a ' * »

645 (©) ' C) “ © "“"2- “' * JANÉ“ „?“-Al uvi'- ] š
* as „„   ď' GQ „gf-sůwwxaÉ—m! „$- “XX (© ' O á š
_ uv * :, 1“—“= © 43, Z. 3
s ' . . „a ' 7' lla—x*:— '“ f:“v, Na; šat; 'Í L*sa *
„- %?É í 'Šb ! “%d gg * * » i
,: am ursko „l;—Ě; paroplavbuy; a;
page éru: oa *
% ***“ ,.,- „___ » .-._„„„__ l
Š Doprava přes moře tak dobrá, že lepší není nikde jinde. Příprava na cestu a poučení
Š co nejdokonalejší. Obsluha vlídná, vlastenecká a úplně nezištná. _É
=* ,—
Širší—3í Ww— 52%;

„„ ——————— ws
FB . 99
m ——oÝo ' 1 az
ggl o 1 3 9 2 54 IŠi
5.7.4 ;á-ř if _Á-ř if„fwrw: CD—
%“ ' % *f*—*_— 9 v :“
g: :; Leden. 9“ Unor. 99 Brezen. %;
„ v_ř_í:H——F
až; Neděle 3'191724319 791421289 613 2927 gg
v? Bandai 4111825 91 81522299 7142128 „- r.-
533 Uterý 5121926 929 91623 91 8152229 ==
a; Středa. 6132927 31917 24 92 91623 39 ga.
:'g Ctvrtek 714 21 28 *411 18 25 9319172431 of
55 Pátek 1 8152229 95912 19 26 14111825 a.:
„53 Sobota. 2 91623 39 „6913 29 27 995121926 ŽŠ
; = ;_ *; *** „:::Í ***—**?va %
1—1 v 99 v V WU“
% f 9 Duben. 99 Kveten. „ Cerven. ŽŠ
=; Neděle * 31917 24 1 815 22291 59121926 ez:
gg Bonděli 4111825 2 916 2339 6132927 ___4
;; ťterý 5121926 319172431 7142128 25.
g * Středa 6132927 94111825 1 8915 2229 ___—_;
gg Ctvrtek 7142128 „5 1219 26 2 9916 23 39 %s:
5-5 Pátek 1 815 22 29 :6 13,29 27 319.17 24 .;
aš.— Sobota 2 916 23 39 371492128 9411918 25 až
m: g_i—Př“— *f*—_9mý7 g,.:
%; 9 Cervenec. 91 Srpen. 99 Zam. ;;
; 41::::%;;;;11Í» rifffff: Í:;9;í;%„f _; .
:: Neděle 9 3919172493191 9 714 21289 41118925 gi
mg Ronděli 9 49111825. —1 815 22 299 591219926 m5
“a v Uterý : 591219 261 2, 91623 39 6.13 2927 5:3
51 “ Středa 9 61329279 139191724319 79142128 gag
až Čtvrtek 9 791421289 949111825 91 815 2229 H—c
53 Pátek 1 89152229; 59121926 92 9,16 23 39 37%
g :: Sobota 92 991623309 96913 2927. 993919917 24 315
2“ az? „ÍWW“ 9řff9—h ' Š'
QE Rijen. 99 Listopad. 99 Prosmec. = %
,.____._ ýgřlíwva—Á.rřv9řrí';::; %
Í; Neděle 2 9162339 61329279 4111825 SŠ“
„2% Bondělí 319172431 71421289 5121926 EF
; % Uterý 41191825 91 81522299 6132927 g
=,; středa * 51291926 92 91623 3399 7142128 mg
že: Gtvrtek 613920 27 13191724 91 815 22 29 gg;
=*; Pátek 7142128 4111825 92 9162339 9 3
Sobota 91 8152229 35121926 319172431 2:
N „__—_" * h . HS

První česká fieme ze heen'eeemi zeležená ve dvee dííeehďsvěte.
v HAMBURKU g v NEW-YORKU

23 kleine Reichenstrasse. [ 1432, I. Avenue.

První česká fieme za hranicemi z—eležená ve evee dílech světe.

Znamenání kufrů a' beden.

Každý kufr a balík, každá bedna a taška
atd., jež pošlou se napřed bud' poštou neb dráhou
na adresu Jos. Pastor, No. 23 kleine Reichen-
strasse, Ham burg,f nechť. se znamená dvěma
písmeny a jednou číslicí. Za písmena nejlépe
třeba volit začáteční písmena jmena a příjmění
vlastníkova a za číslici číslo domu, V kterém
vlastník posledně bydlel. Majitel by sejme—
noval ku příkladu Václav Čech a bydlel by
V čísle 31. Svou_ bednu neb kufr dal by proto
znamenati: V. C. 31. Kdyby měl zavazadel
více, řekněme, čtyry kusy, dal by první zna-
menati V. O. 33, druhé V. Č. 32, .třetí ve.
33 a čtvrté V. O. 34; Kdyby pak o svých za—
vazadlech na mne pan Václav Čech psal a je
jednoduše V. 0. 31—34 jmenoval, Vím ihned,
která zavazadla to. jsou a mnoho-li kousků.
Napíše—lí však skoro jeden každý na kufr jenom
adresu Pastorovu, pak mám zde mnoho kufrů
a beden 'se stejnou adresou a rozeznání spůsobí
obtíží. Kdo proto dá znamenat svá zavazadla
jak uvedeno dle svého jmena a příjmění a dle
čísla domu, .může spoléhat, že budou Vždy
snadno k rozeznání a že on sám označení čili

:).
markovám svých beden a kufrů nezapomene.
Dejme tognu, že by kufr (Václava Čecha, ozna—
čený V. O. 31, někde uváznul a že by se musil
hledati. Podle znaku se lehko vynajde, ale dle
popisu, že je černý, velký, malý atd. těžko;
neb v niagacíneeh leží často sta a tisíce kufrů
a beden.
Mějte se na pozoru po cestě.
Svým cestujíČímuvádím ve známost, že ani
v Lipsku ani v Berlíně a vůbec “po celém Ně—
mecku nikoho nemam, jenž by mne zastupoval.
Kdo proto za mého zastupce po cestě se vydává,
činí tak neopravněné a jeho úmysl samozřejmě
dobrým není. Na nádražích číhají na nezkušené
vystěhovalee všelijacínáhončí všelijakých firem.
Mějte ,se před nimi na pozoru, byt' by i česky
mluvili. Oni jsou Vám cizýmí a V y jim také.
Proč na Vás číhají? Zajisté jen za příčinami
- sobeckými, ba snad i vydřidušskými. Nesvěřujte
jim žádné peníze. nechoďte nik-ani s nimi a
nedávejte jim žádné adresy svych znamych,
kterých chteji jen zneužití. Nenechte se od nich
vyptávat. Raději je odkopněte. Ku postrašení
žádá mnohdy tato čeladka od cestujících pasy
a vyhrožuje policií. Nebojte se. V Německu
se Vám nic nestane. Naopak, zavolejte na ob-
těžující Vás dotíravce Vy strážníka a uvidíte,
jak hbitě dobroclineěek vezme do zaječích.

3
Filialka v Novém Yorku.
Abych našemu _.vystěhovalectvu na obou
stranách moře jak radou tak skutkem přispívati
a česko-americkému obecenstvu obchodní styky
se mnou zároveň usnadniti mohl, otevřel jsem
ve květnu 1890 ňliální závod V Novém Yorku
a kojím se:-nadějí, že nejen krajané opouštějící
svou Vlast, aby usadili se za oceanem, nýbrž
i američtí Čechové cestující na podívanou do
Evropy, při kupování svých přeplavních lístků
za podmínek co do jakosti lodí a cen docela
stejných, rozhodně dají přednost jednateli své
národnosti, jenž nový podal důkaz o neunavné
a obětavé činnosti ve službě cestujícího přes
moře obecenstva českého.
V Hamburku, v květnu 1890.
Jos. Pastor.

4
Verejna osvedceni.

My nížepsani vystěhovalci jeli jsme do Ameriky pro-
střednictvím svého rodáka, pana'Josefa Pastora v Ham-
burku a byli jsme s jehodopravou a s jeho obsoužením
tak dalecedobře spokojeni, ale ani ve snu by nám ne-
bylo napadlo tyto řádky psati, kdyby nám na lodi a pak
v Novém Yorku a při jízdě do Chicaga nebyly bývaly
ohledně jednani pana Pustora otevřeny oči. My myslili;
že pan Pastor nepočína si ani hůře ani lépe ve svých
jednatelských povinnostech, jako každý jiný jednatel, a
nebyli bychom vsi věci daleřpovšimli, kdybysme se nebyli
sešli s jinými Čechy jak na lodi tak v Americe, s nimiž
jsme se: smluvili,'jak každému se vedlo a jak každý byl
obsloužen.-' Porovnávali jsme všechny jednotlivosti a
teprvé tímto porovnáváním nabyli jsme přesvědčení, že
pan Pastor nejedná s vystěhovalci co získuchtivý obchod-
nik, nýbrž co pečlivý otec, poctivý a svrchované svědo-
mitý lidumil a horlivý národovec. Tuto pravdu dosvědčil
nam každý americký občan, s nimž jsme o p. Pastorovi
mluvili, což je už dostatečný důkaz, an by se p. Pastor
tak chvalné pověsti v Americe netěšil, jakou zde vše-
obecně požívá, kdyby věc se neměla tak, tak udavame.
My se totiž na lodi sešli ještě s jinými Čechy, kteří jeli
jiným %rostřednictvim a v Novém Yorku sešli jsme se
opět s echy, kteří přijeli z jiných přístavů a s kterými
jsme jeli společně do Chicaga. Měli jsme proto tu nej-
lepši příležitost všechno vyzvědět a výsledek byl tento:

Kdo jel dle rozpisu p. Pastorem cestujícímu zasla—
ného, dostal se do Hamburku bezpečně beze všeho
strachu a bloudění a uspořil naproti jiným asi 8 zlatých.
Kdo poslal své kufry a bedny, jak pan Pastor každému
v dopise radi, 14 dní napřed co obyčejný naklad, uspořil
na 100 kilech asi 10 zlatých a neměl po železnici žádných
starosti se svými věcmi. Mnoho Čechů tahalo všechen

5
svůj náklad sebou a mělixvelíkých nesnazí a trampot až
k nevypsaní a neplatili sejako mezkové. My byli od—
porušením pana Pastora ubytování v hostinci malém, ale
slušném, a platili jsme po 90 kr. ve třetí třídě a po
1 zl. 20 kr. ve druhé třídě denně za nocleh a stravu.
kdežto jiní v jiných hostincích platili daleko víc a nebyli
tak vlídně obsloužení jako my; hostinšti jim k tomu
ještě vnutili všelijaké zbytečné tretky, jako deky, rum,
ocet, mýdlo, kapky atd._ za drahé penize, tak že vydali
mnoho peněz, kdežto nám hostinský nevnucoval pranic,
bezpochyby rovněž nařízením pana Pastora, takže jsme
neplatili než jen za byt a-stravu a zajisté „ani,o krejcar
více než kdybychom snad přenocovaliv některém hostinci
v Čechach. My měnili u pana Pastora peníze; on nam
počítal, že musíme dát za 1 americký dollar 2 zl. 45% kr.;
my chtěli smlouvat a byli bysme ho skorem rozhněvali;
on povídal, že nebere užitek žádný; my mu ovšem ne-
věřili, až jsme se od spolucestujících dozvěděli, že musili
platit za 1 dollar 2 zl. 50 kr., 2 zl. 52 kr. i také 2 zl.
54 kr.; někteří měnili v Praze za 2 zl. 53 kr. Tu teprv
jsme poznali, že nám p. Pastor dal dollar o 4, 6 až 8 kr.
laciněji než jiní bankéři. I převoz jsme měli lacinější;
o mnoho to ovšem nebylo, ale lacinějším byl. Tolik
můžeme říci, že se rozpočet, který nám p. Pastor zaslal,
na krejcar shodoval až do_Chicaga, kdežto u jiných nee
shodovalvse od 10 do 30 zl., když jsme počíta-li celou
cestu z Čech až do Chicaga. Nejvíce se Češi naplatili
tam. kde se jim jízda na nejlacinější vydávala. Jednomu
vystěhovalci, jenž si také s p. Pastorem dopisoval, jenž
ale pak se dal přemluvit jinam, že prý je to lacinější a
lepší, přišla jízda o 42 zlqdraže, než jak počítal ajelikož
měl pouze 40 zl. na útratu, musil pro 2 zl. zastavit
u jednatele Svůj kufr! Nas chtěli doma různí jednatclové,
jinak pořádní a důvěryhodní mužové, od p. Pastora od»
vratit, ale jak jsme byli tomu rádi, že jsme jinak nejeli.
Těmto pánům .upřímně radíme, aby lidem do Ameriky
cestujíČímodporučovali především pana Pastora, chtějí-li
se jim zavděčit. Pan Pastor nás dal na expresní loď,

6

kteráž nás dovezla Z'Hamburku do Nového Yorku za
týden, kdežto ti, co jeli přes Brémy po rychlolodi, po-
třebovali přes vodu 10 dní, a ti, co jeli přes Antwerpy,
potřebovali 14 dní. Cesta mořská. není právě příjemná.
Nám na štěstí brzo utekla a již osmý den jsme byli
v Novém Yorku, kde jme přenocovali v kulaté budově,
jmenované »Castle Gardena. Poslechli jsme p. Pastora,
jenž nás napřed varoval, uechoditi v Novém Yorku do
žádného hostince,“ kdežto mnozí jiní Češi měli odporučení
na hostinské a dali se od nich odvésti. To jim ale přišlo
hezky draho; dollary jen lítaly, kdežto my v Castle
Gardenu přespali zadarmo. Když jsme na americké že-
leznici tak všecko zpytovali, tu litovali ostatní, že nejeli
prostřednictvím p. Pastora. jenž nam ke všemu ještě dal
na památku pěkně vázanou velkou knihu a sice ročník IV.
jeho »Českých— osad v Americes, v níž jsme po cestě
četli a seznali americké poměry. Také jsme se z té
knihy učili anglický, a dostali jsme také ještě česko-
anglický slovník, který nám při kupování potravin a. na—
pojů v Americe znamenitě posloužil. Jak nyní ze zkuše-
nosti víme, jsou obě tyto knihy pro nováčky v Americe
učiněným zlatem, ač jsme z počátku o ně příliš mnoho
nedbali, a můžeme tvrdit, že stojí pro každého vystěho-
valce za sta a tisíce, kterému na tom záleží, aby se do
amerických poměrů brzo vpravil, na čemž, jak každý
uzná, pravě všechno záleží. My doznáváme, že všickni
ostatni vystěhovalci, ať jeli kteroukoli stranou a jakým-
koli prostřednictvím, zaviděli nam skvělý dar řečených
dvou knih od pana Pastora a litovali velmi, že nejeli
prostřednictvím jednatele jako my, nebo by byli tyto dvě
nad míru užitečné knihy také obdrželi darem a byli by
bývali o hromadu peněz zištnější. My se zmiňujeme
o této záležitosti hlavně z dvou příčin: předně, aby vy-
stěhovalci věděli, co to za člověka ten-pan Pastor, jenž
mnoho hluku nenadělá; i my jsme z počátku bylina
omylu:. on nás arciť vlídně přijal, poučoval nás o,Ame-
ricě;ii£ě'šilw naše manželky, které měly z jízdy strach, ale
nedhtělhcmslevení ani slova slyšet, když jsme počínali

!
smlouvat. Kdyby nám byl převezné o pětku vydražil a
pak dva neb tři zlaté slevil, aneb kdyby nám byl za
dollary počítal po 2 zl. 50 kr. a byl nam pak po dvou
krejcařích slevil, byli bychom bývali rádi a velice spo-
kojeni, poněvadž jsme my Čechové smlouvání z domova
uvykli, ač bychom byli bývalí přede ošizeni; že ale pan
Pastor žádal na nás ceny tak nízké, že z nich skutečně
nic sraziti senemohlo, to nám, upřímně řečeno, v ne-
znalosti věcí“ nebylo vhod, ale když jsme se přesvědčili,
jak poctivě a svědomitě se s námi jednalo, tu máme za
svatou povinnost, dáti to do veřejnosti, a prosíme české
listy za otištění, aby náš lid do Ameriky se ubírající
věděl, na koho se obrátiti má, nechce-li přijíti ku žadné
škodě, chce-li jeti po dobrých “lodích a chce-lí obdržeti
na cestu knihy, z nichž se přes moře tolik naučí, jakoby
v Americe alespoňiuž deset let trávil. A za druhé my-
slíme, že i celé české obecenstvo ma býti upozorněno na
muže, kterého právem prohlásiti dlužno za otce, přítele
a ochrance českého vystěhovalectva, o jehož hmotne
iv duševní blaho on se stará všemožně a vytrvale už po
dlouhou řadu let. vAt' se jenom uváží, že, jede-li do
Ameriky ročně z Čech a z Moravy 8000 osob a každá
osoba ušetří prostřednictvím p. Pastora dle naši zkuše-
nosti asi 25 zl., české vystěhovalectvo ročně 0 dvakrát-
stotisíc zlatých více do Ameriky přiveze, a že předběžné
poučování českých vystěhovalců o An erice,v které pan
Pastor ve svých spisech lidem zdarma podává, v hmot-
nosti ještě asi desateronásobně ceně se rovná. Toho at'
si naše obecenstvo dobře povšimne, a čeští subagenti a
dohazovači po česko-moravském venkově ať utichnou po-
mluvami ničímž než jen nahým ohlebařstvím čili tak
zvanou konkurencí vyvolanými proti muži, jenž pro dobro
vystěhovalců více kona nežli kdokoli jiný. My proto
českým a moravským vystěhovaloům s dobrým'svědomím
odporně-ujeme pana Pastora jsouce na základě vlastní
zkušenosti přesvědčeni, že lepšího opatření a lepších vý-
hod na žádné jiné straně není a také býti nemůže.

Následují podpisy.

8
Josef Pastor v Hamburku.
Podobizna tato sleduje účel, aby po cestě a v přístavě
nikdo nemohl býti matenvšibalskými náhončimi, kteří
k vůlí přimámeni a vykořistěni našeho lidu rádi za
jiného se vydávají.
%