| me in 2000 about 2020:                                       | me in 2020:                                                                                              |
|--------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| We'll have flying cars!                                      | We have cars that brick in underground garages because they lost connectivity and can't update their OS. | 
| We'll have colonies on Mars (or at least on the Moon).       | We can fly a toy helicopter on Mars for a few seconds.                                                   |
| We'll have the Theory of Everything, physics will be solved! | We have 1 pixel photo of a black hole.                                                                   |
| Computers will be so fast and run everything instantly!      | Web pages load slower than old computers booted up.                                                      |
| We will have cured cancer.                                   | We have controversial vaccines that allow you to go to work.                                             |
| Video games will be perfected to flawlessness.               | Games released in unplayable state are the standard.                                                     |
| The art of movie making will be close to perfect.            | Enjoy soulless remakes ruining old classics with forced inclusivity and political propaganda.            |
| Everything will be wireless with batteries lasting forever!  | Phones last one day on battery.                                                                          |
