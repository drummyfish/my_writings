# Rock Carved Binary Data

WORK IN PROGRESS

*version 0.1*

*by drummyfish, released under CC0 1.0, public domain*

Rock Carved Binary Data (RCBD) is a writing format meant for simple recording of small sized binary data on physical media such as paper, wood, stone or plastic, possibly for communication or backups.

Why does this exist? Let's ask ourselves -- as a civilization based on information processing, what information will we leave behind? Compared to old civilizations probably almost none. Data stored with today's overcomplicated computer technology will sooner or later disappear, that's a fact. Data stored on paper is better, but still the commonly used paper probably won't last very long, everything is made as cheaply as possible without ever considering long time durability. While we sacrifice a lot of memory capacity by choosing to carve data in stone, many times a very important information is extremely small, such as a date of an event, number of inhabitants, name of a man, mathematical equation, a short poem etc.

## Details

The format aims especially for the following:

- Simple manual carving, i.e. data characters have to be simple, consisting only of straight lines and as few of them as possible.
- Being somewhat robust, i.e. characters shouldn't contain details that could, along with the information they bear, easily be distorted by weathering.
- Being human readable, i.e. not requiring precise measuring instruments or computers.

## Specification

The data is recorded on a data sheet as a sequence of characters, all of the same size, none of which is blank, which are read left to right and top to bottom.

The first character is the *format marker*. It is different from the proper data characters and serves as a kind of "magic number", its purposes are following:

- Hint on the fact that RCBD is used so that when someone sees the data, he knows how to decode it. For this the character should be a bit unique.
- Show the data sheet orientation so that when the sheet is e.g. rotated or even mirrored, the original orientation can still be recovered. This is important for the correct order of data characters as well as their correct decoding. For this the character mustn't be symmetric in any common way.
- Record the specific *encoding* of the data, i.e. say what the recorded bits actually mean.
- Be simple and friendly to deciphering.

The format marker can appear again in the data which marks a change to different encoding.

The special character in its base form looks like this:

```
 _____
 _____
|
|
|
```

The encoding is given by a binary number encoded by adding additional lines `|`, `-`, `\` and `/`, as shown here:

```

 _____   _____   _____   _____   _____   _____
 _____   _____   _____   _____   _____   _____
|       |  |    |       |  |    |\_     |\_|
|       |  |    |-----  |--+--  |  \_   |  \_     ...
|       |  |    |       |  |    |    \  |  | \

   0       1       2       3       4       5      ...

```

Meaning of encoding numbers are following:

- 0: 7 bit ASCII text
- 1: 8 bit UTF8 text
- 2: 7 bit RCBD common text (see later)
- ...
- 8: 4 bit binary data
- 9: 6 bit binary data (corresponds to base 64)
- 10: 7 bit binary data
- 11: 8 bit binary data
- ...

One *data character* can in its base form record a 7 bit number, but can also be extended to record 8 bits for formats that require it. A data character is never blank.

The first (least significant) bit of a data character is encoded with a diagonal line, either `\` for 0 or `/` for 1 (this is so that the character can't be blank). The following bits are encoded by drawing other lines over this base bit; here each line encodes another bit either by its absence (0) or presence (1). The lines are, in order from the least significant: left vertical, top horizontal, right vertical, bottom horizontal, middle vertical, middle horizontal. If 8 bits are needed, the 8th bit is encoded by a "roof" (`/\`) above the character. The following are examples of binary numbers encoded with data characters:

TODO: hmm, doesn't e.g. |/ look too similar to \|?

```
                                          _.'._
                                         '_____'
 \_          _/   |\_      |   _/        |  |_/|
   \_      _/     |  \_    | _/     ...  |--+--|
     \    /       |    \   |/            |/_|__|
 
   0        1       10       11         11111111
```

It is recommended to also attach a decoding table alongside the encoded data to help with deciphering.

## RCBD Common Text Encoding

RCBD common text encoding is one of possible text encodings, made specifically for RCBD. It tries to optimize and make it easy to record a common text with few symbols made of few lines. In this encoding the following are meanings of character values:

NOTES: We suppose the letter frequencies EAINORSTLDUMCPHGKVBFZYWJQX (from most common to least, averaged over approx. a dozen big languages).

```
0000000 E  \
0000001 A   |
0000010 I   |
0000011 O   | pairable
0000100 N   | letters
0000101 R   | (most frequent)
0000110 S   |
0000111 T  /

0001000 L
0001001 U 
0001010 M 
0001011 V
0001100 C 
0001101 B
0001110 F
0001111 Q
0010000 D
0010001 P
0010010 H
0010011 Z
0010100 G
0010101 Y
0010110 W
0010111 X
0011000 K
0011001 J
0011010 /
0011011 '
0011100 *
0011101 (
0011110 )
0011111 ;
0100000 space
0100001 .
0100010 ?
0100011 -
0100100 ,
0100101 +
0100110 =
0100111 !
0101000 0
0101001 1
0101010 2
0101011 3
0101100 4
0101101 5
0101110 6
0101111 7
0110000 8
0110001 9
0110010 <
0110011 newline
0110100 >
0110101 %
0110110 &
0110111 \
0111000 :
0111001 @
0111010 "
0111011 |
0111100 ~
0111101 {
0111110 }
0111111 switch case (upper is default)
        paired letters:
1000000 EE
1000001 EA
1000010 EI
1000011
1000100
1000101
1000110
1000111
1001000
1001001
1001010
1001011
1001100
1001101
1001110
1001111
1010000
1010001
1010010
1010011
1010100
1010101
1010110
1010111
1011000
1011001
1011010
1011011
1011100
1011101
1011110
1011111
1100000
1100001
1100010
1100011
1100100
1100101
1100110
1100111
1101000
1101001
1101010
1101011
1101100
1101101
1101110
1101111
1110000
1110001
1110010
1110011
1110100
1110101
1110110
1110111
1111000
1111001
1111010
1111011
1111100
1111101
1111110
1111111 TT
```