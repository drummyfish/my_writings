# Consciousness Solved

*Miloslav Číž (drummyfish), 2020, released under CC0 1. (public domain)*

This is how I solve the much debated philosophical questions regarding consciousness, e.g. *what is consciousness*, *where does it come from* etc.

## What is consciousness?

Consciousness is just a word with many wider or narrower meanings depending on the context, just as most other words. It usually means something akin an *awareness* or *internal experience of the world* that is present in brains of living beings. There are two important aspects to consciousness:

1. We, i.e. each individual person, usually see our own consciousness as given, we don't need to prove it, we simply "feel" or "know" it, which is why we are beginning to ask questions about it in the first place. 
2. We **assume** other world entities that show similar behavior to us have the same consciousness, i.e. if it *looks alive like us*, it surely must have very similar internal experience of the world. So we e.g. assume that other humans or "more developed" animals are conscious, unlike rocks or calculators.

## Ad 1

Own consciousness we simply have to take for a fact, an axiom without a proof, just as we take for a fact that the Universe exists -- it is *simply there*.

But why am I in my brain and not in someone else's? For the same reason you live in your house and not in your neighbor's. 


## Ad 2

There is no way to gain a proof or accurate explanation of internal experience of others, so the only way to decide whether something else than ourselves is conscious is **from its behavior**.

Here it is important to realize that **consciousness is not a binary thing**, i.e. just being present or not being present. We judge the presence of consciousness based on the similarity of (usually emotional) behavior to our own behavior. For example seeing *something* show signs of pain makes us believe it is conscious, as we behave that way and show the same reactions to harm.

Now let's take a look at following questions:

- **Are other humans conscious?** Most certainly yes, because the behavior is identical to ours.
- **Are cats conscious?** Very certainly yes, because their emotional behavior is extremely similar to ours, even if they lack e.g. the ability to appreciate music.
- **Are bugs conscious?** Here the answers will probably start to differ. We see reactions to pain, to other members of its species, something resembling *fear*, but these are somewhat different and some higher emotions such as guilt seem to be missing altogether.
- **Are plants conscious?** Many will say they are not, but plants actually do react to their environment, they try to achieve goals, spread, reproduce, heal. They are just very slow, mostly stationary and quiet, behaviorally different from human.
- **Are cells conscious?** There are single-cell organisms that are considered alive, but can they be conscious? They do react to their environment, try to preserve themselves, but the behavior is extremely different from ours and most of us will say they can't possibly be conscious like we are.
- **Are rocks conscious?** Absolute majority will answer no here. But rocks do react to their environment, e.g. if a small rock is kicked, it will fly away. This is an extremely simple reaction, but it is now important to realize this is in principle a reaction happening due to laws of physics just as a bug's reaction to harm is a reaction based on laws of physics (chemistry, electric signals etc.), ultimately being principally the same as processes that happen in our brains.

From this it starts to be clear that consciousness of others is not a yes/no attribute, but rather a **spectrum**. We could intuitively capture this by saying that a human is 100% conscious, a cat 90% (because of seeming to be very limited in higher experiences such as humor), a plant 10%, and a rock 1%.

An individual human experiences changes in consciousness, for example when he goes to sleep. In sleep, we are somewhat aware of our surroundings, we may recall events from when we were asleep, like sounds or smells, but we are much *less* conscious in that we are e.g. unable to make complex reasoning. We can make an assumption that due to simplicity of a bug's nervous system it may be at a similar level of consciousness at which we are when we are sleeping.

As consciousness grows higher, a system becomes capable of abstract reasoning, having a sense of self, empathy, realization of its own mortality, the ability to understand its own internals etc. However, these are just attributes that come with greater computational power and aren't anything special that would be required for general consciousness -- after all, our brains are still not able to completely understand how they work, even if we are conscious.

The above percentages try to capture the *complexity* of inner experience, but in fact this spectrum is **multidimensional**. There exist systems, such as our galaxy, with more complex inner processes than happen in our brains, which however don't resemble our (human) behavior and so we don't see them as conscious or even alive. I.e. a galaxy contains more complex processes than our brains, but we don't perceive them as conscious just because their behavior (for example just the time scale the processes happen in) is so vastly different.

In this sense, **everything is conscious**. Any physical system has a consciousness which results in a behavior, which is always just a more or less complex manifestation of physical laws. Consciousness is a point in multidimensional space. The closer the point is to the point representing our own consciousness, the more likely we are to judge the system as conscious. However, there is no objective criteria for consciousness, every one is equal, and it is just us who measure the degree of consciousness relatively to ourselves.

Even subsystems of conscious systems are themselves conscious. A nation, composed of conscious humans, is itself a conscious system that has its own behavior, makes decision and reacts to the world. A man with clothes is a separate conscious entity from the naked man that's underneath the clothes.

The famous **Chinese Room** is conscious too. It is just "blindly" following and algorithm to look up answers, but so do our own brains. You can't possibly know what inner experience the room's algorithm gives it.

# Sum Up

Own consciousness just exists and has to be accepted as an axiom. Consciousness in another world entity is not binary but a spectrum, a degree to which its behavior resembles our own. **Consciousness doesn't arise**, it is always present, it just gets closer or further away from our own, and may be more or less complex.
