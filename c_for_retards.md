# C for Retards

This is the best tutorial of programming and the C language.

# What's Programming, C etc.

Programming is creating computer programs. Normally computers (their CPUs) only uderstand programs in a special machine binary format (1s and 0s) that is difficult for humans to read, write and work with. On the other hand computers can't understand human language well. Therefore, for the ease of programming, we have invented so called **programming languages** – a middle way between the computer and human language. A programming language looks a bit like a human language, but is much simpler so it's not difficult for humans to learn and computers can understand them well.

TODO algorithm

C is a programming language. It is the best programming language, used by professionals, invented in 1972, in a time before capitalism infected computer technology too much. It's been well established and tested by time. All important software is written in C. It is not the easiest language, but it's extremely powerful, fast, and you need to learn it.

C is defined by a standard, and so there is actually multiple C languages. We're going to use the best standard here, from 1999, called C99.




# First Program

We're going to write a simple C source code of a program, compile it to an executable binary program and finally run it.

If you just want a quick and easy environment for this, search the web for *"C compiler online"* and you'll find websites that let you write, compile and run C programs in your browser. The following paragraphs describe a normal offline setup (which you will sooner or later need).

Firstly you need an editor to write the program with. It must be a **plain text** editor, something aking a generic *notepad* application will suffice. You must **not** use an editor that formats text, such as an office writer. It is best to use an editor that can highlight programming languages – the best one is Vim, but now you're probably too dumb to use it, so try something like Gedit or Geany. Do not use any bloated or proprietary editors (usually called IDEs).

Secondly you need a **compiler** – the program that translates the file with your C program to an actual executable program you can run. The standard compiler is gcc (GNU Compiler Collection), which is typically installed by default on most Unix operating systems (such as GNU/Linux or BSD). It can also be easily installed on inferior systems such as Windows, but if you're using Windows, you should just go uninstall it right now. There also exist other C compilers, such as clang or tcc.

Now open up the text editor and write the following source code in it.

```
// first program

#include <stdio.h>

int main(void)
{
  printf("it works");
  return 0;
}
```

If you're in the web application, simply click something like *run* or *compile*. Otherwise save the file and name it `first.c` (C source code files have `.c` extenision). Then open up a command line of your operating system, in it navigate to the folder with the program (e.g. with the `cd` command) and then compile the source code by writing

```
gcc -o first first.c
```

If you get errors or warnings, you probably made an error while typing that program – just check and correct it. If the command finished silently, there will appear an executable program named `first` in the folder (a command such as `ls` will display the files in the folder). You can now run the program from the command line, e.g. with

```
./first
```

and the program will print out

```
it works
```

You've done it. Now let's quickly analyze the program, without going too much in depth:

- `// first program` is a comment, it doesn't affect the program in any way, it's just there to hold comments in the code for people. Anything after `//` until the end of line is ignored by the compiler.
- `#include <stdio.h>` is a special command that loads an official `stdio` (*standard input/output*) library. This will later allow us to use the command for printing text.
- `int main(void)` is a main "entry point" of the program, we will learn more about it later. Let's now only remember that after this we'll write the actual program commands (program body) between the `{` and `}` brackets.
- `printf("it works");` is a command that will print `it works`. (The `printf` command is a so called *function* made available by the `stdio` library loaded above.) Note that this type of command is alawys terminated by `;` (semicolon).
- `return 0` is a command that's we'll almost see at the end of the program. It tells the operating system that the program has ended correctly (`0` is a code for "OK").

# Good Practices and Advice

- **Minimalism** is the most important thing, never make your program more complex than it needs to be.

# Cheatsheet





