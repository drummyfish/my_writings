UPDATE: this guy here explains it:
  https://invidious.fdn.fr/watch?v=QkWT-xMTm1M
  then
  https://invidious.fdn.fr/watch?v=F0b8b_ykPQk&listen=false

WIP

# Arrow of Time, Consciousness etc.

For the purpose of this exploration let's assume a simple, deterministic universe with linear (non-relativistic) time as our model (regardless of how real universe really works). We don't suppose any "flow" of time, just a universe with spatial dimensions plus a temporal one. Let's also discard any notion of free will as it doesn't make any sense. Brains to us are just computers processing data. Basically let's not complicate this.

Firstly let's examine a question which will later lead to interesting discoveries. The question is

**Why do we remember the past but not the future?**

At first sight it seems that a brain at a certain point in time is able to remember the past but not the future. Or, equally, it seems to have good, accurate information about the past but not about the future. We call the ability to clearly see in the past "remembering" and the ability to imperfectly see the future "predicting".

But is it really so? It is not like we can see ALL past clearly, neither that ALL future is blurry and unclear. There are cases where we:

- See future extremely clearly, e.g. we can very accurately predict where the Moon will be 24 hours from now or that a falling rock will hit the ground.
- See the past extremely unclearly, e.g. prehistory, but also events that happened seconds ago but somewhere where they weren't recorded by anyone.

So really, both the future and the past can exhibit clarity and blurriness to us. 

Some might say that what we've seen with our own eyes, what we remember, is 100% guaranteed to have happened while a prediction, no matter how accurate, may also be incorrect because "future didn't happen yet". However, there is nothing rational about this argument, some future predictions (e.g. that the Sun will rise tomorrow) may statistically be more reliable than memories (a schizophrenic person may vividly remember things that didn't happen, and it's more likely that one is schizophrenic than the eventuality that the Sun will not rise tomorrow).

So with this notion that our brain has clear and unclear visions of both future and the past, why is it that we

1. Are able to distinguish which direction is future and which is past?
2. Seem to perceive ourselves "moving" in time from past towards the future and tend to thing the past is given while future is not?

Ad. 1: some might give the typical answer that "entropy is increasing" to explain how we orient ourselves in time. However, let's imagine an environment without any changing entropy, e.g. a cubic room with a single bouncing ball in it. If we're observing this room, will our brain get confused? It is true that when watching this on video, we wouldn't be able to tell if the video is being played forward or backwards as the laws of motion are time-symmetric. However, seeing a ball bouncing in the room, our brain will at any given instant be able to predict where the ball is going to move next, without being guided by any entropy. How do we do this?

An immediate answer may appear to be e.g. a motion blur which tells us where the ball is coming from. But what if there is no motion blur? If the ball is moving so slowly that the blur is so small that we aren't able to perceive it. Well, what ultimately guides us is our memory. We don't need a motion blue to tell us where the ball was a second ago because we can see where the ball was a second ago in our memory. And indeed, i.e. the retaining of what we've experienced, is what tells us, at any given instant, where the future and past is.

Ad. 2: this is the more difficult question. It seems that it's thanks to our memory, i.e. the ease to quickly, easily and reliably see our own personal past, that we think of seeing the past as "given". Memory is easy, it doesn't require much energy and calculations to deduce the picture of the past from it: it is pretty straightforward to deduce from and excited neuron on the retina that a while ago there must have been an object in front of the retina which reflected a photon that excited the neuron. With future we have nothing like this, our predictions of the future require energy, calculations, speculation, dealing with chaos,... and perhaps due to this unreliability and an elevated need of focus on the future we perceive the future as something that's "not decided", which is likely to be changed by something we couldn't account for in our calculations simply because of the complexity of the task. In any case, it will probably never be able to prove anything about our subjective perception. However, an important question now arises that can have an objective answer:

**What is memory, and why does it only work for the past?**

For simplicity (and without the loss of generality), let's suppose our memory only consists of neurons on the retina: if a photon hits the neutron, it's excitation level is set to 10 and this excitation gradually comes back to 0 with time. A photon exciting a neuron on retina, and this being "remembered", can be illustrated by this illustration:

```
              \photon
               \
                \
retina    0 0 0 10 9 8 7 6 5 4 3 2 1 0 0 0                         
---> time                  A
```

We can see that our brain at moment can easily deduce that 5 time units ago there was an object in front of the retina.

What would an equivalent of this "remembering of the past" be for the future? Let's just reverse the illustration:

```
                                       /
                                      /
                                     /photon
retina    0 0 0 0 1 2 3 4 5 6 7 8 9 10 0 0                         
---> time                 B
```

In this case, a photon would start to gradually get excited and when it reached excitation level 10, it would shoot out a photon that would then somehow be guaranteed to hit an object in the real world. I.e. our retina, our mind, would no longer be the observer of the world, but its creator. We would think something up, kind of "remember the future", and then it would just happen. Note that this is actually what we do with our bodies and what we might call the free will: we simply think of moving our arm, and in the next instant it does move. Perhaps this is where the illusion of free will comes from.

So, did we just prove we actually have a memory for the future, a "free will"? Perhaps, but this kind of memory is limited to our own bodies that we "control". The classic memory, the memory for the past, works for the external world, as well as for our bodies. And so still, the past seems to be "easier" and so we are forced to be constantly focusing on the future more.

But why is it that the memory for the future ("free will") is only limited to our bodies? Why does the traditional memory work for "everything"? In other words, why doesn't the external world  from the future affect our brains in the present the same way the past does?

Thinking about it, it may just be an assymetry illustrated again by an illustration:

```
                 \photon
                  \
                   \
retina       0 0 0 10 9 8 7 6                          
---> time      A        B
```

At moment B we can remember (see) the photon in the past thanks to the lasting excitation on the retina, while at moment A we cannot foresee that a photon will hit the retina. This is caused by the assymetry of the excitation function: at the moment the photon hits the value is 10 and it extends to the future, not in the past. Therefore, from the future, we can make this backwards connection (remembering), while from the past we can't.

But why is the function assymetric like this? Well, I think the answer to this may now really be entropy. It dictates that it's extremely unlikely for photons to start building on the retina by chance and then group up into a single photon shooting out towards an object, but it is extremely likely that if a neuron gets excited by a reflected photon, it will start gradually radiating energy to all directions and diminish its excitation.

But why is entropy like this?...
