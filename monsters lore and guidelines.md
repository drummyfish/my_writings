**This is an early preview WIP document, don't take it as finished in any way.**

# MONSTERs: Lore and Guidelines

*by Miloslav Číž*

*version TODO*

*released under CC0 1.0 (public domain)*

This is an official lore and guidelines document for MONSTERs -- public domain battle MONSTERs. It comes as an extra resource accompanying the set of MONSTERs, and outlines the default **vanialla** lore. It can be used as a **framework** for the creator's work if they don't want to put energy into creating their own and/or wants to have ready answers and explanations, stay consistent and compatible with other works that use this default framework.

As MONSTERs (including this document) are completely public domain under CC0 and not protected by copyright, you can freely modify the lore and/or guidelines and/or the MONSTERs. If you decide to divert from this official world, it is highly recommended that you clearly indicate it in your project at least in its title, as this will prevent confusion and is in everyone's interest.

If you use MONSTERs, please consider sharing your derivative work under CC0 as well -- or at least under a free as in freedom license, such as CC-BY. This is not required, but it is my (the author's) wish. Sharing enriches our culture and unleashes the full potential of our creativity.

# General

MONSTERs are obviously inspired by and very similar to Pokémon, but there is a great number of differences.

The general mood is **peaceful, mostly bright, optimistic and child-friendly**, but the works should be at least equally enjoyable by adults as well. The setting is lightly futuristic and sci-fi -- with everything looking mostly like the present real-world with added clean hi-tech devices here and there. The presented values are aligned with the ideas of **free culture, pacifism, veganism, anarchism, socialism**, but **not** with pseudo-leftist movements such as feminism, LGBT, politically correct etc. Critical, independent and hacker thinking is an important value as well, so mathematical references are very welcome.

Any creator can of course break some of these recommended rules on purpose if they feel like exploring a new concept, and create e.g. a dark-mood, adult-only work.

# World

**People and MONSTERs live on a planet that is not our real-life Earth**, but appears very similar to the point of being practically indistinguishable, except for the geography. The planet has about the same size and gravity as our Earth. It can be referred to as *Earth*, but it's better to use the word *planet* to avoid confusion with our Earth. The planet has different history and geography from our Earth and is located in another part of the Universe, which may possibly be a separate Universe altogether. These details are not further specified.

These specifications imply **there must be no specific references to events, locations, people, brands or companies that appear in our real-world**. However, **elements similar to these can appear** -- there can e.g. be a city resembling New York or a drink resembling Coca-Cola, it just must not be named so and has to have enough differences to be clearly distinguishable as a different thing. This policy also helps protects the derivative works of MONSTERs from infringing on or being limited by intellectual property.

There are no other known planets with life.

**There is one sun, one moon and many stars in the sky**, just like on real-life Earth but again, though called Sun and Moon (with capital S and M) and looking like out real-life equivalents, they are different. Further details about other planets and heavenly bodies are not specified and references to equivalents of named planets (other than Earth), such as Mars, should be avoided. Time measurement, cycles (day, month, year, ...) and durations are the same as on real-life Earth. Units of measurement (e.g. kilometers vs miles) can be chosen depending on the language of the specific work.

**The planet has one big ocean**, referred to simply as *the ocean*. There is also a north and south pole, making the navigation basically the same as on Earth.

**It is not specified how many continents there are** -- each work from the MONSTERs universe can create a new continent or island or another kind of place on the planet at which it will take place. This allows many different MONSTERs works to coexist on the planet without interfering with each other.

# People and Society

The human civilization is a **close-to-ideal communist society**. The **government's role is extremely small**, being very close to **anarchist society** -- there is no single political leader of the world, nor any ruling parties, but there are still individuals considered authorities in many areas, e.g. important scientists, philosophers, MONSTER trainers etc. Though these people typically have no power by legislation, others naturally listen to their advice and recommendations. **Politics is practically never discussed** because it isn't an issue, thanks to people being intelligent and the society having had evolved into an ideal society naturally and effortlessly as a result. There are no serious social issues in the world such as wide-scale poverty or extremist terrorism.

**People don't have to work** and don't have any jobs, all necessary work is taken care of by a highly advanced technology (the word *job* still does exist). People are free to do whatever they like and most of them do some kind of regular activity that resembles today's jobs, e.g. build structures, create art, teach at schools and so on.

**Technology is at a level far ahead of our time**, roughly comparable to a few hundreds of years in the future. **There is no concept of intellectual property** in the world, so everyone is using only free software and hardware, though there is no need to call it that way. So when making a reference to free culture, refer to concepts, not terms like *free software* whose existence would be unjustifiable.

**The society is pacifist** and there are **no guns** or similar weapons intended to hurt or kill people or MONSTERs. There are indeed tools such as knives that can in theory be used as a weapon, but they're purpose is always to serve as a tool useful to living beings. People -- just as MONSTERs -- can die, which can be discussed in a children-friendly way.

There exist tools that serve to pacify occasional directly dangerous individuals (people or MONSTERs) -- **lunatics** -- and so protect the society, but these mustn't hurt the victim, only temporarily immobilize it, knock it unconscious etc. MONSTERs can help with this as well, thanks to their strength. These tools are used very rarely, only when there is no other way. Pacified individuals are then transferred to a kind of detention, where they can do no harm. **This isn't a prison, i.e. a punishment**, only a way to prevent harm to society, and the isolated individual is kept in a nice environment with access to all luxury that normal people have.

**Police** exists, but is in principle different from our real-life police -- they don't have a monopoly on the use of force (that is the above mentioned pacifying tools). They are only a **group of volunteers**, wearing (typically blue) uniforms, who take care of helping people stay safe -- mostly they just walk around, warn people of nearby dangers, offer assistance, give directions etc. Police often uses the help of MONSTERs. Similarly there exist ambulances, fire fighters and so on. However, **there is no army**, since its purpose is just war and oppression -- an evil not present in the MONSTERs world.

**People do not eat MONSTERs** -- everyone is a vegetarian. Eating meat would be perceived the same way in which we view cannibalism in our world.

There is **only one widely used language: the human language**. In specific works this language will mostly take the form of English, but **should not be called English** as this can become problematic if the work is translated. Use the term *human language*. Other languages can appear as well, but again should not be associated with any real-world language (such as French or Spanish) -- a new name and form should be invented.

**Money exist but they're role is limited.** It is not perceived as capital, only convenience tokens for the exchange of things. Hoarding money and gaining profit from business is seen as immoral, as is for example lying, and people don't want to have anything to do with it. Furthermore there is only very limited number of things that can be bought with money and therefore there is no incentive to hoard it -- keep in mind that in an ideal society basic things like food are available to anyone for free and there is no need to be selling it -- so the things being sold are mostly limited to some luxury items and very limited resources. **There is only one widely used currency, called simply coins** (even if it is represented by e.g. bills, i.e. not actual coins).

**Different races of people exist** and are basically the same as in our world -- white people, black people and so on. However, the races are **not** referred to by the area of origin, since these areas don't exist -- so e.g. the words *Asian*, *African* and *European* do not exist. Instead, if needed, the race is simply referred to by skin color or another significant attribute, e.g. a *black* man or a *slant-eyed* woman. The society is **not** politically correct and referring to someone's differences is not seen as "offensive". That would be silly.

# MONSTERs

**There exist many undiscovered species of MONSTERs** beyond the set that comes with this work, which represents only a subset of MONSTERs that are known and documented by humans.

Each MONSTER species consists of a **combination of several possible body part types**. These parts also determine the name of the species. In principle, this works like this: let's say we have body parts B1, B2 and B3 (e.g. body, head and tail), each of which has possible types, e.g. B2 (head) has possible types B1a, B1b, B1c etc. (different types of head). There are specific rules to which parts can be combined. A specific species can then be defined e.g. as B1bB2aB3b. Specific body parts of the individual MONSTER can be trained and permanently change ("evolve", improve) into other parts, e.g. a body type B2a will improve into a better body type B2b once it has been trained enough (e.g. has taken enough hits and so got stronger): this will results in a new name of the monster species. This mechanic makes it possible to have a lot of monster species without creating each one "manually", and will also save the space in a video game as MONSTERs will be generated "procedurally".

**MONSTERs generally posses a very high intelligence** comparable to humans. However they are still of animal nature, often preferring instinct in their decisions and usually requiring more freedom to roam free. They are **unable to speak but able to learn to understand human language** to a big degree. **MONSTERs make various sounds** depending on species, just like real-world animals. They have complex emotions and naturally show empathy.

**There are no animals** known from our world in the MONSTERs's world. MONSTERs are the equivalent of animals and besides MONSTERs and humans there are no high-level organisms in the world. The word *animal* does exist, but it is used very rarely and is an equivalent to the word MONSTER, though often being a pejorative (e.g.: "he behaves like an animal").

**Common trees and plants of our world are present** in the MONSTERs's world, as well as other lower-level organisms, such as viruses (people and MONSTERs can catch cold, but specific viruses like HIV don't exist).

**MONSTERs never kill or seriously injure other MONSTERs** on purpose -- they are very peaceful in nature and don't have a reason for being violent as there are enough resources in the wild and **there are no carnivores**, even if some MONSTERs look like it. Darwinian evolution doesn't necessarily apply in this world.

However **MONSTERs enjoy being competitive** and do fight for fun as long as it part of a game. They are much more resilient than humans and can easily withstand hits that could seriously injure humans and don't even mind minor injuries, which in them heal more quickly and don't cause much pain.

MONSTERs can indeed get seriously injured or sick and can die, and they also die of old age. This can be mentioned and discussed as death is a natural thing, but should be done in a children-friendly way.

# Taming (Catching) MONSTERs

Most MONSTERs naturally enjoy being around humans, playing and collaborating with them. This includes many activities such as helping them with their work, being their pets, but a particularly common reason is to be a part of a MONSTER battling team (see below).

Though it may not always be the case, most **MONSTERs prefer to be tamed** (or *caught*) in order to became connected to humans, because of their animal-like nature. **The word "tame" is preferred from "catch"**, to distinguish MONSTERs from other popular franchises that use the latter.

There is a particular way in which MONSTERs are most often tamed:

1. A human encounters a wild MONSTER, which signals it is willing to be tamed by not running away, but instead awaiting a MONSTER fight.
2. The human calls their already tamed MONSTER to battle with the wild MONSTER.
3. The two MONSTERs fight and if the human's MONSTER is victorious, leaving the wild MONSTER exhausted, the human TODO.

Most MONSTERs born and living in the wild are familiar with this procedure, because they're intelligent to comprehend it and other MONSTERs, such as their parents, are able to communicate this tradition to them. Therefore a lot of young MONSTERs strive to be caught and prepare for that day. But they don't want to be caught just by anyone -- because they want to be a part of a strong team led by a strong trainer, they will fight hard and only let themselves be caught by a trainer whose MONSTER they won't be able to defeat.

As indicated, **not all monsters may want to be caught**, which they simply signal by **running away from humans**. This may be e.g. because they're too young, not feeling ready, or they simply dislike the particular human. Humans don't catch or battle with these monsters -- that would be considered cruel and immoral.

# MONSTERs Battles

As mentioned above, MONSTERs enjoy voluntarily taking part in **friendly** (no deaths or serious injuries) competitive battles with other MONSTERs. Though they often do this on their own in the wild, many of them **especially like being a part of a small team led by a human trainer** -- this model is called **MONSTERs battling** and is the most popular **sport** in MONSTERs universe, similarly to e.g. football in our universe -- football and other sports also exist in MONSTERs universe, but are not as popular.

Though there may exist specific variations of the standard model of MONSTERs battling, the **classic model** is the most prevalent. These are the rules of the classic model:


- Two teams stand against each other in a **battle**. Each team consists of **one human trainer** and **up to five MONSTERs** tamed by the trainer. The battle consists of **one-on-one fights** between MONSTERs of both trainers. The one-on-one fights take place until
  1. all MONSTERs of one team are defeated or
  2. one team's trainer gives up -- this team then loses while the other one is considered the winner. Or
  3. both trainers agree on a draw (e.g. if the battle takes too long), in which case there is no winner.
- At the start of the battle, each trainer decides -- without knowing the decision of the other trainer -- which monster they will release first. They then release the monsters to start the first one-on-one fight. It is **not** possible for a trainer to change their choice of the first MONSTER once they see which monster the other one has chosen.
- Each one-on-one fight is fought until
  1. one of the two MONSTERs is defeated -- either becomes exhausted or gives up by backing up to its trainer -- in which case the defeated monster's trainer decides which of their MONSTERs will continue the fight -- if they have no undefeated monsters left, they lose the battle. Or
  2. a trainer decides to withdraw their MONSTER from the fight and replace it by another of their team's MONSTERs, which doesn't count as a defeat of the MONSTER, so it can later continue to fight in the same battle again. Or
  3. the battle ends by one player giving up or both players agreeing on a draw.
- Though the physical fights are fought only between two MONSTERs, the trainer can be, and often is, involved by speaking to its MONSTER -- they can give specific commands, but the involvement can also be much lighter, e.g. just giving advice on tactics or just cheering up the MONSTER. The trainer may even decide to give a completely free hand to the MONSTER.