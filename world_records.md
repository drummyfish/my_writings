# Drummyfish's Book Of World Records


- **Smallest chess program**: 1257 non-blank characters of C code, Toledo Nanochess by Oscar Toledo, close second Micro-Max with 1433 non-blank characters of C code. In terms of compiled size, LeanChess seems to be the smallest with 288 bytes of IBM PC AT machine code.
- **Loudest sound heard**: 310 dB, eruption of Krakatoa, 1883. The Krakatoa volcano in Indonesia erupted so strongly it could be heard 5000 km away, ruptured eardrums of people 64 km away. The pressure wave was recorded to have traveled round the globe 3 and a half times. The tsunami killed more than 36000 people.
- **Highest multiple birth**: 9 (nonuplets). Several cases of 9 children being born are known, among them: 1971 Australia (5 boys, 4 girls), 1999 Malaysia (5 boys, 4 girls) and 2021 Morocco (5 girls, 4 boys).