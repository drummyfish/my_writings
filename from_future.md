# WIP

*by Miloslav Číž (drummyfish), released under CC0 1.0 (public domain)*

One day I found a file on my computer that wasn't supposed to be there. Upon opening it I've found it held a very peculiar content.

In plain text there stood a message I couldn't believe at first but grew very excited about very soon. It was rather a long, well written text by someone who introduced himself only as "my future friend". The person went on to claim a very astonishing fact that he was writing from far future and provided a number of predictions of future evens as a proof. All of these prophecies (involving such diverse topics as weather, sports and politics) soon came fulfilled and I, thanks to having the necessary knowledge of science and statistics, have excitedly verified the proofs as valid.

At the end of the message he instructed me to leave a reply for him in the same file and that he'd respond soon. To my immense surprise the next day I really did find a new text in the file and so we started to communicate.

Of course I was a skeptic but played along so that I could find out more. I wanted to know all about him and how he was able to speak to me. About himself he only told me he was male who with enthusiasm studied history (i.e. our present) and our language so that he could carry out this communication. He told me that he was there only to tell me about the nature of the future world and didn't even disclose the precise year he was living in. The mechanics through which he communicated was supposedly enabled by his ability to manipulate cosmic radiation to precisely change the content of my computer's memory, but he told me no more than that he would be ready to answer questions about the future of out society and that he would like me to share our conversation with general public, if possible. To comply with his wish, a copy of our dialog follows.

---

**Me**: My first question has to be "is your society living in peace?"

**Him**: Yes, we are living in almost absolute peace and that is partly why I decided to contact you.⁠ I want you to not lose hope as I know lot of wars and violence are present in your time. But the belief in what you still call a utopia is a necessary step towards social progress.

Let me tell you that 

---

**Me**: How do you ensure that your leader don't abuse their power?

**Him**: Society is like a human –⁠ when it is born, it needs an authority, a parent to tell it what's good and bad, to punish and reward for specific behavior, to lead by the hand. Dictators, kings, laws, religions, all of these are needed until society grows up and becomes adult. Adults know what's good and bad and thanks to gained responsibility and wisdom no longer needs a leader.

My society has already grown up. We no longer have any leaders. People are no longer led by people but by pure ideas. We have our wise, our experts, well known individuals, but our people never allow anyone to think for them. In fact, no one wishes for this power as everyone knows that is not good.

---


**Me**: Thank you for explanation. Could you please tell me how advanced is your technology?


--

**Me**: But people need at least some struggle! With everything solved, no shortage and nothing to do people lose their purpose, the meaning of life, they get sick and depressed. How can you exist with no work to do?**

**Him**: But it is not the case that there is nothing to do, most things just don't need to be done and we don't force people to labor. We leave necessary unpleasant work to machines and give people the freedom to choose whatever sense they want to give to their lives. People enjoy meaningful activity and there is great diversity in what they enjoy – art, science, medicine, inventing, even manual work and maintenance of machines is something people do willingly, doubly so when you remove the stress of employment and salary.

---

**Me**: So what am I supposed to do now?

---