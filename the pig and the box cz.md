# O Prasátku a Bedýnce

Bylo nebylo, v malém domečku na venkově žilo samotné staré prasátko jménem Prasátko. Jako spousta jiných prasátek, ani toto nemělo věcí nazbyt. Občas dokonce mělo nouzi i o jídlo. Byl to vepřík bručoun.

Jednoho rána našlo Prasátko na zahradě převeliký tuřín.

"To je ale ohromný tuřín," pomyslelo si. "Pokud vím, žádný tuřín jsem nesadilo. Ani takto veliký. Musím ho hned vytáhnout."

A tak prasátko tahalo a škubalo, a znovu tahalo, až najednou... **RUP!** Tuřín vystřelil ze země a rozpůlil se!

Uvnitř tuřínu byla dřevěná bedýnka.

"To je ale prapodivné místo ke schování bedýnky," řeklo prasátko. "Většinou skladujeme tuříny v bedýnkách, ne naopak. Kdo může být tak hloupý, aby dával bedýnky do tuřínů?"

Prasátko se rozhodlo, že do benýnky bude dávat mrkve. Jednu mrkev vytrhlo ze zahrádky a hodilo ji do bedničky.

A tu najednou něco úžasného! Bedna přepadla a z ní na zem vypadlo DVACET SEDM MRKVÍ!

Prasátko si pomyslelo: "Jsem si dost jisté, že jsem tam zrovna nehodilo dvacet sedm mrkví. To celé dává asi takový smysl jako kraví baletka."

Prasátko se muselo přesvědčit, že neblázní. Pečlivě do bedny hodilo jen JEDNU bramboru. V tu ránu se bedna převrhla a NA ZEM SE VYKUTÁLELO DVACET SEDM BRAMBOR!

Prasátko se zaradovalo. Ta bednička byla kouzelná! Mělo takovou radost, že začalo skotačit jako orangutan! A hned vědělo, že nikomu nedovolí bedničku používat, protože ta patřila jenom *jemu*. Ostatní by beztak chtěli kopírovat jen spoustu zapáchajících věcí jako stará vejce nebo česnek.

A tak prasátko dalo vedle bedničky ceduli:

**KOUZELNÁ BEDNIČKA**

**JENOM PRASǍTKO SMÍ POUŽÍVAT**

**BĚŽTE PRYČ**

Potom šlo a odneslo si svých dvacet sedm mrkví a dvacet sedm brambor do domečku.

Než však stihlo zavřít dveře, přiběhla potrhlá veverka a začala poskakovat jako by měla v kalhotách včely.

"Lízátko Jones!" křičela veverka.

"Prosím?" zeptalo se Prasátko.

"Lííííízátko Jones!" odpověděla vevrka a tak Prasátko zabouchlo dveře a šlo povečeřet.

**BUM BUM BUM BUM BUM!**

Někdo bušil na dveře až prasátko málem vyskočilo z kůže. Doběhlo ke dveřím, otevřelo a venku na schodech stála kačenka jménem Kačenka.

"Ráda bych si půjčila tvou kozelnou bedničku, prosím," řekla Kačenka, "chtěla bych okopírovat jablko a zaplatím ti jeden zlaťák!"

"Dobrá, dobrá," řeklo Prasátko.

Šli spolu ven a Prasátko nechalo Kačenku hodit do bedničky jedno jablko. Bednička se převrátila a NA ZEM VYPADLO DVACET SEDM JABLEK!

"To mě tedy podržte!" řekla Kačenka.

"Ale počkat!" řeklo Prasátko, "na copak ta jalka potřebuješ? Kačenky jablka *nejedí*."

"No—" chtěla vysvětlit Kačenka.

"Ty LŽEŠ!" zakřičelo Prasátko a podalo z domečku kbelík.

"Ta jablka dej sem, " řeklo Prasátko, "To je *kouzelný* kbelík. Pokud se z něj někdo kromě TEBE pokusí vzít jablko, všechna jablka **VYBUCHNOU!**"

"Ale ale ale..." řekla Kačenka.

"Běž pryč!" zakřičelo Prasátko, "vůbec ti nevěřím!"

Poslalo Kačenku pryč s kýblíkem plným jablek a vrátilo se domů, kde našlo přetékající hrnec.

**BUM BUM BUM BUM BUM!**

Někdo bušil na dveře až prasátko málem vyskočilo z bot. Doběhlo ke dveřím, otevřelo a venku na schodech stál malý roztomilý králíček jménem Králíček.

"Lízátko Jones!" zakřičel Králíček.

"PROSÍM?" řeklo Prasátko.

"Netuším, říkala to potrhlá vevrka," řekl Králíček. "To je fuk, chtěl bych si okopírovat banán pro babičku, prosím! Taky jsem ti přinesl zlaťák!"

"Dobrá, dobrá," řeklo Prasátko.

Šli spolu ven a Prasátko nechalo Králíčka hodit do bedničky jeden banán. Bednička se převrátila a NA ZEM VYPADLO DVACET SEDM BANÁNŮ!

"To je paráda!" řekl králíček.

"Ale počkat!" řeklo Prasátko, "na copak ty banány potřebuješ? Králíci banány *nejedí*."

"Ha, tomu nebudeš věřit, " začal Králíček.

"Ty LŽEŠ!" zakřičelo Prasátko a podalo z domečku další kbelík.

"Ty banány dej sem, " řeklo Prasátko, "To je *kouzelný* kbelík. Pokud zjistím, že s těmi banány děláš něco nepatřičného, stačí mi *třikrát* poskočit a veškeré tvé banány **SE VYPAŘÍ!**"

"Ale ale ale..." řekl Králíček.

"Běž pryč!" zakřičelo Prasátko, "tobě taky nevěřím!"

Poslalo Králíčka pryč s kýblíkem plným banánů a vrátilo se domů, kde našlo mrkve v ohni.

**BUM BUM BUM BUM BUM!**

Někdo bušil na dveře až prasátko málem vyskočilo z pyžama. Doběhlo ke dveřím, otevřelo a venku na schodech stála malá pomalá želva jménem Maurice.

"Mám kokos," řekl Maurice.

"To je pěkné," řeklo Prasátko.

"Rád bych jich měl víc," řekl Maurice, "a taky mám s sebou zlaťák." Prasátko si jen povzdechlo.

Šli spolu ven a Prasátko nechalo Maurice hodit do bedničky jeden kokos. Bednička se převrátila a NA ZEM VYPADLO DVACET SEDM KOKOSOVÝCH OŘECHŮ!

"No teeeda," řekl pomalu Maurice.

"Ale počkat!" řeklo Prasátko, "na copak ty kokosové ořechy potřebuješ? Želvy kokos *nejedí*."

Maurice odpovídal tak pomalu, že si Prasátko řeklo, že musí *v něčem* lhát.

"Ty LŽEŠ!" zakřičelo Prasátko a podalo z domečku další kbelík.

"Ty kokosy dej sem, " řeklo Prasátko, "To je *kouzelný* kbelík. Pokud je budeš chtít přinést jinam než do své vlastní kuchyně, kbelík ke mně **SÁM ZPÁTKY ŘILETÍ!**"

"Ale ale ale..." řekl Maurice.

"Vůbec ti nevěřím a navíc z tebe cítím SÝR!" zařvalo Prasáko.

Poslalo Maurice pryč s kýblíkem plným kokosových ořechů a vrátilo se domů, kde zjistilo, že se mu převařily brambory.

Než zavřelo dveře, byla zpátky potrhlá veverka, střeštěně poskakovala a z nosu vyfukovala bublinky.

"Lízátko Jones!" zazpívala veverka a odtančila.

Prasátko to tak naštvalo, že začalo vzteky skákat do vzduchu.

"Běž pryč!" řvalo, "Běž pryč a nech mě na pokoji, ty neposedná koule chlupů!"

A tu se na cestě objevila Kačenka, od hlavy k patě od lepkavé kaše z rozmačkaných jablek.

"Tyyyyy ošklivé Prasátko!" křičela Kačenka.

"Co se TI to stalo?" zalapalo po dechu Prasátko.

"Moje malé káčátko si chtělo dát ke svačině jablíčko a potom najednou VŠECHNA JABLKA VYBOUCHNLA! Prasátko, pročpak se nemůžu o svá jablka podělit s rodinou?"

"Aha..." řeklo prasátko.

"Ty vykutálené Prasátko!" zakřičel Zajíček běžící po cestě k nim.

"A co se stalo TOBĚ?" zalapali po dechu Prasátko s Kačenkou.

"Moje babička dělala banánový koláč!" plakal Králíček, "ale když ho vytáhla z trouby, VŠECHNY BANÁNY SE VYPAŘILY! Byla tak naštvaná, že ho PO MĚ HODILA! Třikrát jsi poskočilo, ŽE ANO, PRASÁTKO?!" řekl Zajíček.

"Aha... no..." řeklo Prasátko.

"Tyyyyy jedno... Prasátko!" křičel Maurice běžící k nim po cestě ve skořápce od kokosového ořechu místo krunýře.

"A co se stalo TOBĚ?" zalapali po dechu Prasátko s Kačenkou a Zajíčkem.

"Chtěl jsem si sníst jeden kokos u sebe v pokoji a najednou kbelík uletěl pryč! Jeden ořech mě uhodil tak, že jsem skončil V NĚM! Proč musím jíst jen v kuchyni, Prasátko?"

"Hmmm... no..." řeklo Prasátko.

"Vypadáš k popukání, Maurice," řekl Zajíček, "můžu tě kutálet?"

Prasátko bylo tak znepokojené, že si sedlo a začalo naříkat.

"Ta kouzelná bednička přináší jenom problémy!" fňukalo, "jablka kvůli ní vybuchují, banány se vypařují a kokosové ořechy útočí na želvy!"

"To neudělala ta bedýnka!" křikla Kačenka, "všechny ty věci jsi udělalo TY! Mělo by ses naučit se s ostatními DĚLIT."

Kačenka měla *pravdu*! Kouzelná bedýnka dělala přesně to, co měla! Problémy přinášelo jenom Prasátko!

A tak se Prasátko rozhodlo všechno napravit. Zařeklo se, že už nikdy nebude používat kouzelné kbelíky, a starou cedulku vyměnilo za novou, na níž stálo:

**KOUZELNÁ SDÍLECÍ BEDÝNKA**

**POUŽÍVEJTE JI!**

Život Prasátka se od té doby moc zlepšil.

Lidé chodili z široka daleka používat jeho kouzelnou bedýnku. Pokaždé, když si okopírovali věci, pár jich nechali i Prasátku. To si obzvláště oblíbilo zeleninu.

Po nějaké době se Prasátko stalo výtečným kuchařem a otevřelo si restauraci s boxíky, hracími skříňkami a židličkami z obrácených kýblíků.

Restauraci pojmenovalo "Lízátko Jones", i když nevědělo proč. 

Všechno nakonec dobře dopadlo.

Tedy až do doby, kdy potrhlá veverka spadla do bedýnky.