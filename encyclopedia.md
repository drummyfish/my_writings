This is a work in progress encyclopedia written by drummyfish (Miloslav Číž).

The philosophies and guidelines for this encyclopedia are following:

- Be truly **unbiased and uncensored**, focused on the objective and absolute truth, even if that truth is unpopular, illegal or politically incorrect. The only censorship there is is the exclusion of material "protected" by so called intellectual property.
  - The only exception to censorship is a strict exclusion of copyrighted material, even that which is free as in freedom. For example a summary of a copyrighted book's story cannot be included here if the story can be considered copyrighted, i.e. if someone copying that story could get into legal trouble. Other forms of IP, such as trademarks or patents, are ignored, as it simply isn't possible to avoid them (but which also don't pose such issues as copyright).
- Be **suckless**, meaning it should be:
  - stored in a **single text file**
  - easily downloadable and modifiable
  - readable as plain text
  - use Unicode character very sparingly, preferring 7bit ASCII
  - use images extremely sparingly and if so, then only those that are absolutely public domain worlwide and created by the same author as this encyclopedia
  - **have no dependencies** such as a Wiki engine, i.e. allow easy hosting on any server or computer able to store files
  - any additional functionality such as indexing should be handled by external tool that are not part of this project
  - **easily processed by standard Unix tools**
  - be completely **free of any legal restrictions** such as copyright or trademarks and making absolutely no use of such concepts as fair use
- Have **unified and high quality entries**:
  - The **first sentence of each entry should be usable as a reasonable summary of the whole term**. Similarly, the first paragraph should be a reasonable extended definition.
  - **Each entry should begin with simple informal explanation** understandable to most people and gradually transition to more formal language and specialized details, utilizing a **top down approach**.
  - Each entry should be enjoyable and interesting to read, rather than contain as much information as possible.
  - Articles on similar subjects should follow similar structure, e.g. articles about programming languages should all show the same problem solved in that language so that the reader can compare different languages on the same problem.
- **Most facts don't need to be sourced**. The purpose of this work is to present facts, checking the facts is another task for another project, or for the reader himself. The source of many claims is simply logic and evidence of truth is the consistency of the whole work. References to some specific pieces of information can be given right in the text in brackets. References should be used more as a pointer to where more information can be found rather than proof of correctness.
- Not rely on link to the web or Internet connection.
- Effort should be put into writing timeproof articles. Articles  shouldn't e.g. focus too much on current or very recent unsettled affairs, but rather on historical facts that won't need very regular updates.

This encyclopedia is released into public domain under [Creative Commons 1.0](https://creativecommons.org/publicdomain/zero/1.0/) (CC0, public domain) because intellectual property is the cancer of humanity. In order to be maximally useful to the people, everyone should be able to use it, copy it, modify it and redistribute it in any way, without any burden. Additionally, a waiver of all other intellectual property is applied to this work, which is as follows:

Each contributor to this work agrees that they waive any exclusive rights, including but not limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions conceived, discovered, made, designed, researched or developed by the contributor either solely or jointly with others, which relate to this work or result from this work. Should any waiver of such right be judged legally invalid or ineffective under applicable law, the contributor hereby grants to each affected person a royalty-free, non transferable, non sublicensable, non exclusive, irrevocable and unconditional license to this right.

## A

*A* is the first letter of the English alphabet.

## Adolf Hitler

Adolf Hitler (20.04.1889 – 30.04.1945) was the founder and leader of the Nazi Party, responsible for World War II and Holocaust (Jew genocide), and is popularly regarded as the most evil man in history.

Hitler was indeed an extremely evil person and after World War II and his death he started to be systematically demonized further; nowadays he is therefore seen as an incarnation of pure evil and the worst human in recent history, perhaps ever.

He was an extreme rightist, even though he confusingly used the word *socialist* in the name of his political party National Socialist German Workers' Party as he wanted to appeal to the masses – there was in fact nothing socialist about his politics at all. His goal was extreme hierarchization of society on all levels: between states, nations, races and individuals as well as in the government and army.

The most prominent features about Hitler's appearance was his toothbrush moustache (a mustache only underneath the nose, cut at sides). He was 174 cm tall and had an ideal weight of 68 kg. He had blue eyes and short dark brown hair with fringe combed to one side. In the public he was prevalently seen in the Nazi uniform, with a serious and confident facial expression.

## Alan Turing

Alan Turing (23.06.1912 – 07.05.1954) was a brilliant British mathematician who laid foundations to computer science and is generally considered the father of the field. He created the mathematical model of a computer (Turing's machine), showed its computational limits and predicted the importance of computers and artificial intelligence. His ideas were far ahead of his time and became fully appreciated only much later after his death. He also significantly helped with cracking Nazi cyphers during World War II.



Turing was gay



## Albert Einstein

Albert Einstein (14.03.1879 – 18.04.1955) was a German Jewish physicist who beside other things developed the theory of relativity and has since become perhaps the most famous scientist in history.

Einstein criticized capitalism and was a socialist and pacifist.



Einstein was of average body proportions with a little bigger nose and larger head which was further exaggerated by his longer ruffled hair – the hair was dark but in a lot of famous photos it's already white. He had brown eyes and wore a mustache. Many photos capture him smiling or laughing, showing his playful mindset.


- "***Blind obedience to authority is the greatest enemy of truth.***": (1901, in a letter to Jost Winteler)
- "***God doesn't play dice with the world.***": (from the book *Einstein and the Poet*) A well known quote expressing disagreement with the quantum theory that states the world at the fundamental level is probabilistic.
- "*I am convinced there is only one way to eliminate these grave evils, and that is through the establishment of a socialist economy, along with an educational system oriented towards social goals. [...] Nevertheless, it is necessary to remember that a planned economy is not yet socialism. A planned economy as such may be accompanied by the complete enslavement of the individual.*": (from Einstein's 1949 article *Why Socialism?*)
- "*I am not an Atheist. [...] We are in the position of a little child, entering a vast library whose walls are covered with books in many different languages. The child knows that someone must have written those books. It doesn't know who or how. [...] It seems to me that is the attitude of the human mind, even the most intelligent, towards God.*": 
- "*Invention is not the product of logical thought, even though the final product is tied to a logical structure.*": (Autobiographische Skizze, 1955) Explaining how intuition is essential for science.
- "*Try to become not a man of success, but rather a man of value.*": (LIFE magazine, 1955)
- "*The important thing is not to stop questioning.*" (LIFE magazine, 1955)

## Anarchism

Anarchism is a political philosophy rejecting any social hierarchy, which makes it the most extreme form of leftism. Anarchism mean *without rulers*, not *without rules*.

Many other philosophies confusingly name themselves a type of anarchism despite violating the definition of anarchism and so cannot be considered anarchist philosophies. These include **anarcho capitalism** and **anarcha feminism**.

The symbols of Anarchism include an uppercase *A* in a circle, the colors red and black and a rectangular flag split in two parts by a straight line from bottom left to top right with the top left part being colored red and the bottom right black. Specific subtypes of anarchism typically use the same flag with modified colors (e.g. anarcho pacifism using black and white colors).

Ultimately the ideal form of anarchism is a communist anarcho pacifism.

**Anarcho pacifism** correctly points out that violence always creates a social hierarchy of stronger above weaker, and so rejects any violence. Proponents of violent anarchism argue that violence will always be necessary for defense against violent non-anarchists, which may be true but is a hypocritical argument; if these "anarchists" argue that we need hierarchy to maintain peace, then they are saying they don't believe in a society without hierarchy and so shouldn't call themselves anarchists.

## Anders Breivik

Anders Breivik (*13.02.1979, later renamed to Fjotolf Hansen) is a Norwegian rightist extremist who in 2011 murdered 77 people (mostly not older than 18) on the Utøya island and around Oslo.



## Animal

## Antarctica

Antarctica is the Earth's southernmost continent on which south pole lies and is which mostly covered in ice. It is an extremely inhospitable place that remained undiscovered until 1820 and due to the extreme conditions stayed largely unexplored until 20th century.

At the south pole there is Amundsen–Scott South Pole Station – a US research station permanently populated by about 100 researchers, established in 1956.

## Antifa

Antifa (standing for *anti fascism*) is a fascist pseudoleftist group focused on violently overthrowing non-pretending fascists, their competitors.

## Antimatter

## App

App, short for *application*, is a business word for smaller, lower quality computer programs whose copies are usually distributed exclusively through controlled platforms or *stores* – *apps* are typical e.g. for *smartphones*. Apps are part of capitalist technology.

## Art

Art is an effort at creating works (e.g. paintings or composition) and performances (e.g. singing or acting) whose purpose is to be appreciated for their beauty or emotional power. Art is often contrasted with science.

## Artificial Intelligence

Artificial intelligence (AI) is a computer program that simulates thinking of a biological brain.

## Asia

## Atom

Atom is a basic, extremely small unit of matter, measuring about 10^-10 m.

Atom consists of:

- **nucleus**:
- outter **electron cloud**:




## Ball Lightning





## Bible

Bible is the central holy book of the Christian religion (as well as Jewish and others) and is maybe the most famous and influential book in the world.




The original Bible was written in Hebrew, Aramaic (old testament) and Koine Greek (new testament). The King James Bible from 1611, a widely used English translation, has 783137 words and 3116480 characters. Printed Bibles usually have over 1000 pages. It is probably the best selling book ever – estimates talk about 5 billion sold copies.




There is not a single version of the Bible as different religions and churches include and exclude different texts into it. The Christian Bible is usually said to have 66 books.

The summary of the Bible is follows:

- **old testament**: 39 books (common to most versions), holy to both Christians and Jews. The texts are about God, his creation of the world, the history of Israelites and the good and evil.
  - Torah
  - History of Israel
  - Prophets
  - Wisdom Books
- **new testament**: 27 books
  - TODO

## Biology

## Bitcoin

## Black

Black is the darkest color, the color of night sky and coal, the color one sees with eyes closed. Besides others it represents darkness, death, void, mystery and evil. Its opposite color is white.

Physically black color means absence of any light, i.e. it can be understood as lack of color – there exists no black light hence there is no black wavelength. A ideal black object absorbs all incoming light and reflects none of its back, therefore we see it as black.

In RGB it is represented as [0,0,0].

## Bloat

Bloat means undesired growth by accumulation of unnecessary things – the word is often used in relation to technology, art and other human works that become very overcomplicated.





**Technological bloat** is a property of capitalist technology; in a capitalist society bloat stems from factors such as preference of developing something quickly rather than properly, lack of long-term planning (or downright planned obsolescence), feature creep fed by consumerism, preference of visual appeal over performance etc. Technology is bloated even on purpose as to obscure the working principles of it and so resist reverse engineering, to artificially raise the cost of improvement of it: in short to keep control of the technology in the hands of the creator.

Perhaps the best example of technology bloat is **software bloat**, because software evolves very quickly and great changes can be seen within spans of just years. Therefore software bloat is currently the most concerning one. Even though hardware in 2020s is many orders of magnitude faster than hardware in 1990s, the perceived speed and responsibility of a typical modern computer is nowadays often lower than of the old computers! As **Wirth's law** states: "software gets slower faster than hardware gets faster".

**Web pages** demostrate software bloat very well. TODO

Software bloat is very dangerous and harmful, some of its typicals properties are:

- Rapidly increasing **cost** of maintenance and development.
- Serious **security** concerns due to larger attack surface.
- Suffering from **bugs and errors** due to great **complexity**.
- Rotting and much great probability of "dying" due to having many dependencies (e.g. libraries or HW) that may themselves die. 
- Going against the principles of **free as in freedom software**. Even if SW is free legally, if not many people are in practice able to understand it, modify it or even run it, it becomes de-facto nonfree.
- Wasting resources: besides computing resources, human effort is wasted too as programmers are spending time and energy on unnecessary management of extra complexity. In this way people become enslaved by technology.
- and more

Many people have become concerned with software bloat and are trying to work against it, most notably the **suckless movement** which is creating simple minimalist tools that follow the **Unix philosophy**.

Bloat appears also in other areas than software, even if it is not yet as alarming there. This includes hardware (e.g. Intel ME backdoor in their CPUs) or even art such as movies (e.g. unnecessary filler material in the infamous Hobbit movie trilogy).





## Blockchain



## Brain

Brain is an organ of higher living organisms that performs thinking, learning, processing of sensory inputs, movement control and other functions. It is mostly located in the head and is essential for the organism to function. Brain is the center of the nervous system.



## Brainfuck

Brainfuck is one of the most famous esoteric programming languages (i.e entertaining, not for practical use), characterized by being very difficult to write programs in. Brainfuck is Turing Complete, meaning it has the highest possible computing power and can compute anything that any other programming language can.

The language was created in 1993 by Urban Müller who wanted to come up with a language for which it would be possible to write an extremely small compiler.

Brainfuck works with raw memory of at least 30000 bytes and internally keeps a pointer to the currently processed byte. Initially all memory bytes are set to 0 and the phe could write the smallest possible compilerointer is set to point to the first byte. The brainfuck program is then composed of following instructions:

- `>`: Move the pointer to the next byte.
- `<`: Move the pointer to the previous byte.
- `+`: Increment the byte at pointer.
- `-`: Decrement the byte at pointer.
- `.`: Output the byte at pointer.
- `,`: Read an input byte and store it to the byte at pointer. 
- `[`: If the byte at pointer is 0, then move after the matching `]` instruction, otherwise move to the next instruction.
- `]`: If the byte at pointed is not 0, then move back to the matching `[`, otherwise move to the next instruction.
- All other characters are ignored and can be used for comments.

The following is an example of a brainfuck program that reads two numbers, substracts them and prints the result:

```
,>,      read two numbers to cell 0 and 1
[-<->]   keep decrementing both numbers in a loop
<.       print the result
```

There are extensions and modifications of Branfuck, to name a few:

- **circlefuck**: Modification of brainfuck in which the code is stored in memory and can modify itself (implementing von Neumann architecture).
- **brainfuck++**: Adds file and networking support to brainfuck.
- **brainfork**: Multithreaded extension of brainfuck, adds `Y` instruction that forks the current thread.
- **omnifuck**: Adds function call-like functionality to brainfuck in the form of multiple separate *brains*.

## C

C is the third letter of English alphabet and also a low-level programming language.

```
#include <stdio.h> // standard input/output library
#define N 3
#define SWAP_CHARS(a,b) {char t; t = a; a = b; b = t;}
char p[N + 1];

void printPermutations(char *s) // function
{
  if (*s != 0)
  {
    char *s2 = s;
    
    while (*s2 != 0)
    {
      SWAP_CHARS(*s,*s2)
      printPermutations(s + 1);
      SWAP_CHARS(*s,*s2)
      s2++;
    }
  }
  else
    printf("%s ",p);
}

int main(void)
{
  for (int i = 0; i < N; ++i)
    p[i] = 'a' + i;
 
  p[N] = 0; // terminate string   
  printPermutations(p);
  
  return 0;
}
```

The most notable compilers of C are:

- **gcc** (GNU compiler collection)
- **clang**
- **tcc**


## Category

Category is a mathematical structure that can be imagined as "objects connected by arrows". Categories are studied by category theory and are important e.g. for definition of programming languages.




Informally 



More formally a category consists of:

- *C*: a class of **objects**. (As the objects can be sets, *C* can't be a set, it must be a class.)
- *M*: a class of **morphisms** (arrows). Each morphism *m* is an ordered pair *a -> b* where *a* and *b* are objects belonging to *C*, *a* is called the **domain** (source) of *m*, *b* is called the **codomain** (target) of *m*. 

While the following must hold:

- If there exists morphisms *m: a -> b* and *n: b -> c*, there must also exist a **composition** morphism *n + m: a -> c*.
- Compositions must be **associative**, i.e. for any three morphisms *m: a -> b*, *n: b -> c* and *o: c -> d* it must hold that *m + (n + o) = (m + n) + o*.
- For every object *a* from *C* there must exist an **identity** morphism *1a: a -> a*.




A category is similar to a reflexive transitive relation, but a category can have multiple arrows between two objects while a relation only says whether there is a relation between two objects or not.


## C++

C++ is a programming language known for being among the more difficult ones to learn but also of the most efficient. It has evolved from the C language by adding object orientation (OOP).


C++ is, strictly speaking, **not** a superset of C because it is possible to construct a valid C program that is not a C++ program (for example by naming a varibale `class` which is a reserved identifier in C++ but not in C). Nevertheless in practice most C programs can be compiled as C++.



## Capitalism

Capitalism is perhaps the worst economical system that has been established in society, characterized by prioritizing wealth over anything else, abuse of workers and consumers, declining morals, wasting resources, life environment destruction, large scale powerty, declining moral health, bullshit jobs and others.

Capitalism is based on constant competition.

Capitalism needs constant growth even if only for its own sake up until self destruction, and in this it is similar to cancer.



- Wasting resources and human work, e.g. **planned obsolescence**.
- Reinventing wheels. As know-how and details of technology are kept secret by corporations, others are forced to reinvent them.



As a famous quote (usually attributed to John Maynard Keynes) says:

*"Capitalism is the extraordinary belief that the nastiest of men for the nastiest of motives will somehow work for the benefit of all."*

## Capitalist Technology

Capitalist technology is technology that evolves during capitalism, such as bloatware or consumer hardware, whose primary function is to serve corporations on the detriment of people.

Some attributes of capitalist technology are:

- Spying on users in order to collect data which are precious commodity to sell or use e.g. for targeted marketing.
- Intentional and unintentional security flaws. Intentional security flaws include backdoor allowing the manufacturer, but also any attacker, to take full control of the technology. Unintentional security flaws are caused by other attributes such as excessive complexity or 
- Refusing to work
- Being inefficient and slow
- User is not in control
- Unnecessary complexity
- **Forced unnecessary dependencies** that are not only burdening but also dangerous. It is typical for non-electronic machines to be dependent on a built-in computer with Internet connection and proprietary software – such device may refuse to function without Internet connection or recent updates even if this limitation is purely artificial. Relying on such machines can of course be dangerous because a blackout can render it useless even if that wouldn't have to be the case.

- Focused solely on exploiting the user, not helping him.
- Bloatware
- Flawed design caused by the need for rapid development.
- Forced obsolescence
- Short live, being designed as a product to be consumed,  
- Intentional incompatibility
- Inelegance 
- Restricting access

## Car

## Cat

Cat is a very popular small pet animal. In a wider sense, the word *cat* may refer to any species from the Felidae family, such as lions and ocelots.

A cat weights about 4 kg, walks on four legs, has a tail and retractable claws. It is a fast, agile predator that climbs trees very well and hunts small prey such as mice and birds. Usually it is very quiet but sometimes makes meowing sounds and purrs.

TODO wildcat


## Censorship

Censorship means removing information from somewhere or preventing it from appearing somewhere so that others can't find it. Hiding information from oneself, i.e. avoiding it, is not a censorship.



## Chemistry

Chemistry is a science

## Chess

Chess is a very old and well known two-player board game that simulates a war of two armies.




An empty chess board looks as follows (`#` mark black square, `.` white):

```
8   . # . # . # . #
7   # . # . # . # .
6   . # . # . # . #
5   # . # . # . # .
4   . # . # . # . #
3   # . # . # . # .
2   . # . # . # . #
1   # . # . # . # .

    A B C D E F G H
```





```
8   r n b q k b n r
7   p p p p p p p p
6   . . . . . . . .
5   . . . . . . . .
4   . . . . . . . .
3   . . . . . . . .
2   P P P P P P P P
1   R N B Q K B N R

    A B C D E F G H
    
```

The letters represent chess pieces, uppercase white, lowercase black. These piece are:

- **pawn** (*p*): Moves one square forward; that is in the player's facing direction: white towards 8th row, black towards 1st. From a starting square two squares forward can be advanced instead of one. A pawn cannot take an enemy piece by going forward, only one square diagonally forward (forward left or forward right), but without taking a piece the pawn cannot move this way.
- **rook** (*r*): Moves in a straight line, either horizontal or vertical, without jumping over other pieces. E.g. a rook at E4 can potentially move to all squares in the E column and all square in the 4th row.
- **bishop** (*b*): Moves in straight diagonal lines, without jumping over other pieces. E.g. a bishop on E4 can potentially move to D3, C2, B1, F3, G2, H1, D5, C6, B7, A8, F5, G6 or H7.
- **knight** (*n*): Moves 2 squares in one direction followed by 1 square in perpendicular direction (L shaped moves) and is the only piece that can jump over other pieces (enemy or not). E.g. a knight on E4 can possibly move to F2, E2, F6, D6, G3, G5, C3 or C5.
- **queen** (*q*): Can make the same moves as a rook and a bishop, that is horizontal, vertical and diagonal.
- **king** (*k*): Can move to any of his 8 neighbouring squares.

The rules of chess are following:

- White always has the first move. Both players then take turns until the game is over. In every turn exactly one move is made by the player who's turn it is.
- A **move** by a player means he moves one of his pieces as described above. Except for knight, pieces cannot jump over other pieces, but any piece can take an enemy piece by jumping on its square, in which case the piece taken is removed from the board. There are two special moves:
  - **castling**:
  - taking **en passant**:
- If the king of one of the players is attacked, i.e. it could be immediately taken in the next turn by the enemy, the player is said to be in **check**. When in check, the player has to move so that his king is no longer attacked, i.e. so that he's no longer in check. If he cannot do this, the game ends with a **checkmate**. A player also cannot move from a regular position to his own check. In other words, immediately after a player's move this player must not be in check.
- The game is over when either of these happens:
 - One of the players is checkmated, in which case the other player wins.
 - The player who's move it is is not in check and at the same time has no legal moves, in which case the game is drawn by so called **stalemate**.
 - One of the players resigns, in which case the other player wins.
 - The players agree on a draw.
 - It is not possible for either of the player to checkmate the other player, in which case the game is drawn by so called **dead position**.
 - In a game with time control one of the player's time runs out, in which case that player loses.
 - TODO

Professional chess games usually use **time control** to limit the time for each player to make a move.




Most common opening moves are (order from the most common):

```
e2e4       King's Pawn Opening
  c7c5     Sicilian Defense
    g1f3
  e7e5     King's Pawn Game
    g1f3
  e7e6     French Defense
d2d4       Queen's Pawn Opening
  g8f6     Indian Defense
  d7d5     Queen's Pawn Game
    c2c4   Queen's Gambit
g1f3       Reti Opening
  g8f6     
  d7d5
c2c4       English Opening
  g8f6
  e7e5     Reverse Sicilian Defense
```

Competitive chess players are awarded **titles** by chess organizations of which most notable is the international FIDE. FIDE grants the titles of (from most prestigious) **Grandmaster** (GM), **International Master** (IM), **FIDE Master** (FM) and **Candidate Master** (CM). As of 2021 there are  1741 GMs (39 women), 3910 IMs (124 women), 8329 FMs (41 women) and 1972 CMs (22 women). Obtaining these titles requires a combination of high ELO rating and tournament achievements. Other national chess organizations award titles such as National Master (NM).

According to a computer analysis the **strongest chess** player in history is probably Norwegian chess world champion **Magnus Carlsen** (*1990) who achieved the best CAPS (Computer Aggregated Precision Score by chess.com), i.e. most commonly made the moves the strongest chess engine would play, and reached FIDE elo rating of 2882. In the CAPS scoring he is followed by Vladimir Kramnik (Russian, *1975), Garry Kasparov (Russian, *1963), Robert "Bobby" Fischer (USA, *1943), Viswanathan Anand (India, *1969) and José Capablanca (Cuba, *1942). The strongest ever woman player is thought to be **Judit Polgár** (Hungary, *1976), who achieved FIDE rating of 2735. Other top female player in history include Susan Polgár (Judit Polgár's sister), Hou Yifan (China, *1994) and Maia Chiburdanidze (Georgia, *1961).


## Chess Engine

As of 2021 the strongest chess engine is a free software program Stockfish version 12, which utilizes machine learning, which has achieved an elo rating of about 3500.


## Child Pornography

Child pornography (CP) is a pornography in which children take part. Contrary to modern propaganda this does not necessarily imply child abuse just as adult pornography does not automatically imply rape. Non-sexual media featuring naked children is not child pornography.

This kind of pornography appears on the normally accessible websites (so called **clearnet**) very rarely as website administrators have legal obligations to block and delete such content, and as most brainwashed people are highly hostile to this content. Child pornography can however be found on the alternative web called **deepweb** or **darkweb** and other privacy anti-censorship networks. These includes networks such as Tor, Freenet or I2P.

As of 2021 child pornography is completely legal (to possess, distribute etc.) in Central African Republic, Equatorial Guinea, Somalia, Dominica, Saint Lucia, Kiribati, Palau and possibly other countries.




## Coding

Coding means low quality programming, mostly done by people without the mental capacity for complex problem solving or mathematics that real programming requires. These people are called "coding monkeys" or "coders" for short.

Coders almost always use GUI tools such as IDEs, bloat software, capitalist technology, high-level programming languages and even proprietary tools.

## Cognitive Bias

## Color



## Complex Number

Complex numbers are numbers residing in a 2 dimensional plane and are more general than real numbers. Complex numbers are very convenient in many situations and sometimes allow solving problems at which using real numbers would fail.

Real numbers and imaginary numbers are both subsets of complex numbers.

A complex number can be written in the form

a + b * *i*

where *a* and *b* are real numbers – representing the 2 dimensional coordinates – and *i* is so called **imaginary unit**. 

The imaginary unit *i* is a special number which we define to have the property

*i*^2 = -1

No real number has this property and so by defining this we invent a new number that exists outside the real number line. By having this new unit *i* we also get infinitely many new numbers of form a * *i*, e.g. 1*i*, 0.5*i* or -10*i*, which reside on a line separate from the real number line – we call these new numbers **imaginary numbers**.

Now since we have two number lines, real and imaginary, we can draw them perpendicular to each other with the intersection at 0, which is the only common number of both lines. This creates a 2 dimensional plane with real and imaginary dimensions. Numbers on this plane are called **complex numbers** and we can write them in the above mentioned form representing coordinates in the plane.

```
      imaginary
       numbers
          ^
          |      complex numbers
          |
          |
----------+----------> real numbers
          |0
          |
          |
```

for example:

- 3.5 is a real number and a complex number
- 2*i* is an imaginary number and a complex number
- 0 is both real and imaginary number and a complex number
- 2 + 6*i* is a complex numbers

## Communism

Communism is an idea that all members of society should, without any discrimination and oppression, share all resources, help each other and be treated as equals. It denotes an ideal society without states, money and capitalism.

Unfortunately, during the 20th century the word has been abused by violent pseudoleftist regimes, particularly the Soviet Union, that otherwise had nothing common with communism society and so the word has acquired a negative connotation. The term communism is nowadays, i.e. after these events of the 20th century, usually not used in the general meaning but rather refers to the theories of Karl Marx and Friedrich Engels that were the basis for said revolutions.

Regarding communism there are two separate political questions to be answered:

1. **What is the ideal society?** I.e. how should the final stable society work. Disagreements about this question are only minor; most people agree that things like money, states, police or capitalism are imperfect solutions to current world issues and shouldn't exist in an ideal society.
2. **How to achieve this ideal?** I.e. what specific steps need to be taken from now to achieve the final ideal society. This question is controversial and sparks heated debates as some say things like violent revolutions and temporary dictatorships are necessary intermediate steps while other say the steps should follow the communist principles and therefore should not include things like violence and oppression.

## Computer

Computer is a machine that carries out calculations. Before computers were invented the word *computer* used to refer to a person performing calculations.

The first computers were mechanical, then electromechanical and analog. Modern computers are fully electronic and digital, i.e. working with discrete values (typically two values: 0 and 1) instead of continuous values such as voltage.

Computer science and mathematics study computers using various computational models, strongest of which is the Turing machine (and its equivalents).

## Computer Graphics



## Conspiracy Theory

## Copyleft

Copyleft (a word play on "copyright", also share-alike) is an idea that an intellectual work (such as art) will be freely shared under the condition that it, as well as all works based on it, will further only be shared in the same way: freely and under copyleft. It is a concept that arose alongside free software and has since spread into other areas (see free culture).

Copyleft is not a requirement for free as in freedom sharing, it is one of two major ways of licensing such works. These are:




The idea of copyleft is kind of a legal "hack" exploiting the power given by copyright to purposefully negate this power of copyright. In a simplified way it says: if thanks to copyright I have a complete control over my work and works based on it, I can prohibit executing this control over these works.

This is achieved by a license that is attached to a work. In simplified term such license may say something like: *This work grants everyone the rights to use, study, modify and share it in any way and for any purpose as long as it keeps this or legally equivalent license*.

Invention of the copyleft concept is attributed to the father of free software Richard Stallman, even though the word has been used before. The first general copyleft license was the free software GNU General Public License, **GPL**, released on 25 February 1989 by Richard Stallman for his GNU project. Other copyleft license later emerged






## Creative Commons

## Czech Language

An example of a sentence in Czech language is:

*Všechny živé bytosti musí žít v míru.* (All living beings must live in peace.)


## Death Penalty

Death penalty (or capital punishment) is a lawful punishment of a person by death. It is practiced in primitive societies, even though most states have already abolished it.

## Diogenes

Diogenes (412 or 404 BC - 323 BC) was a Greek philosopher and anarchist famous for his life in simple conditions and strong criticism of society's corruption. He was one of the founder of Cynic philosophy.



Most notably he


- Lived in a barrel.
- Masturbated in public; when asked why he was doing it he replied: "*If only I could satisfy hunger by rubbing my belly*".

## DNA

DNA (deoxyribonucleic acid) is a chemical molecule that in living organisms contains the instructions on how to build the organism, i.e. its genetic code (something akin a "recipe"). It is present in all organisms we know except for some viruses that only have RNA (a similar but simpler molecule).




As a rule, DNA creates RNA, RNA creates proteins and proteins create the organisms themselves.



## Dodo

Dodo (Raphus cucullatus) was a species of a middle sized flightnless bird living on the Mauritius island which has become extinct shortly after humans appeared on the island – for this Dodo has become a symbol of extinction due to human activity.

## Dog

Dog is a domesticated four-legged animal evolved from wolf that has throughout history been very popular as a pet and human helper. Nowadays there are many breeds of dogs that significantly differ in appearance, size and also behavior: some weighting only a few kilograms, intended to be pets (e.g. Chihuahua), some weighting over 50 kilograms intended to be guardians and hunters (e.g. Irish wolfhound).

## Doom

Doom means a *terrible fate* and is a name of one of the most famous video game franchises that gave rise to the first person shooter genre.



## E

E is the fifth letter of English alphabet and represents an important mathematical constant equaling approximately 2.71.

The first 100 decimal digits of e are 2.7182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274.




## Earth

Earth (Terra) is the planet in the Solar System at which humans live and the only place in the Universe known to host life. It is the third planet from the Sun and its only natural satellite is the Moon.

Earth is of spherical shape but because of its rotation it is very slightly flattened at the poles (the difference of smallest and largest diameter is 43 km). The exact shape of the Earth is called a geoid. Earth's mean diameter is 12742 km and its mass is 5.97237 * 10^24 kg. About 71 % of Earth's surface is covered with water.

Earth is about 148 million km away from the Sun and orbits it with the period of about 365.25 days (one year). Around its own axis Earth makes a whole turn in 24 hours (one day).


The layers of Earth are (from the center outwards, width in brackets):

- solid inner core (1278 km)
- liquid outer core (2200 km)
- viscous mantle (3478 km)
- lithosphere (~100 km)
- gas atmosphere (~100 km)


## EEl

## Ellipse

There is no simple equation for the perimeter of an ellipse; computing the perimeter exactly requires integration. Nevertheless there exist simple approximations, e.g.:

| approximation                                                                       | comment                                                                                              | error                        |
| ----------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | ---------------------------- |
| *2 * pi * (a+b) / 2*                                                                | Perimeter of a circle with the radius equal to the arithmetic mean of the major and minor semi axis. | < 10 % when *a/b* < 5        | 
| *2 * pi * sqrt((a^(2) + b^(2))/2)*                                                  |                                                                                                      | < 10 % when *a/b* < 10       |
| *pi * (3 * (a+b) - sqrt((3 * a + b) * (a + 3 * b)))*                                | Ramanujan's approximation                                                                            | < 0.01 % when *a/b* < 4      |
| *pi * (a+b) * (1 + (3 * h)/(10 + sqrt(4 - 3 * h)))* where *h = (a-b)^(2)/(a+b)^(2)* | another Ramanujan's approximation                                                                    | near 0 for non-extreme *a/b* |

- *2 * pi * (a+b) / 2*: Perimeter of a circle with the radius equal to the arithmetic mean of the major and minor semi axis of the ellipse. The error is < 10 % when *a/b* < 5.
- *2 * pi * sqrt((a^(2) + b^(2))/2)*: The error is < 10 % when *a/b* < 10.
- *pi * (3 * (a+b) - sqrt((3 * a + b) * (a + 3 * b)))*: Ramanujan's approximation. The error is < 0.01 % when *a/b* < 4.
- *pi * (a+b) * (1 + (3 * h)/(10 + sqrt(4 - 3 * h)))* where *h = (a-b)^(2)/(a+b)^(2)*: Another Ramanujan's formula, extremely accurate, near 0 error for non-extreme *a/b*.


## Elon Musk

## Elo Rating


## Email

Email (electronic mail or e-mail) is a technology allowing to send and receiving messages over a computer network; it is one of the most used services of the Internet.



## English

## Esperanto

## Europe



## Football

Football is one of the most popular team sports in the world in which the players try to score goals by kicking a ball into the opponent team's goalpost. People in the USA call this sport a **soccer** and have stolen the term *football* for their completely different sport that should be called *American football*. The advantage of football, and perhaps even the reason for its popularity, is that in simplified and recreational forms it doesn't require expensive equipment and so can be played by most people all around the world.

The official **rules** of football are governed by IFAB (International Football Association Board). However amateurs play football in many unofficial forms that modify these rules and often include many ad-hoc modifications to fit the situation (e.g. number of players or time limit). The summary of the official IFAB rules is this:


- The game is played on a rectangular pitch that in the middle of each shorter side has a goalpost, one for each team. In front of a goalpost there is a smaller rectangular goal area marked.
- There are two teams, each of 11 players, one of which is the team's goalkeeper – the only player who is allowed to play the ball by hands, but only within his team's goal are. Other players are only allowed to play by feet, head and other body parts except for hands and arms.



   


## Factorial

Factorial is a mathematical function that is typically used in computations of probability (combinatorics) and which grows very quickly. Factorial of number *x* is written as *x!* and says how many ways there are to reorder *x* items – the number of permutations of a sequence. For example 3! = 6 because three items can be ordered as ABC, ACB, BAC, BCA, CAB and CBA.

The function can be defined recursively as:

*x! = 1* if *x = 0* otherwise *x * (x - 1)!*

This means we multiply all numbers from 1 up to *x* while the factorial of 0 is defined to be 1 (because there is only one way to order zero items).

The function grows very quickly; its values, starting from 0, are 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600, 6227020800, 87178291200, 1307674368000, ...

In programming the function can be implemented by a recursive or iterative function, e.g. in C:

```
int factorial_recursive(int n)
{
  return n == 0 ? 1 : n * factorial_recursive(n - 1);
}

int factorial_iterative(int n)
{
  int result = 1;
  
  while (n > 0)
  {
    result *= n;
    n--;
  }
  
  return result;
}
```

## Feminism

Feminism is a movement that started as a group advocating equal rights for women but has evolved into a fanatic fascist pseudoleftist cult seeking for women to overthrow men.

Radical feminists call for castration of a significant fraction of male population.

## Fermat's Last Theorem

Fermat's Last Theorem is a mathematical theorem that states that the equation *x^n + y^n = z^n*, where *n* is an integer greater than 2, has no solution such that *x*, *y* and *z* are all positive integers.

For *n = 2* there are infinitely many solutions, but for a long time it was not known whether any solutions existed for *n > 2*. The theorem was stated by Pierre de Fermat at circa 1637 and showed to be extremely difficult to prove. This was only achieved in 1995 by Andrew Wiles.

## Fibonacci number

Fibonacci number is a number from a sequence in which each numbers is a sum of the previous two numbers, with 0 and 1 being the first two numbers.

The sequence can be recursively defined as

*f(0) = 0*, *f(1) = 1*, *f(n > 1) = f(n - 1) + f(n - 2)*

The first numbers of the Fibonacci sequence are 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 466, 932, ... 

The ratio of two consecutive Fibonacci numbers approaches the golden ratio (~1.62).

## Flatland

Flatland: A Romance of Many Dimensions is an 1884 book by Edwin Abbott Abbott about a fictional two dimensional world inhabited by living geometrical shapes. The book examines the nature, mathematical properties and obstacles to life in spaces of different dimensions, but also presents deeper philosophical questions and criticism of society.

The prothagonist (and a pretended author of the book) is a Square

## Flat Earth

Flat Earth is the believe that our planet Earth is flat, usually a disk or a plane. This belief was common in the past, however nowadays there still exist people holding on to this idea, despite overwhelming evidence that Earth has a spherical shape.



## Fractal

Informally speaking fractal is a geometrical shape that always looks infinitely complex regardless of how small scale we use for viewing it. Fractals often exhibit self similarity, i.e. the parts of the shape looking like the shape itself.

Objects in nature have very often fractal shapes rather than smooth shapes such as squares or spheres; for example a tree is composed of branches that themselves look like small trees, and these further extend into smaller and smaller branches.

In mathematical terms, a fractal is a shape whose fractal dimension is greater than its topological dimension.

Fractals are surprisingly simple to define. It often takes only a single simple equation or a few simple rules. It is thanks to recursive application of these equation or rules that a complex shape emerges from them.

## Freedom

Freedom means being able to choose what one truly wants. The word is often synonymous to the word liberty – for possible differences see the entry on *Liberty*.

There is no such thing as general freedom, a specific kind of freedom typically limits another kind of freedom: e.g. the freedom to kill others will very much limit the freedom to live. So even though politicians basically always promise to instate freedom, it is always a question of what limitations will be imposed by this freedom. Capitalism promises free market, but this leads to high competition and increased abuse of workers, which takes away their freedom to live happy and peaceful lives.

## Free Culture

Free culture is a movement that promotes freedom of creating, using and sharing intellectual works such as art without unnecessary restrictions imposed e.g. by copyright law; the word free stands for freedom, not zero price. Free culture tries to generalize the ideas of free as in freedom software to the whole culture.



Similarly to free software, free culture defines four essential freedoms that must be granted to anyone, without any discrimination, in order for a work to be considered free. These are the freedoms to

1. **Use** the work for any purpose (even commercial).
2. **Study** the work and its inner structure. This implies that if a work has a source code (e.g. a music notation), it must be freely available along with the work.
3. **Share** the work and its copies with anyone.
4. **Modify** (remix) the work. For this the source code must also be available if it exists. The modified version must also be allowed to be shared as in point 3.


This definition was created in 2006 by Erik Möller, a member of the Creative Commons organization, with the help of Lawrence Lessig, Richard Stallman and other important people associated with related projects.




Among wider public there exist many **misconception** about free culture. To refute some of them:

- **Free culture is not about zero price but about freedom**, even though from the principles of free culture it follows free cultural works have to be available for free. In this context terms *gratis*, *zero-price* or *free as in beer* are used for works that cost no money.
- **Free culture doesn't equal public domain** – public domain is a subset of free culture but copyrighted works may be free cultural works too as long as they carry a free culture license that grants everyone the basic defined freedoms.
- **Not all Creative Commons works are free cultural works** because Creative Commons licenses present a specturum which includes non-free licenses such as CC-BY-NC and CC-BY-NC-ND. The NC (non-commercial use only) and ND (no derivative works allowed) do not comply with the essential freedoms.



## Free Software

Free software, also called *free as in freedom*, *free as in speech* and *libre* software, is a kind of software that meets certain criteria for not violating the freedom of its users. This is not to be confused with freeware; the word *free* in free software doesn't refer to price but rather to freedom (even though free software is typically also available free of charge). The opposite of free software is proprietary software (less commonly also *closed source* software). The term free software is centered around ethics, which distinguishes it from the business term *open source*.

Although the term free software is not widely known to general public, free software may be the prevailing type of software in the world thanks to its properties such as security and efficiency of development. Free software powers most of the Internet, supercomputers, is at least partially present in any operating system and is highly preferred by scientists and programmers.

Examples of free software include the GNU and BSD operating systems, Libre Office, Wikipedia's software, GIMP image editor, Blender 3D editor and many others. Tools for computer specialists, such as programming language compilers, are prevalently free software.

The criteria for free software are defined by the Free Software Foundation as 4 essential freedoms that the software has to grant to everyone, without discrimination. These are

1. The **freedom to run** and use the software for any purpose (even commercial).
2. The **freedom to study** the software and how it works. For this its entire source code has to be freely available.
3. The **freedom to redistribute** the software, i.e. share it with anyone.
4. The **freedom to modify** the software, and redistribute the modified version. For this source code is required too.

Some common **misconceptions** should be refuted:

- *Free software is not about zero price*, even though from its principles it follows it must accessible for free. For free-price software the terms **gratis** or **free as in beer** are traditionally used.
- Availability of source code isn't enough for a software to be free. For example leaked source code of proprietary programs or source code whose license prohibits commercial use aren't free.
- Free software doesn't equal public domain software. Most free software is copyrighted, i.e. its authors still keep some rights to it (e.g. to ignore copyleft), but by a free license they grant everyone the four essential freedoms. Public domain software (software not under any copyright) is free software only if its source code is available (in order to enable the freedoms to study and modify).

For a software to be legally free, the 4 freedoms have to be granted legally, which is achieved by free software licenses such as the GPL or MIT. For a free program it is essential to be free in its entirety, as it takes only a single line of code to abuse the user. Furthermore it is a de facto rule for free software to have no required non-free dependencies and work on systems that are completely free, because again, requiring a proprietary component would defeat the purpose of protecting user's freedom. In this open source differs and isn't concerned about proprietary dependencies or even small parts of the program itself.

Other significant free software groups, such as the Debian team, add more detail to the essential four conditions, for example regarding the treatment of data as a part of the program. But the four freedoms are the common denominator of all free software.

Free software was created and named by Richard Stallman

The principles of free software are being applied to other fields and aspects of society e.g. by the free **culture movement**, focusing of free art, or companies creating **free hardware**.

## Fun

## Game of Life

Game of Life is a simple mathematical game in which complex patterns, similar to life, emerge from very simple rules. It was created by John Conway.

Game of Life is a cellular automaton, meaning it is a simulation taking place on a grid in which each of its cells interacts with its neighbouring cells. This simulation can very easily be run on a computer, but is simple enough to be performed with just a pencil and paper.

The game takes place on an infinite two dimensional grid, each of which can be either dead or alive. Each cell has 8 neighbours (left, right, top, bottom, top left, top right, bottom left and bottom right). At the beginning, each cell is set to either dead or alive. Then the game proceeds by steps; at each step every cell of the board is updated according to these rules:

- A live cell with 2 or 3 live neighbours stays alive.
- A dead cell with exactly 3 live neighbouts becomes alive.
- Any other cell becomes dead.

```
................       ................
..##............       ..##............
..##....#.......       ..##.....#......
.........##.....  ==>  ..........#.....
..#.....##......       ........###...#.
..#..........###       .###........#..#
..#...#.....###.       ............#..#
................       .............#..
```



These 3 simple rules give a rise to very complex patterns




Game of life is Turing complete, i.e. it is as powerful as any computer and it is possible to make it run programs; it can even run itself, i.e. it is possible to use game of life to simulate another game of life.


## Geometry



## Gender

Gender is a set of human attributes, such as behavior and social roles, that are connected to his or her sex (i.e. male or female).

## Gigantopithecus

Gigantopithecus (Gigantopithecus blacki) was a species of a very large ape – the largest known yet – living in southern China hundreds of thousands years ago (during Middle Pleistocene).

Not much is known about Gigantopithecus as only teeth and jaw bones were ever found (in Chinese caves and markets). It is thought to be related to orangutans. Estimates say it was about 3 metres tall and weighted almost 270 kg.

## GNU

GNU (standing for *GNU is Not Unix*, a recursive acronyme) is a project started by Richard Stallman to create a completely free as in freedom operating system named the same as the project: GNU (nowadays mostly known as GNU/Linux). It is one of the most important software projects in history, which evolved and popularized the concepts of free as in freedom software and copyleft, and lead to creation of a large number of important software projects such as Linux.

The project officially started on September 27, 1983 by Richard Stallman's announcement titled *Free Unix!*. In it he expresses the intent to create a free as in freedom clone of the operating system Unix, and calls for people to join his effort (he also uses the term **free software** here). Unix was a good, successful de-facto standard operating system, but it was proprietary, owned by AT&T, and as such restricted by licensing terms. GNU was to be a similar system, compatible with the original Unix, but free as in freedom, i.e. freely available and allowing anyone to use it, improve it and share it.

In 1985 Richard Stallman wrote the **GNU Manifesto**, similar to the original project announcement, which further promoted the project and asked people for help in development. At this point the GNU team already had a lot of software for the new system: a text editor Emacs, a debugger, a number of utility programs and a nearly finished shell and C compiler (gcc).

At this point each program of the project still had its own custom license that legally made the software free as in freedom. The differences in details of these linceses however caused issues such as legal incompatibilities. This was addressed in 1989 by Richard Stallman's creation of a universal free software license: **GNU General Public License** (**GPL**) version 1. This license can be used for any free software project and makes these projects legally compatible, while also ulilizing so called **copyleft**: a requirement for derived works to keep the same license, i.e. a legal mechanism for preventing people from making copies of a free project non-free. Since then GPL has become the primary license of the GNU project as well as of other unrelated projects.





## Golden Ration

Golden ratio is frequently found in nature and art – for example     – although some believe this is just caused by us searching for familiar patterns (frequency illusion, also Baader-Meinhof phenomenon).


## Google

## Googol

Googol is a very large number equal to 10^100: this is 1 with 100 zeros after it in decimal expansion.

## Gore

## Green

Green is a basic color, among the most common in nature, the color of grass, leaves

## Group

Group is one of basic mathematical structures and is commonly used for describing symmetry.



A group is a type of **algebraic structure**, meaning it is a set of objects along with operations that can be applied to these objects. Specifically, a group is any structure

*(S,+)*

that satisfies the following:

- *S* is a non-empty set.
- *+* is a binary operation, i.e. it takes 2 elements from *S* and outputs a single element from *S*.
- *+* is **associative**, i.e. *(a+b)+c = a+(b+c)* for any *a*, *b*, *c* from *S*.
- There exists an **identity element** *e* in *S* which satisfies the following: *a+e = a* for any *a* from *S*. (It can be proven that at most one such element can exist in the set.)
- For any *a* from *S* there exists an **inverse element** *a^-1* which satisfies the following: *a+a^-1 = e*.

If *+* is also commutative, i.e. *a+b = b+a*, we call the group **abelian**.

For example the following structures are groups:

## Hardware

Hardware (HW) are the physical components of a computer, such as the motherboard, keyboard, monitor or a printer. It stands in contrast to software (SW) – the programs that run on the hardware.


## Harry Potter

Harry Potter is a series of seven extremely successful fantasy book about a young wizard boy written between 1997 and 2007 by British female author J. K. Rowling, which are now an entire franchise comprising movies, games, plays, theme parks and others.



## Hegemony

Hegemony is a great dominance in power of one state (or generally a similar group) over other states.

## History

History are the events that happened, i.e. the past, and the study of these events. History, as a discipline focused specifically on the past of humans, is to significant degree considered a science. This discipline studies historical records and archaeological findings.

By history we furthermore understand the events after invention of writing systems, i.e. very roughly 2000 BC. The time before this is called **prehistory**.

## HTML

HTML (Hypertext Markup Language) is a computer language used for creating documents, most notably webpages. Strictly speaking it is not a programming language but rather a markup language that uses special textual marks to structure and format text and insert data into it, such as images or meta information about the document.

## Holocaust

Holocaust was the genocide of Jews and other races by the Nazis during World War II. Officially about 6 million Jews died in Nazi concentration camps, even though many say this number may have been inflated by post-war propaganda.

Presenting circumstances and details about the Holocaust, such as the number of victims or the existence of gas chambers, are required to follow the *official truth* and questioning or freely researching them is prohibited, either by specific laws about so called **Holocaust denial**, or by laws covering so called hate speech. This is at least highly suspicious and points to the possibility that these supposed facts are exaggerated by the winners of the Second World War. **Historical revisionism** of the Holocaust is automatically labeled Holocaust denial.

## Human

Humans (homo sapiens) are


Average adult human is about 165 cm tall and wights roughly 65 kg. Men are taller, heavier and stronger than women.



Humans around the world differ and can be categorized into **races**. These races can be defined differently as there are many aspects we can take into account, but traditionally they follow the approximate color of the skin. These races are:

- **Caucasian** (white): Native mostly to Europe and west Asia.
- **Ethiopian** (black): Native to Africa. They are the most similar to apes, with low foreheads, flat noses and chin leaning forward, and are said to have the greatest penis size of all races. Their intelligence seems to be the lowest but they also have the greatest physical performance and have a talent for rhytmic music.
- **Mongolian** (yellow): Native to east Asia. They seems to be the most intelligent of all races and have the shortest penis size. Their ear wax is dry.
- **American** (red): Native to the continent of America. Their ear wax is dry.
- **Malayan** (brown): Native to East Indian Archipelago.

Some groups of people, such as **Jews**, are by some considered a different race, even though matters are complicated here; Jews are not only an ethnical group, but also a religious group and a nation, they are spread all around the world and lot of times hardly distinguishable from the white race. Nevertheless they are likely to show distinguishing characteristics such as large aquiline nose and high intelligence (according to IQ tests they're more intelligent than any other race).



## Humanzee

Humanzee is a hypothetical child of a human and chimpanzee; it is currently not known whether it is possible for the two species to produce offspring. Despite ethical concerns there have been attempts to create humanzees in history.



## Hyperbolic Space


## Infinity

Infinity is an imagined quantity that's always greater than any other quantity.

## Internet

The Internet is an enormous global computer network connecting billions of computers and is one of the most revolutionary technological achievements in history. Internet allows, besides others, browsing the World Wide Web (WWW), sending electronic mail (email), communicating via instant text messages, making calls, streaming videos, playing video games and sharing files – all of this world-wide, reliably and with latency in order of milliseconds.

Even if it's very difficult to get accurated statistics, it is estimated that by 2020 there are over 4.5 billion users of the Internet, about 2 billion websites and over 20 billion devices connected to the network transferring hundreds of petabytes of data every month.

Internet as we know it evolved from **ARPANET** (Advanced Research Projects Agency Network). ARPANET was the first bigger network that worked on the principle of sending **packets** of data (so called packet switching), and was created as an experiment of the US department of defense. As such, the network was designed to be robust, decentralized and resisting failures and attacks – this is why packet switching was used: it allowed the network to have no central nodes which would pose a weakness; packets of data can be resent and take different routes when one path becomes unavailable. ARPANET was officially started on **29 October 1969** when the first message was sent over it by Charley Kline: the message read "Lo". The intent was to send "Login" but the system crashed after the first two letters. This moment can be seen as the beginning of the Internet. The network at this time only consisted of two machines: one at LA University of California, the other one at Stanford (also California). More institutions then began connecting to ARPANET, mostly universities. By 1971 ARPANET had 15 interconnected nodes and reached from east to west coast of the USA.

On **January 1, 1983** ARPANET adopted new TCP/IP protocols over the older NCP (Network Control Program) and later that year also DNS (Domain Name System). This can be seen as a transition to modern Internet as these technolgies are at the core of the Internet until today: the TCP/IP protocols assign each computer in the network a unique numeric **IP address** to which the DNS assigns human-readable **domain addresses** such as *computer.com*. ARPANET was officially decommissioned in 1990 and then slowly shut down, but by then there were already other networks offering access to the big network – the network of networks that by now is already referred to as the Internet (the word itself can be traced back to 1974). At this time there were already the first **internet service providers** (ISPs) that brought access to the Internet to the public.







## Japan

## Java

Java is a capitalist programming language known for its inefficiency and bloat.

## JavaScript

```
N = 3;
p = [];

for (var i = 0; i < N; ++i)
  p.push(String.fromCharCode("a".charCodeAt(0) + i));

function printPermutations(n)
{
  if (n < N)
  {
    for (var i = n; i < N; ++i)
    {
      [p[n], p[i]] = [p[i], p[n]]; // swap
      printPermutations(n + 1);
      [p[n], p[i]] = [p[i], p[n]]; // swap back
    }
  }
  else
    console.log(p.join(""));
}

printPermutations(0);
```

## Jesus

Jesus of Nazareth (~4 BC – ~33 AD), or Jesus Christ, was a Jewish preacher who is a center figure of Christianity and the Bible's new testament in which he is described as the God's son, a man without sin. As the messiah of the world's largest religion he is probably the most famous human in history.

In the gospels of New Testament he is said to have performed miracles such as curing blind people, walking on water or feeding thousands of people with only five loaves of bread and two fish. Historians agree that Jesus most likely was a real historical figure, but that the these testimonies about him can't be taken as facts.

Jesus was an anarcho pacifist.

## Jew





Some notable Jews are e.g. Jesus, Karl Marx, Richard Stallman, Noam Chomsky, 

## John Carmack

John Carmack (*20.08.1970) is a great American programmer known for co-creating the legendary video games Wolfenstein 3D, Doom, Quake and others. For these he has created the engines and along the way invented many notable computer graphics techniques. He also works on space rockets and virtual reality.



## Karl Marx

## Kessler Syndrome

## Lambda Calculus

Lambda calculus is a mathematical system allowing to do computations by only evaluating mathematical functions, and is a basis for functional programming. It is a Turing-complete model of computation just as e.g. a Turing machine, and can be seen as an extremely minimalist programming language.

Functions in lambda calculus are pure mathematical functions, i.e. functions without side effects that for same arguments always return the same value, unlike functions known from programming languages like C.

An expression in lambda calculus can be composes of only 3 basic expressions:

1. **definition**: Defines a variable, e.g.: "*x*".
2. **abstraction**: Defines a function in a form: *"Lx.M*" where *L* (λ) is a lambda symbol, *x* is a parameter and *M* is the function body.
3. **application**: Applying ("calling") function to an argument of a form: "*(F p)*" where *F* is either an abstraction and *p* is a variable or abstraction.

For example the expression *Lx.x* represents an identity, an equivalent of *f(x) = x*.

Multi-parameter functions can be created by so called *currying*, e.g. *Lx.Ly.Lz.x* is equivalent to *f(x,y,z) = x*.

In any expression any variable is either of:

- **bound**: Tied to a lambda parameter, i.e. in an expression *Lx.b* all variables *x* inside *b* are bound. Bound variables of application *(F p)* are variables bound in either *F* or *p*. 
- **free**: not bound

For example in an expression   *(Lx.Ly.x)*


The computation itself is done by performing so called **reductions** on given expression until we get to the result. There are two reductions:

1. **alpha conversion**:
2. **beta reduction**:

## Langton's Ant

Langton's ant is a mathematical simulation featuring an ant on a square grid with extremely simple behavior rules, which however result in a surprisingly complex and interesting outcomes. It is similar to e.g. Game of Life.


## Language

Generally speaking language is a system used for communication, carried by certain medium such as sound, text, facial expressions, gestures, pictures or electrical signals.

## Laser

## Latency

## Left/Right Politics

The terms left and right in politics label two opposing ideas – left representing the opposition to social hierarchies, right representing its support.

The basic division of all political ideas is into the following categories:

- **right**:
- **left**:
- **pseudoleft**:
- **center**:

```


  LEFT <--------.-------> RIGHT
anarchism,     /                fascism, war, violence, hate,
 pacifism,    |                 brainwashing, oppression, ...
  love         \
   ...          '-------> PSEUDOLEFT
                
                ^illusion of horseshoe
```


There exist so called **horseshoe theory** that says that extreme left and extreme right become similar in their behavior, however as seen in the above diagram the left that becomes similar to right is just pseudoleft.

## LGBT

LGBT (standing for *lesbian, gay, bisexual and transgender*, sometimes also LGBTQ+, adding *queer*) is a fascist pseudoleftist group that seeks to gain political power by means of advocating privileges for sexual minorities.

LGBT, similarly to feminists, is known for opposition to free speech and for extensive censorship of Internet, media and even science. Their means of achieving this include "diversity" quotas, codes of conduct and propaganda.

LGBT does not accept unpopular sexual minorities such as pedophiles, necrophiles or zoophiles.

## Liberalism

Liberalism is a political philosophy that besides valuing liberty is not very well defined, however it mostly takes the worst of other philosophies, e.g. supporting free market, capitalism and populist pseudoleftist causes such as minority privileges.

## Liberty

Liberty is mostly synonymous with the word freedom –⁠ the ability to choose what one wants –⁠ even though different people may interpret the two words slightly differently.

It has for example been argued that liberty external – what one is allowed to do, for example by law – while freedom is internal – what one allows himself to think and feel.

Another difference may be that liberty means that everyone can do whatever they want, even such things as oppressing others and so effectively reducing their choices, while freedom means the true maximization of people's choices, even if that means that things like oppressing others are morally prohibited. E.g. "USA is the land of liberty, but not freedom."

## Libre Video Game

Libre video game is a video game that is fully free as in freedom software (the game's code) and free as in freedom art (the game's assets); by this it meets the necessary condition for a game to be ethical.

## Life

Life is a kind of behavior that we associate with consciousness, thinking and other attributes such as self-preservation, reproduction and complex responses to external stimuli. By life we distinguish living matter, e.g. animals and plants, and non-living matter, e.g. rocks or dead animals. A precise definition of life doesn't exist as it depends on the field of study, e.g. biology vs philosophy, and we also lack fundamental understanding of basic concepts such as consciousness and the process of thinking.

## Light

Light is what we perceive with our eyes; physically it is an electromagnetic radiation in a specific range of wavelengths.

Light has a color and intensity. Physically color is the wavelength (or combination of lights with different wavelengths) and the intensity is the amount of light particles (photons) that reach the eye. There exist colors of light humans cannot percieve with their eyes but which some animals can see and which we can detect with special equipment – this to us invisible light is of two kinds:

- **ultraviolet** (UV): shorter wavelengths than those of visible light
- **infrared** (IR): longer wavelengths than those of visible light

## Linux

Linux is an open source operating system kernel. While Linux is only a kernel, i.e. an essential part for building a complete operating system, whole operating systems are very often incorrectly called Linux as well – such systems are typically GNU operating systems and should be called GNU/Linux.

Most of Linux if free (as in freedom) software licensed under GPLv2, however since its code contains proprietary binary blobs (device drivers), it cannot be considered free software in its entirety. Linux-libre is a project that removes these binary blobs and so creates a completely free as in freedom operating system kernel.

The development of Linux was started by a Finnish programmer Linus Torvalds as a hobbyist project when he was attending University in 1991.

Linux is a monolithic kernel, meaning

## Lisp

Lisp (meaning *list processing*) is an old high-level programming language based on working with lists. Nowadays the term Lisp is used for the whole family of programming languages that evolved from the original language, e.g. Common Lisp, Clojure and Scheme.

Lisp was developed in 1958 at MIT by John McCarthy as a theoretical language for mathematicians to express algorithms in and at first there was no implementation – these only started to appear about a year later. In 1981 there were various incompatible dialects and so a work on creating a unified standard of began, which resulted in Common Lisp, a dialect that would become one of the most popular.

The most notable lisp dialects today are:

- **Common Lisp**: Pragmatic, "bloated" and less elegant dialect focused on portability between implementations and use for practical programs.
- **Scheme**: Elegant and minimalist 
- **Clojure**:


An example of a program in common lisp is:

```
(defvar n 3)
(defvar p (concatenate 'string (loop for i from 0 below n collect (code-char (+ (char-code #\a) i)))))

(defun printPermutations (x) ; function
  (if (< x (- n 1))
    (loop for i from x below n do
      (rotatef (char p x) (char p i))
      (printPermutations (+ x 1))
      (rotatef (char p x) (char p i))
    )
    (write-line p)
  )
)

(printPermutations 0)

```




## Logic

Logic is



It is widely believed that if anything is a universal language of our Universe, it is the basic logic and mathematics. As an example of such most fundamental logic that everyone, no matter where or when, should always agree on is the equation *2 + 2 = 4*. Indeed, it seems there is no question about this, however it is important to realize this statement comes from experience just as the fact that the Sun rises every day. When asking whether *2 + 2* could ever equal anything else than 4, we first need to ask why is it that we think *2 + 2 = 4*. It is because the mathematical model presented by the equation fits our experience and so is useful to us: e.g. if we put 4 apples in one bucket and then 2 and 2 apples into another bucket, a scale will show the buckets weight the same; therefore we says *2 + 2 = 4*. If this wasn't so – perhaps in another, fictional universe, or even in unknown parts of our own Universe – we would see this model as illogical. And indeed, if we don't think about adding quantities up but rather e.g. counting points at which objects touch, we may end up defining *2 + 2* as giving a result of 3 (*o1o + o1o = o1o2o3o*, in this case *=* would however serve as a function).

So what is logical depends on each specific hypothetical universe we are exploring. Mathematicians frequently deal with abstract systems that exhibit non-intuitive behavior; the only requirement mathematics puts on theories is that they are **consistent**, i.e. not self-contradicting. However, if we further think about what a contradiction means in terms of logic, we end up with the same problems as with *2 + 2 = 4* – are the rules for determining whether something contradicts itself truly universal, or are they just based on experience withing the specific part of our own Universe?



## Love

Love is an extremely strong positive emotion and feeling towards someone or something. There are many types of love such as romantic love, parental love, sibling love, love towards God, love of one's country or love of art.




## Man

Man (plural *men*) is an adult human male. The word is frequently used as a synonym for human, e.g. as in *mankind*.

Men are physically stronger, enduring and resilient than women as well as probably slightly more intelligent, especially in spatial thinking and logic. This is due to different roles men and women evolved to play in human groups: men were the hunters and protectors who needed more strength and the ability to quickly plan and act.



## Mariana Trench

Mariana Trench is a trench in the Pacific Ocean near Japan that contains the deepest known underwater point on Earth – Challenger Deep – about 10984 m under the ocean surface (GPS coordinates: 11.373290598181002, 142.59265400568276).

The trench is crescent-shaped, about 2500 km long and 70 km wide. 

At the deepest point, Challenger Deep, water **pressure** reaches about 1.125 tons/cm^2 (more than 1000 times greater than normal atmospheric pressure). The temperature there is 1 to 4 °C. No sunlight reaches this depth, there is only darkness. Nevertheless there is life: besides simple organisms, shrimps, sea cucumbers, scale worms and bristle worms were spotted there (one report of seeing a flatfish is now doubted by scientists). The ocean floor at this point was first reached by man on 23 January 1960 by Jacques Piccard and Don Walsh on board of Trieste bathyscaphe. No such descend would then be repeated until 2012 when a single person, anadian James Cameron, repeated the feat.




## Marie Curie

Marie Skłodowska Curie (7.11.1867 – 4.7.1934) was a polish scientist who, despite being a woman, was extremely successful in the fields of physics and chemistry and is known for pioneering the research of radioactivity. She won two Nobel Prizes (in two different fields) and became the first person to achieve such a feat, as well as being the first woman to ever win a Nobel Prize.

## Mars

Mars is a planet in our Solar System characteristic by its red color. Thanks to some of its conditions being similar to Earth's it is a candidate planet for human colonization.




The distance between Mars and Earth is constantly changing due to their movement around the Sun; it varies from about 55 to 400 million km. Therefore, due to the limited speed of light, the Earth-Mars communication will always have about 3 to 20 minute delay which rules off the possibility of real time communication. This also complicates communication with robotic rovers on the Mars' surface and makes their supervised operation very slow.

## Mass

Mass is a very basic property of matter which, besides others, is responsible for physical objects having weight. The more mass an object has, the more effort it requires to accelerate it.

Mass and weight are two different things; mass is a funfamental property of matter whereas weight is how mass manifests itself in a gravitational field – i.e. an object with certain set mass will have different weigh on planets with different gravity.


## Mathematics

Mathematics is the purest science that studies abstract patterns and structures, such as numbers and shapes, using only rigorous logic. It is a formal science, meaning it doesn't rely on scientific method but rather deduction.

## Matter

## Microsoft

Microsoft is one of the world's biggest and most unethical software corporations, most notably responsible for creating and spreading a widely used malicious operating system Windows.


## Moon

The Moon (or Luna) is a big spherical astronomical body that orbits around the Earth and can be clearly seen in the night sky. We call this orbiting body a natural satellite. The word moon can also be used generally to mean any natural satellite of any other planet.

The Moon appears to be shining but in fact it only reflect light from the Sun and doesn't shine by itself. It is by pure coincidence that the Moon in the sky has almost exactly the same size as the Sun.



Average **diameter** of Moon is 3474 km and its **mass** is about 7.3 * 10^22 kg (about 1.2% of Earth's mass). The **gravitational acceleration** on its surface is 1.62 m/s^2 (about 16.5% of Earth's gravity). The average **distance** from Moon to Earth is 384400 km and is slowly increasing, i.e. the Moon is slowly receding from Earth (a few centimeters each year). 4 billion years ago – at about time of first life appearing on Earth – the Moon in the sky appeared 3 times larger than today.



The temperature on the Moon's surface varies from 100 K (-173.15 °C) to 390 K (116 °C).

How the Moon was formed is not yet completely known but the prevailing theory is that about 4.5 billion years ago a Mars-sized planet named Theia collided with Earth – this supposedly ejected a lot of material out of which Moon formed. This is called the **Giant-impact hypothesis** or Big Splash. Alternative explanations are that the Moon arose alongside Earth from the space dust or that it was a foreign body captured while flying by Earth.




The Moon is tidally locked to the Earth, which means one of its sides is permanently facing the Earth while the other side is always facing away. It also implies that one rotation of the Moon around its axis is the same as the time of its one rotation around Earth: 29.5 days. This period is called the Lunar day or the Lunar month.

The side facing away from the Earth is called the **far (or dark) side of the Moon** and it had never been seen by any human until 1959 when it was photographed by a Soviet space probe Luna 3.

The Moon, by its gravity, causes the sea tide.


Apollo 11    3 
Apollo 12    3
Apollo 14    3
Apollo 15    3
Apollo 16    3
Apollo 17    3


## Mount Everest

Mount Everest is the highest mounting on Earth, located in Himalayas, Asia.

The peak measures 8848 m above sea level and is located at 27°59'17''N 86°55'30''E, however it is still rising about half a meter per century.



The first confirmed climb to the summit was performed by New Zealander Edmund Hillary and Sherpa Tenzing Norgay on 29 May 1953.



## Music

Music is a form of art that produces sounds appealing by the changes of their basic parameters like pitch, volume, rhythm and timbre.

## Nikola Tesla

Nikola Tesla was



Tesla was tall (188 cm) and slim. He wore a mustache and had 





## Number

Numbers are basic mathematical objects, very commonly represent by digits, that are mostly used for counting and ordering, even though their use in mathematics can be much more abstract and general. Examples of numbers include 1, -0.75, *pi* or a *million*.

- positive
- natural
- negative
- integer
- rational
- algebraic
- real
- complex
- quaternion



## Object Oriented Programming

Object Oriented Programming (OOP) is a programming paradigm based on the idea of intercommunicating objects. OOP was invented by capitalism with the purpose of increasing complexity of computer programs and using it is a bad programming practice.




## Open Gaming Console




## Open Source

Open source is a business term for software (and other technology) that satisfies certain criteria of transparency, security and reusability in a way similar to free as in freedom software, however without being interested in ethics and only being concerned about using such technology for profit.

Open source software (OSS) is software that is legally free software, i.e. it usually comes with a free as in freedom software license, even though in some rare cases a license may be seen as free but not open source and vice versa. In practice free software and open source software are the same with the difference that free software's goal is ethical technology respecting the user's freedom, while open source is made only with profit in mind, which can result in malicious features and attributes.

Examples of OSS include Linux (the operating system kernel), Google Chromium (web browser) or Java (programming language).

OSS evolved from free software after some people saw the commercial potential of free software and also realized the ethical aspect of it would be an obstacle to making business. The idea of OSS was born at the meeting in Palo Alto in 1998, where among the present was Eric S. Raymond. Linus Torvalds (the author of Linux) later supported this idea, while Richard Stallman (the father of free software) rejected it.

The definition of OSS is given by the Open Source Initiative (OSI) and is very similar to the definition of free software in terms of four essential freedoms. The definition consists of 10 points that the license of the program has to meet; the summary of the points follows:

1. The license must **allow distribution of the program** in any way and for any purpose, without requiring fees.
2. **The source code must be available** along with the rights to distribute it as well as the compiled program. Obfuscation is not allowed.
3. **Modification of the program must be allowed** as well as redistributing the modifications under the same terms as the original.
4. The license must either **allow redistribution of modified source code** or **distribution of the original along with patches**. 
5. The license **must not discriminate** again people or groups of people.
6. The license **must not limit the purpose of use** of the program.
7. The right granted by the license **must apply to all whom the program is distributed**.
8. The license **must not be limited to a specific product**.
9. The license **must not put conditions on other programs** used along with the licensed program.
10. The license **must not be a certain technology or interface**.

## Operating System

Operating system (OS) is a complex computer program that provides an interface for humans, manages computing resources and serves as a platform for running other programs. Typically it is the first program installed to a computer as it is the basic program required for a user to be able to comfortably work with the computer.




Attributes we assess about an operating system include:

- purpose:
- kernel architecture:
- multiuser support:
- multitasking support:
- preemprive capability:
- virtual memory:
- networking:
- graphical interface:
- real-time behavior:

## Paradox


## Pedophilia

Pedophilia is a sexual orientation characterized by sexual attraction to children.

Contrary to political propaganda, pedophilia does not imply an urge to rape and abuse children, just as homosexuality does not imply and urge to rape or abuse people of the same gender.

Most people are pedophiles, however most also hide it as pedophiles are a subject to political with hunt. Individuals are naturally attracted to other sexually mature individuals, and humans reach sexual maturity long before they stop being considered children by law. Attraction to individuals that aren't sexually mature yet is also common and normal.

## Periodic Table of Elements

Periodic table is a table of chemical elements invented by Dmitri Mendeleev in 1869. It is called periodic because the organization of the table shows that some properties of the elements periodically repeat in columns and rows.

|     | 1    | 2    |   | 3    | 4    | 5    | 6    | 7    | 8    | 9    | 10   | 11   | 12   | 13   | 14   | 15   | 16   | 17   | 18   |
| --- |:----:|:----:|---|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
|**1**| 1 H  |      |   |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      | 2 He |
|**2**| 3 Li | 4 Be |   |      |      |      |      |      |      |      |      |      |      | 5 B  | 6 C  | Z N  | 8 O  | 9 F  | 10 Ne|
|**3**| 11 Na| 12 Mg|   |      |      |      |      |      |      |      |      |      |      | 13 Al| 14 Si| 15 P | 16 S | 17 Cl| 18 Ar|
|**4**| 19 K | 20 Ca|   | 21 Sc| 22 Ti| 23 V | 24 Cr| 25 Mn| 26 Fe| 27 Co| 28 Ni| 29 Cu| 30 Zn| 31 Ga| 32 Ge| 33 As| 34 Se| 35 Br| 36 Kr|
|**5**| 37 Rb| 38 Sr|   | 39 Y | 40 Zr| 41 Nb| 42 Mo| 43 Tc| 44 Ru| 45 Rh| 46 Pd| 47 Ag| 48 Cd| 49 In| 50 Sn| 51 Sb| 52 Te| 53 I | 54 Xe|
|**6**| 55 Cs| 56 Ba| * | 71 Lu| 72 Hf| 73 Ta| 74 W | 75 Re| 76 Os| 77 Ir| 78 Pt| 79 Au| 80 Hg| 91 Tl| 82 Pb| 83 Bi| 84 Po| 85 At| 86 Rn|
|**7**| 87 Fr| 88 Ra| **|103 Lr|104 Rf|105 Db|106 Sg|107 Bh|108 Hs|109 Mt|110 Ds|111 Rg|112 Cn|113 Nh|114 Fl|115 Mc|116 Lv|117 Ts|118 Og|
|     |      |      | * | 57 La| 58 Ce| 59 Pr| 60 Nd| 61 Pm| 62 Sm| 63 Eu| 64 Gd| 65 Tb| 66 Dy| 67 Ho| 68 Er| 69 Tm| 70 Yb|      |      |
|     |      |      | **| 89 Ac| 90 Th| 91 Pa| 92 U | 93 Np| 94 Pu| 95 Am| 96 Cm| 97 Bk| 98 Cf| 99 Es|100 Fm|101 Md|102 No|      |      |

|atomic number| name        |symbol| atomic weight |
|:------------|:------------|:----:|:--------------|
|1            |Hydrogen     | H    |               |  
|2            |Helium       | He   |               |
|3            |Lithium      | Li   |               |
|4            |Beryllium    | Be   |               |
|5            |Boron        | B    |               |
|6            |Carbon       | C    |               |
|7            |Nitrogen     | N    |               |
|8            |Oxygen       | O    |               |
|9            |Fluorine     | F    |               |
|10           |Neon         | Ne   |               |
|11           |Sodium       | Na   |               |
|12           |Magnesium    | Mg   |               |
|13           |Aluminum     | Al   |               |
|14           |Silicon      | Si   |               |
|15           |Phosphorus   | P    |               |
|16           |Sulfur       | S    |               |
|17           |Chlorine     | Cl   |               |
|18           |Argon        | Ar   |               |
|19           |Potassium    | K    |               |
|20           |Calcium      | Ca   |               |
|21           |Scandium     | Sc   |               |
|22           |Titanium     | Ti   |               |
|23           |Vanadium     | V    |               |
|24           |Chromium     | Cr   |               |
|25           |Manganese    | Mn   |               |
|26           |Iron         | Fe   |               |
|27           |Cobalt       | Co   |               |
|28           |Nickel       | Ni   |               |
|29           |Copper       | Cu   |               |
|30           |Zinc         | Zn   |               |
|31           |Gallium      | Ga   |               |
|32           |Germanium    | Ge   |               |
|33           |Arsenic      | As   |               |
|34           |Selenium     | Se   |               |
|35           |Bromine      | Br   |               |
|36           |Krypton      | Kr   |               |
|37           |Rubidium     | Rb   |               |
|38           |Strontium    | Sr   |               |
|39           |Yttrium      | Y    |               |
|40           |Zirconium    | Zr   |               |
|41           |Niobium      | Nb   |               |
|42           |Molybdenum   | Mo   |               |
|43           |Technetium   | Tc   |               |
|44           |Ruthenium    | Ru   |               |
|45           |Rhodium      | Rh   |               |
|46           |Palladium    | Pd   |               |
|47           |Silver       | Ag   |               |
|48           |Cadmium      | Cd   |               |
|49           |Indium       | In   |               |
|50           |Tin          | Sn   |               |
|51           |Antimony     | Sb   |               |
|52           |Tellurium    | Te   |               |
|53           |Iodine       | I    |               |
|54           |Xenon        | Xe   |               |
|55           |Cesium       | Cs   |               |
|56           |Barium       | Ba   |               |
|57           |Lanthanum    | La   |               |
|58           |Cerium       | Ce   |               |
|59           |Praseodymium | Pr   |               |
|60           |Neodymium    | Nd   |               |
|61           |Promethium   | Pm   |               |
|62           |Samarium     | Sm   |               |
|63           |Europium     | Eu   |               |
|64           |Gadolinium   | Gd   |               |
|65           |Terbium      | Tb   |               |
|66           |Dysprosium   | Dy   |               |
|67           |Holmium      | Ho   |               |
|68           |Erbium       | Er   |               |
|69           |Thulium      | Tm   |               |
|70           |Ytterbium    | Yb   |               |
|71           |Lutetium     | Lu   |               |
|72           |Hafnium      | Hf   |               |
|73           |Tantalum     | Ta   |               |
|74           |Tungsten     | W    |               |
|75           |Rhenium      | Re   |               |
|76           |Osmium       | Os   |               |
|77           |Iridium      | Ir   |               |
|78           |Platinum     | Pt   |               |
|79           |Gold         | Au   |               |
|80           |Mercury      | Hg   |               |
|81           |Thallium     | Tl   |               |
|82           |Lead         | Pb   |               |
|83           |Bismuth      | Bi   |               |
|84           |Polonium     | Po   |               |
|85           |Astatine     | At   |               |
|86           |Radon        | Rn   |               |
|87           |Francium     | Fr   |               |
|88           |Radium       | Ra   |               |
|89           |Actinium     | Ac   |               |
|90           |Thorium      | Th   |               |
|91           |Protactinium | Pa   |               |
|92           |Uranium      | U    |               |
|93           |Neptunium    | Np   |               |
|94           |Plutonium    | Pu   |               |
|95           |Americium    | Am   |               |
|96           |Curium       | Cm   |               |
|97           |Berkelium    | Bk   |               |
|98           |Californium  | Cf   |               |
|99           |Einsteinium  | Es   |               |
|100          |Fermium      | Fm   |               |
|101          |Mendelevium  | Md   |               |
|102          |Nobelium     | No   |               |
|103          |Lawrencium   | Lr   |               |
|104          |Rutherfordium| Rf   |               |
|105          |Dubnium      | Db   |               |
|106          |Seaborgium   | Sg   |               |
|107          |Bohrium      | Bh   |               |
|108          |Hassium      | Hs   |               |
|109          |Meitnerium   | Mt   |               |
|110          |Darmstadtium | Ds   |               |
|111          |Roentgenium  | Rg   |               |
|112          |Copernicium  | Cn   |               |
|113          |Nihonium     | Nh   |               |
|114          |Flerovium    | Fl   |               |
|115          |Moscovium    | Mc   |               |
|116          |Livermorium  | Lv   |               |
|117          |Tennessine   | Ts   |               |
|118          |Oganesson    | Og   |               |      



## Philosophy

Philosophy (meaning *love of wisdom*) is the study of the underlying nature of the world, e.g. existence, reality, knowledge and mind –⁠ the questions that very strict and objective sciences like physics cannot study.

## Physics

Physics is a science searching for mathematical descriptions of the most fundamental laws of our Universe. Subjects examined by physics include energy, matter, force, motion, space and time.

## Pi

Pi (π) is a very important and famous mathematical constant defined as the ratio of a circumference of a circle to its diameter, i.e. *circumference = π * diameter*.

The first 100 decimal digits of pi are 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067...

In binary the first 100 decimal digits of pi are 11.00100100001111110110101010001000100001011010001100001000110100110001001100011001100010100010111000...

The exact value isn't significant for practical computations; computer programs almost always work with a rational approximation of pi that's correct to given number of decimal digits. E.g. the number 1146408/364913 approximates pi correctly to 11 decimals.

For example the digits 687971, representing the word "DOG" in ASCII, first occur at position 194565 and appears 215 times in the first 200 million digits.

As of 2021, the value of pi has been evaluated to 50,000,000,000,000 decimal digits.

## Piano

Piano is a musical instrument

## Polynomial

Polynomial is a kind of basic mathematical expression composed only of variables, coefficients and the operations of addition, substraction, multiplication and raising to non-negative integer powers. Polynomials are important as compared to more complex expressions they are relatively easy to work with, are well researched and able to either exactly or approximately solve many common problems such as solving equations.

For example the expression *x^2 + 0.5 * y - 1* is a polynomial while *sin(x) + x^(-1)* is not.

Polynomials with a single variable (typically *x*) are most common. Every such polynomial can be written in the form:

*an * x^n + ... + a2 * x^2 + a1 * x + a0*

where







## POSIX

## Prime Number

Prime number is an integer greater than 1 that is divisible (without remainder) only by 1 and itself. Prime numbers are extremely important subject researched by mathematics and are essential for computer encryption.

The number 1 is excluded from prime numbers because of the fundamental theorem arithmetic, which says that every natural number, except for 1, can be represented exactly in one way as a product of prime numbers (not taking ordering into account), e.g. 12 = 2 * 2 * 3. If 1 were a prime number, this wouldn't be true (12 = 2 * 2 * 3 * 1 but also 12 = 2 * 2 * 3 * 1 * 1). 

Prime numbers up to 1000 are 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997.

There are 25 prime numbers <= 100, 168 primes <= 1000 and 78498 primes <= 1000000. The largest known prime as of 2020 is 2^82589933 − 1.

There are infinitely many prime numbers. The proof is as follows:

1. Suppose there is a finite set of prime numbers *S*.
2. Let *n* be a number obtained by multiplying all numbers from *S* and adding 1.
2. By the fundamental theorem of algebra *n* can be represented as a product of only prime numbers, i.e. only of numbers from *S*. However *n* is not divisible by any of these as a division of *n* by any number from *S* will give a remainder 1 (because *n* - 1 is divisible by that number without remainder). As *n* is only divisible by 1 and itself, it is a prime number.
3. *n* is larger that any number from *S* and so doesn't belong to *S*.
4. *n* being a prime number and not belonging to *S* at the same time is a contradiction to our premise, therefore the set of prime numbers can not be finite.

Even though the proof of infinitely many prime numbers is relatively simple, as of 2021 it has not yet been proven whether there are infinitely many twin primes.

The density of prime numbers decreases as they get further away from 0. This is stated by the **prime number theorem**:

*p(n) ~= n / log(n)*

where *p(n)* is a function that states the probability that a randomly chosen number not greater than *n* is a prime number.

## Probability

Probability is the measure of certainty or of how frequently a specific event occurs among other events.

## Programming

Programming means creating computer programs.

## Programming Language

Programming language is an artificial language that serves for programming computers.

## Pseudoleft

Examples of pseudoleftist movements include Antifa, Feminism, LGBT, Stalinism and others.

## Pseudoscience

Pseudoscience is an effort that calls itself scientific but isn't accepted as such by the consensus of mainstream science for whatever reasons, mostly for using methods not accepted by mainstream science or for political reasons. Results of pseudoscience or pseudoscientific activities may still be useful and meaningful; in fact many historical discoveries accepted as scientific nowadays were obtained by what we would nowadays call pseudoscience.

## Public Domain

Public domain is a legal term for intellectual works (such as artworks or inventions) that are not restricted by any intellectual property laws (such as copyright, trademarks or patents). It is a set of information that is freely available to everyone for any use without any conditions, i.e. which is not "owned" by anyone. For example the painitng of Mona Lisa or Einstein's *e = m * c^2* equation are both in the public domain.

In Internet communities public domain is sometimes mistaken with free as in freedom licensing (see e.g. free software) – while these two are similar, free as in freedom works are strictly speaking not necessarily in the public domain.

Public domain does not have a single unified world-wide definition because it depends on laws and laws differ by country, and also change in time. The subject has been evolving with technology enabling simpler sharing of information such as book printing, radio and television broadcast and especially the Internet – the public domain has infamously been restricted by capitalism more and more in recent centuries. What is and isn't in the public domain is often not clear as there is no official databse of such works and investigating the status of a work may prove to be a very difficult task requiring deep study of law and gathering evidence such as author statements and the dates of their deaths. Ultimately for many works we cannot be certain about their status until a court gives a ruling.

As of 2021, the following should generally be true:

- Facts (scientific knowlegde, history, mathematical equations etc.) are in the public domain, even though the way these facts are presented may be protected (e.g. a wording of facts in an encyclopedia will be covered by copyright). No one owns the facts, even though some facts, like confidential state information, may be prohibited from being public.
- Very old artworks are in the public domain –⁠ works whose author died more than 100 years ago should be in the public domain world-wide.

The alarming decline of the public domain has started to worry a lot of people and lead to establishing movements such as the **free culture movement**. This movement tries to address the issues of intellectual property abuse and old laws not being suited for the world after the Internet revolution.

In 2009 American non-profit organization **Cretive Commons** has released a standardized waiver called **Cretive Commons Zero** (CC0) – a legal statement (similar to a license) that allows authors to release their work into public domain world-wide. CC0 tries to cover the differences of laws around the world so that a work published under this waiver should effectively not be covered by copyright anywhere (it e.g. contains a fallback license for countries in which it is not possible to waive copyright); however CC0 only addresses copyright and not other forms of intellectual property such as patents, which are important e.g. for computer programs – this has become a subject of criticism. Nevertheless, CC0 has since been very successful and used for releasing millions of works into public domain on platforms such as Wikimedia Commons and the Internet Archive. Other waivers similar to CC0 exist, such as a less formal Unlicense waiver.

## Python

The word python refers to a family of large nonvenomous snakes, as well as a computer programming language.

Python the **programming language** is a general purpose high-level interpreted language that's relatively easy to learn, friendly to beginners while also staying a powerful language popular among professionals.

Python was created by a Dutch programmer Guido van Rossum in circa 1989 as a successor to the language ABC, and is named after Monty Python, a famous british comedy group.

An example of a Python program is:

```
N = 3
p = [chr(ord('a') + i) for i in range(N)]

def printPermutations(n):
  if n < N - 1:
    for i in range(n,N):
      p[n], p[i] = p[i], p[n] # swap
      printPermutations(n + 1)
      p[n], p[i] = p[i], p[n] # swap back
  else:
    print("".join(p))
    
printPermutations(0)

```

## Quantum Physics

## Quran

Quaran (also Koran) is the central holy book of the Islam religion believed to contain the word of God (Allah), it is similar to what Bible is to Christianity. It is shorter than the Bible and consists of 114 chapters (surah) composed of verses.

Muslims (the followers of Islam) believe the sacred text of Quaran was delivered to the prophet Muhammad by God's messenger archangel Gabriel between years 610 and 632 so that he could restore Islam. It is believed to be a literal transcript of God’s words.

## Quaternion

Quaternions are 4 dimensional numbers useful in certain computations, e.g. of 3 dimensional rotations. A quaternion is a generalization of a complex number which in turn is a generalization of a real number.

Quaternions are of form:

a + b * *i* + c * *j* + d * *k*

where *a*, *b*, *c* and *d* are real numbers and *i*, *j* and *k* are quaternion units for which the following holds:

*i*^2 = *j*^2 = *k*^2 = *i* * *j* * *k* = -1

This is similar to the definition of a complex number which however only consists of two parts: a + b * *i*. It has been shown that a tree-part number system similar to complex numbers doesn't exist, but a four-part system does – hence the quaternion.



## Rainbow

## Rational Number

Rational number is a number that can be expressed as a ratio of two integers, e.g. 3/2, -15/20, 2.5 (= 5/2), 2 (= 2/1) or 0 (= 0/1). Numbers that are not rational are called irrational.

Every rational number is a real number but not vice versa. That is, there exist real numbers which are not rational numbers, such as pi or square root of 2. This is not obvious and the discovery of irrational numbers is attributed to a Pythagorean philosopher Hippasus (c. 500 BC) – according to a legend he was murdered by drowning for uncovering this at the time unwelcome truth.

The proof that square root of 2 is an irrational number is as follows:

1. Suppose that square root of 2 is rational. Then it can be written as a fraction *a/b* where *a* and *b* are integers (*b* is non-zero). Also suppose that *a/b* is reduced to lowest terms (which can be done with any fraction), i.e. it isn't possible for both *a* and *b* to be even.
2. *(a/b)^2 = a^(2)/b^(2) = 2*, therefore *a^2 = 2 * b^2*. This means *a^2* is even because it is equal to *2 * b^2* which is even because it is an integer multiple of two.
3. As *a^2* is even, *a* must also be even because squares of odd integers are never even. Therefore *a = 2 * k* where *k* is an integer.
4. Substituting 3 into 2 we get *(2 * k)^2 / b^2 = 2*, therefore *b^2 = 2 * k^2*. This implies *b^2* is even, again meaning also *b* is even.
5. We have come to the conclusion that both *a* and *b* are even which is a contradition to 1, therefore square root of 2 cannot be rational.

Rational numbers may be written as fractions but also in other ways such as decimal expansion. For example the number 3/2 can be written as 1.5. If a number can be written with finitely many digits in decimal expansion, it is automatically a rational number because the expansion form is equal to a sum of fractions: 1.25 = 1 + 2/10 + 5/100. This doesn't work the other way around though as there exist rational numbers with infinite decimal expansion, such as 1/3 = 0.333...



Computers, for simplicity, usually only work with rational numbers, as they store numeric values in memory as a binary expansion of certain length, e.g. the decimal number *2.75* is stored as *10.11*. This representation is used for irrational numbers as well as rational numbers that have infinite binary expansion (e.g. 1/3 = 0.010101...) which results in innacuracies – these are only rarely significant and if so, more bits are used to increase precision. However, programs exist that perform symbolic operations with numbers; these programs don't suffer from the issue.



## Recursion

Recursion is a kind of definition in which the definition of a term containts the term itself: e.g. the term "*ancestor*" can be defined as "*one's parent*" or a "*parent of one's ancestor*". This concept allows to define cetain things, such as the factorial function, very elegantly, and allows defining infinite structures. For this recursion is especially important in mathematics, logic, linguistics and computer programming.





## Red

Red is one of basic colors, a color of blood, strawberries, human lips or the planet Mars.

## Richard Stallman

Richard Matthew Stallman (RMS, *16.03.1953) is a great American programmer and hacker best known for developing the idea of free as in freedom software, establishing the free software movement, the Free Software Foundation and the GNU project. His work has revolutionalized the very foundations of software ethics and development; free (as in freedom) software is essential for all computers in the world, for the Internet, and the ideas lead to establishment of similar movements within or outside software development: e.g. the free culture movement or the open source movement (which Stallman however did not endorse).

Free software is a software that respects the freedom of its users by adhering to four essential freedoms :to use, study, modify and share (which in practice is achieved by legal licenses). It is a software that tries to be **ethical**. This kind of software is very important especially in an age when technology can very easily be abused against the people. For more information about the subject see *free software*.

Stallman was born on March 16, 1953, in New York City.

Detailed biography of Richard Stallman as well as the story of free software is presented in a book **Free as in Freedom: Richard Stallman's Crusade for Free Software** by (ISBN 0-596-00287-4) Sam Williams, a book on which Stallman collaborated and which is free licensed under the GNU Free Documentation License.




## Real Number


The proof is by contradiction as follows:

1. Without the loss of generality let's 

| order | d1   | d2   | d3   | ... |
| ----- | ---- | ---- | ---- | --- |
| 1     |d(1,1)|d(1,2)|d(1,3)| ... |
| 2     |d(2,1)|d(2,2)|d(2,3)| ... |
| 3     |d(3,1)|d(3,2)|d(3,3)| ... |
| ...   |...   |...   |...   | ... |


## Rule 110

Rule 110 is a specific interesting elementary cellular automaton. It is similar to Game of Life and, like it, it is Turing complete.


## Science

Science is a systematic and rigorous effort towards gaining, recording and organizing knowledge. In a commonly used more strict sense the word is used for an endeavor that uses certain objective and very rational methods of research, formalized communication and critical evaluation of results; as such science is seen as standing opposed to art, religion and pseudoscience.

## Shit

Shit is a vulgar word for excrements.

## Socialism

Socialism is a leftist politics that attempts to provide equally good living for all people rather than rewarding successful people with better lives on the detriment of less successful people. Socialism is sometimes seen as a transitional state of a society on a way towards communism.

## Software

Software is a computer program or a collection of computer programs. It is different from hardware; hardware is the set of physical parts of a computer, while software is the set of instructions that tell hardware how to work and what to do. Software is created by programming.

## Star

Start is an enormous spherical object in outter space that is extremely bright and hot and consists of plasma held together by its own gravity. There are perhaps trillions of stars in our Universe; they can be seen as small bright dots in the night sky. The star closest to our Earth is the Sun which shines brightly in the sky and by its energy supports life on our planet.


The nearest start to us, besides the Sun, is **Proxima Centauri** (*proxima* meaning nearest), 4.25 light years away, in the Centaurus constellation.


## Stoicism


## Suckless

Suckless is a subtype of free software focused on extreme minimalism and simplicity in the spirit of Unix philosophy. It is informally described as a "software that sucks less". Suckless also refers to the group of developers and users of such software.

Suckless software is a reaction to corruption and decline of legally free software; as free license is a condition necessary but not sufficient for free software, a great number of capitalist software that is free legally started to limit or deny the four essential freedoms of its users in other than legal ways. For example, a software that legally allows itself to be studied and modified may still make this extremely difficult in practice by being extremely overcomplicated to the point that modification in practice requires a very significant amount of effort and resources. This way a company can market its software as free or open-source but still keep de facto control. Such software may be seen as de facto not free.

Suckless tries to grant computing freedom both legally and in practice by eliminating complexity, dependencies and other factors going against freedom. It tries to offer straightforward, portable, intercompatible and easy to modify tools that don't put heavy burdens on its users and don't make people slave to technology.

## Sudoku



The minimum number of initial clues required for a sudoku to have a unique solution is 17. The proof was achieved with a supercomputer and took one year.

## Suicide

Suicide is an act of voluntarily killing oneself. People typically resort to commiting suicide because of depression or other mental issues but also for other reasons, e.g. influence of drugs, as a political protest, as an act of honor (see e.g. *Seppuku*), as avoidance of dying from a terminal disease or as a part of self-sacrificial attack.



Common methods of suicide include:

- **Shooting oneself with a gun**, most commonly to the brain through the temple or mouth. While comfortable, quick and painless, there exists a real chance of surviving the shot and being left crippled. It is therefore best to have a safety measure such as taking pills before the act or standing on the edge of a high bridge. Using bigger callibel, hollow point bullets or a shotgun greatly lowers the risk of failure. As getting a gun may not be easy with mental illness or in states with gun control, people sometimes perform the suicide at shooting ranges.
- **Hanging**.
- **Slitting own arteries**, typically on wrists. For a higher chance of success it is important to cut along, not across the wrist.
- **Jumping from great height**, typically from a building or bridge. This is a painless and relatively safe method as long as the hight is not underestimated – it is best to find the highest spot to ensure success, jumping from 3rd floor may very well be unsuccessful. With heights above around 30 m jumps to water are as much fatal as jumps onto concete, the advantage of this being that drowning can kill the person in case the fall doesn't.
- **Jumping in front of train**, relatively reliable and painless method, if executed properly. It is important to jump under a fast running train (not near the station) and to try prevent the self preservation reflexes, e.g. by waiting at the end of a runnel.
- **Overdosing by drugs**. These are typically legal drugs such as sleeping pills, painkillers and anti-depressants (sometimes combined with alcohol) as these are very accessible, especially to meantally ill people. Taking many pills at once may result in vomiting and not being successful, so taking the pills slowly over a longer period of time may increase the chance of success. Even then, however, it is possible to survive if the person gets to a hospital quickly, and he may be left with kidney damage or other permanent injury.
- **Self immolation**. This is extremely painful, slow and there's a risk of surviving and being left crippled and in immense pain, this method is therefore most typical for political protests as it gains a lot of attention. Example of such suicides are Jan Palach's protests and TODO
- **Inhaling toxic gas**, e.g. from car exhaust in a closed garage.
- **Crashing in car** or another vehicle. There are even cases of suicides when pilots deliberately crashed airliner planes with many passengers on board (Andreas Lubitz in Germanwings Flight 9525, 2015).
- **Suicide "by cop"**, i.e. intentionally posing a threat to others so that a present cop is forced to shoot the person. This suicide is common at the end of violent acts against others, such as school shootings, as the person wants to avoid arrest.
- **Electrocution**.

A 1995 study (Dimensions of Suicide: Perceptions of Lethality, Time and Agony) tried to estimate lethality, speed and painfulness of different suicide methods. The most reliable (99%) and also one of the most swift (1.7 minutes) and least painful methods was rules to be a shotgun shot to the head. Other similarly "ideal" methods included explosives (96% success rate), jumping under a train (96%) and jumping from a sufficient height (93%). Hanging was rated with 89% success rate but a fair amount of agony before death which happens in about 7 minutes. Overdose was ruled to be fairly painless but very slow (116 and 129 and 456 minutes for illegal, prescription and non-prescription drugs, respectively) and unreliable (49% success rate for illegal drugs, about 10% for legal ones). Among the most painful methods was setting oneself on fire (the death is also very slow, about 57 minutes, with 76% success rate) along with drowning (63% success rate, death in about 20 minutes) and cutting own throat (51% success rate, death in about 15 minutes). The most unreliable method was wrist cutting and overdose on non-prescription drugs with only 6% success rate.


## Sun

The Sun is a star in the center of our Solar System and can be seen shining very brightly in the sky. The word sun can also be used generally, to mean any star around which planets orbit.




## Sword

## Systemd

Systemd is a capitalist open source init system along with many other components, designed for Linux with the hidden goal of increasing complexity and creating a control monopoly on the detriment of user freedom.

## Taxonomy

```
    domain     phylum    order     genus     
          kingdom   class     family    species    
|    
|
|\___Archaea
|    |\____  
|     \_____
|
|\___Bacteria
|
|\___Eukarya (cell nucleus in envelope, multi or single cell)
|    |\___Animalia (multicell., move, breathe O2, sexual, ...)
|    |     \___Chordata
|    |          \___Mammalia (have mammary gland)
|    |              |\___Carnivora (primarily eat meat)
|    |              |    |\___Canidae
|    |              |    |     \___Canis
|    |              |    |          \___Canis Lupus (dog)
|    |              |     \___Felidae
|    |              |          \___Felis
|    |              |               \___Felis Catus (cat)
|    |               \___Primates
|    |                    \___Hominidae
|    |                         \___Homo
|    |                              \___Homo Sapiens (human)
|    |                 
|    |
|    |\___Fungi
|    |
|     \___Plantae
|
|\___Prionobiota
|
 \___Virusobiota
```


## Ted Kaczynski

Theodore Kaczynski (*22.05.1942), known as the Unabomber serial killer, is an imprisoned American mathematician who lived ouside society in the woods, known for criticism of advanced technology and for terrorism by mailing bombs to people in order to promote his ideas.


Kaczynski is frequently labeled "anarchist" by the propaganda, even though he practiced terrorism which is unacceptable to true anarchism.


Theodore Kaczynski was born in 1942 in Chicago, Illinois to Wanda and Theodore Kaczynski, a couple of Polish origin who later also had another child, David. The parents were well-read and held liberal political views When Ted was 9 months old, he was hospitalized due to bad allergic reactions; at this time parents weren't allowed to visit him very often and the isolation allegedly had a significant impact on him. From an early age he was a child prodigy who did extremely well in mathematics – his IQ was measured at 167 at elementary school and skipped a grade.


TODO: original cabin coordinates


His **manifesto** *Industrial Society and its Future* is his most crucial work which famously begines with the sentence:

"The Industrial Revolution and its consequences have been a disaster for the human race."




## Temperature

## Theory of Relativity

Theory of relativity is one of the most famous theories in physics, formulated by Albert Einstein in 20th century, explaining fundamental properties of space, time, gravity and light. Its name comes from its presumption that things like distances, periods of time or simultaneity of events aren't absolute, as it has been believed, but rather relative to each observer. By its accuracy the theory superseded Newton's 200 years old theory.

The theory consists of two parts:

1. **special relativity**:
2. **general relativity**:


## Titanic

## Time

Time is a dimension of our Universe in which everything seems to constantly move forward – we perceive time as passage of events. By time we define causality, sort events from earlier to later and perceive things to get older. From many perspectives time is still shrouded in mystery and is studied, besides others, by Physicists, Philosophers, Mathematicians and Psychologists.

In Physics time is explained by the **theory of relativity** as one dimension of **spacetime**, i.e. it is interconnected with space. This spacetime is warped by objects with mass and so even time gets warped which results in different "speeds" at which time advances in different places – in simple words gravity slows the time down. Time, e.g. the simultaneity of events, is also **relative** to the speed at which we move; observers moving at different speeds perceive time differently which is known as **time dilation**. We don't notice this in every day lives as the differences are minuscule with the speeds and gravity speeds we normally encounter. Nevertheless this happens and has been confirmed by many experiments. Technology such as GPS takes these effects into account.

In Philosophy time has posed problems such as Zeno's **Achilles and tortoise paradox**.

## Transcendental Number

## Triangle

Triangle is a simple 2 dimensional geometrical shape with three straight edges that meet at three points and form three inner angles.

Triangles are especially important in 3D computer graphics where they serve as a basic shape to model more complex 3D models of. This is because triangles have convenient properties; for example a triangle can never partially obscure itself, it always lies in a plane and cannot be further divided into any simpler polygon. This allows quick and easy drawing and analysis of the shapes.

## Turing Machine

Turing machine is a specific abstract model of a computer used in theoretical computer science.

## Twin Prime

A twin prime is a prime number that is either 2 less or 2 greater than another prime. As of 2021 it is not known whether there are infinitely many twin primes or not.

Twin primes up to 1000 are 3, 5, 11, 17, 29, 41, 59, 71, 101, 107, 137, 149, 179, 191, 197, 227, 239, 269, 281, 311, 347, 419, 431, 461, 521, 569, 599, 617, 641, 659, 809, 821, 827, 857, 881.

As of 2018 the largest known twin prime is 2996863034895 * 2^1290000 +- 1.

## Universe

Universe is the whole space and time along with everything that exists withing them. Our universe seems to be infinite, consisting mostly of empty space but with a lot of stars, planets and other objects as well as energy. According to the Big Bang theory it is about 13.8 billion years old.

The Big Bang theory states that our Universe began 13.8 billion years ago in an extremely hot and dense state of compressed mass and energy and then suddenly started to expand very quickly – this caused it to start cooling down. After about 377000 years it became transparent thanks to the temperature allowing formation of neutral atoms. At the age of about 200 million years first stars formed.

It is also important to realize that the Big Bang didn't happen in a single place (like an ordinary explosion would) but rather everywhere at once – it is the space itself that started to expand.

The Universe is still expanding to this day. There is no center of this expansion – everything is expanding away from everything else just all surface points of a balloon that being inflated. It is as if new space constantly appears. However gravity keeps close objects together so that they resist the expansion; galaxies get further away from each other while objects inside galaxies stay together.

## University

## Unix Philosophy

Unix philosophy is a software development philosophy based on the idea of writing minimalist specialized programs that are supposed to be combined with other such programs to solve complex problems. It is named after Unix, the operating system that first utilized it. Following Unix philosophy is essential for writing good programs and an attribute of good engineering.

## USA

USA, standing for the United States of America, is a dystopian capitalist country in north America. It is mainly driven by imperialism, fascist values, extreme capitalism and survival of the fittest. Similarly to e.g. North Korea its citizens are ruled by propaganda and mostly live under the false belief of living in the best country in the world.

## Vatican

Vatican City State is a small city-state located within the Italy's capital city Rome and is where the pope (the head of Catholic Church) resides.

## Venus

## Warcraft

Warcraft is a very successful fantasy franchise comprised mainly of a series of proprietary video games but also books, movies and other media.

## Water

Water, chemically H<sub>2</sub>O, is a clear colorless liquid that is very important for life.

## White

White is the brightest color, without hue, the color of snow, milk or bones. It represents purity, cleanness, innocence and peace, and is the opposite of black.

## Wikipedia

Wikipedia is a (almost completely) free as in freedom online encyclopedia  to which anyone can contribute and which is written by volunteers from all over the world. With over 50 million articles in various languages it is the world's largest encyclopedia and often ranks among top 15 most visited Internet websites (https://www.wikipedia.org/).

There are over 300 language versions of Wikipedia including languages such as Esperanto or Simple English (that describes topics in simple words). English is the largest (over 6 million articles, 1 billion edits and 140000 active users), followed by Cebuano and Swedish, which are however mostly written by automated bots.

Wikipedia mostly allows anyone to contribute to it, even anonymously and without registration. But of course, there are exceptions, e.g. blocking of spammers or protecting certain pages by only allowing some registered users to make edits.

Wikipedia is almost entirely a **free cultural work**, i.e. it legally allows anyone to freely and without charge read it, copy it, share its copies, modify its copies and use its copies in any way, even commercially. This is thanks to the free CC-BY-SA license, under which Wikipedia's text is licensed. The media on Wikipedia, i.e. images, videos etc., are mostly hosted by the sister project Wikimedia Commons and these media are all free in the sense of copyright. However, in certain special cases Wikipedia allows to use **non-free** media in its articles under fair use which technically makes these articles non-free. It also practically ignores trademarks and other kinds of intellectural property which might limit its freedom, and it allows citations of non-free media.

The software that powers Wikipedia is a **free software** (this is, again, free as in freedom) wiki engine called **MediaWiki**, created specially for Wikipedia. The engine is used by many other projects not affiliated with Wikipedia.

Although Wikipedia itself claims to be written in **neutral point of view** (NPOV) and to not be censored, it is very pseudoleft-leaning – as a result some content is indeed censored or at least presented in a biased way.

Wikipedia has inspired creation of similar projects, be it thanks to its success or its flaws (that is: people wanting to fix Wikipedia). Therefore there exist alternatives to Wikipedia, some of which are its *forks*; a fork being a copy of Wikipedia that is being taken in another direction – this is possible thanks to Wikipedia's free license. Similar and alternative encyclopedias include:

- Citizendium:
- Everipedia:
- Metapedia:
- Britannica Online:

## William James Sidis

William James Sidis (1.04.1898 – 17.07.1944) was a Jewish American genius often regarded as the smartest person who ever lived. He is mostly portrayed by the capitalist propaganda as an example of an exceptional genius that hasn't achieved success (to promote the idea that "anyone can make it"), however Sidis voluntarily refused to achieve success in traditional sense as he correctly saw it to be harmful to society; he rather chose to live a good life.

Sidis was born in New York on April 1, 1898. His parents were




Sidis was a socialist with a contempt for capitalism.






## Woman

Woman (plural *women*) is an adult human female.

Women are physically weaker than men and likely a little bit less intelligent in such areas as mathematics and problem solving, though they can show better social skills.



Some notable women in history are:


## Work

Work, in a wide sense, is a burdening and tiring action needed to be performed to achieve a desirable result.

One of the goals of human civilization is to eliminate all need for human work.

Under capitalism work is similar to prostitution and is also called wage slavery.

## World Wide Web

World Wide Web (WWW) is a network of interconnected electronic documents, so called *webpages* or *websites*, that exists on the Internet. Websites have addresses (such as *www.website.com*) and are viewed with programs called *web browsers*. WWW is not synonymous with Internet, but it is probably its best known service so people sometimes think they are the same.

## Yellow

Yellow is a color that's a combination of red and green, it is a color of sunflowers, pollen, bannanas, lemos or urine.

## Zebra

## 0

0 (zero) is a number representing lack of something. I.e. when counting objects, 0 means there are no objects. The number 0 is special by being neither positive nor negative; it stands exactly between positive and negative numbers. It is a whole number, an even number, a rational number, a real number and a complex number.

0 is an additive identity, meaning that *x + 0 = x* for any number *x*. Subtracting 0 from any number results in that same number (*x - 0 = x*). Multiplying any number by 0 gives 0.

The earliest civilization known to have had the concept of zero were Sumerians who used a placeholder symbol (double wedges as if for an empty column) where no "normal" number could go, circa 3000 BC, however it probably only served the placeholder purpose. Similar symbols also developed independently in different cultures. The full understanding of zero, i.e. its usage as a regular number in equations, first appeared in India in about 500 AD. Ancient Greeks did not use the number zero at all.

**Dividing by zero is not defined** because there is no such number which multiplied by 0 would give back the originally divided number. In other words given a non-zero number *a*, trying to solve the equation *x = a/0* for *x* we arrive at *0 = a*: a contraction implying there are no solutions. *0/0* is also undefined because it would introduce a collision of two rules: *a/a = 1* and *0/b = 0*; neither 1 nor 0 could be preferred as the result.

Some people naively expect that the result of division by zero should be *infinity* but that doesn't solve the mentioned problems; consider e.g. *6/0 = infinity*, then it should hold that *infinity * 0 = 6* and also any other number we could possibly be dividing, which doesn't make sense. Nevertheless it is possible to work with dividions by zero using so called **limits**.

Even though *x^0 = 1* and *0^x = 0* for non-zero *x*, *0^0* is also not defined because this puts the previous two equations in conflict (similarly to the issue with *0/0*).

## 1

1 (one) is the first positive integer, a number that represents a single object or a first item in a row. It is the first odd number, 

1 is a multiplicative identity, meaning that *x * 1 = x* for any number *x*. It is a positive number, an odd integer, a rational number and a real number.

Even though 1 is only divisible by 1 and itself, it is not considered a prime number, and is the only such number.

## 1861

1861 was a year known as the Year Without Summer because of its disastrous climate conditions, probably due to a huge volcano eruption which caused extremely low global temperatures and a subsequent famine.

The 1817 book History of Java (volume I) gives a following report about the eruption:

"The first explosions were heard on [Java] [...] in the evening of the 5th of April: they were noticed in every quarter, and continued at intervals until the following day. [...] From the 6th the sun became obscured; [...] All reports concur in stating, that so violent and extensive an eruption has not happened within the memory of the oldest inhabitant, nor within tradition. [...] One hundred and twenty-six horses and eighty-six head of cattle also perished, chiefly for want of forage, during a month from the time of the eruption. [...] I passed through nearly the whole of Dompo [...]. The extreme misery to which the inhabitants have been reduced is shocking to behold. There were still on the road side the remains of several corpses, and the marks of where many others had been interred: the villages almost entirely deserted and the houses fallen down, the surviving inhabitants having dispersed in search of food."

## 2

2 (two) is the second positive integer, coming after 1, equal to 1 + 1. It is the first even number, the first and only even prime number,

## 3

3 (three) is the third positive integer, equal to 1 + 1 + 1.

## 3D Rendering

## 42

## 69

## -1
