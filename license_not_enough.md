# License Isn't Enough For A Software To Be Free

It never has been and it's becoming apparent now more than ever. Decades of highly evolved capitalism have been, through unprecedented power of tech giants, rapidly shaping technology to prey on people, to invade their privacy, to make them dependent, to make them consume, to push tons of advertisement and brainwashing, refuse compatibility and attempts at repairs and fixes. Everything's unnecessarily and dangerously interdependent, wasteful, "in the cloud" and based on constant maintenance and updates, constant artificial growth that only exists for the sake of keeping the capitalist machine going. Technology that was meant to be our victory has become the cancer of our society.

This evil of constant and blind pursuit of maximum profit for all costs permeates the whole design of today's technology, from the ground up, from programming languages to user interfaces –⁠ everything is designed with no other goal than to make more money, and this design is incompatible with freedom.

It is not anymore just about the technology, it is the whole culture around technology that's been formed to stand opposed to the user: the idea that you need yet higher resolution, a new phone with more cameras, a faster graphics card, a fridge with artificial intelligence, watch connected to the Internet, a photorealistic game with weekly content updates, an interface that flashes, animates and makes smalltalk with you.

A license isn't nearly enough to deal with this. It never has been and no one ever said it would be. Richard Stallman has indeed foreseen this from the start. If we read the GNU project's definition of free software, we read it starts with the sentence

*A program is free software if the program's users have the four essential freedoms: ...*

There is no mention of any legal grant of freedoms, it is only said the user needs to **have** the freedom, not just by law but mainly de facto, practically.

A license is but one tool for helping freedom, but throwing a license at oppressive culture won't magically make it free.

Yes, a free software license was a revolution in the 80s, back when technology wasn't so spoiled, back when there was only one obstacle to freedom: copyright. Ten decades later now? A license is a start, an important but tiny first step, a condition necessary but not sufficient. A step after which, sadly, most projects stop.

A legal right to modify a program is worth nothing if almost no one can execute it. If a project consists of 10 millions lines of highly complex code and requires a deep know how and very specific setup to just compile, it is practically exclusively controlled by its developer, usually a company with a lot of money to pay several full time developers. If a program can, in theory, run on old computers but decides to rather require the latest hardware to enable "popular features", and in doing so discriminates against poor users or users who use old computers e.g. for security reasons, in a sense betrays freedom 0: the freedom of indiscriminatory use. If a project makes the decision to strive for momentarily popularity and decides to tie itself to dozens of dependencies, many of which are likely to die within a decade, it makes the decision to discriminate against the users in the future.

No one is concerned with real freedom anymore. No one knows the essentials of good engineering, efficiency, the Unix principles. Dare to voice criticism? Be prepared to be met with a recommendation to shut up, "we have a free license, therefore we're free", "that's how things are done today" or better yet "we're open source, our source is open!" Projects simply copy paste the proprietary culture with all its malicious design and put up a "free" sticker as a mark of coolness, perhaps a nice brand to help the marketing, to get a few more stars on GitHub for a nice portfolio.

We've become –⁠ or have been lead to become –⁠ so focused on the means, the license, that we've forgotten the ends: the freedom. The culture of free software and free art isn't a copy paste of today's culture with a piece of paper attached. It is a whole new way of thinking. A one that requires us to abandon the shallow short-sighted design of consumer technology, and art as well. A decision to stand out of the popular, to say no to the modern when modern means yet more sophisticated in restricting freedom.

Yes, we need licenses today, but we need much more. So let's look into the future, and in doing so let's take a step back from the wrong path to the intersection we've already visited, so that we can start heading in a new, better direction.