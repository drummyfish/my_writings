# WIP

# Biography

As a child I experienced the last days of the pre-Internet era, the world without computers and cell phones. And like that, I also still remember the days of the young, free Internet.

Having been born to a post-communist country at the exact moment the regime fell, I had the rare opportunity to spot the brief moment of a newly born capitalism, the moment at which everything prospers and flourishes, a moment that can nevertheless never last for too long as the capitalist system is destined to become its own cancer.

As I grew up I watched the downfall of the civilization once great. A decadence began shortly after I reached adulthood. It was brought about by the aforementioned illness of capitalism.

# 1990s

90s were the decade of my childhood. I experienced the best of the decade and now I know it was a true blessing to have been born to such special times – I say so because I would later, in retrospect, with an effort to account for nostalgia, judge the decade as the best, the one at which civilization peaked. After this everything would only go downhill about the civilization, and about my life. The 90s were easy, joyful, full of innovation and blooming art. And the few signs of incoming downfall that might have been arising I didn't see, as I was yet a child.

The decade was furthermore especially prominent in a country where I lived, Czechoslovakia – it was the first decade of a newly established capitalism, after 41 years of the totalitarian "communist" regime. As I was born, people were just starting to breathe the air of newly regained freedom, but still under the mentality of togetherness and mutual help that was needed to survive in the previous regime. This was the very rare moment of history that briefly occurs at the beginning of capitalism, when businesses are not yet big enough to lose human conscience, when the hard competition has not yet suffocated all morality, when people are not yet being daily brainwashed by ads, spied on by technology and forced to slave themselves to death. Unfortunately, this can never last.


## 1990

I was born