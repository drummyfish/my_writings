WORK IN PROGRESS

This is a description of an original fantasy world that can server as a framework for other works such as books or games.



## General

The spirit and principles of creating works from this universe are similar to those of the games from the proprietary Elder Scrolls universe.

The world tries to correspond to the real world in many aspects so that basic concepts stay the same: for example the length of day and year are the same as in real world so that the in the universe the words "day" and "year" exist with the same meanings. The world also has one Sun and sky similar to the real sky so that it is easier to e.g. make fan movies without requiring editing of the sky. This friendliness to derivative art, simplicity and correspondence to real world is part of the general spirit of this work.

**Canon**: material in this text is only a guideline, and outline, a helper that should serve artists create specific works. Therefore, this text doesn't make any part of the lore canon. The official canon should only be derived from material present in the specific works. E.g. if some historical event is described in this text here, it's not canon yet; but when the event appears described in a book in a game from this universe, it becomes canon.

## Universe

At the scale of humans the physics resembles our Universe closely, however the physical laws of the real-world universe /(energy conservation, relativity, quantum field theory, even Darwinian evolution etc.) don't necessarily apply.

The Universe is a **sphere that's inside itself**, a non-Euclidean space.

```
               ____a
           .-''    ''-.
          /            \
         |(S)  -''-b    |
        (     (    )     )
         |     '--'     |
          \_          _/
            '-.____.-'
```

*a* is an imaginary border sphere that is identical to *b*, i.e. *b* contains the whole *a*, including itself, and so on.

Gravity pulls everything towards this world border *ab*, i.e. over time there has accumulated solid matter alongside *ab*, forming kind of a hollow **Earth** (inside which it contains itself). This planet has two sides which form two worlds in this universe:

- **the outside**: The outer surface of the planet, normal living beings live here, the environment is similar to Earth.
- **the inside**: The inner surface of the planet, hostile, appears as a sky to the outside world. Being on this surface is like being inside a sphere.

*S* is the **Sun**. The sun is a huge hot ball that is rolling over the surface of the world sphere, or "moving across the sky". By this, the inside is a very hostile hot place not habitable by most beings. The Sun doesn't just move along a circle over the sky, but follows a complex curve that over time makes it visit the whole surface of the sky and which doesn't create any privileged world axis tied to the Sun's motion. The motion itself is driven by a subtle "wobbling" of the world sphere, which by gravity moves the Sun. The cause of this "wobble" isn't further explained yet. The Sun's path description follows:

1. At the first level the Sun rotates about an axis A1 at a frequency of one rotation per day. This creates the day-night cycle. 
2. The axis A1 itself furthermore rotates about an axis A2, which is always perpendicular to A1, at a frequency of one rotation of per N days, where N is the number of days in the year. This creates the year cycle. Axes A1 and A2 together give rise to four points on the sphere without changing seasons, 2 hot (Sun always travels through the highest sky point). Let's call these two spots the *hot spots*.
3. Furthermore, the axis A2 rotates about an axis A3, which is perpendicular to A2, at a rate of one rotation per N years. This axis makes the hot spots move very slowly, following a great circle. This creates a cycle we can call an *age*, and which takes N years.
4. Furthermore, axis A3 rotates about an axis A4, which is perpendicular to A3, at a rate of one rotation per N ages. This makes the axis of hot spot travel rotate, and creates a long cycle we can call an *epoch*, which takes N * N years.
5. And so on until infinity, i.e. there is infinitely many of these axes, but the later axes are hardly noticeable as the rotations about them are extremely slow.

**Stars** are big crystals (the size of buildings) that are scattered through the whole Earth. Their origin is unknown but they were probably created at the same time at which the Earth formed. When the Sun gets close to these crystals, they absorb part of its energy and start glowing and emitting heat themselves (they keep enough energy to shine for the whole year). The start crystals are not only in the sky, but also underneath earth and on the outside surface, but they only glow in the sky as only there they can be "charged" by the Sun. On the outside surface these crystals are rare as they're overgrown by vegetation or have been mined by people over the centuries.

TODO: Moon

An important aspect of this space is that objects crossing the world border *ab* get bigger or smaller. I.e. if a person living on the outside surface would dig through the ground downwards, he would get to the inside world, i.e. come out of the "sky", then if he forced himself against gravity further inside (e.g. with a rocket) back to the ("smaller") outside world, he would have made himself bigger; a giant to other beings living on the outside.

There may potentially exist more spheres as the one described here -- each such sphere is kind of a *world bubble* inside the whole Universe of existence, they may be connected by some kind of "teleportation" or space warping. These *world bubbles* are also called **planets** in this Universe, even if the meaning is not completely equal to that of a planet in real-world Universe.

## History

## Races

TODO

- Humans:
- **Orcs**: Orcs are one with nature.
- Dwarves:
- "rhino" people?
- "eagle" people?

## Magic

Magic is something like "the force". It has a physical explanation: there is energy everywhere, even in emptiness, and this energy is arranged in structures that are in balance, so the energy doesn't manifest itself under normal circumstances. Magic means manipulating this structure so that the energy is unleashed, like when a gentle touch can unleash a great force if it is applied to a heave object balanced in an unstable position.

## Names

TODO
