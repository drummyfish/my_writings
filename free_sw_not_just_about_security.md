# Free Software Is Not Just About Security



Discussing the definition of free software recently, I got an argument from someone that went something like this: I don't care if the non-functional data are free because they don't matter for security of my software. Even if true, this has struck me as the guy was implying free software to him was just about security.

Indeed, there is nothing wrong about security being your main reason for using free software. Security is definitely one of the main selling points of free software and one of the greatest driving forces behind its development. It was just the way the person put it, the way he implied this is the default, sole reason for its existence which made me wonder: is this how most people see it?

We do not call our movement a *secure* software movement or *safe* software movement, we call it a **free** software movement, and there is a reason. Free software goes beyond the simple freedom from being abused. It is an important freedom, don't get me wrong, but to define free software as a software that is secure is in essence no different from defining it e.g. as a software that you can get for free.

What other freedoms are there? For example the creative freedom, to name one. It would be unethical to artificially prevent someone from improving a piece of software if that someone is capable and willing to do so – it is so because modifying a copy of software does not erase or take away the original piece of software from the author, thanks to software being a form of information.

I can support this claim by the fact that the free software movement sparked and inspired the creation of movements such as free culture that apply the same rules to another kind of information: art. This is a type of information that doesn't deal with any security, yet it's an area that benefits from implementing the same ethical rules. And this goes further on, to sharing hardware designs, scientific knowledge, datasets and so on.

It is the freedom of education, the possibility to study someone else's creation, the freedom of using a tool in a way that best achieves the goal, the freedom to encourage others to stand on your shoulders, the freedom to turn off your device if you want it turned off.

Information wants to be free, it is free naturally and it's the beautiful thing about this universe. Once information pops up, it's there for everyone. It's a small miracle – imagine for a second if we could do this with food. We have this miracle of information freedom, and yet somehow we are trying to smother it by artificially limiting use, studying, copying and sharing of information. It is this freedom that's in danger that is in the name of free software. It is this general freedom of information and all the thousands of freedoms it gives birth to that we are trying to preserve with free software.