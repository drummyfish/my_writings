WIP

These are some tips for being homeless and/or living outside of this shitty
system.

## General

- **Staying nice looking and well behaved has many advantages**, people will let you in places where normally dirty homeless aren't allowed. Also double consider options that would make you known as the "troublemaker", that can make police and others hostile to you.
- **Trash bins are goldmines**, you may find a lot of useful stuff in them, people just throw away perfectly usable things very often.
- There are sometimes **electricity plugs** in underground passages for maintenance, you can use them to charge your phone etc. Also other plugs will probably be found in other public interiors.
  
## Money

- Check out **charities, homeless centers, churches** etc., they might be able to provide some quick emergency cash.

- **Begging**: possibly the easiest way to get money. Remember, be very nice, not drunk, honesty can help. Try to look like you're new on the street, that someone kicked you out of home, that you just need to buy a bit of food. You may also try to lie with a story like "my wallet was stolen and I need money for a bus to get home". Don't bother people and don't call too much attention, police might come for you.
- Small amounts of money can be found **near places where people frequently handle cash** and wallets, e.g. supermarkets, ATMs, vending machines etc. Look in places where dropped coins might end up (cracks in sidewalk etc.).
- Check the **vending machines**, people sometimes forget to take returned coins.
- Some coins may be found in **fountains, holy places, shrines** etc. as superstitious people leave spare coins there for good luck.
- **Small jobs**: just quickly help someone and ask for small money, get creative. You can lend a help people working on their house. Another idea: near a shop offer an old lady to carry her bags home (don't ask for money yet), while you're doing it talk to her, be nice, friendly, ask about her life (old people are lonely), and once you get to her home very kindly mention that you're homeless and could use some spare money for food.
- **Along train tacks you can find a lot of metal** that falls off trains and is left by maintenance workers, you might collect it and sell it.
- **You can return carts or beer bottles** near supermarkets to get some cash.
- You may possibly **sell stuff you find in trash to pawn shops**.

## Food

- Check out **food banks, charities, churches** etc., they will probably give you something for free.
- Some religious groups provide food, specifically e.g. **Sikhs give free food** (called Langar) in their churches to everyone who comes no matter their religion, social statues etc.
- **Fruit and food for animals**: you'll find this in many gardens and even public city places, just collect it at night. In the fields you might be able to **dig out some potatoes**.
- **Vending machines** contain food that's normally not guarded by humans, e.g. near big train stations. It might be not so hard to dig some food out at night, but try to not damage the machine (companies won't probably might so much a stolen piece of food but they will mind a damaged equipment, they could remove the machine, or if you get caught you might be forced to pay for the damage).
- In shopping malls, **food prices are lower late** in the evening, before closing hours, because they need to get rid of what's left, otherwise they have to throw it away.
- **Fishing** can provide a food source, you don't need a fishing pole, it's enough to make a landing net out of a t-shirt on a stick and take some small fish out of water.
- In case of extreme hunger **bugs and worms** are an easy source of protein.
- Early in the morning when it's still dark **when supplies arrive to small shops**, you might be able to quickly steal something from the truck when no one's around (just quickly "walk by" and take something under your jacket). You should observe their routine for a few days in order to be well prepared.
- **Trash bins** may contain food that's still good. But double check it's really edible. Make sure to **check out trash behind supermarkets, shops and restaurants**, they may just throw away perfectly good food -- it might be good to also just ask them, they'll probably give you something rather than throwing it away.
- **Drinkable water** can sometimes be found near sport and school playgrounds.
- **Stealing food delivery**: you may steal e.g. food delivered in front of the door of elderly people or someone who's not currently at home.
- **Shoplifting**: be careful to not get into trouble with security guards, they might beat you.

## Safety/Shelter

- Primitive **heating** (e.g. in a tent) can be achieved with **heated rocks** or **bottles/bags filled with hot water**.
- Free **hot water** can sometimes be found flowing from factories to nearby brooks/rivers, they sometimes use it for cooling.
- In very cold winters some choose to **voluntarily go to prison** where they get housing, food and health care. However keep in mind that police may get hostile to you in the future if you become the "troublemaker". Also be sure to commit the right crime, do **NOT** cause material damage or harm anyone, you don't want to get a fine, pay for damage or get a life sentence. Check your country's laws, possible crimes to commit: repeated theft, walking naked in public etc.
- You might make a tiny shelter out of a **big trash can** that you can steal from in front of someone's house at night. Just take it somewhere to a nearby forest, wash the inside, bury it horizontally in ground, insulate it with leaves and throw dirt on top, then put some cloth inside to sleep on -- pretty cozy.

## Hygiene

- There are quite a lot of **public showers** in cities or gas stations (for truck drivers).
- Free **hot water** can sometimes be found flowing from factories to nearby brooks/rivers, they sometimes use it for cooling.

## Health

- **Hospitals and doctors are obliged by law to help people in immediate emergency**, so if you're real badly sick, probably just drop yourself somewhere near a hospital and look like you're dying, they should help you. You might also try to **fake some mental illness** (again remember to not do damage to anything or anyone) so that they lock you up in a mental hospital for a while where you should get normal health care as well. You may pretend you want to commit suicide or that you are seeing things.

## Entertainment

- **Newspapers/magazines** can be stolen from people's mailbox, just wait early in the morning when it's still dark when someone delivers it to the subscriber's box and take it. Old newspapers are also handy for many things (fire, wiping, ...).
