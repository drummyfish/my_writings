WIP

# Xonotic Beginner Tutorial

*brief short and simple*

by Drummyfish

for Xonotic 0.8.2

*released under CC0 1.0, public domain (https://creativecommons.org/publicdomain/zero/1.0/)*

## about

**Xonotic** (formerly **Nexuiz**) is a completely **free as in freedom** (including the content) and **free of cost** community made **arena shooter** similar to e.g. **Quake III**, but more rich, complex and modern, and runs on **GNU/Linux, Win and Mac**. Improvements over older games include many more weapons and game modes, mutators, improved physics and movement, grapling hook, voting etc.






## weapons

| weapon          | description                                                                                                        | similar to QIII's      | prim. fire (LMB)                                                                | sec. fire (RMB)                                                                   | good for                  |
| --------------- | ------------------------------------------------------------------------------------------------------------------ | ---------------------- | ------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **blaster**     | **spawn weapon**, fast precise shots, low dmg, strong push, infinite ammo                                          |                        | **shoot** (small quick blast)                                                   | switch to **prev. weapon**                                                        | **movement** (high jumping, climbing, getting speed, ...), **pushing enemies** (off the map, away from items, ...)                                                                                                                             |
| **shotgun**     | **spawn weapon**, **spread** shots, can **bash**                                                                   | shotgun, gauntlet      | **shoot** (big spread)                                                          | **bash** (**melee**, strong but with initial delay)                               | close and mid range                                                                                                                                                                                                                            |
| **machine gun** | **quick firing rate**, long magazine reloads                                                                       | machine gun, lightning | **shoot** (higher spread)                                                       | rapidly shoot **multiple bullets** (lower spread)                                 | finishing low HP enemies, mid (prim. fire) to long (sec. fire) range                                                                                                                                                                           |
| **mortar**      | throws **grenades** with high **splash dmg**                                                                       | grenade launcher       | **on-touch** explosive grenade                                                  | **timed** grenade                                                                 | **splash damage**, attack **behind corners** (sec. fire)                                                                                                                                                                                       |
| **electro**     | fires **direct shots** and **sprays highly explosive balls**, has **splash dmg** Overusers are considered **noobs**|                        | **shoot** direct shot                                                           | **spray electro balls** on floor (prim. shoot explodes them)                      | against **groups** of enemies, **defending**, area denial (tight corridors, key spots, ...), attack behind corners                                                                                                                             |
| **crylink**     | shoots diverging **particles bouncing of walls**                                                                   |                        | fire **diverging bouncing particles**, releasing the button makes them converge | fire **multiple particles flying forward**                                        | **slowing down** players (e.g. flag carriers), **pulling** players (off of map etc.), **movement** (sec. fire under self gives high speed and allows **climbing walls**), **saving self** when falling off the map (climb with sec. fire)  attack **behind corners** |
| **vortex**      | very **strong sniper**, **long range laser beam**, goes through multiple enemies, **difficult aiming**             | railgun                | **shoot** laser beam (recharges over time, charged gives more damage)           | **zoom**                                                                          | **long range**                                                                                                                                                                                                                                 |
| **hagar**       | rapidly fires **small rockets directly forward**, difficult to master, splash dmg                                  |                        | **shoot** rockets forward                                                       | holding **loads up multiple rockets**, releasing fires them at once (very strong) | against enemies in **tight corridors** (prim. fire), quick **surprise shots** (sec. fire)                                                                                                                                                      |
| **devastator**  | shoots **rockets** with high **splash damage**                                                                     | rocket launcher        | **shoot**, hold the button for **curving** the rocket                           | explode rocket **in air**                                                         | attack **behind corners** (prim. fire), **movement** (jumps and double jumps, getting speed), pushing enemies                                                                                                                                  |
| following are extra weapons, **not normally available**, **not important to know** | --- | --- | --- | --- | --- |
| tuba            | joke weapon, plays music (with mouse buttons and movement keys), does damage in a small radius                     |                        | **play** a tone                                                                 | **play** a different tone                                                         | entertainment                                                                                                                                                                                                                                  |
| mine layer      | lays mines that explode when someonw walks on them                                                                 |                        | lay a **mine**                                                                  | **remotely detonate** mines                                                       | area denial, defending, creating traps                                                                                                                                                                                                         |
| HLAC | | | | | |
| rifle | | | | | |
| T.A.G. seeker | | | | | |
| Grappling Hook | | | | | |
| Port-O-Launch | | | | | |
| vaporizer       | **sniper**, similar to vortex, used in instagib mode                                                               | railgun                | **shoot** instakill laser beam                                                  | shoot a **blast** (same as blaster's)                                             | long range                                                                                                                                                                                                                                     |
| Fireball| | | | | |

## game modes

Mode rules can differ by server.

| mode                | abbrev. | rules | notes |       
| ------------------- | ------- | ----- | ------|
| **deathmatch**      | **DM**  | All vs all, self dmg on, first with X frags wins.  | classic and common mode |
| **team deathmatch** | **TDM** | Two (usually) teams, self dmg on, instant respawn after death, first team with X collective frags wins. | With unequal number of people on teams can be a more fair team mode than CA, but a weak player can hurt its team by being fragged a lot. | 
| **clan arena** | **CA** | Two (usually) teams, self dmg off, everyone spawns with all weapons, ammo, HP and armor, no item pickups on map, played by rounds (no respawn within round, first eliminated team loses round), first to win X rounds wins | One of most common modes, good for learning. Electro can be abused too much in this mode. Thanks to no self dmg and all weapons for everyone movement is extremely fast and fights are rough. |
| **capture the flag** | **CTF** | Two (usually) teams, each guarding a flag, self dmg is on, taking enemy flag and carrying it to own base scores points, first with X points wins. | |
| **last man standing**| **LMS** | All vs all, everyone spawns with all weapons amd anp (available on map), full HP and armor, no item pickups on map, everyone starts with X lives, the last surviving player wins.   | Encourages **camping**, not good on large maps. |
| **freeze tag** | **FT** | Two (usually) teams, self dmg is on, players spawn with all weapons, ammo, full HP and armor, played by rounds, first to win X rounds wins, killing an enemy freezes him, frozen player can be revived by a teammate. |  |
| **duel** | | |


## console

- Open with "`;`". 
- `tab` autocompletes command, double `tab` shows all possibilities.
- More details in links.

| command | description |
| ------- | ----------- |
| vcall   | vote        |
| status  | |

## tips

- **Binding weapons is essential.** Bind to keys around WSAD for fast switching and **comboing** (switching weapons after each shot, makes you fire faster and make best use of them). Most important combo is likely **devastator, mortar, vortex** -- master it.
- **Movement is crucial** (for evasion, taking cover, quick position changes, ...), and though is very similar to other arena shooters (strafe jumping, circle jumping, rocket jumping, sliding, ...), there are many new elements, such as **turning mid air** (releasing forward key), crylink speeding, double jumping (exploding devastator below self), speeding by landing on skewed surface etc. For details see links.
- In order to play well the **game settings need to be configured**, importantly e.g. binding keys, turning off music and some effects (damage blur, 3D items, ...), field of view (FOV), force player models, crosshair and HUD etc.
- **Devastator can be used to jump high** (shoot under your feet) **or even jump mid air** (double jump) or accelerate fast (shoot with RMB pressed so the rocket immediately explodes and pushes you). Though mostly only in modes with no self-damage (use **blaster** if self damage is on).
- **Crylink can give you high speed instantly**, shoot sec. fire aiming 45 degrees towards ground. Same can be used to climb walls. Mostly usable only when self damage is off.
- **Explosions (and piercing shots) go through thin walls**, so you can kill people through ceiling etc.
- **Low level players make a distinct screaming sound** -- finish them off whenever you spot them (good for this are machine gun or shotgun).
- **Sound gives you a lot of information** (position of enemies, low HP players, items being taken, ...), so it is best to play with headphones and no music.


## links

- Xonotic **homepage**: https://xonotic.org/guide
- Xonotic on **libregamewiki**: https://libregamewiki.org/Xonotic
- Xonotic on **Wikipedia**: https://en.wikipedia.org/wiki/Xonotic
- Xonotic **forums**: https://forums.xonotic.org
- Xonotic **wiki**: https://xonotic.fandom.com/wiki/Xonotic_Wiki
- Xonotic **IRC chat**: https://www.xonotic.org/chat
- Xonotic **development IRC**: #xonotic @ freenode
- XonStat (**stats about games and players**): http://stats.xonotic.org
- Halogene's excellent detailed Xonotic **guide/tutorial**: https://xonotic.org/guide
- advanced **console tutorial/documentation**: https://forums.xonotic.org/showthread.php?tid=2987
- Xonotic **code repository** (can also **report bugs** there): https://gitlab.com/xonotic/xonotic
- XDF, Xonotic defrag academy (**movement tutorial**): https://hooktube.com/watch?v=crEfpslf-g4
- Xonotic **pickup games** IRC: https://xonotic.org/pickup/
- this **cheatsheet**: https://gitlab.com/drummyfish/my_writings/blob/master/xonotic_beginner_cheatsheet.md



