WIP

## legal hack (the greatest secret)

An anonymous company sells a legal hack on Tor that allows to bypass most laws, get rich etc. This information is extremely expensive and secret and the buyer has to be thoroughly checked (as to know they won't leak the information), but is the most valuable information of all time. The description of the package says the hack works in all countries, because it is not based on a specific form of law, but is instead a logical flaw in the very essential principles of law, that are mathematically impossible to fix -- punishing the hack by law would mean the law itself would have to punish all people and the system would collapse. It says that if revealed publicly, it would be known as by far the biggest discovery in the history of mathematics and logic, but that mustn't happen because the world would collapse because all laws would suddenly stop working.

## super mathematics (extreme brain capacity)

Around Earth people start appearing -- always highly intelligent mathematicians -- with a special ability to sometimes effortlessly predict outcome of chaotic events and solve problems that shouldn't be solvable in given time (NP hard problems). It is not clear how they do it or in which situations they are able to do it, the special people say it is a rational discipline that's above mathematics, but cannot be understood by most even very smart people and they cannot explain it, in the same way a human cannot explain e.g. quadratic equations to a cat, they simply don't have the mental capacity. The discipline is more difficult than mathematics because it cannot be written down -- once it is written down, the idea is no longer valid (similarly how quantum mechanics introduced effecting by observation). Everything is explained so that normal humans, i.e. the readers cannot understand, but can only get a glimpse of the ideas.

## programming monks (beauty of programming)

Man goes hiking (?) in a jungle, discovers a temple of programming monks that live completely isolated, make their own computers and program just for the sake of writing beautiful programs, have developed perfectly elegant technology and software, including an **ideal programming language**.

